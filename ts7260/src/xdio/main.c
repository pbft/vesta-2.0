#include <assert.h>
#include <stdio.h>
#include <unistd.h>
#include "xdio.h"

/*
  main.c - XDIO API test program
  Author: Michael Schmidt
  Copyright (c)2006 Technologic Systems
 */

/*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License v2 as published by
*  the Free Software Foundation.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.

*  You should have received a copy of the GNU General Public License
*  along with this program; if not, write to the Free Software
*  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

//----------------------------------------------------------------------

/*
  DIO2 header

  DIO pin 
  0   1
  1   3
  2   5
  3   7
  4   9
  5   11
  6   13
  7   15
  8   17
  9   19
  10  4
  11  6
  12  8
  13  10
  14  12
  15  14

  This module demonstrates the use of the XDIO functions. It requires one
  TS-7300 board and two connector wires (one of which may be a jumper).

 */
int main() {
  struct xdio xdio1,xdio2,*xd1=0,*xd2=0;
  int i;
  long long min,ct;

  if (open_xdio(&xdio1,0)) {
    xd1 = &xdio1;
  }
  if (open_xdio(&xdio2,1)) {
    xd2 = &xdio2;
  }
  assert(xd1);

  
  /*
    input pins are pulled high if nothing is connected.
    output low on DIO0.
    check the current state of DIO1. if its the same as DIO0,
    then ask the user to disconnect the two, otherwise ask for them
    to be connected.  Poll DIO1 until its state changes.

    a fancy authentication scheme whereby we check to make sure the
    pin are really connected to each other (and not ground) is left 
    as an exercise for the reader.
  */

  printf("Clock period is %d ns\n",setClockSpeed(xd1,1));

  setPinDirection(xd1,0,OUTPUT);
  setPin(xd1,0,LOW);
  setPinDirection(xd1,2,OUTPUT);
  setPin(xd1,2,LOW);
  setPinDirection(xd1,1,INPUT);

  if (getPin(xd1,1) == LOW) {
  //    printf("Please disconnect DIO0 (pin 1) from DIO 1 (pin 3)\n");
  //    while (getPin(xd1,1) == LOW);
  // Ok, not really, we want these to be connected for later tests...
  } else {
    printf("Please connect DIO0 (pin 1) to DIO 1 (pin 3)\n");
    while (getPin(xd1,1) == HIGH);
    printf("Thank you.\n");
  }

  /*
    Now test the pulse timer.    
   */

  min = 0x7FFFFFFFFFFFFFFFLL;
  if (pinLevelChanged(xd1,1)) { // clear any old glitches
    // printf("Pin 0 changed earlier.\n");
  }

  for (i=0;i<10;i++) {
    setPin(xd1,0,LOW);
    initPulseTimer(xd1,1,0,PULSE_HIGH_TIME);
    setPin(xd1,0,HIGH);
    usleep(1);
    setPin(xd1,0,LOW);
    ct = getPulseTimer(xd1,0,0);
    if (ct < min) {
      min = ct;
    }
  }
  if (pinLevelChanged(xd1,1)) {
    printf("Glitch detected ok\n");
  } else {
    printf("Glitch detection failed\n");
  }


  printf("usleep(1) slept no less than %lld ns.\n",min);

  /*
    Test PWM
   */

  initCounter(xd1,EDGE_COUNTER,NO_RESET);

  setClockSpeed(xd1,0);
  assert(startPWM(xd1,100,500000));


  i = 10000;
  while (i--) {
    // \r doesn't scroll on my system
    fprintf(stderr,"\r%d",getQCTR(xd1));
  }
  pausePWM(xd1);
  fprintf(stderr,"\n");

  close_xdio(xd1);
  close_xdio(xd2);
  return 0;
}
