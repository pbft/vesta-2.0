TS-XDIO datasheet
=================

The TS-XDIO core is a GPIO core with "eXtended Digital I/O" functionality
including pulse-width modulation, quadrature and edge counting, and pulse
timing with IRQ/DRQ support.  Each core uses around 231 FPGA logic cells
on Altera architectures.

There are two TS-XDIO cores in the TS-7300 and one in the TS-7260.

TS-XDIO programming is done via eight-bit accesses starting at the BASE
physical memory address listed for each respective core below.  There are four
memory locations for each core, corresponding to BASE+0, BASE+1, BASE+2, and
BASE+3.

                              ARM Clock_Period
        Core1 BASE Core2 BASE IRQ  Fast Slow
TS-7260 0x12C00000    N/A      32  68ns 30518ns
TS-7300 0x72000040 0x72000044  40  13ns 26667ns

Each TS-XDIO core features:

* eight (3.3V level) pins each programmable as an output, or a schmitt-trigger 
  input pulled high on no connect
* glitch detector (all pins) to catch edges / pulses as short as one fast
  clock period without continuous software polling
* 16-bit edge-counter (pin 7) with max incoming clock period of one-half
  the core clock frequency (7.3728Mhz for TS-7260, 38.4615Mhz for TS-7300)
* 16-bit quadrature counter (pins 6,7) with max increment / decrement rate 
  of one-half the maximum edge-counter frequency.
* Pin 5 can optionally be used as an index pulse for reseting the 
  edge counter or quadrature counter to 0. 
* PWM (pulse width modulation) output on pin 4 with period between two
  fast clock periods and 8190 slow clock periods, with 0.02% duty cycle
  precision
* One-shot programmable high or low output pulse (pin 4) of duration 
  between one fast clock period and 4095 slow clock periods
* pulse / period timer (any pin) can measure high/low pulse time or full 
  period lengths at fast clock period resolution



Register Map
==========

Register
# bits  NAME    Description
0 7-6   MODE    Selects what registers 1-3 are:
                0 - pin direction, data register, and IRQ / DRQ control
                1 - edge / quadrature counter and glitch monitor
                2 - input pulse timer and pin select
                3 - PWM / pulse high / low-time set
0 5-4   PTMODE  Pulse timer mode and polarity:
                0 - negedge to posedge (low pulse time)
                1 - posedge to negedge (high pulse time)
                2 - negedge to negedge (period time)
                3 - posedge to posedge (period time)
0   3   PTRST   Write: Pulse Timer reset
                Read: waiting for trailing edge
0   2   QUADEN  Quadrature enable / edge counter disable
0   1   START   PWM enable / pulse start
0   0   CLKRATE clock select TS-7260 / TS-7300
		0: fast  14.7456 Mhz / 75.0 Mhz (68ns/13ns period)
		1: slow   32.768 Khz / 37.5 Khz (30518ns/26667ns period)
------- MODE == 0
1 7-0   DIR     DIO direction register (1 - output, 0 - input) 
2 7-0   DATA    DIO data register
3   7   DRQ     DRQ select (1 - drq, 0 - irq)    
3   6           Reserved (may be used in customer specific TS-XDIO cores)
3   5   IRQ_IP  irq/drq on quadrature / edge counter index pulse (if enabled)
3   4   IRQ_PT  irq/drq on pulse timer trailing edge
3   3   IRQ_GLITCH_4_7 irq/drq on pin glitch (pins 4-7)
3   2   IRQ_GLITCH_0_3 irq/drq on pin glitch (pins 0-3)
3   1   IRQ_OF  irq/drq on quadrature / edge overflow / underflow
3   0   IRQ_CD  irq/drq on quadrature change direction
------- MODE == 1
1   7   IP_EN_5 enable index pulse on pin 5 (1 - reset counter on edge)
                DATA bit 5 = 0  -> reset counter on positive edge
                DATA bit 5 = 1  -> reset counter on negative edge
1 6-0   GLITCH  1 - glitch detected on pin, 0 - inactive since last read
                Cleared on read
2 7-0   QCTR_LO bits 7-0 of edge / quadrature counter 
3 7-0   QCTR_HI bits 15-8 of edge / quadrature counter 
------- MODE == 2
1   7   PT_ACC  pulse time accumulate - keeps on running adding pulse times
1 6-4   INPNO   input pin number (0-7)
1 3-0   PT_EX   bits 19-16 of pulse timer
2 7-0   PT_LO   bits 7-0 of pulse timer
3 7-0   PT_HI   bits 15-8 of pulse timer
------- MODE == 3
1 7-4   THI_HI  bits 11-8 of high time
  3-0   TLO_HI  bits 11-8 of low time
2 7-0   THI_LO  bits 7-0 of high time
3 7-0   TLO_LO  bits 7-0 of low time, one-shot mode enabled if all zeroes
		In one-shot mode, pulse polarity is inversion of DATA bit 4 
		In either case, actual high time and/or low time is one clock 
		longer then specified by these registers.

Notes
=====
* Can use 20-bit pulse timer as free-running timer by setting accumulate
  bit and setting pin select to PWM (pulse width modulation) output (pin 5).  
  14.7456Mhz counter can be enabled by watching periods, slower free running
  counters can be had by clever use of high-time and low-time PWM and 
  accumulating pulse times.  (e.g.  high-time 1, low-time 224 creates a 
  65.536Khz free-running timer that overflows once every 16 seconds)

* Edge counter counts *both* edges of waveform, divide by 2 for number of
  periods / frequency.  To count X edges and then interrupt, preload counter
  value with 2^16 - X and enable IRQ on overflow.

* Max / min quadrature count can be gotten by enabling IRQ / DRQ on quadrature
  change of direction.  Max / Min is value -/+ 1 as long as IRQ / DRQ latency 
  is less than 1 quadrature cycle time.

* Core does not reset -- registers and state remain if board undergoes reset.
  On powerup DIO data direction register is all 0's (inputs), but other 
  register contents are undefined. 

