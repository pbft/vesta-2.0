/*
  core.c - XDIO register access routines
  Author: Michael Schmidt
  Copyright (c)2006 Technologic Systems
*/

/*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License v2 as published by
*  the Free Software Foundation.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.

*  You should have received a copy of the GNU General Public License
*  along with this program; if not, write to the Free Software
*  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USB
*/

void setMODE(unsigned char *base,int v) {
  base[0] = (base[0] & ~(0xC8)) | ((v & 0x03) << 6);
}

int getMODE(unsigned char *base) {
  return (base[0] >> 6) & 0x03;
}

void setPTMODE(unsigned char *base,int v) {
  base[0] = (base[0] & ~(0x38)) | ((v & 0x03) << 4);
}

int getPTMODE(unsigned char *base) {
  return (base[0] >> 4) & 0x03;
}

void setPTRST(unsigned char *base,int v) {
  base[0] = (base[0] & ~(0x08)) | ((v & 0x01) << 3);
}

int getPTRST(unsigned char *base) {
  return (base[0] >> 3) & 0x01;
}

void setQUADEN(unsigned char *base,int v) {
  base[0] = (base[0] & ~(0x0C)) | ((v & 0x01) << 2);
}

int getQUADEN(unsigned char *base) {
  return (base[0] >> 2) & 0x01;
}

void setSTART(unsigned char *base,int v) {
  base[0] = (base[0] & ~(0x0A)) | ((v & 0x01) << 1);
}

int getSTART(unsigned char *base) {
  return (base[0] >> 1) & 0x01;
}

void setCLKRATE(unsigned char *base,int v) {
  base[0] = (base[0] & ~(0x09)) | ((v & 0x01) << 0);
}

int getCLKRATE(unsigned char *base) {
  return (base[0] >> 0) & 0x01;
}

void setDIR(unsigned char *base,int v) {
  setMODE(base,0);
  base[1] = (base[1] & ~(0xFF)) | ((v & 0xFF) << 0);
}

int getDIR(unsigned char *base) {
  setMODE(base,0);
  return (base[1] >> 0) & 0xFF;
}

void setDATA(unsigned char *base,int v) {
  setMODE(base,0);
  base[2] = (base[2] & ~(0xFF)) | ((v & 0xFF) << 0);
}

int getDATA(unsigned char *base) {
  setMODE(base,0);
  return (base[2] >> 0) & 0xFF;
}

void setIRQ_CD(unsigned char *base,int v) {
  setMODE(base,0);
  base[3] = (base[3] & ~(0x01)) | ((v & 0x01) << 0);
}

int getIRQ_CD(unsigned char *base) {
  setMODE(base,0);
  return (base[3] >> 0) & 0x01;
}

void setIRQ_OF(unsigned char *base,int v) {
  setMODE(base,0);
  base[3] = (base[3] & ~(0x02)) | ((v & 0x01) << 1);
}

int getIRQ_OF(unsigned char *base) {
  setMODE(base,0);
  return (base[3] >> 1) & 0x01;
}

void setIRQ_GLITCH0_3(unsigned char *base,int v) {
  setMODE(base,0);
  base[3] = (base[3] & ~(0x04)) | ((v & 0x01) << 2);
}

int getIRQ_GLITCH0_3(unsigned char *base) {
  setMODE(base,0);
  return (base[3] >> 2) & 0x01;
}

void setIRQ_GLITCH4_7(unsigned char *base,int v) {
  setMODE(base,0);
  base[3] = (base[3] & ~(0x08)) | ((v & 0x01) << 3);
}

int getIRQ_GLITCH4_7(unsigned char *base) {
  setMODE(base,0);
  return (base[3] >> 3) & 0x01;
}

void setIRQ_PT(unsigned char *base,int v) {
  setMODE(base,0);
  base[3] = (base[3] & ~(0x10)) | ((v & 0x01) << 4);
}

int getIRQ_PT(unsigned char *base) {
  setMODE(base,0);
  return (base[3] >> 4) & 0x01;
}

void setIRQ_IP(unsigned char *base,int v) {
  setMODE(base,0);
  base[3] = (base[3] & ~(0x20)) | ((v & 0x01) << 5);
}

int getIRQ_IP(unsigned char *base) {
  setMODE(base,0);
  return (base[3] >> 5) & 0x01;
}

void setRSVD(unsigned char *base,int v) {
  setMODE(base,0);
  base[3] = (base[3] & ~(0x40)) | ((v & 0x01) << 6);
}

int getRSVD(unsigned char *base) {
  setMODE(base,0);
  return (base[3] >> 6) & 0x01;
}

void setDRQ(unsigned char *base,int v) {
  setMODE(base,0);
  base[3] = (base[3] & ~(0x80)) | ((v & 0x01) << 7);
}

int getDRQ(unsigned char *base) {
  setMODE(base,0);
  return (base[3] >> 7) & 0x01;
}

void setIP_EN_5(unsigned char *base,int v) {
  setMODE(base,1);
  base[1] = (base[1] & ~(0x80)) | ((v & 0x01) << 7);
}

int getIP_EN_5(unsigned char *base) {
  setMODE(base,1);
  return (base[1] >> 7) & 0x01;
}

void setGLITCH(unsigned char *base,int v) {
  setMODE(base,1);
  base[1] = (base[1] & ~(0x7F)) | ((v & 0x7F) << 0);
}

int getGLITCH(unsigned char *base) {
  setMODE(base,1);
  return (base[1] >> 0) & 0x7F;
}

void setQCTR_LO(unsigned char *base,int v) {
  setMODE(base,1);
  base[2] = (base[2] & ~(0xFF)) | ((v & 0xFF) << 0);
}

int getQCTR_LO(unsigned char *base) {
  setMODE(base,1);
  return (base[2] >> 0) & 0xFF;
}

void setQCTR_HI(unsigned char *base,int v) {
  setMODE(base,1);
  base[3] = (base[3] & ~(0xFF)) | ((v & 0xFF) << 0);
}

int getQCTR_HI(unsigned char *base) {
  setMODE(base,1);
  return (base[3] >> 0) & 0xFF;
}

void setPT_ACC(unsigned char *base,int v) {
  setMODE(base,2);
  base[1] = (base[1] & ~(0x80)) | ((v & 0x01) << 7);
}

int getPT_ACC(unsigned char *base) {
  setMODE(base,2);
  return (base[1] >> 7) & 0x01;
}

void setINPNO(unsigned char *base,int v) {
  setMODE(base,2);
  base[1] = (base[1] & ~(0x70)) | ((v & 0x07) << 4);
}

int getINPNO(unsigned char *base) {
  setMODE(base,2);
  return (base[1] >> 4) & 0x07;
}

void setPT_EX(unsigned char *base,int v) {
  setMODE(base,2);
  base[1] = (base[1] & ~(0x0F)) | ((v & 0x0F) << 0);
}

int getPT_EX(unsigned char *base) {
  setMODE(base,2);
  return (base[1] >> 0) & 0x0F;
}

void setPT_LO(unsigned char *base,int v) {
  setMODE(base,2);
  base[2] = (base[2] & ~(0xFF)) | ((v & 0xFF) << 0);
}

int getPT_LO(unsigned char *base) {
  setMODE(base,2);
  return (base[2] >> 0) & 0xFF;
}

void setPT_HI(unsigned char *base,int v) {
  setMODE(base,2);
  base[3] = (base[3] & ~(0xFF)) | ((v & 0xFF) << 0);
}

int getPT_HI(unsigned char *base) {
  setMODE(base,2);
  return (base[3] >> 0) & 0xFF;
}

void setTHI_HI(unsigned char *base,int v) {
  setMODE(base,3);
  base[1] = (base[1] & ~(0xF0)) | ((v & 0x0F) << 4);
}

int getTHI_HI(unsigned char *base) {
  setMODE(base,3);
  return (base[1] >> 4) & 0x0F;
}

void setTLO_HI(unsigned char *base,int v) {
  setMODE(base,3);
  base[1] = (base[1] & ~(0x0F)) | ((v & 0x0F) << 0);
}

int getTLO_HI(unsigned char *base) {
  setMODE(base,3);
  return (base[1] >> 0) & 0x0F;
}

void setTHI_LO(unsigned char *base,int v) {
  setMODE(base,3);
  base[2] = (base[2] & ~(0xFF)) | ((v & 0xFF) << 0);
}

int getTHI_LO(unsigned char *base) {
  setMODE(base,3);
  return (base[2] >> 0) & 0xFF;
}

void setTLO_LO(unsigned char *base,int v) {
  setMODE(base,3);
  base[3] = (base[3] & ~(0xFF)) | ((v & 0xFF) << 0);
}

int getTLO_LO(unsigned char *base) {
  setMODE(base,3);
  return (base[3] >> 0) & 0xFF;
}

