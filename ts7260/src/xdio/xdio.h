typedef struct xdio {
  unsigned char *base;
  unsigned char glitch;
  int fd,model;
} xdio;

enum TS_MODEL { 
  TS7260 = 7260,
  TS7300 = 7300
};

enum PIN_DIRECTION { INPUT=0, OUTPUT=1 };
enum PIN_VALUE { LOW=0, HIGH=1 };

enum PULSE_TYPE {
  PULSE_LOW_TIME = 0,    NEGEDGE_TO_POSEDGE = 0,
  PULSE_HIGH_TIME = 1,   POSEDGE_TO_NEGEDGE = 1,
  PULSE_PERIOD_LOW = 2,  NEGEDGE_TO_NEGEDGE = 2,
  PULSE_PERIOD_HIGH = 3,  POSEDGE_TO_POSEDGE = 3
};

enum COUNTER_TYPE { EDGE_COUNTER=0, QUADRATURE=1 };
enum INDEX_TYPE { NO_RESET=-1, NEGEDGE_RESET=1, POSEDGE_RESET=0 };

int open_xdio(struct xdio *x,int num);
void close_xdio(struct xdio *x);

int setClockSpeed(xdio *x,int fast);

void setPinDirection(xdio *x,int pin,enum PIN_DIRECTION dir);
void setPin(xdio *x,int pin,enum PIN_VALUE val);
int getPin(xdio *x,int pin);


void initPulseTimer(xdio *x,int pin,int acc,enum PULSE_TYPE ptype);
long long getPulseTimer(xdio *x,int sync,int reset);

int pinLevelChanged(xdio *x,int pin);

void generatePulse(xdio *x,long long forNs);
int startPWM(xdio *x,int freq,int microDutyCycle);
void pausePWM(xdio *x);
void resumePWM(xdio *x);

void initCounter(xdio *x,enum COUNTER_TYPE type,enum INDEX_TYPE index);

int getQCTR(xdio *x);
void setQCTR(xdio *x,int v);
int getPT(xdio *x);

void register_dump(struct xdio *x);
