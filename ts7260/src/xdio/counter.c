#include <assert.h>
#include <stdio.h>
#include <unistd.h>
#include "xdio.h"

/*
  main.c - XDIO API test program
  Author: Michael Schmidt
  Copyright (c)2006 Technologic Systems
 */

/*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License v2 as published by
*  the Free Software Foundation.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.

*  You should have received a copy of the GNU General Public License
*  along with this program; if not, write to the Free Software
*  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

//----------------------------------------------------------------------

/*
  DIO2 header

  DIO pin 
  0   1
  1   3
  2   5
  3   7
  4   9
  5   11
  6   13
  7   15
  8   17
  9   19
  10  4
  11  6
  12  8
  13  10
  14  12
  15  14

  This module demonstrates the use of the XDIO functions. It requires one
  TS-7300 board and two connector wires (one of which may be a jumper).

 */
int main() {
  struct xdio xdio1,xdio2,*xd1=0;
  int i;
  long long min,ct;

  if (open_xdio(&xdio1,0)) {
    xd1 = &xdio1;
  }
  assert(xd1);

  
  /*
    input pins are pulled high if nothing is connected.
    output low on DIO0.
    check the current state of DIO1. if its the same as DIO0,
    then ask the user to disconnect the two, otherwise ask for them
    to be connected.  Poll DIO1 until its state changes.

    a fancy authentication scheme whereby we check to make sure the
    pin are really connected to each other (and not ground) is left 
    as an exercise for the reader.
  */

  printf("Clock period is %d ns\n",setClockSpeed(xd1,1));

  initCounter(xd1,EDGE_COUNTER,NO_RESET);

  i = 10000;
  while (i--) {
    printf("\n%d",getQCTR(xd1));
    setQCTR(xd1,0);
    sleep(1);
  }

  close_xdio(xd1);
  return 0;
}
