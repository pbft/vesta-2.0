/*****************************************************************************/
//    This file is part of the Vesta Control System Software Suite
//    Copyright Bill Kuhns and Vermont Energy Control Systems LLC.
//
//    This is free software: you can redistribute it and/or modify
//    it under the terms of the Lesser GNU General Public License (LGPL)
//    as published by the Free Software Foundation, either version 3 of the License,
//    or any later version.
//
//    This software is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    Lesser GNU General Public License for more details.
//
//    You should have received a copy of the Lesser GNU General Public License
//    along with this software.  If not, see <http://www.gnu.org/licenses/>.
/*****************************************************************************/

#define PROCESSNAME  FUNCTION_NAME(7xxx hwio)
// Process all physical I/O from NFCS hardware (ad9700 and dio64). Read and write to shared memory.
// Perform scaling, calibration, and linearization as needed.

// This is a periodic background task

#include <stdio.h>
#include <math.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/types.h>
#include "vesta.h"

// PC104 base address in TS=72xx address space
#define TS7260_PC104_8BIT_IO_BASE 0x11C00000
#define TS7800_PC104_8BIT_IO_BASE 0xEE000000
#define PC104_8BIT_LEGACY_IO_BASE 0x11E00000
#define MODEL_REGISTER 0x22000000

// Address in PC104 space of PC104 cards (determined by jumpers on board)
#define DIO64_BASE 0x100
#define AD1_BASE 0x160
#define AD2_BASE 0x168

// TS9700 #1
#define AD1COMMAND AD1_BASE
#define AD1DATA AD1_BASE + 2
#define AD1_DACLSB AD1_BASE + 4
#define AD1_DACSTATUS AD1_BASE + 6

#define ADREADYBIT 7

// TS9700 #2
#define AD2COMMAND AD2_BASE
#define AD2DATA AD2_BASE + 2
#define AD2_DACLSB AD2_BASE + 4
#define AD2_DACSTATUS AD2_BASE + 6

// TS-DIO64
#define DIO64_ID DIO64_BASE
#define DIO64_OUT DIO64_BASE + 4
#define DIO64_IN DIO64_BASE + 8

// DIO 0 and 1 (lcd 0-7 and dio0 0-7)
#define DIO_BASE 0x80840000
#define DIO_0_DATA 0
#define DIO_0_DDR 0x10
#define DIO_1_DATA 4
#define DIO_1_DDR 0x14
// DIO 3 is lcd header en/rs/wr bits
#define DIO_3_DATA 40
#define DIO_3_DDR 0x44
#define DIO_3_MASK 0xE0

typedef struct {
  short int ordinal;
  short int channel;
  short int handler;
  volatile byte_t *adcommand;
  volatile byte_t *lsb;
  volatile byte_t *msb;
  int adraw;
  short int eid;
} ts9700ai_struct;

typedef struct {
  short int ordinal;
  short int channel;
  short int handler;
  volatile short *daclsb;
  short int eid;
} ts9700ao_struct;

typedef struct {
  short int ordinal;
  short int handler;
  volatile byte_t *dio64_in;
  short int eid;
} ts9700di_struct;

typedef struct {
  short int ordinal;
  short int handler;
  volatile byte_t *dio64_out;
  short int eid;
} ts9700do_struct;

main() {
  byte_t *pc104base;
  byte_t *pc104legacy;
  int *ts7800pc104_1;
  int *ts7800pc104_2;
  short *ts7800pc104_3;
  short *ts7800pc104_4;
  int ts7800;

  volatile byte_t *modelregister;
  volatile byte_t *cardid;

  int fd;
  FILE *fp;
  float reading;
  float volts;
  int c, i, j, lut;
  short dacvalue;
  int temp[50];
  float lookup[4][36];

  //int adraw[16];

  char tname[80];
  char fname[80];
  char msgbuff[80];
  short raw, oldraw;
  byte_t ts_dio64_1, ts_9700_1, ts_9700_2;
  int aiptr, aoptr, diptr, doptr;

  long long start_usec;

  // Get shared memory links
  get_shm(SERVER_ID);

  // Set up structures for I/O cards
  ts9700ai_struct *ts9700ai = (ts9700ai_struct *) calloc(shm->counts->ai_count,
      sizeof(ts9700ai_struct));
  ts9700ao_struct *ts9700ao = (ts9700ao_struct *) calloc(shm->counts->ao_count,
      sizeof(ts9700ao_struct));
  ts9700di_struct *ts9700di = (ts9700di_struct *) calloc(shm->counts->di_count,
      sizeof(ts9700di_struct));
  ts9700do_struct *ts9700do = (ts9700do_struct *) calloc(shm->counts->do_count,
      sizeof(ts9700do_struct));

  // Initialize structures for I/O cards
  aiptr = aoptr = diptr = doptr = 0;
  for (i = 0; i < shm->counts->hw_count; i++) {
    // If it's ours
    //printf("line %d handler %d\n", i, shm->hwconfig[i].handler);
    if (shm->hwconfig[i].handler == SERVER_ID) {
      switch (shm->hwconfig[i].type) {
      case 1:
        // Analog Input
        for (j = 0; j < shm->hwconfig[i].channelcount; j++) {
          ts9700ai[aiptr + j].ordinal = shm->hwconfig[i].ordinal;
          ts9700ai[aiptr + j].eid = shm->hwconfig[i].eid + j;
          ts9700ai[aiptr + j].channel = j;
          ts9700ai[aiptr + j].handler = SERVER_ID;
          //printf("Analog Input %d = element %d\n", aiptr + j,
          //    ts9700ai[aiptr + j].eid);
        }
        aiptr += shm->hwconfig[i].channelcount;
        break;
      case 2:
        // Analog Output
        for (j = 0; j < shm->hwconfig[i].channelcount; j++) {
          ts9700ao[aoptr + j].ordinal = shm->hwconfig[i].ordinal;
          ts9700ao[aoptr + j].eid = shm->hwconfig[i].eid + j;
          ts9700ao[aoptr + j].channel = j;
          ts9700ao[aoptr + j].handler = SERVER_ID;
          //printf("Analog Output %d = physio %d\n", aoptr + j,
          //    ts9700ao[aoptr + j].eid);
        }
        aoptr += shm->hwconfig[i].channelcount;
        break;
      case 3:
        // Discrete Input
        for (j = 0; j < shm->hwconfig[i].channelcount; j++) {
          ts9700di[diptr + j].ordinal = shm->hwconfig[i].ordinal;
          ts9700di[diptr + j].eid = shm->hwconfig[i].eid + j;
          ts9700di[diptr + j].handler = SERVER_ID;
          //printf("Discrete Input %d = physio %d\n", diptr + j,
          //    ts9700di[diptr + j].eid);
        }
        diptr += shm->hwconfig[i].channelcount;
        break;
      case 4:
        // Discrete Output
        for (j = 0; j < shm->hwconfig[i].channelcount; j++) {
          ts9700do[doptr + j].ordinal = shm->hwconfig[i].ordinal;
          ts9700do[doptr + j].eid = shm->hwconfig[i].eid + j;
          ts9700do[doptr + j].handler = SERVER_ID;
          //printf("Discrete Output %d = physio %d\n", doptr + j,
          //    ts9700do[doptr + j].eid);
        }
        doptr += shm->hwconfig[i].channelcount;
        break;
      }
    }
  }
  //printf("DIa %d handler %d\n",1,ts9700di[1].handler);

  fd = open("/dev/mem", O_RDWR | O_SYNC);
  if (fd < 0) {
    sprintf(logbuffer, "could not open /dev/mem");
    printlog(PROCESSNAME, logbuffer, 1);
    exit(1);
  }

  // Map model register
  modelregister = (byte_t *) mmap(0, getpagesize(), PROT_READ | PROT_WRITE,
      MAP_SHARED, fd, MODEL_REGISTER);
  if (modelregister == MAP_FAILED ) {
    sprintf(logbuffer, "mmap failed to map model register");
    printlog(PROCESSNAME, logbuffer, 1);
    exit(1);
  }

  // **************************************************************************************
  // Map PC104 address space
  // 9700 and dio64 cards live there
  // **************************************************************************************

  // TS7260 has a model register value of 2, assume any other is 7800 for the moment

  if ((*modelregister & 3) == 2) {
    sprintf(logbuffer, "TS-7260 detected");
    printlog(PROCESSNAME, logbuffer, 0);
    pc104base = (byte_t *) mmap(0, getpagesize(), PROT_READ | PROT_WRITE,
        MAP_SHARED, fd, TS7260_PC104_8BIT_IO_BASE);
    ts7800 = 0;
  } else {
    sprintf(logbuffer, "TS-7800 detected");
    printlog(PROCESSNAME, logbuffer, 0);
    pc104base = (byte_t *) mmap(0, getpagesize(), PROT_READ | PROT_WRITE,
        MAP_SHARED, fd, TS7800_PC104_8BIT_IO_BASE);
    pc104legacy = (byte_t *) mmap(0, getpagesize(), PROT_READ | PROT_WRITE,
        MAP_SHARED, fd, PC104_8BIT_LEGACY_IO_BASE);
    ts7800pc104_1 = (int *) 0xE8000030;
    ts7800pc104_2 = (int *) 0xE8000034;
    ts7800pc104_3 = (short *) 0xE8000038;
    ts7800pc104_4 = (short *) 0xE800003C;
    ts7800 = 1;
  }
  if (pc104base == MAP_FAILED ) {
    sprintf(logbuffer, "mmap failed to map PC104 registers");
    printlog(PROCESSNAME, logbuffer, 1);
    exit(1);
  }
  //printf("DIb %d handler %d\n",1,ts9700di[1].handler);

  // 9700 #1 pointers

  cardid = pc104base + AD1COMMAND + 1;
  //printf("%x %x %hx\n", pc104base, cardid, *cardid);
  if (*cardid == 0x97) {
    for (i = 0; i < shm->counts->ai_count; i++) {
      if (ts9700ai[i].ordinal == 0 && ts9700ai[i].handler == SERVER_ID) {
        ts9700ai[i].adcommand = pc104base + AD1COMMAND;
        ts9700ai[i].lsb = pc104base + AD1DATA;
        ts9700ai[i].msb = pc104base + AD1DATA + 1;
      }
    }
    for (i = 0; i < shm->counts->ao_count; i++) {
      if (ts9700ao[i].ordinal == 0 && ts9700ao[i].handler == SERVER_ID) {
        ts9700ao[i].daclsb = (short *) (pc104base + AD1_DACLSB);
      }
    }
    sprintf(logbuffer, "TS-9700 Card #1  mapped: Card Id is %2x", *cardid);
    printlog(PROCESSNAME, logbuffer, 0);
    ts_9700_1 = 1;
  } else {
    sprintf(logbuffer, "TS-9700 Card #1 not found");
    printlog(PROCESSNAME, logbuffer, 1);
    ts_9700_1 = 0;
  }
  // 9700 #1 pointers

  cardid = pc104base + AD2COMMAND + 1;
  //printf("%x %x %hx\n", pc104base, cardid, *cardid);
  if (*cardid == 0x97) {
    for (i = 0; i < shm->counts->ai_count; i++) {
      if (ts9700ai[i].ordinal == 1 && ts9700ai[i].handler == SERVER_ID) {
        ts9700ai[i].adcommand = pc104base + AD2COMMAND;
        ts9700ai[i].lsb = pc104base + AD2DATA;
        ts9700ai[i].msb = pc104base + AD2DATA + 1;
      }
    }
    for (i = 0; i < shm->counts->ao_count; i++) {
      if (ts9700ao[i].ordinal == 1 && ts9700ao[i].handler == SERVER_ID) {
        ts9700ao[i].daclsb = (short *) (pc104base + AD2_DACLSB);
      }
    }
    sprintf(logbuffer, "TS-9700 Card #2  mapped: Card Id is %2x", *cardid);
    printlog(PROCESSNAME, logbuffer, 0);
    ts_9700_2 = 1;
  } else {
    sprintf(logbuffer, "TS-9700 Card #2 not found");
    printlog(PROCESSNAME, logbuffer, 1);
    ts_9700_2 = 0;
  }
  //printf("DIc %d handler %d\n",1,ts9700di[1].handler);

  // dio64 pointers
  // dio64 documentation says it's in 'legacy' PC104 space, (0x11E00100) but it appears 
  // at 0x11C00100

  cardid = pc104base + DIO64_ID;
  //printf("%x %x %hx\n", pc104base, cardid, *cardid);
  if (*cardid == 0xa4) {
    for (i = 0; i < shm->counts->di_count; i++) {
      if (ts9700di[i].ordinal == 0 && ts9700di[i].handler == SERVER_ID) {
        ts9700di[i].dio64_in = pc104base + DIO64_IN;
      }
    }

    for (i = 0; i < shm->counts->do_count; i++) {
      if (ts9700do[i].ordinal == 0 && ts9700do[i].handler == SERVER_ID) {
        ts9700do[i].dio64_out = pc104base + DIO64_OUT;
      }
    }
    sprintf(logbuffer, "TS-DIO64 Card mapped: Card Id is %2x", *cardid);
    printlog(PROCESSNAME, logbuffer, 0);
    ts_dio64_1 = 1;
  } else {
    sprintf(logbuffer, "TS-DIO64 Card not found");
    printlog(PROCESSNAME, logbuffer, 1);
    ts_dio64_1 = 0;
  }

  start_usec = getNow();

  //******************************************************************************************/
  //
  // Set initial conditions. For now, just dio direction. The control task may override.
  //
  //******************************************************************************************/

  // Do forever
  while (1) {

    if (reloadRequired(SERVER_ID)) {

      // Read voltage lookup table data (Starts at 145 C, 5 degree increments)
      // Data from thermistor.csv, generated by thermistor.xls
      // ********** FIX THIS! Should read temp (or other values) from file! *************

      for (i = 0; i < 4; i++) {
        sprintf(&fname[0], "/etc/vesta/lookup0%02d.csv", i);
        fp = fopen(fname, "r");

        while (fscanf(fp, "%d", &j) > 0) {
          fscanf(fp, "%s", &tname);
          fscanf(fp, "%s", &tname);
          for (j = 0; j < 36; j++) {
            fscanf(fp, "%f", &lookup[i][j]);
            temp[j] = 145 - j * 5;
          }
        }
        fclose(fp);
      }

      // Claim our elements
      for (i = 0; i < shm->counts->element_count; i++) {
        if (shm->elements[i].pvn == 'p' && shm->elements[i].io == 'i') {
          claimElement(i, SERVER_ID);
        }
      }

      // Clear our bit in the reload flag
      clearReloadFlag(SERVER_ID);
    }
    //printf("size of ai %d\n", sizeof(ts9700ai));
    for (c = 0; c < shm->counts->ai_count; c++) {
      if (ts9700ai[c].handler == SERVER_ID) {
        oldraw = ts9700ai[c].adraw;
        raw = 5000;
        i = 0;
        //printf("AI %d cmd %d ch %d msb %d lsb %d ord %d phys %d\n", c,
        //    ts9700ai[c].adcommand, ts9700ai[c].channel, ts9700ai[c].msb,
        //    ts9700ai[c].lsb, ts9700ai[c].ordinal, ts9700ai[c].eid);
        // Loop on noisy read - undiagnosed bug in 9700?
        while ((abs(raw - oldraw) > 5) && (i++ < 5)) {
          *ts9700ai[c].adcommand = ts9700ai[c].channel;
          usleep(20);
          // Make sure it's done
          while (!(*ts9700ai[c].adcommand & (1 << ADREADYBIT)))
            ;
          raw = (short) (256 * *ts9700ai[c].msb + *ts9700ai[c].lsb);
        }
        volts = (256 * *ts9700ai[c].msb + *ts9700ai[c].lsb) * 2.5 / 4096;
        ts9700ai[c].adraw = (short) (256 * *ts9700ai[c].msb + *ts9700ai[c].lsb);
        //printf("ch %d %f v\n", c, volts);
        if (ts9700ai[c].adraw == 0 || ts9700ai[c].adraw == 4095) {
          shm->aivalue[ts9700ai[c].ordinal * 8 + ts9700ai[c].channel] = 0;
          shm->aistatus[ts9700ai[c].ordinal] =
              shm->aistatus[ts9700ai[c].ordinal]
                  | (1 << (ts9700ai[c].channel % 8));
        } else {
          // Calibrate.
          volts = volts * shm->stypes[shm->elements[ts9700ai[c].eid].stype].gain
              * shm->elements[ts9700ai[c].eid].gain
              + shm->stypes[shm->elements[ts9700ai[c].eid].stype].offset;
          //printf("ch %d %f v\n", c, volts);

          // Use lookup table?
          if (shm->stypes[shm->elements[ts9700ai[c].eid].stype].lut > 0) {

            //  Find lookup table cell
            i = 0;
            lut = shm->stypes[shm->elements[ts9700ai[c].eid].stype].lut;
            while (lookup[lut][i] < volts) {
              i++;
            }
            reading = temp[i - 1]
                + ((volts - lookup[lut][i - 1]) * (temp[i] - temp[i - 1])
                    / (lookup[lut][i] - lookup[lut][i - 1]));
            reading = reading + shm->elements[ts9700ai[c].eid].offset;
            shm->aivalue[ts9700ai[c].ordinal * 8 + ts9700ai[c].channel] = (isnan(reading) ? 0 : reading);
          } else {
            volts = volts + shm->elements[ts9700ai[c].eid].offset;
            shm->aivalue[ts9700ai[c].ordinal * 8 + ts9700ai[c].channel] = (isnan(volts) ? 0 : volts);
          }
        }
        //printf("AI %d = %f (%f v)\n",
        //    ts9700ai[c].ordinal * 8 + ts9700ai[c].channel,
        //    shm->aivalue[ts9700ai[c].ordinal * 8 + ts9700ai[c].channel], volts);
      }
    }
    // D/A channels
    for (c = 0; c < shm->counts->ao_count; c++) {
      if (ts9700ao[c].handler == SERVER_ID) {
        dacvalue = (int) (elementValue(ts9700ao[c].eid)
            * shm->stypes[shm->elements[ts9700ao[c].eid].stype].gain
            * shm->elements[ts9700ao[c].eid].gain
            + shm->stypes[shm->elements[ts9700ao[c].eid].stype].offset
            + shm->elements[ts9700ao[c].eid].offset);
        if (dacvalue > 4095) {
          dacvalue = 4095;
        }
        dacvalue = dacvalue + (ts9700ao[c].channel << 14);

        *ts9700ao[c].daclsb = dacvalue;
      }
    }

    // Process dio64
    for (c = 0; c < shm->counts->di_count; c++) {
      if (ts9700di[c].handler == SERVER_ID) {
        shm->divalue[c] = ~ts9700di[c].dio64_in[c];
        //printf("di %d %d\n",c,shm->divalue[c]);
      }
    }

    for (c = 0; c < shm->counts->do_count; c++) {
      if (ts9700do[c].handler == SERVER_ID) {
        //printf("DO %d %d\n",c,shm->dovalue[c]);
        ts9700do[c].dio64_out[c] = shm->dovalue[c];
        shm->dovalue[c] = ts9700do[c].dio64_out[c];
      }
    }
    // We've done with our I/O - now update element values to reflect what we've read
   //printf("aivalue %f %f %f %f\n",shm->aivalue[0],shm->aivalue[1],shm->aivalue[2],shm->aivalue[3]);
    for (i = 0; i < shm->counts->element_count; i++) {
      if (shm->elements[i].pvn == 'p' && shm->elements[i].io == 'i') {
      	//printf("setting %d\n",i);
        setElementFromInput(i);
      }
    }
    //sprintf(logbuffer, "Cycle completed - sleeping");
    //printlog(PROCESSNAME, logbuffer, 0);

    // Sleep for desired interval
    timedSleep(PROCESSNAME, shm->config->io_period, &start_usec);
  }
}
