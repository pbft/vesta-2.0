
/*****************************************************************************/
//    This file is part of the NoFossil Control System Software (NFCSS)
//    Copyright 2007, 2008, 2009 Bill Kuhns
//
//    NFCSS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    any later version.

//    NFCSS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with NFCSS.  If not, see <http://www.gnu.org/licenses/>.
/*****************************************************************************/

// Read analog inputs from shared memory and output formatted HTML

#include <stdio.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include "vesta.h"
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/dir.h>
#include <sys/param.h>
#include <string.h>
#include <errno.h>

// Global for readfiles


/*****************************************************************************/
 
main(){
  char pathname[80];
  char *myquery, *p[20], *v[20];
  int i, j;

  // form variables
  int ip1, ip2, ip3, ip4;
  int gw1, gw2, gw3, gw4;
  int dns11, dns12, dns13, dns14;
  int dns21, dns22, dns23, dns24;

  FILE *fp;
  char linebuff[256];


  printf ("Content-type: text/html\n\n<html>");
  printf ("<head><link rel=stylesheet type=text/css href=/vesta.css></head>\n");
  printf ("<body bgcolor=darkgray>\n");

  // Start HTML
  printf ("<h3>Vesta Network Configuration</h3>\n");

  printf ("<p><a href=/vestaHomepage>[Control Panel]</a><br>\n");

  printf ("<p>This page allows changing Vesta network settings. Use this with EXTREME caution, \n");
  printf ("as any incorrect values may make the Vesta inoperable and require it to be shipped back \n");
  printf ("to the factory.</p><p>In particular, make sure that the Vesta IP address is a valid and \n");
  printf ("unused IP address on your existing network.</p><p>After making changes, cycle power on \n");
  printf ("the Vesta. If you have changed the IP address of the Vesta itself, enter the new address \n");
  printf ("in your browser's URL bar after cycling Vesta power.</p>\n");


  /*****************************************************************************/

  // Get form data, process any edits

  /*****************************************************************************/
  myquery = getenv("QUERY_STRING");

  /* break apart parameter string on '&' and '=' characters */

  j = 0;

  // Weirdness: sometimes myquery is NULL, and you don't want to do strlen on it.
  // Other times, it's a zero lenght string. Either way, don't parse it. 

  if (myquery != NULL){
    if (strlen(myquery) > 0){
      while(myquery=(split(myquery,&p[j],&v[j]))){
        j++;
      }
      j++;
      cleanup(j,&v[0]);
    }
  }

  ip1=192;
  ip2=168;
  ip3=1;
  ip4=8;

  gw1=192;
  gw2=168;
  gw3=1;
  gw4=11;

  dns11=192;
  dns12=168;
  dns13=1;
  dns14=11;

  dns21=192;
  dns22=168;
  dns23=1;
  dns24=5;

  if (j > 0){
    for(i=0;i<j;i++){
      //printf("p: %s v: %s<br>\n",p[i],v[i]);
      // capture parameters for use later

      //int ip1, ip2, ip3, ip4;

      if (!strcmp(p[i], "ip1")){
        sscanf(v[i],"%d",&ip1);
      }

      if (!strcmp(p[i], "ip2")){
        sscanf(v[i],"%d",&ip2);
      }

      if (!strcmp(p[i], "ip3")){
        sscanf(v[i],"%d",&ip3);
      }

      if (!strcmp(p[i], "ip4")){
        sscanf(v[i],"%d",&ip4);
      }

      //int gw1, gw2, gw3, gw4;

      if (!strcmp(p[i], "gw1")){
        sscanf(v[i],"%d",&gw1);
      }

      if (!strcmp(p[i], "gw2")){
        sscanf(v[i],"%d",&gw2);
      }

      if (!strcmp(p[i], "gw3")){
        sscanf(v[i],"%d",&gw3);
      }

      if (!strcmp(p[i], "gw4")){
        sscanf(v[i],"%d",&gw4);
      }

      //int dns11, dns12, dns13, dns14;

      if (!strcmp(p[i], "dns11")){
        sscanf(v[i],"%d",&dns11);
      }

      if (!strcmp(p[i], "dns12")){
        sscanf(v[i],"%d",&dns12);
      }

      if (!strcmp(p[i], "dns13")){
        sscanf(v[i],"%d",&dns13);
      }

      if (!strcmp(p[i], "dns14")){
        sscanf(v[i],"%d",&dns14);
      }

      //int dns21, dns22, dns23, dns24;

      if (!strcmp(p[i], "dn21")){
        sscanf(v[i],"%d",&dns21);
      }

      if (!strcmp(p[i], "dns22")){
        sscanf(v[i],"%d",&dns22);
      }

      if (!strcmp(p[i], "dns23")){
        sscanf(v[i],"%d",&dns23);
      }

      if (!strcmp(p[i], "dns24")){
        sscanf(v[i],"%d",&dns24);
      }
    }
    // write files
    printf("<table><tr><td class=boldblue bgcolor=red><b>Updated Network Configuration!</b> Review and ensure network settings are correct before cycling power.</td></tr></table><br>\n");

    if (fp = fopen("/etc/sysconfig/network_cfg","w")){
      fprintf(fp,"### Web generated configuration file\n###\n\n");
      fprintf(fp,"        NETWORKING=yes\n");
      fprintf(fp,"        GATEWAY=%d.%d.%d.%d\n",gw1,gw2,gw3,gw4);
      fprintf(fp,"        GW_DEV=eth0\n");
      fprintf(fp,"        HOSTNAME=\"Vesta\"\n");
      fprintf(fp,"        BOOTPROTO=static\n");
      fprintf(fp,"        DEFRAG_IPV4=no\n");
      fprintf(fp,"        FORWARD_IPV4=no\n");
      fclose(fp);
    }else{
      printf("Error opening network_cfg: %s\n",strerror(errno));
    }

    if (fp = fopen("/etc/sysconfig/ifcfg-eth0","w")){
      fprintf(fp,"DEVICE=eth0\n");
      fprintf(fp,"IPADDR=%d.%d.%d.%d\n",ip1,ip2,ip3,ip4);
      fprintf(fp,"NETMASK=255.255.255.0\n");
      fprintf(fp,"NETWORK=%d.%d.%d.0\n",ip1,ip2,ip3);
      fprintf(fp,"BROADCAST=%d.%d.%d.255\n",ip1,ip2,ip3);
      fprintf(fp,"BOOTPROTO=static\n");
      fprintf(fp,"#BOOTPROTO=dhcp\n");
      fprintf(fp,"ENABLE=yes\n");
      fclose(fp);
    }else{
      printf("Error opening ifcfg-eth0: %s\n",strerror(errno));
    }
  
    if (fp = fopen("/etc/resolv.conf","w")){
      fprintf(fp,"nameserver %d.%d.%d.%d\n",dns11, dns12, dns13, dns14);
      fprintf(fp,"nameserver %d.%d.%d.%d\n",dns21, dns22, dns23, dns24);
      fclose(fp);
    }else{
      printf("Error opening resolv.conf: %s\n",strerror(errno));
    }

    if (fp = fopen("/etc/hosts","w")){
      fprintf(fp,"127.0.0.1\tlocalhost\tlocalhost\n");
      fprintf(fp,"%d.%d.%d.%d\tVesta\tVesta\n",ip1,ip2,ip3,ip4);
      fclose(fp);
    }else{
      printf("Error opening hosts: %s\n",strerror(errno));
    }
  }
  // White Container table - tab page contents

  printf("<form action=vestaNetCfg method=get>\n");
  printf ("<table bgcolor=white>\n");

  // ifcfg-eth0: Read file to get current values
  fp = fopen("/etc/sysconfig/ifcfg-eth0","r");
  // Toss first line
  fgets(linebuff,256,fp);
  fscanf(fp,"IPADDR=%d.%d.%d.%d",&ip1,&ip2,&ip3,&ip4);
  fclose(fp);

  printf ("<tr><td>Vesta IP Address:</td>\n");
  printf ("<td><input size=3 max=3 name=ip1 value=%d>.\n",ip1);
  printf ("<input size=3 max=3 name=ip2 value=%d>.\n",ip2);
  printf ("<input size=3 max=3 name=ip3 value=%d>.\n",ip3);
  printf ("<input size=3 max=3 name=ip4 value=%d></td>\n",ip4);
  printf ("</tr>\n");

  // network_cfg: Read file to get current values
  fp = fopen("/etc/sysconfig/network_cfg","r");
  // Toss first 4 lines
  fgets(linebuff,256,fp);
  fgets(linebuff,256,fp);
  fgets(linebuff,256,fp);
  fgets(linebuff,256,fp);
  fscanf(fp,"GATEWAY=%d.%d.%d.%d",&gw1,&gw2,&gw3,&gw4);
  fclose(fp);

  printf ("<tr><td>Network Gateway Address:</td>\n");
  printf ("<td><input size=3 max=3 name=gw1 value=%d>.\n",gw1);
  printf ("<input size=3 max=3 name=gw2 value=%d>.\n",gw2);
  printf ("<input size=3 max=3 name=gw3 value=%d>.\n",gw3);
  printf ("<input size=3 max=3 name=gw4 value=%d></td>\n",gw4);
  printf ("</tr>\n");

  // resolv.conf: Read file to get current values
  fp = fopen("/etc/resolv.conf","r");
  fscanf(fp,"nameserver %d.%d.%d.%d",&dns11,&dns12,&dns13,&dns14);
  if (fscanf(fp,"nameserver %d.%d.%d.%d",&dns21,&dns22,&dns23,&dns24) != 4){
    dns21=192;
    dns22=168;
    dns23=1;
    dns24=5;
  }
  fclose(fp);

  printf ("<tr><td>Network DNS Server 1:</td>\n");
  printf ("<td><input size=3 max=3 name=dns11 value=%d>.\n",dns11);
  printf ("<input size=3 max=3 name=dns12 value=%d>.\n",dns12);
  printf ("<input size=3 max=3 name=dns13 value=%d>.\n",dns13);
  printf ("<input size=3 max=3 name=dns14 value=%d></td>\n",dns14);
  printf ("</tr>\n");

  printf ("<tr><td>Network DNS Server 2:</td>\n");
  printf ("<td><input size=3 max=3 name=dns21 value=%d>.\n",dns21);
  printf ("<input size=3 max=3 name=dns22 value=%d>.\n",dns22);
  printf ("<input size=3 max=3 name=dns23 value=%d>.\n",dns23);
  printf ("<input size=3 max=3 name=dns24 value=%d></td>\n",dns24);
  printf ("</tr>\n");

  printf("<tr><td colspan=2 align=left><input type=submit name=Submit value=\"Update Settings\"></td></tr>\n");
  // Close container table
  printf("</table></form>\n");

  printf("</body></html>\n");

}


