//************************************************************************************************
// i2c I/O for BBB
// Initial test is TI ADS1115 4 channel 16 bit ADC at address 01001000 (ADDR pin grounded)
// device is /dev/i2c-2 on pins p9-17 and p9-18
// Consider ADS1120 SPI device as well
//************************************************************************************************

#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>
#include "ads1x15.h"

void sensors_ADC_init(int cycles);

int main(){
  struct timeval begin,end;
  unsigned long long begin_usec,end_usec;
  int cycles = 100;

  // Capture execution time
  gettimeofday(&begin,NULL);
  begin_usec = begin.tv_sec * 1000000 + begin.tv_usec;

 	sensors_ADC_init(cycles);

  gettimeofday(&end,NULL);
  end_usec = end.tv_sec * 1000000 + end.tv_usec;

	printf("ET: %f Sample time: %d usec\n",(float)(end_usec-begin_usec)/1000000,(int)((end_usec-begin_usec)/(cycles*4)));
	return 0;
}


void i2cwrite(int file, char *buf, int size){
  const char *buffer;
	if (write(file,buf,size) != size) {
			// ERROR HANDLING: i2c transaction failed
			printf("2: Failed to write to the i2c bus.\n");
			buffer = strerror(errno);
			printf(buffer);
			printf("\n\n");
	}
}

int i2cread(int file, char*buf, int size){
  const char *buffer;
	if (read(file,buf,size) != size) {
		// ERROR HANDLING: i2c transaction failed
		printf("3: Failed to read from the i2c bus.\n");
		buffer = strerror(errno);
		printf(buffer);
		printf("\n\n");
		return(0);
	}else{
		return(size);
	}
}

void sensors_ADC_init(int cycles) {
    int file,i,j,k,value,cfg_msb,cfg_lsb;
    char filename[40];
    const char *buffer;
    int addr = 0b01001000;        // The I2C address of the ADC

    char buf[10] = {'\0','\0','\0','\0','\0','\0','\0','\0','\0'};
    float data, fval;

    float fmax[4];
    float fmin[4];

    sprintf(filename,"/dev/i2c-2");
    if ((file = open(filename,O_RDWR)) < 0) {
        printf("Failed to open the bus.");
        /* ERROR HANDLING; you can check errno to see what went wrong */
        exit(1);
    }

    if (ioctl(file,I2C_SLAVE,addr) < 0) {
        printf("Failed to acquire bus access and/or talk to slave.\n");
        /* ERROR HANDLING; you can check errno to see what went wrong */
        exit(1);
    }

    // Works from command line....
    // i2cset -y 2 0x48 1 0xC385 w
    // i2cget -y 2 0x48 0 w
    // 0x95ab

    // From TI data sheet:
    // Set config
    // 0b10010000 (0x90, 0x48 + 0 bit)	7 bit i2c addr and low r/w bit
    // 0b00000001	(0x01)	config register address
    // 0b10000100 (0x84)	MSB of config data - sets to contunuous conversion
    // 0b10000011 (0x83)	LSB of config data
    // Set pointer
    // 0b10010000	(0x90, 0x48 + 0 bit)	7 bit i2c addr and low r/w bit
    // 0b00000000	(0x00)	points to conversion register
    // Read.....

  	for(i=0;i<4;i++){
  		fmax[i] = 0;
  		fmin[i] = 10;
  	}
    for(j=0; j<cycles; j++){
			for(i = 0; i<4; i++) {
				// Write config - set input mux (single ended reads)
				cfg_msb = (short int) i << 4;
				cfg_msb |= 0x40;
				cfg_msb |= ADS1015_REG_CONFIG_OS_SINGLE >> 8;
				cfg_msb |= ADS1015_REG_CONFIG_PGA_4_096V >> 8;				// +/- 4.096V range
				cfg_msb |= ADS1015_REG_CONFIG_MODE_SINGLE >> 8;				// Single sample

				cfg_lsb |= ADS1015_REG_CONFIG_DR_920SPS;							// 9440 usec/sample (8000 usec usleep), 1100uV noise

				cfg_lsb |= ADS1015_REG_CONFIG_DR_1600SPS;						// 9375 usec/sample (8000 usec usleep), 800uV noise
																															// 5406 usec/sample (4000 usec usleep), 2800uV noise

				//cfg_lsb |= ADS1015_REG_CONFIG_DR_2400SPS;					  // 5406 usec/sample (4000 usec usleep), 2800uV noise
																															// 9423 usec/sample (8000 usec usleep), 2800uV noise
				cfg_lsb |= ADS1015_REG_CONFIG_CMODE_TRAD;
				cfg_lsb |= ADS1015_REG_CONFIG_CPOL_ACTVLOW;
				cfg_lsb |= ADS1015_REG_CONFIG_CLAT_NONLAT;
				cfg_lsb |= ADS1015_REG_CONFIG_CQUE_NONE;

				buf[0] = 0b00000001;
				buf[1] = cfg_msb;
				buf[2] = cfg_lsb;
				i2cwrite(file,buf,3);
				usleep(8000);

				// Write pointer (tell ADC to report data)
				buf[0] = 0;
				i2cwrite(file,buf,1);

				// Read data
				if(i2cread(file,buf,2)){
					if(buf[0] & 0x80){
						buf[0] = 0;
						buf[1] = 0;
					}
					value = buf[0] * 256 + buf[1];
					fval = (float)value/32767.0 * 4.096 ;
					if(fval > fmax[i]){
						fmax[i]=fval;
					}
					if(fval < fmin[i]){
						fmin[i]=fval;
					}
					//printf("Channel %02d MSB %0X LSB %0X Data: %0X %0X Value: %d Float: %f\n",i,cfg_msb,cfg_lsb,buf[0],buf[1],value,fval );
				}
			}
    }
		for(i=0;i<4;i++){
			printf("Channel %02d Min: %0.4f Max: %0.4f Range: %0.4f\n",i,fmin[i],fmax[i],fmax[i]-fmin[i] );
		}
}
