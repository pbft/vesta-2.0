#include <stdio.h>
#include<sys/types.h>
#include<sys/mman.h>
#include <unistd.h>
#include<fcntl.h>
#include "core.h"
#include "xdio.h"

/*
  xdio.c - XDIO C API implementation
  Author: Michael Schmidt
  Copyright (c)2006 Technologic Systems
 */

/*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License v2 as published by
*  the Free Software Foundation.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.

*  You should have received a copy of the GNU General Public License
*  along with this program; if not, write to the Free Software
*  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

/*
  We don't support IRQ/DRQ (requires OS driver)
 */

void safe_setIP_EN_5(xdio *x,int v) {
  unsigned char c;

  setMODE(x->base,1);
  c = x->base[1];
  x->glitch |= c & 0x7F;
  x->base[1] = (c & 0x7F) | (v << 7);
}

int safe_getIP_EN_5(xdio *x) {
  unsigned char c;

  setMODE(x->base,1);
  c = x->base[1];
  x->glitch |= c & 0x7F;
  return (c >> 7) & 1;
}

int safe_getGLITCH(xdio *x) {
  x->glitch |= getGLITCH(x->base);
  return x->glitch;
}

//---------------------------------------------------------------------------
// Extentions to core to handle registers spread across multiple fields
//---------------------------------------------------------------------------

int getQCTR(xdio *x) {
  unsigned char *base = x->base;
  int a1,b1,a2;

  a1 = getQCTR_HI(base);
  b1 = getQCTR_LO(base);
  a2 = getQCTR_HI(base);
  if (b1 > 0x7F) {
    return (a1 << 8) + b1;
  } else {
    return (a2 << 8) + b1;
  }
}

void setQCTR(xdio *x,int v) {
  unsigned char *base = x->base;

  setQCTR_LO(base,v & 0xFF);
  setQCTR_HI(base,(v >> 8) & 0xFF);
}

int getPT(xdio *x) {
  unsigned char *base = x->base;
  int a1,b1,c1,a2,b2;

  a1 = getPT_EX(base);
  b1 = getPT_HI(base);
  c1 = getPT_LO(base);
  b2 = getPT_HI(base);
  a2 = getPT_EX(base);
  if (b1 > 0x7F && c1 > 0x7F) {
    return (a1 << 16) + (b1 << 8) + c1;
  } else {
    return (a2 << 16) + (b2 << 8) + c1;
  }
}

/* Hardware is not wired to accept writing to Pulse Timer registers
void setPT(unsigned char *base,int v) {
  setPT_LO(base,v & 0xFF);
  setPT_HI(base,(v >> 8) & 0xFF);
  setPT_EX(base,(v >> 16) & 0x0F);
}
*/

int getTHI(unsigned char *base) {
  int a1,b1,a2;

  a1 = getTHI_HI(base);
  b1 = getTHI_LO(base);
  a2 = getTHI_HI(base);
  if (b1 > 0x7F) {
    return (a1 << 8) + b1;
  } else {
    return (a2 << 8) + b1;
  }
}

void setTHI(unsigned char *base,int v) {
  setTHI_LO(base,v & 0xFF);
  setTHI_HI(base,(v >> 8) & 0xFF);
}

int getTLO(unsigned char *base) {
  int a1,b1,a2;

  a1 = getTLO_HI(base);
  b1 = getTLO_LO(base);
  a2 = getTLO_HI(base);
  if (b1 > 0x7F) {
    return (a1 << 8) + b1;
  } else {
    return (a2 << 8) + b1;
  }
}

void setTLO(unsigned char *base,int v) {
  setTLO_LO(base,v & 0xFF);
  setTLO_HI(base,(v >> 8) & 0xFF);
}

//---------------------------------------------------------------------------
// Implementation of various XDIO tasks
//---------------------------------------------------------------------------

/*
  What sorts of things can we do?
  1. We can set up each pin individually as an input or output.
     If it is an output we can change the output value.
     If it is an input we can sample the input value.
  2. The "pulse timer" can be used on one pin
  3. All pins can be monitored for glitches.
     If we had a driver to pass IRQs up to us we could wait
  4. Pin 4 (as an output) can output PWM or a single pulse
  5. We can count edges on pin 7 (presumably only when its an input?)
     OR we can count quadrature on pins 6 and 7
     Pin 5 can be used to reset the edge/quadrature counter

  Meta-Tasks
  6. Stopwatch
  7. Free-running 
 */
// TASK 1

void setPinDirection(xdio *x,int pin,enum PIN_DIRECTION dir) {
  setDIR(x->base,(getDIR(x->base) & ~(1 << pin)) | (dir ? (1<<pin) : 0));
}

void setPin(xdio *x,int pin,enum PIN_VALUE val) {
  setDATA(x->base,(getDATA(x->base) & ~(1 << pin)) | (val ? (1<<pin) : 0));
}

int getPin(xdio *x,int pin) {
  return (getDATA(x->base) & (1 << pin)) != 0;
}

// TASK 2

void initPulseTimer(xdio *x,int pin,int acc,
		     enum PULSE_TYPE ptype) {
  setPTRST(x->base,0);
  setPT_ACC(x->base,acc);
  setINPNO(x->base,pin);
  setPinDirection(x,pin,INPUT);
  setPTMODE(x->base,ptype);
  setPTRST(x->base,1);
}

int clockPeriodInNs(xdio *x) {
  int slow = getCLKRATE(x->base);
  int ns;

  if (slow) {
    if (x->model == TS7260) {
      ns = 30518;
    } else {
      ns = 26667;
    }
  } else {
    if (x->model == TS7260) {
      ns = 68;
    } else {
      ns = 13;
    }
  }
  return ns;
}

int setClockSpeed(xdio *x,int fast) {
  setCLKRATE(x->base,!fast);
  return clockPeriodInNs(x);
}

// Return nanoseconds measured by the pulse timer
long long getPulseTimer(xdio *x,int sync,int reset) {
  long long ns;

  if (sync) {
    while (getPTRST(x->base) == 1); // wait for end of current pulse
  }
  ns = getPT(x) * clockPeriodInNs(x); 
  if (reset) {
    setPTRST(x->base,1);
  }
  return ns;
}

// TASK 3
int pinLevelChanged(xdio *x,int pin) {
  int result;
  
  if (pin == 7) {
    return 0; // can't detect glitches on this pin, use edge counter instead
  }
  safe_getGLITCH(x);
  result = (x->glitch & (1 << pin)) != 0;
  x->glitch &= ~(1 << pin);
  return result;
}

// TASK 4
// Pulse pin 4 to the inversion of its current value for the specified
// number of nanoseconds, rounded DOWN to the nearest clock tick
void generatePulse(xdio *x,long long forNs) {
  setPinDirection(x,4,OUTPUT); // make sure we are an output
  setTLO(x->base,0); // single pulse
  setTHI(x->base,forNs / clockPeriodInNs(x));
  setSTART(x->base,1);
}

// Output PWM on pin 4 at the given frequency in Hz (truncated to the 
// nearest clock tick) and duty cycle, expressed in 10,000ths of a percent.
// (e.g. 500,000 = 50%)
// returns FALSE if an out of range frequency or duty cycle is specified,
// or if pin 4 is not open for pulses
int startPWM(xdio *x,int freq,int microDutyCycle) {
  int totalNs, nsHigh, nsLow,clksHigh,clksLow,period;

  if (freq == 0 || microDutyCycle == 0) {
    return 0; // Not PWM!
  }
  setPinDirection(x,4,OUTPUT);
  totalNs = 1000000000 / freq;
  nsHigh = (int)((long long)totalNs * microDutyCycle / 1000000);
  nsLow = totalNs - nsHigh;
  period = clockPeriodInNs(x);
  clksHigh = nsHigh / period;
  clksLow = nsLow / period;
  if (clksHigh == 0 || clksLow == 0) {
    return 0; // Not PWM!
  }
  setTLO(x->base,clksLow);
  setTHI(x->base,clksHigh);
  /*  
  printf("totalNs=%d, %d hi, %d lo\n",totalNs,nsHigh,nsLow);
  printf("period=%d, %X hi, %X lo\n",period,clksHigh,clksLow);
  printf("%02X %02X %02X\n",x->base[1],x->base[2],x->base[3]);
  printf("%02X\n",x->base[0]);
  */
  setSTART(x->base,1);
  return 1;
}

void pausePWM(xdio *x) {
  setSTART(x->base,0);
}

void resumePWM(xdio *x) {
  setSTART(x->base,1);
}

// TASK 5
void initCounter(xdio *x,enum COUNTER_TYPE type,enum INDEX_TYPE index) {
  setPinDirection(x,7,INPUT);
  if (type == QUADRATURE) {
    setPinDirection(x,6,INPUT);
  }
  setQUADEN(x->base,type);
  if (index != NO_RESET) {
    safe_setIP_EN_5(x,0);
    setPin(x,5,index);
  } else {
    safe_setIP_EN_5(x,0);
  }
  setQCTR(x,0);
}

// Debugging/Informational
void register_dump(struct xdio *x) {
  unsigned char *c = x->base;

  printf("PTMODE=%d PTRST=%d QUADEN=%d START=%d CLKRATE=%d\n",
	 getPTMODE(c),getPTRST(c),getQUADEN(c),getSTART(c),getCLKRATE(c));
  printf("DIR=%02X DATA=%02X\n",getDIR(c),getDATA(c));
  printf("IP_EN_5=%d GLITCH=%02X QCTR_LO=%02X QCTR_HI=%02X QCTR=%04X\n",
	 safe_getIP_EN_5(x),safe_getGLITCH(x),getQCTR_LO(c),getQCTR_HI(c),getQCTR(x));
  printf("PT_ACC=%d INPNO=%d PT_EX=%X PT_HI=%02X PT_LO=%02X PT=%05X\n",
	 getPT_ACC(c),getINPNO(c),getPT_EX(c),getPT_HI(c),getPT_LO(c),getPT(x));
  printf("THI_HI=%02X THI_LO=%02X THI_LO=%02X TLO_LO=%02X THI=%04X TLO=%04X\n",
	 getTHI_HI(c),getTHI_LO(c),getTHI_LO(c),getTLO_LO(c),getTHI(c),getTLO(c));
}

//---------------------------------------------------------------------------
// Initialization Routines
//---------------------------------------------------------------------------
#define TS_MODEL_7260 2
#define TS_MODEL_7300 3

int getTSMODEL() {
  unsigned short *start;
  int fd;
  int mod;

  fd = open("/dev/mem", O_RDONLY | O_SYNC);
  if (fd == -1) {
    perror("open:");
    return 0;
  }
  start = mmap(0, getpagesize(), PROT_READ, MAP_SHARED, fd, 0x22000000);
  if (start == MAP_FAILED) {
    perror("mmap:");
    return 0;
  }
  mod = start[0] & 7;
  munmap(start,getpagesize());
  close(fd);
  switch (mod) {
  case TS_MODEL_7260: mod = TS7260; break;
  case TS_MODEL_7300: mod = TS7300; break;
  default: mod=-1; break;
  }
  return mod;
}

void *open_mem(int *fd,off_t addr) {
  off_t page;
  unsigned char *start;

  *fd = open("/dev/mem", O_RDWR | O_SYNC);
  if (*fd == -1) {
    perror("open:");
    return 0;
  }
  page = addr & 0xfffff000;
  start = mmap(0, getpagesize(), PROT_READ|PROT_WRITE, MAP_SHARED, *fd, page);
  if (start == MAP_FAILED) {
    perror("mmap:");
    return 0;
  }
  return (void *)(start + (addr & 0xFFF));
}

int open_xdio(struct xdio *x,int num) {
  x->model = getTSMODEL();
  if (x->model == TS7260) {
    if (num) {
      return 0;
    }
    x->base = open_mem(&x->fd,0x12C00000);
  } else if (x->model == TS7300) {
    x->base = open_mem(&x->fd,0x72000040 + (num ? 4:0));
  } else {
    fprintf(stderr,"This program only supports the TS-7260 and TS-7300\n");
    fprintf(stderr,"Neither of those boards were detected.\n");
    return 0;
  }
  if (!x->base) {
    return 0;
  }
  x->glitch = 0;
  return 1;
}

void close_xdio(struct xdio *x) {
  if (!x) {
    return;
  }
  munmap((void *)((int)x->base & 0xFFFFF000),getpagesize());
  close(x->fd);
  x->base = 0;
}
