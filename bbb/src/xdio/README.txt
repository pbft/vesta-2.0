To compile and use the XDIO sample program, here is what you need to do:

1. Edit 'Makefile' to point to your installation of crosstools.
   (Alterately, you could build directly on a TS-7260/TS-7300)

2. Type 'make'.  Copy 'xdio-test' to your board.

3. Install two jumper wires on your board: one between DIO0 and DIO1,
and the other between DIO4 and DIO7.  These are on the DIO2 header.

4. Run 'xdio-test'.  If you like, you can remove the DIO0/DIO1 jumper
before running the program: the program will detect this and ask you to
put it back on.


Here is a description of the files included with this release:
README.txt  - This file
TS-XDIO.txt - describes the XDIO registers and functionality
API.txt     - documents the XDIO API
xdio.h      - include this file in source code utilizing the XDIO API
xdio.c      - implements the XDIO API
core.h      - low-level API to directly access XDIO registers
core.c      - implements low-level XDIO register access
main.c      - sample program utilizing the XDIO API
Makefile    - simple Makefile to build the sample program
