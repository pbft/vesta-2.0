
/*****************************************************************************/
//    This file is part of the NoFossil Control System Software (NFCSS)
//    Copyright 2007, 2008, 2009 Bill Kuhns
//
//    NFCSS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    any later version.

//    NFCSS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with NFCSS.  If not, see <http://www.gnu.org/licenses/>.
/*****************************************************************************/

// *********** BUG LIST ******************************************************

#define PROCESSNAME  FUNCTION_NAME(mwireless)
// Monitor shared memory and make control decisions

#include <stdio.h>
#include <fcntl.h>
#include <sys/types.h>
//#include "vesta.h"
#include <termios.h>
#include <unistd.h>
#include <sys/signal.h>

#define BAUDRATE B115200
#define MODEMDEVICE "/dev/ttyO2"
#define _POSIX_SOURCE 1 /* POSIX compliant source */
#define FALSE 0
#define TRUE 1

volatile int STOP = FALSE;

void signal_handler_IO(int status); /* definition of signal handler */

int modem, c, n;
unsigned char obuf[255];

main(){
  long long start_usec;
  FILE *fp;
  unsigned char msgframe[56];

  // Set static frame content
  msgframe[0] = 0xC5;			// Start of message

  int i, x, cycle;
  int sleeptime;
  struct timeval now;

  int msgLength = 25;
  int msgFlag;
  struct termios oldtio, newtio;
  struct sigaction saio; /* definition of signal action */
  sigset_t set = { 0 };
  //unsigned char ibuf[255];
  //unsigned char msg[255];
  char entry[200];

  /* open the device to be non-blocking (read will return immediately) */
  modem = open(MODEMDEVICE, O_RDWR | O_NOCTTY | O_NONBLOCK);
  if (modem < 0) {
    perror(MODEMDEVICE);
    exit(-1);
  }

  /* install the signal handler before making the device asynchronous */
  saio.sa_handler = signal_handler_IO;
  saio.sa_mask = set;
  saio.sa_flags = 0;
  saio.sa_restorer = NULL;
  sigaction(SIGIO, &saio, NULL );

  /* allow the process to receive SIGIO */
  fcntl(modem, F_SETOWN, getpid());
  /* Make the file descriptor asynchronous (the manual page says only
   O_APPEND and O_NONBLOCK, will work with F_SETFL...) */
  //fcntl(modem, F_SETFL, FASYNC);
  fcntl(modem, F_SETFL, O_ASYNC|O_NONBLOCK|fcntl(modem, F_GETFL));

  tcgetattr(modem, &oldtio); /* save current port settings */
  /* set new port settings for canonical input processing */
  //newtio.c_cflag = BAUDRATE | CRTSCTS | CS8 | CLOCAL | CREAD;
  newtio.c_cflag = BAUDRATE | CS8 | CLOCAL | CREAD;
  newtio.c_iflag = IGNPAR | ICRNL;
  newtio.c_oflag = 0;
  newtio.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);
  newtio.c_cc[VMIN] = 1;
  newtio.c_cc[VTIME] = 0;
  tcflush(modem, TCIFLUSH);
  tcsetattr(modem, TCSANOW, &newtio);

  // Get shared memory links
  //get_shm(INSTEON_ID);

  //===========================================================================================================
  //
  // Initialize and begin the execution loop
  //
  //===========================================================================================================

  //start_usec = getNow();
  //shm->config->reload = shm->config->reload | ( 1 << INSTEON_ID);
  //imptr = 0;
  cycle = 0;

  // Read Insteon types
  //itypecount = readInsteonTypeFile(&itypes[0]);

  /*
  // Do forever
  while (1){
    // Sleep for desired interval
    // timedSleep(PROCESSNAME, shm->config->rule_period, &start_usec);
    //timedSleep(PROCESSNAME, 5, &start_usec);
  }
  */
/* restore old port settings */
tcsetattr(modem, TCSANOW, &oldtio);
}

void processMessage(char *msg, int msgLength){

}

int checksum(char *msg){
	int i, csum=0;
	for(i=6;i<21;i++){
		csum += (~msg[i]+1);
	}
	return(csum);
}


/***************************************************************************
* signal handler. sets wait_flag to FALSE, to indicate above loop that     *
* characters have been received.                                           *
***************************************************************************/

void signal_handler_IO(int status) {

  static int i, imptr = 0, res;
  static int msgLength = 25;
  static unsigned char ibuf[255];
  static unsigned char msg[255];

  // We got some data to read. Add it after last byte in buffer.
  imptr += read(modem, &ibuf[imptr], 255);
  ibuf[imptr] = '\0';
  // Now we have bytes in buffer. First *should* be 0x02.
  // Search through buffer for 0x02
  i=0;
  while(ibuf[i] != 0x02 && i<imptr){
  	i++;
  }
  // If we didn't have 0x02 at the start, shift buffer (may zero it out)
  if(i>0){
  	//printf("Junk at start of buffer: ");
  	//printMessage(ibuf,imptr);
    memcpy(&obuf[0],&ibuf[i],imptr-i);
    memcpy(&ibuf[0],&obuf[0],imptr-i);
    imptr -= i;
    ibuf[imptr] = '\0';
    //printf("Cleaned buffer: ");
  	//printMessage(ibuf,imptr);
  }
  // If we have a message left, try and process it

	if(imptr > 0){
		// If NAK, skip
		if(ibuf[2] == 0x15){
			msgLength = 3;
		}else{
			// Set expected message length
			// Add extended message check here
			if(ibuf[1] == 0x62){
				if(ibuf[5] & 0x10){
					msgLength = 23;
				}else{
					msgLength = 9;
				}
			}
			if(ibuf[1] == 0x50){
				msgLength = 11;
			}
			if(ibuf[1] == 0x51){
				msgLength = 25;
			}
		}
	}

  while (imptr >= msgLength){
    // copy message to msg, slide balance of input buffer down.
    memcpy(msg,ibuf,msgLength);
    //printMessage(msg,msgLength);
    if(msgLength > 3 && msg[1] != 0x62){
      processMessage(msg,msgLength);
    }
    if(imptr > msgLength){
      memcpy(&obuf[0],&ibuf[msgLength],imptr - msgLength);
      memcpy(&ibuf[0],&obuf[0],imptr - msgLength);
      imptr -= msgLength;
      if(ibuf[0] == 0x02){
        if(ibuf[2] == 0x15){
          msgLength = 3;
        }else{
          // Set expected message length
    			if(ibuf[1] == 0x62){
    				if(ibuf[5] & 0x10){
    					msgLength = 23;
    				}else{
    					msgLength = 9;
    				}
    			}
    			if(ibuf[1] == 0x50){
    				msgLength = 11;
    			}
    			if(ibuf[1] == 0x51){
    				msgLength = 25;
    			}
        }
      }else{
      	if(res > 0){
					printf("Error: not start of message 2. %d bytes - ",res);
					for(i=0;i<res;i++){
						printf("%X ",(int)ibuf[i]);
					}
					printf("\n");
				}
      }
    }else{
      imptr = 0;
    }

  }


}

