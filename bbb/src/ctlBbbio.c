/*****************************************************************************/
//    This file is part of the Vesta Control System Software Suite
//    Copyright Bill Kuhns and Vermont Energy Control Systems LLC.
//
//    This is free software: you can redistribute it and/or modify
//    it under the terms of the Lesser GNU General Public License (LGPL)
//    as published by the Free Software Foundation, either version 3 of the License,
//    or any later version.
//
//    This software is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    Lesser GNU General Public License for more details.
//
//    You should have received a copy of the Lesser GNU General Public License
//    along with this software.  If not, see <http://www.gnu.org/licenses/>.
/*****************************************************************************/

#define PROCESSNAME  FUNCTION_NAME(7xxx hwio)
// Process all physical I/O. Read and write to shared memory.
// Perform scaling, calibration, and linearization as needed.

// This is a periodic background task

// libpruio? http://stackoverflow.com/questions/23842767/reading-analog-inputs-fast-in-beaglebone-black
#include <errno.h>
#include <stdio.h>
#include <math.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <linux/i2c-dev.h>
#include "ads1x15.h"
#include "vesta.h"

#define GPIO_SIZE  0x00000FFF

// OE: 0 is output, 1 is input
//#define GPIO_OE 0x14d
//#define GPIO_IN 0x14e
//#define GPIO_OUT 0x14f

#define GPIO_OE 0x4d
#define GPIO_IN 0x4e
#define GPIO_OUT 0x4f

#define USR0_LED (1<<21)
#define USR1_LED (1<<22)
#define USR2_LED (1<<23)
#define USR3_LED (1<<24)

// BeagleBone has 4 GPIO banks with 32 channels each. Each has a different base address
unsigned int gpio_base[4] = { 0x44E07000, 0x4804C000, 0x481AC000, 0x481AE000 };

// We'll need a shared memory block mapped to each
int mem_fd[4];
char *gpio_mem[4], *gpio_map[4];

// I/O access
volatile unsigned *gpio[4];
unsigned int creg[4];

// Actual I/O channels map to GPIO block/channel and corresponding physical pin.

// array mapping DI channels to GPIO blocks and channels
int di_map[8][2] = { { 2, 2 },		// P8/7
		{ 2, 3 },		// P8/8
		{ 2, 5 },		// P8/9
		{ 2, 4 },		// P8/10
		{ 0, 23 },	// P8/13
		{ 0, 26 },	// P8/14
		{ 0, 27 },	// P8/17
		{ 2, 1 }  	// P8/18
};

// array mapping DO channels to GPIO blocks and channels
int do_map[12][2] = { { 1, 13 },	// P8/11
		{ 1, 12 },	// P8/12
		{ 1, 15 },	// P8/15
		{ 1, 14 },	// P8/16
		{ 1, 18 },	// P9/14
		{ 1, 16 },	// P9/15
		{ 1, 19 },	// P9/16
		{ 1, 17 },	// P9/23
		{ 1, 21 },	// LED0
		{ 1, 22 },	// LED1
		{ 1, 23 },	// LED2
		{ 1, 24 }	  // LED3
};

typedef struct {
	short int ordinal;
	short int channel;
	short int handler;
	volatile byte_t *adcommand;
	volatile byte_t *lsb;
	volatile byte_t *msb;
	int adraw;
	short int eid;
} ts9700ai_struct;

typedef struct {
	short int ordinal;
	short int channel;
	short int handler;
	volatile short *daclsb;
	short int eid;
} ts9700ao_struct;

typedef struct {
	short int ordinal;
	short int handler;
	int gpioblock;
	long unsigned int mask;
	volatile byte_t *dio64_in;
	short int eid;
} ts9700di_struct;

typedef struct {
	short int ordinal;
	short int handler;
	int gpioblock;
	long unsigned int mask;
	volatile byte_t *dio64_out;
	short int eid;
} ts9700do_struct;

void i2cwrite(int, char *, int);
int i2cread(int, char *, int);

main() {

	int fd;
	FILE *fp;
	float reading;
	float volts;
	int c, i, j, lut;
	short dacvalue;
	int temp[50];
	float lookup[4][36];

	char tname[80];
	char fname[80];
	char msgbuff[80];
	short raw, oldraw;
	byte_t ts_dio64_1, ts_9700_1, ts_9700_2;
	int aiptr, aoptr, diptr, doptr;
	int i2cFile, k, value, cfg_msb, cfg_lsb;
	char filename[40];
	const char *buffer;
	int addr = 0b01001000;        // The I2C address of the ADC

	char buf[10] = { '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0' };
	float data, fval;

	long long start_usec;

	// Get shared memory links
	get_shm(SERVER_ID);

	// Set up structures for I/O cards
	ts9700ai_struct *ts9700ai = (ts9700ai_struct *) calloc(shm->counts->ai_count,
			sizeof(ts9700ai_struct));
	ts9700ao_struct *ts9700ao = (ts9700ao_struct *) calloc(shm->counts->ao_count,
			sizeof(ts9700ao_struct));
	ts9700di_struct *ts9700di = (ts9700di_struct *) calloc(shm->counts->di_count,
			sizeof(ts9700di_struct));
	ts9700do_struct *ts9700do = (ts9700do_struct *) calloc(shm->counts->do_count,
			sizeof(ts9700do_struct));

	// Get I2C file handle
	sprintf(filename, "/dev/i2c-2");
	if ((i2cFile = open(filename, O_RDWR)) < 0) {
		printf("Failed to open the bus.");
		/* ERROR HANDLING; you can check errno to see what went wrong */
		exit(1);
	}

	if (ioctl(i2cFile, I2C_SLAVE, addr) < 0) {
		printf("Failed to acquire bus access and/or talk to slave.\n");
		/* ERROR HANDLING; you can check errno to see what went wrong */
		exit(1);
	}

	// Initialize structures for I/O cards
	aiptr = aoptr = diptr = doptr = 0;
	for (i = 0; i < shm->counts->hw_count; i++) {
		// If it's ours
		//printf("line %d handler %d\n", i, shm->hwconfig[i].handler);
		if (shm->hwconfig[i].handler == SERVER_ID) {
			switch (shm->hwconfig[i].type) {
			case 1:
				// Analog Input
				for (j = 0; j < shm->hwconfig[i].channelcount; j++) {
					ts9700ai[aiptr + j].ordinal = shm->hwconfig[i].ordinal;
					ts9700ai[aiptr + j].eid = shm->hwconfig[i].eid + j;
					ts9700ai[aiptr + j].channel = j;
					ts9700ai[aiptr + j].handler = SERVER_ID;
					printf("Analog Input %d = element %d\n", aiptr + j,
							ts9700ai[aiptr + j].eid);
				}
				aiptr += shm->hwconfig[i].channelcount;
				break;
			case 2:
				// Analog Output
				for (j = 0; j < shm->hwconfig[i].channelcount; j++) {
					ts9700ao[aoptr + j].ordinal = shm->hwconfig[i].ordinal;
					ts9700ao[aoptr + j].eid = shm->hwconfig[i].eid + j;
					ts9700ao[aoptr + j].channel = j;
					ts9700ao[aoptr + j].handler = SERVER_ID;
					printf("Analog Output %d = physio %d\n", aoptr + j,
							ts9700ao[aoptr + j].eid);
				}
				aoptr += shm->hwconfig[i].channelcount;
				break;
			case 3:
				// Discrete Input
				//for (j = 0; j < shm->hwconfig[i].channelcount; j++) {
				for (j = 0; j < 8; j++) {
					ts9700di[diptr + j].ordinal = shm->hwconfig[i].ordinal;
					ts9700di[diptr + j].eid = shm->hwconfig[i].eid + j;
					ts9700di[diptr + j].handler = SERVER_ID;
					ts9700di[doptr + j].mask = (1 << di_map[j][1]);
					ts9700di[doptr + j].gpioblock = di_map[j][0];
					//printf("Discrete Input %d = physio %d\n", diptr + j,
					//    ts9700di[diptr + j].eid);
				}
				diptr += shm->hwconfig[i].channelcount;
				break;
			case 4:
				// Discrete Output
				//for (j = 0; j < shm->hwconfig[i].channelcount; j++) {
				for (j = 0; j < 12; j++) {
					ts9700do[doptr + j].ordinal = shm->hwconfig[i].ordinal;
					ts9700do[doptr + j].eid = shm->hwconfig[i].eid + j;
					ts9700do[doptr + j].handler = SERVER_ID;
					ts9700do[doptr + j].mask = (1 << do_map[j][1]);
					ts9700do[doptr + j].gpioblock = do_map[j][0];
					//printf("Discrete Output %d = physio %d\n", doptr + j,
					//   ts9700do[doptr + j].eid);
				}
				break;
			}
		}
	}
	//printf("DIa %d handler %d\n",1,ts9700di[1].handler);

	fd = open("/dev/mem", O_RDWR | O_SYNC);
	if (fd < 0) {
		sprintf(logbuffer, "could not open /dev/mem");
		printlog(PROCESSNAME, logbuffer, 1);
		exit(1);
	}

	// Enable all GPIO banks
	// Without this, access to deactivated banks (i.e. those with no clock source set up) will (logically) fail with SIGBUS
	// Idea taken from https://groups.google.com/forum/#!msg/beagleboard/OYFp4EXawiI/Mq6s3sg14HoJ
	system("echo 5 > /sys/class/gpio/export");
	system("echo 65 > /sys/class/gpio/export");
	system("echo 105 > /sys/class/gpio/export");

	// Set up memory map for all 4 GPIO banks

	for (i = 0; i < 4; i++) {

		/* open /dev/mem */
		if ((mem_fd[i] = open("/dev/mem", O_RDWR | O_SYNC)) < 0) {
			printf("can't open /dev/mem \n");
			exit(-1);
		}

		/* mmap GPIO */
		gpio_map[i] = (char *) mmap(0,
		GPIO_SIZE,
		PROT_READ | PROT_WRITE,
		MAP_SHARED, mem_fd[i], gpio_base[i]);

		if (gpio_map[i] == MAP_FAILED) {
			printf("mmap error %d\n", (int) gpio_map);
			exit(-1);
		}

		// Always use the volatile pointer!
		gpio[i] = (volatile unsigned *) gpio_map[i];

		// Get direction control register contents - outputs
		creg[i] = *(gpio[i] + GPIO_OE);
		for (j = 0; j < 12; j++) {
			if (do_map[j][0] == i) {
				printf("%d %d %X: ", do_map[j][0], do_map[j][1], creg[i]);
				creg[i] = creg[i] & (~(1 << do_map[j][1]));
				printf("%X %X: \n", (~(1 << do_map[j][1])), creg[i]);
			}
		}
		// Set new direction control register contents
		*(gpio[i] + GPIO_OE) = creg[i];

		// Get direction control register contents - inputs
		creg[i] = *(gpio[i] + GPIO_OE);
		for (j = 0; j < 8; j++) {
			if (di_map[j][0] == i) {
				printf("%d %d %X: ", di_map[j][0], di_map[j][1], creg[i]);
				//creg[i] = creg[i] | (1<<di_map[j][1]);
				printf("%X %X: \n", 1 << di_map[j][1], creg[i]);
			}
		}
		// Set new direction control register contents
		//*(gpio[i] + GPIO_OE) = creg[i];

	}

	// Clear default behavior
	system("echo none > /sys/class/leds/beaglebone:green:usr0/trigger");
	system("echo none > /sys/class/leds/beaglebone:green:usr1/trigger");
	system("echo none > /sys/class/leds/beaglebone:green:usr2/trigger");
	system("echo none > /sys/class/leds/beaglebone:green:usr3/trigger");

	/*
	 // Set outputs
	 creg[1] = creg[1] & (~USR0_LED);
	 creg[1] = creg[1] & (~USR1_LED);
	 creg[1] = creg[1] & (~USR2_LED);
	 creg[1] = creg[1] & (~USR3_LED);
	 creg[1] = creg[1] & (~(1<<28));
	 */

	start_usec = getNow();

	//******************************************************************************************/
	//
	// Set initial conditions. For now, just dio direction. The control task may override.
	//
	//******************************************************************************************/

	// Do forever
	while (1) {

		if (reloadRequired(SERVER_ID)) {

			// Read voltage lookup table data (Starts at 145 C, 5 degree increments)
			// Data from thermistor.csv, generated by thermistor.xls
			// ********** FIX THIS! Should read temp (or other values) from file! *************

			for (i = 0; i < 4; i++) {
				sprintf(&fname[0], "/etc/vesta/lookup0%02d.csv", i);
				fp = fopen(fname, "r");

				while (fscanf(fp, "%d", &j) > 0) {
					fscanf(fp, "%s", &tname);
					fscanf(fp, "%s", &tname);
					for (j = 0; j < 36; j++) {
						fscanf(fp, "%f", &lookup[i][j]);
						temp[j] = 145 - j * 5;
					}
				}
				fclose(fp);
			}

			// Claim our elements
			for (i = 0; i < shm->counts->element_count; i++) {
				if (shm->elements[i].pvn == 'p' && shm->elements[i].io == 'i') {
					claimElement(i, SERVER_ID);
				}
			}

			// Clear our bit in the reload flag
			clearReloadFlag(SERVER_ID);
		}

		// Start Analog Input

		//for (c = 0; c < shm->counts->ai_count; c++) {
		for (c = 0; c < 4; c++) {
			if (ts9700ai[c].handler == SERVER_ID) {
				// Write config - set input mux (single ended reads)
				cfg_msb = (short int) c << 4;
				cfg_msb |= 0x40;
				cfg_msb |= ADS1015_REG_CONFIG_OS_SINGLE >> 8;
				cfg_msb |= ADS1015_REG_CONFIG_PGA_4_096V >> 8;			// +/- 4.096V range
				cfg_msb |= ADS1015_REG_CONFIG_MODE_SINGLE >> 8;			// Single sample

				cfg_lsb |= ADS1015_REG_CONFIG_DR_1600SPS;						// 9375 usec/sample (8000 usec usleep), 8000uV noise
				cfg_lsb |= ADS1015_REG_CONFIG_CMODE_TRAD;
				cfg_lsb |= ADS1015_REG_CONFIG_CPOL_ACTVLOW;
				cfg_lsb |= ADS1015_REG_CONFIG_CLAT_NONLAT;
				cfg_lsb |= ADS1015_REG_CONFIG_CQUE_NONE;

				buf[0] = 0b00000001;
				buf[1] = cfg_msb;
				buf[2] = cfg_lsb;
				i2cwrite(i2cFile, buf, 3);
				usleep(8000);

				// Write pointer (tell ADC to report data)
				buf[0] = 0;
				i2cwrite(i2cFile, buf, 1);

				// Read data
				if (i2cread(i2cFile, buf, 2)) {
					if (buf[0] & 0x80) {
						buf[0] = 0;
						buf[1] = 0;
					}
					value = buf[0] * 256 + buf[1];
					volts = (float) value / 32767.0 * 4.096;
					//printf("ch %d %f v\n", c, volts);
					//ts9700ai[c].adraw =
					//		(short) (256 * *ts9700ai[c].msb + *ts9700ai[c].lsb);
					//printf("ch %d %f v\n", c, volts);

					// Test for error....

					//if (ts9700ai[c].adraw == 0 || ts9700ai[c].adraw == 4095) {
					if (0) {
						shm->aivalue[ts9700ai[c].ordinal * 8 + ts9700ai[c].channel] = 0;
						shm->aistatus[ts9700ai[c].ordinal] =
								shm->aistatus[ts9700ai[c].ordinal]
										| (1 << (ts9700ai[c].channel % 8));
					} else {

						// Calibrate.
						volts = volts
								* shm->stypes[shm->elements[ts9700ai[c].eid].stype].gain
								* shm->elements[ts9700ai[c].eid].gain
								+ shm->stypes[shm->elements[ts9700ai[c].eid].stype].offset;
						//printf("ch %d %f v\n", c, volts);

						// Use lookup table?
						if (shm->stypes[shm->elements[ts9700ai[c].eid].stype].lut > 0) {

							//  Find lookup table cell
							i = 0;
							lut = shm->stypes[shm->elements[ts9700ai[c].eid].stype].lut;
							while (lookup[lut][i] < volts) {
								i++;
							}
							reading = temp[i - 1]
									+ ((volts - lookup[lut][i - 1]) * (temp[i] - temp[i - 1])
											/ (lookup[lut][i] - lookup[lut][i - 1]));
							reading = reading + shm->elements[ts9700ai[c].eid].offset;
							shm->aivalue[ts9700ai[c].ordinal * 8 + ts9700ai[c].channel] = (
									isnan(reading) ? 0 : reading);
						} else {
							volts = volts + shm->elements[ts9700ai[c].eid].offset;
							shm->aivalue[ts9700ai[c].ordinal * 8 + ts9700ai[c].channel] = (
									isnan(volts) ? 0 : volts);
						}
					}
				}
			}
		}
		// End analog input

		// Inputs in Vesta are in blocks of 8
		//for (c = 0; c < shm->counts->di_count; c++) {
		for (c = 0; c < 8; c++) {
			if (ts9700do[c].handler == SERVER_ID) {
				i = ts9700di[c].gpioblock;
				j = *(gpio[i] + GPIO_IN) & ts9700di[c].mask;
				//printf("Input %d value %X %X",c,*(gpio[i] + GPIO_IN),shm->divalue[(c/8)]);
				if (j) {
					shm->divalue[(c / 8)] = shm->divalue[(c / 8)] & ~(1 << c);
					//printf(" %X\n",shm->divalue[(c/8)]);
				} else {
					shm->divalue[(c / 8)] = shm->divalue[(c / 8)] | (1 << c);
				}
				//printf(" %X\n",shm->divalue[(c/8)]);
			}
		}

		// Outputs in Vesta are in blocks of 8
		//for (c = 0; c < shm->counts->do_count; c++) {
		for (c = 0; c < 12; c++) {
			if (ts9700do[c].handler == SERVER_ID) {
				i = ts9700do[c].gpioblock;
				if (shm->dovalue[(c / 8)] & (1 << (c % 8))) {
					*(gpio[i] + GPIO_OUT) = *(gpio[i] + GPIO_OUT) | ts9700do[c].mask;
				} else {
					*(gpio[i] + GPIO_OUT) = *(gpio[i] + GPIO_OUT) & (~ts9700do[c].mask);
				}
			}
		}

		for (i = 0; i < shm->counts->element_count; i++) {
			if (shm->elements[i].pvn == 'p' && shm->elements[i].io == 'i') {
				//printf("setting %d\n",i);
				setElementFromInput(i);
			}
		}

		// Set LEDs
		//*(gpio + GPIO_OUT) = *(gpio + GPIO_OUT) | USR0_LED;
		//*(gpio + GPIO_OUT) = *(gpio + GPIO_OUT) | USR1_LED;
		//*(gpio + GPIO_OUT) = *(gpio + GPIO_OUT) | USR2_LED;
		//*(gpio[1] + GPIO_OUT) = *(gpio[1] + GPIO_OUT) | USR3_LED;

		// Clear LEDs
		//*(gpio + GPIO_OUT) = *(gpio + GPIO_OUT) & (~USR0_LED);
		//*(gpio + GPIO_OUT) = *(gpio + GPIO_OUT) & (~USR1_LED);
		//*(gpio + GPIO_OUT) = *(gpio + GPIO_OUT) & (~USR2_LED);
		//*(gpio + GPIO_OUT) = *(gpio + GPIO_OUT) & (~USR3_LED);

		// Sleep for desired interval
		timedSleep(PROCESSNAME, shm->config->io_period, &start_usec);
	}
}

void i2cwrite(int file, char *buf, int size) {
	const char *buffer;
	if (write(file, buf, size) != size) {
		// ERROR HANDLING: i2c transaction failed
		printf("2: Failed to write to the i2c bus.\n");
		buffer = strerror(errno);
		printf(buffer);
		printf("\n\n");
	}
}

int i2cread(int file, char*buf, int size) {
	const char *buffer;
	if (read(file, buf, size) != size) {
		// ERROR HANDLING: i2c transaction failed
		printf("3: Failed to read from the i2c bus.\n");
		buffer = strerror(errno);
		printf(buffer);
		printf("\n\n");
		return (0);
	} else {
		return (size);
	}
}
