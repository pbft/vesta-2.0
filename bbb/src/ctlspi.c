/**
* Returns a shared script property
*
* @param {string} key the name of the property
* @return {string} the property value
*/
function getLibraryProperty(key) {
  return ScriptProperties.getProperty(key);
}

/**
* Returns SalesForce data
*
* @param {string} soql the SalesForce query to be executed
* @return {array} the parsed results
*/
function getSfData(soql) {
  var accessToken = ScriptProperties.getProperty("accessToken");
  var serverUrl = ScriptProperties.getProperty("serverUrl");
  var instanceUrl = ScriptProperties.getProperty("instanceUrl");

  if (!accessToken || !serverUrl || !instanceUrl) {
    processAuth2();
  }

  var queryUrl = instanceUrl + "/services/data/v21.0/query?q="+encodeURIComponent(soql);
  var options = {
    method : "GET",
    headers : { "Authorization" : "OAuth "+accessToken }
  };

  try {
    var response = UrlFetchApp.fetch(queryUrl, options);
  } catch(err) {
    processAuth2();
    var response = UrlFetchApp.fetch(queryUrl, options);
  }
  return Utilities.jsonParse(response.getContentText());
}

/**
* Inserts data into SalesForce
*
* @param {array} payload the SalesForce data package
* @param {string} table the SalesForce table to receive the data
* @return {array} the result
*/
function insertSfData(payload,table){

  var accessToken = ScriptProperties.getProperty("accessToken");
  var serverUrl = ScriptProperties.getProperty("serverUrl");
  var instanceUrl = ScriptProperties.getProperty("instanceUrl");

  if (!accessToken || !serverUrl || !instanceUrl) {
    processAuth2();
  }
  var queryUrl = instanceUrl + "/services/data/v25.0/sobjects/" + table + "/";
  var headers =
      {
        "Authorization" : "OAuth "+accessToken,
        "Content-type" : "application/json"
      };
  var options =
      {
        headers : headers,
        method : "POST",
        payload : payload
      };
  try {
    var response = UrlFetchApp.fetch(queryUrl, options);
  } catch(err) {
    processAuth2();
    var response = UrlFetchApp.fetch(queryUrl, options);
  }
  return response;
}

/**
* Update existing SalesForce record
*
* @param {array} payload the SalesForce data package
* @param {string} table the SalesForce table to receive the data
* @param {string} id the ID of the SalesForce record
* @return {array} the result
*/
function updateSfData(payload,table,id){

  var accessToken = ScriptProperties.getProperty("accessToken");
  var serverUrl = ScriptProperties.getProperty("serverUrl");
  var instanceUrl = ScriptProperties.getProperty("instanceUrl");

  if (!accessToken || !serverUrl || !instanceUrl) {
    processAuth2();
  }
  var queryUrl = instanceUrl + "/services/data/v28.0/sobjects/" + table + "/" + id + "?_HttpMethod=PATCH";
  var headers =
      {
        "Authorization" : "OAuth "+accessToken,
        "Content-type" : "application/json"
      };
  var options =
      {
        headers : headers,
        method : "POST",
        payload : payload
      };
  try {
    var response = UrlFetchApp.fetch(queryUrl, options);
  } catch(err) {
    processAuth2();
    var response = UrlFetchApp.fetch(queryUrl, options);
  }
  return response;
}
/**
* Formats a number as currency
*
* @param {number} v the value to be formatted
* @return {string} the result
*/
function dollarize(v){
  v = v.toString().replace(/\$|\,/g, '');
  if (isNaN(v)) v = "0";
  sign = (v == (v = Math.abs(v)));
  v = Math.floor(v * 100 + 0.50000000001);
  cents = v % 100;
  v = Math.floor(v / 100).toString();
  if (cents < 10) cents = "0" + cents;
  for (var i = 0; i < Math.floor((v.length - (1 + i)) / 3); i++)
  v = v.substring(0, v.length - (4 * i + 3)) + ',' + v.substring(v.length - (4 * i + 3));
  return (((sign) ? '' : '-') + '$' + v + '.' + cents);
}

/**
* Cleans a string of illegal characters
*
* @param {string} dirty the value to be formatted
* @return {string} the filename-safe string
*/
function sanitize(dirty){
  dirty = dirty.replace("&","and");
  dirty = dirty.replace(/\'/g, ' ');
  dirty = dirty.replace(/\-/g, ' ');
  dirty = dirty.replace(/\./g, ' ');
  dirty = dirty.replace(/~/g, ' ');
  dirty = dirty.replace(/\+/g, ' ');
  dirty = dirty.replace(/_/g, ' ');
  return dirty.trim();
};


/**
* Obtains SalesForce authorization tokens
*
*/
function processAuth(){

  // Read OAuth consumer key / secret of this client app from script properties,
  // which can be issued from Salesforce's remote access setting in advance.

  // Keys for VMEC account
  ScriptProperties.setProperty('sfConsumerKey','3MVG9y6x0357HledNANgtolocZ_2GKgZOpHR3WxQyRlma46hDU29ZRpmbVE0ae19IIa.jh71g3K6pdoCinD9S');
  ScriptProperties.setProperty('sfConsumerSecret','1483736225982327368');

  var sfConsumerKey = ScriptProperties.getProperty("sfConsumerKey");
  var sfConsumerSecret = ScriptProperties.getProperty("sfConsumerSecret");

  if (!sfConsumerKey || !sfConsumerSecret) {
    Browser.msgBox("Register Salesforce OAuth Consumer Key and Secret in Script Properties");
    return;
  }

  // Register new OAuth service, named "salesforce"
  // For OAuth endpoint information, see help doc in Salesforce.
  // https://na7.salesforce.com/help/doc/en/remoteaccess_oauth_1_flows.htm

  var oauth = UrlFetchApp.addOAuthService("salesforce");
  oauth.setAccessTokenUrl("https://login.salesforce.com/_nc_external/system/security/oauth/AccessTokenHandler");
  oauth.setRequestTokenUrl("https://login.salesforce.com/_nc_external/system/security/oauth/RequestTokenHandler");
  oauth.setAuthorizationUrl("https://login.salesforce.com/setup/secur/RemoteAccessAuthorizationPage.apexp?oauth_consumer_key="+encodeURIComponent(sfConsumerKey));
  oauth.setConsumerKey(sfConsumerKey);
  oauth.setConsumerSecret(sfConsumerSecret);

  // Convert OAuth1 access token to Salesforce sessionId (mostly equivalent to OAuth2 access token)
  var sessionLoginUrl = "https://login.salesforce.com/services/OAuth/u/28.0";
  var options = {
    method : "POST",
    oAuthServiceName : "salesforce",
    oAuthUseToken : "always"
  };
  var result = UrlFetchApp.fetch(sessionLoginUrl, options);
  var txt = result.getContentText();
  var accessToken = txt.match(/<sessionId>([^<]+)/)[1];
  var serverUrl = txt.match(/<serverUrl>([^<]+)/)[1];
  var instanceUrl = serverUrl.match(/^https?:\/\/[^\/]+/)[0];
  Logger.log(accessToken);
  ScriptProperties.setProperty('accessToken',accessToken);
  ScriptProperties.setProperty('serverUrl',serverUrl);
  ScriptProperties.setProperty('instanceUrl',instanceUrl);

};

function getCallbackURL(callbackFunction){
   var url = "https://script.google.com/a/macros/vmec.org/d/MRNwUtnK-znSwsyx6ys5IYhZuvFJJ6UN9/edit?uiv=2&tz=America/New_York&docTitle=VMEC+Script+Library+1.0&csid=tn0HRAXgGHha2VKkJg6zI8w.17109810494972989044.8159460832599071448&mid=ACjPJvFGhPhXVx7t-EiIyYo-U9FZKOtI2ZYJJkkTqAFpnzOMEGwNErakfZbDtzBIoRwWinKDdFAYVOI3NNqaGYs5MSmMv0cNrCLpS8otY_Mo3D-lQSo&hl=en_US";
   //var url = ScriptApp.getService().getUrl();      // Ends in /exec (for a web app)
   url = url.slice(0, -4) + 'usercallback?state='; // Change /exec to /usercallback
   var stateToken = ScriptApp.newStateToken()
       .withMethod(callbackFunction)
       .withTimeout(120)
       .createToken();
   return url + stateToken;
 }

var tokenPropertyName = 'accessToken';

function processAuth2(){

  // Read OAuth consumer key / secret of this client app from script properties,
  // which can be issued from Salesforce's remote access setting in advance.

  // Keys for VMEC account
  //ScriptProperties.setProperty('sfConsumerKey','3MVG9y6x0357HledNANgtolocZ_2GKgZOpHR3WxQyRlma46hDU29ZRpmbVE0ae19IIa.jh71g3K6pdoCinD9S');
  //ScriptProperties.setProperty('sfConsumerSecret','1483736225982327368');
  Logger.log("A");
  var sfConsumerKey = ScriptProperties.getProperty("sfConsumerKey");
  var sfConsumerSecret = ScriptProperties.getProperty("sfConsumerSecret");
  var sfAccessToken = ScriptProperties.getProperty("accessToken");

  if (!sfConsumerKey || !sfConsumerSecret) {
    Browser.msgBox("Register Salesforce OAuth Consumer Key and Secret in Script Properties");
    return;
  }

  // Register new OAuth service, named "salesforce"
  // For OAuth endpoint information, see help doc in Salesforce.
  // https://na7.salesforce.com/help/doc/en/remoteaccess_oauth_1_flows.htm
  /*
  var oauth = UrlFetchApp.addOAuthService("salesforce");
  oauth.setAccessTokenUrl("https://login.salesforce.com/_nc_external/system/security/oauth/AccessTokenHandler");
  oauth.setRequestTokenUrl("https://login.salesforce.com/_nc_external/system/security/oauth/RequestTokenHandler");
  oauth.setAuthorizationUrl("https://login.salesforce.com/setup/secur/RemoteAccessAuthorizationPage.apexp?oauth_consumer_key="+encodeURIComponent(sfConsumerKey));
  oauth.setConsumerKey(sfConsumerKey);
  oauth.setConsumerSecret(sfConsumerSecret);
  */
  /*
  var oauth = OAuth2.createService('salesforce');
  oauth.setTokenUrl("https://login.salesforce.com/_nc_external/system/security/oauth/AccessTokenHandler");
  oauth.setRequestTokenUrl("https://login.salesforce.com/_nc_external/system/security/oauth/RequestTokenHandler");
  oauth.setAuthorizationUrl("https://login.salesforce.com/setup/secur/RemoteAccessAuthorizationPage.apexp?oauth_consumer_key="+encodeURIComponent(sfConsumerKey));
  oauth.setConsumerKey(sfConsumerKey);
  oauth.setConsumerSecret(sfConsumerSecret);
  */
  // Convert OAuth1 access token to Salesforce sessionId (mostly equivalent to OAuth2 access token)

  /*
  Logger.log(sfConsumerKey);
  Logger.log(sfAccessToken);
  sfAccessToken = '00DE0000000bdRW!AQUAQBrKG7S3_QCgfVQ2Zwi4WRKNWUPViTu1QqdiJPisZI6tH8tQ9HIpd9gWqmTD1HwguczXxcK18qg1fAxIkkPW7QVsp1.R';
  var sessionLoginUrl = "https://login.salesforce.com/services/OAuth/u/28.0";
  var payload = {
    oAuthServiceName : 'salesforce',
    client_id : encodeURIComponent(sfConsumerKey),
    oAuthUseToken : 'always'
  };
  var options = {
    method : 'POST',
    muteHttpExceptions : true,
    payload :payload
  };
  var result = UrlFetchApp.fetch(sessionLoginUrl, options);
  Logger.log(result);
  var txt = result.getContentText();
  var accessToken = txt.match(/<sessionId>([^<]+)/)[1];
  var serverUrl = txt.match(/<serverUrl>([^<]+)/)[1];
  var instanceUrl = serverUrl.match(/^https?:\/\/[^\/]+/)[0];
  Logger.log(accessToken);
  ScriptProperties.setProperty('accessToken',accessToken);
  ScriptProperties.setProperty('serverUrl',serverUrl);
  ScriptProperties.setProperty('instanceUrl',instanceUrl);
  */

  // Ask user to authorize (from https://www.salesforce.com/us/developer/docs/api_rest/)
  //var url = "https://login.salesforce.com/services/oauth2/authorize";
  //var url = "https://login.salesforce.com/_nc_external/system/security/oauth/RequestTokenHandler";
  //var riurl = "https://spreadsheets.google.com/macros/";
  var riurl = "https://script.google.com/macros/d/MRNwUtnK-znSwsyx6ys5IYhZuvFJJ6UN9/authCallback";
  var url = "https://login.salesforce.com/services/oauth2/authorize?response_type=token&scope=full&client_id=" + encodeURIComponent(sfConsumerKey) + "&redirect_uri=" + encodeURIComponent(riurl);
  Logger.log(url);
 try
  {
    var result = UrlFetchApp.fetch(url);
      }
  catch(err)
  {
    Logger.log("code: " + result.getResponseCode());
    Logger.log("text: " + result.getContentText());

  }

  Logger.log("B");
/*

  var payload = {
    'response_type':'code',
    'client_id':sfConsumerKey,
    'scope':'full',
    'redirect_uri' : riurl
  };
   var options = {
    'method':'get',
    'muteHttpExceptions' : true,
    'payload':payload
  };

 try
  {
    var result = UrlFetchApp.fetch(url, options);
      }
  catch(err)
  {
    Logger.log("code: " + result.getResponseCode());
    Logger.log("text: " + result.getContentText());

  }

  Logger.log("C");
  var txt = result.getContentText();
  Logger.log("D");
  Logger.log(txt);

  // Convert OAuth1 access token to Salesforce sessionId (mostly equivalent to OAuth2 access token)
  var url = 'https://login.salesforce.com/services/oauth2/token';
  var payload = {
    //'grant_type':'authorization_code',
    'grant_type':'refresh_token',
    'refresh_token':sfAccessToken,
    'client_id':sfConsumerKey,
    'client_secret':sfConsumerSecret
  };

  var options = {
    'method':'post',
    'payload':payload
  };

  var result = UrlFetchApp.fetch(url, options);
  var txt = result.getContentText();
  var accessToken = txt.match(/<sessionId>([^<]+)/)[1];
  var serverUrl = txt.match(/<serverUrl>([^<]+)/)[1];
  var instanceUrl = serverUrl.match(/^https?:\/\/[^\/]+/)[0];
  ScriptProperties.setProperty('accessToken',accessToken);
  ScriptProperties.setProperty('serverUrl',serverUrl);
  ScriptProperties.setProperty('instanceUrl',instanceUrl);
*/
  /*
  var sessionLoginUrl = "https://login.salesforce.com/services/OAuth/u/28.0";
  var options = {
    method : "POST",
    oAuthServiceName : "salesforce",
    oAuthUseToken : "always"
  };
  var result = UrlFetchApp.fetch(sessionLoginUrl, options);
  var txt = result.getContentText();
  var accessToken = txt.match(/<sessionId>([^<]+)/)[1];
  var serverUrl = txt.match(/<serverUrl>([^<]+)/)[1];
  var instanceUrl = serverUrl.match(/^https?:\/\/[^\/]+/)[0];
  ScriptProperties.setProperty('accessToken',accessToken);
  ScriptProperties.setProperty('serverUrl',serverUrl);
  ScriptProperties.setProperty('instanceUrl',instanceUrl);
  */
};

//  Client ID   156138427548-21ja182hk61b79a2r620b797a06k5ic8.apps.googleusercontent.com
//  Email address 156138427548-21ja182hk61b79a2r620b797a06k5ic8@developer.gserviceaccount.com
//  Client secret 9PD1OWKtGoktmeU420HLgsHY
//  Redirect URIs	https://script.google.com/macros/d/MRNwUtnK-znSwsyx6ys5IYhZuvFJJ6UN9/usercallback
//  Access Token  00DE0000000bdRW!AQUAQBrKG7S3_QCgfVQ2Zwi4WRKNWUPViTu1QqdiJPisZI6tH8tQ9HIpd9gWqmTD1HwguczXxcK18qg1fAxIkkPW7QVsp1.R

function getDriveService() {
  // Create a new service with the given name. The name will be used when
  // persisting the authorized token, so ensure it is unique within the
  // scope of the property store.
  //return OAuth2.createService('drive')
  Logger.log("oas1");
  var oas = OAuth2.createService('sheets')

      // Set the endpoint URLs, which are the same for all Google services.
      .setAuthorizationBaseUrl('https://accounts.google.com/o/oauth2/auth')
      .setTokenUrl('https://accounts.google.com/o/oauth2/token')

      // Set the client ID and secret, from the Google Developers Console.
      .setClientId('156138427548-21ja182hk61b79a2r620b797a06k5ic8.apps.googleusercontent.com')
      .setClientSecret('9PD1OWKtGoktmeU420HLgsHY')

      // Set the name of the callback function in the script referenced
      // above that should be invoked to complete the OAuth flow.
      .setCallbackFunction('authCallback')

      // Set the property store where authorized tokens should be persisted.
      .setPropertyStore(PropertiesService.getUserProperties())

      // Set the scopes to request (space-separated for Google services).
      //.setScope('https://www.googleapis.com/auth/drive')
      .setScope('https://www.googleapis.com/auth/speadsheets https://www.googleapis.com/auth/documents https://www.googleapis.com/auth/drive https://www.googleapis.com/auth/script.external_request https://www.googleapis.com/auth/script.scriptapp https://www.googleapis.com/auth/script.storage https://www.googleapis.com/auth/spreadsheets https://www.googleapis.com/auth/userinfo.email')

      // Below are Google-specific OAuth2 parameters.

      // Sets the login hint, which will prevent the account chooser screen
      // from being shown to users logged in with multiple accounts.
      .setParam('login_hint', Session.getActiveUser().getEmail())

      // Requests offline access.
      //.setParam('access_type', 'offline')
      .setParam('access_type', 'online')

      // Forces the approval prompt every time. This is useful for testing,
      // but not desirable in a production application.
      .setParam('approval_prompt', 'force');
  Logger.log("oas2");

      return oas;
}

function showSidebar() {
  var driveService = getDriveService();
  if (!driveService.hasAccess()) {
    var authorizationUrl = driveService.getAuthorizationUrl();
    var template = HtmlService.createTemplate(
        '<a href="<?= authorizationUrl ?>" target="_blank">Authorize</a>. ' +
        'Reopen the sidebar when the authorization is complete.');
    template.authorizationUrl = authorizationUrl;
    var page = template.evaluate();
    DocumentApp.getUi().showSidebar(page);
  } else {
    Logger.log('No Access');
  }
}

function authCallback(request) {
    Logger.log(request);
  var tokenResponse = JSON.parse(response);
  //store the token for later retrival
  UserProperties.setProperty(tokenPropertyName, tokenResponse.access_token);
}
function usercallback(request) {
    Logger.log(request);
  var tokenResponse = JSON.parse(response);
  //store the token for later retrival
  UserProperties.setProperty(tokenPropertyName, tokenResponse.access_token);
}

function makeRequest() {
  var driveService = getDriveService();
    Logger.log('makerequest1');

  /*
  var response = UrlFetchApp.fetch('https://www.googleapis.com/drive/v2/files?maxResults=10', {
    headers: {
      Authorization: 'Bearer ' + driveService.getAccessToken()
    }
  });
  */
  Logger.log('makerequest');
}
/* getQuoteFolder: 'sanitize' company name and project name (remove illegal characters).
Navigate and/or create directory structure.
The folder for 'My Project' in 'My Company' with a project number of 34567
would be 'Clients/M/My Company/34567 - My Project'
Set cells for libV.sanitized names, directories, and folder ID values.
*/

function getQuoteFolder(cname,qname,qnumber){

  var qname = sanitize(qname);
  var cname = sanitize(cname);
  var fname = qnumber + " - " + qname;

  // Get base directory
  var root = DocsList.getFolder("Clients");

  // Try alpha directory
  var path = "Clients/" + cname.substring(0,1).toUpperCase();
  try {
    var alpha = DocsList.getFolder(path);
  } catch(err) {
    alpha = root.createFolder(cname.substring(0,1).toUpperCase());
    //Browser.msgBox("Created Client Alpha Folder: " + path);
  }

  // Add company to path, create directory if needed
  path += "/" + cname;
  try {
    var comp = DocsList.getFolder(path);
  } catch(err) {
    comp = alpha.createFolder(cname);
    //Browser.msgBox("Created Client Folder: " + path);
  }

  // Add quote to path, create directory if needed
  path += "/" + fname;
  try {
    var quote = DocsList.getFolder(path);
  } catch(err) {
    quote = comp.createFolder(fname);
    //Browser.msgBox("Created Quote Folder: " + path);
  }
  return(quote);

};

// New code for oauth2

function doGet(e) {
  var HTMLToOutput;
  Logger.log("doGet!");

  if(e.parameters.code){//if we get "code" as a parameter in, then this is a callback. we can make this more explicit
    getAndStoreAccessToken(e.parameters.code);
    HTMLToOutput = '<html><h1>Finished with oAuth</h1></html>';
  }
  else if(isTokenValid()){//if we already have a valid token, go off and start working with data
    HTMLToOutput = '<html><h1>Already have token</h1></html>';
  }
  else {//we are starting from scratch or resetting
    return HtmlService.createHtmlOutput("<html><h1>Lets start with oAuth</h1><a href='"+getURLForAuthorization()+"'>click here to start</a></html>");
  }

  HTMLToOutput += getData();
  return HtmlService.createHtmlOutput(HTMLToOutput);
}

//do meaningful google access here
function getData(){
  var getDataURL = 'https://www.googleapis.com/oauth2/v1/userinfo';
  var dataResponse = UrlFetchApp.fetch(getDataURL,getUrlFetchOptions()).getContentText();
  return dataResponse;
}

////oAuth related code
  //oauth.setTokenUrl("https://login.salesforce.com/_nc_external/system/security/oauth/AccessTokenHandler");
  //oauth.setRequestTokenUrl("https://login.salesforce.com/_nc_external/system/security/oauth/RequestTokenHandler");
  //oauth.setAuthorizationUrl("https://login.salesforce.com/setup/secur/RemoteAccessAuthorizationPage.apexp?oauth_consumer_key="+encodeURIComponent(sfConsumerKey));
  //oauth.setConsumerKey(sfConsumerKey);
  //oauth.setConsumerSecret(sfConsumerSecret);


//hardcoded here for easily tweaking this. should move this to ScriptProperties or better parameterize them

var sfConsumerKey = ScriptProperties.getProperty("sfConsumerKey");
var sfConsumerSecret = ScriptProperties.getProperty("sfConsumerSecret");

var AUTHORIZE_URL = "https://login.salesforce.com/setup/secur/RemoteAccessAuthorizationPage.apexp?oauth_consumer_key="+encodeURIComponent(sfConsumerKey); //step 1. we can actually start directly here if that is necessary
var TOKEN_URL = "https://login.salesforce.com/_nc_external/system/security/oauth/AccessTokenHandler"; //step 2. after we get the callback, go get token

var CLIENT_ID = sfConsumerKey;
var CLIENT_SECRET = sfConsumerSecret;
//var REDIRECT_URL= ScriptApp.getService().getUrl();
var REDIRECT_URL= "https://spreadsheets.google.com/macros/";


//this is the user propety where we'll store the token, make sure this is unique across all user properties across all scripts
var tokenPropertyName = 'accessToken';
var baseURLPropertyName = 'instanceUrl';

/*
  var result = UrlFetchApp.fetch(sessionLoginUrl, options);
  var txt = result.getContentText();
  var accessToken = txt.match(/<sessionId>([^<]+)/)[1];
  var serverUrl = txt.match(/<serverUrl>([^<]+)/)[1];
  var instanceUrl = serverUrl.match(/^https?:\/\/[^\/]+/)[0];
  ScriptProperties.setProperty('accessToken',accessToken);
  ScriptProperties.setProperty('serverUrl',serverUrl);
  ScriptProperties.setProperty('instanceUrl',instanceUrl);
*/

//this is the URL where they'll authorize with salesforce.com
//may need to add a "scope" param here. like &scope=full for salesforce
//example scope for google - https://www.googleapis.com/plus/v1/activities
function getURLForAuthorization(){
  Logger.log(REDIRECT_URL);
  return AUTHORIZE_URL + '?response_type=code&client_id='+CLIENT_ID+'&redirect_uri='+REDIRECT_URL +
    '&scope=https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile&state=/profile';
}

//Google requires POST, salesforce and slc worked with GET
function getAndStoreAccessToken(code){
  var parameters = {
     method : 'post',
     //payload : 'client_id='+CLIENT_ID+'&client_secret='+CLIENT_SECRET+'&grant_type=authorization_code&redirect_uri='+REDIRECT_URL+'&code=' + code
     payload : 'client_id='+CLIENT_ID+'&client_secret='+CLIENT_SECRET+'&grant_type=authorization_code&redirect_uri='+REDIRECT_URL+'&code=get'
   };

  var response = UrlFetchApp.fetch(TOKEN_URL,parameters).getContentText();
  var tokenResponse = JSON.parse(response);

  //store the token for later retrival
  UserProperties.setProperty(tokenPropertyName, tokenResponse.access_token);
}

//this may need to get tweaked per the API you are working with.
//for instance, SLC had content type of application/vnd.slc+json. SLC also allows lower case 'bearer'
function getUrlFetchOptions() {
  var token = UserProperties.getProperty(tokenPropertyName);
  return {
            "contentType" : "application/json",
            "headers" : {
                         "Authorization" : "Bearer " + token,
                         "Accept" : "application/json"
                        }
         };
}

function isTokenValid() {
  var token = UserProperties.getProperty(tokenPropertyName);
  if(!token){ //if its empty or undefined
    return false;
  }
  return true; //naive check

  //if your API has a more fancy token checking mechanism, use it. for now we just check to see if there is a token.
  /*
  var responseString;
  try{
     responseString = UrlFetchApp.fetch(BASE_URI+'/api/rest/system/session/check',getUrlFetchOptions(token)).getContentText();
  }catch(e){ //presumably an HTTP 401 will go here
    return false;
  }
  if(responseString){
    var responseObject = JSON.parse(responseString);
    return responseObject.authenticated;
  }
  return false;*/
}
