/*****************************************************************************/
//    This file is part of the NoFossil Control System Software (NFCSS)
//    Copyright 2007, 2008, 2009 Bill Kuhns
//
//    NFCSS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    any later version.
//    NFCSS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//    You should have received a copy of the GNU General Public License
//    along with NFCSS.  If not, see <http://www.gnu.org/licenses/>.
/*****************************************************************************/

// Implement modified PID algorithm for closed-loop control
// BUGS:
//  - Need to read lower limit and purge variables from config file
#include <stdio.h>
#include <fcntl.h>
#include <math.h>
#include <sys/mman.h>
#include <sys/types.h>
#include "vesta.h"
#include <stdlib.h>
#include <errno.h>
#include <wiringPi.h>

#define PROCESSNAME  FUNCTION_NAME(ctlgpio)
#define BUTTON_PIN 0


// the event counter
volatile int eventCounter = 0;

// -------------------------------------------------------------------------
// myInterrupt:  called every time an event occurs
void myInterrupt(void) {
   eventCounter++;
}

main() {

  unsigned long long start_usec;
  unsigned long long now_usec;
  int i;

  // Get shared memory links
  get_shm(SERVER_ID);


  start_usec = getNow();
  if (wiringPiSetup () < 0) {
      fprintf (stderr, "Unable to setup wiringPi: %s\n", strerror (errno));
      return 1;
  }
  // set Pin 17/0 generate an interrupt on high-to-low transitions
  // and attach myInterrupt() to the interrupt
  if ( wiringPiISR (BUTTON_PIN, INT_EDGE_FALLING, &myInterrupt) < 0 ) {
      fprintf (stderr, "Unable to setup ISR: %s\n", strerror (errno));
      return 1;
  }

  // Do forever
  while (1) {
    if (reloadRequired(SERVER_ID)) {
      // Claim our elements
      // Clear our bit in the reload flag
      clearReloadFlag(SERVER_ID);
    }

    now_usec = getNow();

    printf( "%d\n", eventCounter );
    //eventCounter = 0;

    // Sleep for desired interval
    timedSleep(PROCESSNAME, shm->config->io_period, &start_usec);
  }
}
