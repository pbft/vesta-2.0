
/*****************************************************************************/
//    This file is part of the NoFossil Control System Software (NFCSS)
//    Copyright 2007, 2008, 2009 Bill Kuhns
//
//    NFCSS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    any later version.

//    NFCSS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with NFCSS.  If not, see <http://www.gnu.org/licenses/>.
/*****************************************************************************/

// Read binary logfile and output csv file with correct mime type
// A logfile is all the data from a single day
// Data in the logfile is record headers and binary images from shared memory.
// At present there are two types of data: Shared Memory image and raw data
// Each type of data record is prefaced by a header record which contains a timestamp and
// the size and type of the following data: as follows:

// Header: data type 's', size 50880 (or whatever), time xxx-xx-xx 00:00
// Data: Shared memory image
// Header: data type 'd', size 768 (or whatever), time xxx-xx-xx xx:xx
// Data: element values
// Header: data type 'd', size 768 (or whatever), time xxx-xx-xx xx:xx
// Data: element values
// .....

// At present, the first and only the first record contains the shared memory image.
// That means that if elements are added or changed during the day, their names will not appear in the logfile.
// The data will be there under the default element name.
// This could be addressed by writing a new header when changes are made.

#include <fcntl.h>

#include <stdio.h>
#include <sys/types.h>
#include "vesta.h"

void print_header(int head, int all, int i, byte_t *e){

  int j,c;
  // See if there's an element assigned to this I/O
  for (j=0;j<shm->counts->element_count;j++){
      if(head){
        printf( "\t%s",shm->elements[j].name);
      }
  }
}

main(){
  int i,j,c,nth,head,nohead,tz,e,all;
  char units;
  FILE *fp;
  byte_t elemental[500];
  char filename[40];
  char charbuff[40];
  char *myquery, *p[8], *v[8];
  float fval;

  struct timeval now;
  long start_sec;
  long now_usec;

  time_t curtime;
  time_t disptime;
  time_t start_seconds;
  time_t end_seconds;
  time_t secs, local_secs, gmt_secs;
  struct tm *logtime;
  struct tm *stime;
  struct tm *etime;
  int gmtoffset;

  shm_index_struct localShmIndex;
  int spacer[100];
  shm_index_struct *shm = &localShmIndex;
  count_struct *shmaddr = NULL;

  logheaderstruct logheader;
  float *evals;

  printf("Content-type: application/vnd.ms-excel ");
  printf("Content-disposition: attachment; filename=mydata.csv\n\n");

  myquery = (char*)getenv("QUERY_STRING");

  /* break apart parameter string on '&' and '=' characters */

  j = 0;

  // Weirdness: sometimes myquery is NULL, and you don't want to do strlen on it.
  // Other times, it's a zero length string. Either way, don't parse it.

  if (myquery != NULL){
    if (strlen(myquery) > 0){
      while(myquery=(char*)split(myquery,&p[j],&v[j])){
        j++;
      }
      j++;
      cleanup(j,&v[0]);
    }
  }

  // This program requires one parameter: the filename of the logfile.
  // There are several others that can be optionally specified. Default behaviors are defined here.

  // start and end are epoch times. They can be used to limit which datalog records are output.
  // Default is to output all records in the logfile.
  start_seconds = 0;
  end_seconds = time(NULL);

  // 'nohead' is true if we want data with no header. False by default - send header line
  // 'head' is true if we want only the header. False by default - send data too
  nohead = 0;
  head = 0;
  // By default, timezone is GMT. User can specify offset in hours.
  tz = 0;
  // By default, units are read from log data configuration. Can be overridden with query string data
  units = 'X';
  // By default, only print elements (not unassigned I/O)
  all = 0;

  if (j > 0){
    for(i=0;i<j;i++){
      //printf("p: %s v: %s<br>\n",p[i],v[i]);

      if (!strcmp(p[i], "file")){
        sscanf(v[i],"%s",&filename);
      }
      if (!strcmp(p[i], "start")){
        sscanf(v[i],"%ld",&start_sec);
        start_seconds = (time_t)start_sec;
      }
      if (!strcmp(p[i], "end")){
        sscanf(v[i],"%ld",&start_sec);
        end_seconds = (time_t)start_sec;
      }
      if (!strcmp(p[i], "nth")){
        sscanf(v[i],"%d",&nth);
      }
      if (!strcmp(p[i], "tz")){
        sscanf(v[i],"%d",&tz);
      }
      if (!strcmp(p[i], "units")){
        sscanf(v[i],"%c",&units);
      }
      if (!strcmp(p[i], "nohead")){
        nohead = 1;
      }
      if (!strcmp(p[i], "all")){
        all = 1;
      }
    }
  }

  // Logfile times are GMT (but broken into days at localtime boundaries).
  // Calculate GMT offset

  //stime = localtime(&start_seconds);
  //local_secs = mktime(stime);
  //stime = gmtime(&start_seconds);
  //gmt_secs = mktime(stime);
  //gmtoffset = local_secs - gmt_secs;
  //printf("GMT offset = %d\n",gmtoffset);
  stime = localtime(&start_seconds);
  etime = localtime(&end_seconds);

  fp = fopen(filename,"r");

  // read header. Future version should just read size value and error check.
  fread(&logheader,sizeof(logheader),1,fp);
  //printf("Read %d header bytes\n",sizeof(logheader));
  //printf("shm size = %d\n",logheader.nextrecsize);
  // Now that we know shm size, read it in
  shmaddr = malloc(logheader.nextrecsize);

  fread(shmaddr,logheader.nextrecsize,1,fp);
  //printf("Read %d data bytes\n",logheader.nextrecsize);
  //printf("Counts = %d %d\n",shmaddr->rule_count,shmaddr->element_count);

  if(!feof(fp)){
    shm->counts = shmaddr;
    shm->config = shm->counts + 1;
    shm->evals = shm->config + 1;
    shm->elements = shm->evals + shm->counts->element_count;
// If units weren't specified, get from log data config record
    if(units == 'X'){
//      units = logheader.shm.config[0].centigrade ? 'C' : 'F';
    }
    if(!nohead){
      // Print column headings
      printf("Time");
    }
    j=0;


    for(i=0;i<shm->counts->element_count;i++){
         if(!nohead){
          printf ("\t%s",shm->elements[i].name);
        }
    }


    if(!nohead){
      printf ("\n");
    }
  }else{
    printf("No file %s\n",filename);
  }
  fflush(stdout);

  evals = malloc(shm->counts->element_count * sizeof(float));

  while(!feof(fp)){
    fread(&logheader,sizeof(logheader),1,fp);
    //printf("Read %d header bytes\n",sizeof(logheader));
    //printf("data size = %d\n",logheader.nextrecsize);
    if(!feof(fp)){
      fread(evals,logheader.nextrecsize,1,fp);
      //printf("Read %d data bytes\n",logheader.nextrecsize);
      //disptime = logheader.curtime + tz * 3600;
      disptime = logheader.curtime;
      logtime=(struct tm *)localtime(&disptime);
      if(logheader.curtime > (start_seconds) && logheader.curtime < (end_seconds)){
        strftime(&charbuff,20,"%m/%d/%Y %H:%M:%S",logtime);

        printf ("%s",charbuff);

        for(i=0;i<shm->counts->element_count;i++){
          printf ("\t%0.2f",evals[i]);
        }

        printf ("\n");
      }
    }
  }
  fclose(fp);
  free(shmaddr);
  free(evals);
}


