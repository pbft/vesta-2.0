/*****************************************************************************/
//    This file is part of the Vesta Control System Software Suite
//    Copyright Bill Kuhns and Vermont Energy Control Systems LLC.
//
//    This is free software: you can redistribute it and/or modify
//    it under the terms of the Lesser GNU General Public License (LGPL)
//    as published by the Free Software Foundation, either version 3 of the
//    License, or the Free Software Foundation, either version 3 of the License,
//    or any later version.
//
//    This software is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    Lesser GNU General Public License for more details.
//
//    You should have received a copy of the Lesser GNU General Public License
//    along with this software.  If not, see <http://www.gnu.org/licenses/>.
/*****************************************************************************/

#include <stdio.h>
#include <sys/types.h>
#include "vesta.h"

void print_element(int i, int owner, char *val, char *name, char *color, int edit, int edit2){

  // Element owner
  if (edit || edit2){
    printf("<form action=vestaWebEdit method=get>\n");
  }
  printf("<tr>");
  printf("<td class=smallpad>%d</td>\n",owner);

  // Element name
  printf("<td class=smallpad>");
  if (edit){
    printf("<input class=minimal type=text name=name size=20 value=\"%s\" onchange=\"submit();\"></td>",name);
  }else{
    printf("%s</td>",name);
  }

  printf("<td class=smallpad align=right bgcolor=%s>",color);
  if (edit2){
    printf("<input name=value type=text size=5 value=%s onchange=\"submit();\"></td>\n",val);
  }else{
    printf("%s</td>\n",val);
  }

  if (edit || edit2){
    printf("<td><input type=hidden name=editmode value=e>\n");
    printf("<input type=hidden name=view value=element>\n");
    printf("<input type=hidden name=id value=%d>\n",i);
    printf("<input type=hidden name=action value=myelement>\n");
    // Need this to avoid inferred click on up arrow when user hits return in text field
    printf("<input type=image name=actionx value=myelement src=/images/pxclear.gif>\n");
    printf("</td>\n");

    // Don't allow first element of group to move up
    if (isFirst(i)){
      printf("<td align=right>&nbsp;</td>\n");
    }else{
      printf("<td class=nopad>\n");
      printf("<a href=/cgi-bin/private/vestaWebEdit?editmode=e&view=element&id=%d&action=MoveUp>",i);
      printf("<img hspace=2 border=0 src=/images/up-arrow.png></img></a></td>\n");

    }
    // Don't allow last element to move down
    if (isLast(i)){
      printf("<td>&nbsp;</td>\n");
    }else{
      //printf("<td class=nopad><input type=image name=action value=MoveDn src=/images/down-arrow.png></td>\n");
      printf("<td class=nopad>\n");
      printf("<a href=/cgi-bin/private/vestaWebEdit?editmode=e&view=element&id=%d&action=MoveDn>",i);
      printf("<img hspace=2 border=0 src=/images/down-arrow.png></img></a></td>\n");
    }
    //printf("<td class=nopad><input type=image name=action value=delbtn src=/images/delete-btn.png></td>\n");
    //if(shm->elements[i].vdev.vtype != 'x'){
    //	printf("<td>&nbsp;</td>\n");
    //}else{
    	printf("<td class=nopad>\n");
    	printf("<a href=/cgi-bin/private/vestaWebEdit?editmode=e&view=element&id=%d&action=delbtn>",i);
    	printf("<img hspace=2 border=0 src=/images/delete-btn.png></img></a></td>\n");
    //}
  }
  printf("</tr>\n");
  if (edit || edit2){
    printf("</form>\n");
  }

}

// Is this element the first in sequence of it's type? If so, the element that points to it will be different
int isFirst(int eid){
  int i;
  for (i=1;i < shm->counts->element_count;i++) {
    if(shm->elements[i].next == eid){
      // This one points to us. Is it same as us?
      if(shm->elements[i].pvn == shm->elements[eid].pvn && shm->elements[i].ptype == shm->elements[eid].ptype){
        return(0);
      }else{
        return(1);
      }
    }
  }
  return(0);
}

// Is this element the last in sequence of it's type? If so, the element that it points to will be different
int isLast(int eid){
  int i;

    if(shm->elements[eid].next == -1){
      return(1);
    }
    i = shm->elements[eid].next;

    if(shm->elements[i].pvn == shm->elements[eid].pvn && shm->elements[i].ptype == shm->elements[eid].ptype){
      return(0);
    }else{
      return(1);
    }
  return(0);
}

void print_drule(int i, int rw){
  
  printf("\n\n<tr>");
  if(rw){
    print_edit_icons(i,'d');
  }
  printf("<td><img valign=center src=/images/%sdot.png>\n",shm->rules[i].drule.active ? "red" : "wht");
  printf("Set <b>%s</b> if <b>%s</b> is",shm->elements[shm->rules[i].drule.target].name,shm->elements[shm->rules[i].drule.e1].name);
  //printf(" i=%d e1=%d ",i,shm->rules[i].drule.e1);
  if(shm->rules[i].drule.diff.rval.value != 0){
    if(shm->rules[i].drule.diff.eflag){
      printf(" at least  <b>%s</b>",shm->elements[shm->rules[i].drule.diff.rval.eid].name);
    }else{
      printf(" at least  <b>%0.1f</b>",shm->rules[i].drule.diff.rval.value);
    }
  }
  //printf(" is at least <b>%s</b>",shm->elements[shm->rules[i].drule.diff].name);

  switch (shm->rules[i].drule.gle) {
    case 'g': printf(" greater than ");
      break;
    case 'l': printf(" less than ");
      break;
    case 'e': printf(" equal to ");
      break;
  }

  if(shm->rules[i].drule.e2.eflag){
    printf("<b>%s</b>",shm->elements[shm->rules[i].drule.e2.rval.eid].name);
  }else{
    printf("<b>%0.1f</b>",shm->rules[i].drule.e2.rval.value);
  }


  //printf("<b>%s</b>",shm->elements[shm->rules[i].drule.e2].name);
  if(shm->rules[i].drule.deadband.eflag){
    printf(" with a deadband of <b>%s</b>",shm->elements[shm->rules[i].drule.deadband.rval.eid].name);
  }else{
    printf(" with a deadband of <b>%0.1f</b>",shm->rules[i].drule.deadband.rval.value);
  }

  //printf(" with a deadband of <b>%s</b></td></tr>",shm->elements[shm->rules[i].drule.deadband].name);
}

void print_crule(int i, int rw){

  printf("\n\n<tr>");
  if(rw){
    print_edit_icons(i,'c');
  }
  printf("<td class=boldblue>// %s</td></tr>",shm->rules[i].crule.ctext);
}

void print_wrule(int i, int rw){

  char msg[80];
  char *x;

  strcpy(msg,shm->rules[i].wrule.ctext);
  printf("\n\n<tr>");
  if(rw){
    print_edit_icons(i,'w');
  }
  printf("<td><img valign=center src=/images/%sdot.png>\n",shm->rules[i].wrule.active ? "red" : "wht");
  x = strtok(msg,";");
  if(x != NULL){
    x = strtok(NULL,";");
    if (x != NULL){
      printf("When <b>%s</b> is %s true",shm->elements[shm->rules[i].wrule.state].name, (shm->rules[i].wrule.tnt == 0) ? "not":"");
      printf(" send warning to <b>%s</b> ",x);
    }else{
      printf("Invalid rule: %s",shm->rules[i].wrule.ctext);
    }
  }else{
    printf("Invalid rule: %s",shm->rules[i].wrule.ctext);
  }
  printf("</td></tr>\n");
}

void print_mrule(int i, int rw){

  printf("\n<tr>");
  if(rw){
    print_edit_icons(i,'m');
  }
  printf("<td><img src=/images/%sdot.png>\n",shm->rules[i].mrule.active ? "red" : "wht");
  printf("Set <b>%s</b>",shm->elements[shm->rules[i].mrule.target].name);

  if(shm->rules[i].mrule.e1.eflag){
    printf(" to <b>%s</b>",shm->elements[shm->rules[i].mrule.e1.rval.eid].name);
  }else{
    printf(" to <b>%0.1f</b>",shm->rules[i].mrule.e1.rval.value);
  }

  //printf("Set <b>%s</b> to <b>%s</b>",shm->elements[shm->rules[i].mrule.target].name,shm->elements[shm->rules[i].mrule.e1].name);

  switch (shm->rules[i].mrule.op) {
    case '+': printf(" plus ");
      break;
    case '-': printf(" minus ");
      break;
    case '/': printf(" divided by ");
      break;
    case '*': printf(" times ");
      break;
  }
  if(shm->rules[i].mrule.e2.eflag){
    printf("<b>%s</b>",shm->elements[shm->rules[i].mrule.e2.rval.eid].name);
  }else{
    printf("<b>%0.1f</b>",shm->rules[i].mrule.e2.rval.value);
  }

  //printf("<b>%s</b></td></tr>",shm->elements[shm->rules[i].mrule.e2].name);
}

void print_arule(int i, int rw){
  
  printf("\n\n<tr>");
  if (rw){
    print_edit_icons(i,'a');
  }
  printf("<td><img src=/images/%sdot.png>\n",shm->rules[i].arule.active ? "red" : "wht");
  printf("Set <b>%s</b>\n",shm->elements[shm->rules[i].arule.target].name);

  if(shm->rules[i].arule.v1.eflag){
    printf(" to <b>%s</b>",shm->elements[shm->rules[i].arule.v1.rval.eid].name);
  }else{
    printf(" to <b>%0.1f</b>",shm->rules[i].arule.v1.rval.value);
  }

  //printf(" to <b>%s</b>\n",shm->elements[shm->rules[i].arule.v1].name);

  printf(" if <b>%s</b> is %s true\n",shm->elements[shm->rules[i].arule.e1].name, (shm->rules[i].arule.n1 == 1) ? "not":"");

  if (shm->rules[i].arule.e2 > 0){
    printf(" and <b>%s</b> is %s true",shm->elements[shm->rules[i].arule.e2].name, (shm->rules[i].arule.n2 == 1) ? "not":"");
  }

  if (shm->rules[i].arule.e3 > 0){
    printf(" and <b>%s</b> is %s true",shm->elements[shm->rules[i].arule.e3].name, (shm->rules[i].arule.n3 == 1) ? "not":"");
  }

  printf("</td></tr>\n");
}

void print_trule(int i, int rw){

  printf("\n\n<tr>");
  if(rw){
    print_edit_icons(i,'t');
  }
  printf("<td><img src=/images/%sdot.png>\n",shm->rules[i].trule.active ? "red" : "wht");
  printf("Set <b>%s</b> ",shm->elements[shm->rules[i].trule.target].name);
  if(shm->rules[i].trule.e2.eflag){
    printf(" for <b>%s</b> seconds ",shm->elements[shm->rules[i].trule.e2.rval.eid].name);
  }else{
    printf(" for <b>%0.1f</b> seconds ",shm->rules[i].trule.e2.rval.value);
  }
  printf(" when <b>%s</b> %s %s</td></tr>\n",
	 shm->elements[shm->rules[i].trule.e1].name,
	 (shm->rules[i].trule.ib) ? "becomes" : "is",
	 (shm->rules[i].trule.tf) ? "true" : "false");
}

void print_prule(int i, int rw){
  
  printf("<tr>");
  if(rw){
    printf("<td rowspan=3><a target=rulewindow href=/cgi-bin/private/vestaRuleEdit?rtype=p&id=%d&action=x><img hspace=1 border=0 src=/images/delete-btn.png></img></a>",i);
    printf("<a target=rulewindow href=/cgi-bin/private/vestaRuleEdit?rtype=p&id=%d&action=e><img hspace=1 border=0 src=/images/edit-btn.png></img></a></td>",i);
  }
  printf("<td rowspan=3><img src=/images/%sdot.png></td>\n",shm->pids[i].active ? "red" : "wht");
  printf("<td>Control <b>%s</b> ",shm->elements[shm->pids[i].e_co].name);
  printf(" to make <b>%s</b> ",shm->elements[shm->pids[i].e_pv].name);
  printf(" close to <b>%s</b> ",shm->elements[shm->pids[i].e_sp].name);
  printf(" based on <b>%s</b> ",shm->elements[shm->pids[i].e_load].name);
  printf(" when <b>%s</b> is true</td>",shm->elements[shm->pids[i].e_ctl].name);
  printf("</tr><tr>\n");
  printf("<td> gain = <b>%2.1f</b>",shm->pids[i].kc);
  printf(" damping = <b>%2.1f</b>",shm->pids[i].ti);
  printf(" lmin = <b>%2.1f</b>",shm->pids[i].lmin);
  printf(" lmax = <b>%2.1f</b>",shm->pids[i].lmax);
  printf(" Min Output = <b>%2.1f</b>",shm->pids[i].coMin);
  printf(" Max Output = <b>%2.1f</b></td>",shm->pids[i].coMax);
  printf("</tr><tr>\n");
  printf("<td>Purge Threshold = <b>%2.1f</b>",shm->pids[i].purgeLimit);
  printf(" Purge cycle interval = <b>%2.1f</b>",shm->pids[i].purgeInterval);
  printf(" Purge duration = <b>%2.1f</b></td>",shm->pids[i].purgeCycle);

  printf("</tr>\n");
}

void print_edit_icons(int i, char rtype){
  printf("<td class=smallpad><a target=rulewindow href=/cgi-bin/private/vestaRuleEdit?rtype=%c&id=%d&action=x><img hspace=3 border=0 src=/images/delete-btn.png></img></a>",rtype,i);
  printf("<a target=rulewindow href=/cgi-bin/private/vestaRuleEdit?rtype=%c&id=%d&action=u><img hspace=3 border=0 src=/images/up-arrow.png></img></a>",rtype,i);
  printf("<a target=rulewindow href=/cgi-bin/private/vestaRuleEdit?rtype=%c&id=%d&action=d><img hspace=3 border=0 src=/images/down-arrow.png></img></a>",rtype,i);
  printf("<a target=rulewindow href=/cgi-bin/private/vestaRuleEdit?id=%d&action=n><img hspace=3 border=0 src=/images/insert-up-btn.png></img></a>",i);
  printf("<a target=rulewindow href=/cgi-bin/private/vestaRuleEdit?rtype=%c&id=%d&action=e><img hspace=3 border=0 src=/images/edit-btn.png></img></a></td>",rtype,i);
  
}

void restoreBackup(char *chname){
  char entry[200];

  sprintf(entry, "sudo /etc/init.d/vesta stop >> /var/vesta/sd/logs/vesta.log 2>&1");
  // sudo broken on 7260 for now - don't do.
  // system(entry);


  // Delete config files.
  sprintf(entry, "rm /usr/local/vesta/data/* >> /var/vesta/sd/logs/vesta.log 2>&1");
  system(entry);

  // Used to delete everything here. Now just delete data files
  sprintf(entry, "mv /var/www/public/index.html /var/www/public/index.html.bak >> /var/vesta/sd/logs/vesta.log 2>&1");
  system(entry);
  sprintf(entry, "mv /var/www/private/index.html /var/www/private/index.html.bak >> /var/vesta/sd/logs/vesta.log 2>&1");
  system(entry);

  sprintf(entry, "cd /; /bin/tar -xzf /var/vesta/backups/%s >> /var/vesta/sd/logs/vesta.log 2>&1",
      chname);
  system(entry);

  sprintf(entry, "mv /var/www/public/index.html.bak /var/www/public/index.html >> /var/vesta/sd/logs/vesta.log 2>&1");
  system(entry);
  sprintf(entry, "mv /var/www/private/index.html.bak /var/www/private/index.html >> /var/vesta/sd/logs/vesta.log 2>&1");
  system(entry);

  sprintf(entry, "chown 580:580 /usr/local/vesta/data/* >> /var/vesta/sd/logs/vesta.log 2>&1");
  system(entry);

  sprintf(entry, "chmod 664 /usr/local/vesta/data/* >> /var/vesta/sd/logs/vesta.log 2>&1");
  system(entry);

  read_diskfiles();

  sprintf(entry, "sudo /etc/init.d/vesta start >> /var/vesta/sd/logs/vesta.log 2>&1");
  // sudo broken on 7260 for now - don't do.
  //system(entry);
  shm->config->reload = (unsigned long int) -1;
}

void printTabs(char *view){
  struct timeval now;
  time_t timestamp;
  struct tm * timeinfo;
  char buffer[30];

  #define ACTSTART "<td align=right><img src=/images/tab-act-left.gif></td>"
  #define ACTCTR "<td align=center background=/images/tab-act-ctr.gif><a class=bigtabblk "
  #define ACTEND "</td><td><img src=/images/tab-act-right.gif></td>\n"
  #define PASSTART "<td align=right><img src=/images/tab-psv-left.gif></td>"
  #define PASCTR "<td align=center background=/images/tab-psv-ctr.gif><a class=bigtab "
  #define PASSEND "</a></td><td><img src=/images/tab-psv-right.gif></td>\n"

  // format local time
  gettimeofday(&now, NULL );
  timestamp = time(NULL );
  timeinfo = localtime(&timestamp);
  strftime(buffer, 25, "%H:%M (%I:%M %p)", timeinfo);

  // Black background table

  printf(
      "<table style=\"min-width:900px\" bgcolor=darkblue cellpadding=0 border=0 cellspacing=0>\n");

  // Display heading and time
  printf(
      "<tr><td style=font-size:large;font-family:Times;color:lightblue;font-weight:bold; colspan=12 valign=top>VESTA Web Interface</td>\n");
  printf(
      "<td class=normal height=25 colspan=12 align=right valign=top><B style=color:lightblue>%s</B></td></tr>\n",
      buffer);

  // Tabs
  printf("<tr>\n");
  printf("%s%s href=/cgi-bin/vestaHomepage?view=home>Home%s",
      (!strcmp(view,"home") ? ACTSTART : PASSTART),
      (!strcmp(view,"home") ? ACTCTR : PASCTR),
      (!strcmp(view,"home") ? ACTEND : PASSEND));

  printf("%s%shref=/public/svg_chart.html?view=charts>Charts%s",
      (!strcmp(view,"charts") ? ACTSTART : PASSTART),
      (!strcmp(view,"charts") ? ACTCTR : PASCTR),
      (!strcmp(view,"charts") ? ACTEND : PASSEND));

  printf("%s%shref=/public/index.html>GUI%s", PASSTART, PASCTR, PASSEND);

  printf("%s%shref=/cgi-bin/private/vestaPhysicalEdit?editmode=p>Physical I/O%s",
      (!strcmp(view,"physical") ? ACTSTART : PASSTART),
      (!strcmp(view,"physical") ? ACTCTR : PASCTR),
      (!strcmp(view,"physical") ? ACTEND : PASSEND));

  printf("%s%shref=/cgi-bin/private/vestaWebEdit?editmode=e&view=element>Data Elements%s",
      (!strcmp(view,"element") ? ACTSTART : PASSTART),
      (!strcmp(view,"element") ? ACTCTR : PASCTR),
      (!strcmp(view,"element") ? ACTEND : PASSEND));

  printf("%s%shref=/cgi-bin/private/vestaWebEdit?editmode=r&view=rule>Rules%s",
      (!strcmp(view,"rule") ? ACTSTART : PASSTART),
      (!strcmp(view,"rule") ? ACTCTR : PASCTR),
      (!strcmp(view,"rule") ? ACTEND : PASSEND));

  printf("%s%shref=/cgi-bin/private/vestaWebEdit?editmode=p&view=logs>Logs%s",
      (!strcmp(view,"logs") ? ACTSTART : PASSTART),
      (!strcmp(view,"logs") ? ACTCTR : PASCTR),
      (!strcmp(view,"logs") ? ACTEND : PASSEND));

  printf("%s%shref=/cgi-bin/private/vestaWebEdit?editmode=p&view=system>System%s",
      (!strcmp(view,"system") ? ACTSTART : PASSTART),
      (!strcmp(view,"system") ? ACTCTR : PASCTR),
      (!strcmp(view,"system") ? ACTEND : PASSEND));

  printf("</tr>\n<tr><td colspan=24>\n");

  // Metal Container table - tab page contents

  printf("      <table background=/images/metal_bg2.jpg width=\"100\%\">\n");

}

void printSubTabs(subview){
  printf("<tr>\n");
#define ACTTAB "<td class=tab bgcolor=white><a class=bigtabblk "
#define PASTAB "<td class=tab><a class=bigtab "

  printf(
      "%s href=vestaPhysicalEdit?editmode=p&subview=%d>Analog Inputs</a></td>\n",
      (subview == AIVIEW ? ACTTAB : PASTAB), AIVIEW);
  printf(
      "%s href=vestaPhysicalEdit?editmode=p&subview=%d>Analog Outputs</a></td>\n",
      (subview == AOVIEW ? ACTTAB : PASTAB), AOVIEW);
  printf(
      "%s href=vestaPhysicalEdit?editmode=p&subview=%d>Discrete Inputs</a></td>\n",
      (subview == DIVIEW ? ACTTAB : PASTAB), DIVIEW);
  printf(
      "%s href=vestaPhysicalEdit?editmode=p&subview=%d>Discrete Outputs</a></td>\n",
      (subview == DOVIEW ? ACTTAB : PASTAB), DOVIEW);
  printf(
      "%s href=vestaPhysicalEdit?editmode=p&subview=%d>Network I/O</a></td>\n",
      (subview == NETVIEW ? ACTTAB : PASTAB), NETVIEW);
  printf(
      "%s href=vestaInsteonEdit?editmode=p&subview=%d>Insteon I/O</a></td>\n",
      (subview == INSVIEW ? ACTTAB : PASTAB), INSVIEW);
  printf(
      "%s href=vestaPhysicalEdit?editmode=p&subview=%d>Wireless I/O</a></td>\n",
      (subview == XBWVIEW ? ACTTAB : PASTAB), XBWVIEW);
  printf("</tr></table>\n");

  printf("<table background=/images/metal_bg2.jpg width=100%>\n");
}
