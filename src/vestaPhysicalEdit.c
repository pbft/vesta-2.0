/*****************************************************************************/
//    This file is part of the Vesta Control System Software Suite
//    Copyright Bill Kuhns and Vermont Energy Control Systems LLC.
//
//    This is free software: you can redistribute it and/or modify
//    it under the terms of the Lesser GNU General Public License (LGPL)
//    as published by the Free Software Foundation, either version 3 of the
//    License, or the Free Software Foundation, either version 3 of the License,
//    or any later version.
//
//    This software is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    Lesser GNU General Public License for more details.
//
//    You should have received a copy of the Lesser GNU General Public License
//    along with this software.  If not, see <http://www.gnu.org/licenses/>.
/*****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include "vesta.h"
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/dir.h>
#include <sys/param.h>

/*****************************************************************************/

main() {
  float reading;
  int c, i, j, n, x, vc, tr, port;
  int physical, element, rule, home, logs, sysview;
  int subview;
  int e_edit, r_edit, p_edit;
  int stype;
  int pnl, con, ch;
  char chtype[3];
  char pathname[80];
  struct direct **files;
  int fcount;
  char *ptr;
  float floatval;
  struct timeval now;
  time_t timestamp;
  struct tm * timeinfo;
  char buffer[30];

  char entry[200];
  char chname[40];
  char addr[20];
  char bgcolor[10];

  byte_t dio[20], rmask;

  FILE *fp;
  char *myquery, *p[20], *v[20];
  char linebuff[256];
  char action[20];
  char editmode;

  char addrbuff[10];
  char bytebuff[3];
  //int idevcount = 0;
  int itypecount = 0;

  element_struct temp_element;

  get_shm(0);

  printf("Content-type: text/html\n\n<html>");
  printf("<head><link rel=stylesheet type=text/css href=/vesta.css></head>\n");
  printf("<body bgcolor=darkblue>\n");

  /*****************************************************************************/

  // Get form data, process any edits
  /*****************************************************************************/
  myquery = getenv("QUERY_STRING");

  /* break apart parameter string on '&' and '=' characters */

  j = 0;

  // Weirdness: sometimes myquery is NULL, and you don't want to do strlen on it.
  // Other times, it's a zero length string. Either way, don't parse it.

  if (myquery != NULL ) {
    if (strlen(myquery) > 0) {
      while ((myquery = (split(myquery, &p[j], &v[j])))) {
        j++;
      }
      j++;
      cleanup(j, &v[0]);
    }
  }

  // Set element values to shared memory values for all variables
  //set_elements(shm, elements, shm->element_count);

  e_edit = 0;
  r_edit = 0;
  p_edit = 0;
  element = 0;
  rule = 0;
  physical = 0;
  home = 0;
  logs = 0;
  sysview = 0;
  subview = 0;
  floatval = 0;

  if (j > 0) {
    for (i = 0; i < j; i++) {
      //printf("p: %s v: %s i: %d<br>\n", p[i], v[i], i);

      if (!strcmp(p[i], "subview")) {
        sscanf(v[i], "%d", &subview);
      }
      // Set action
      if (!strcmp(p[i], "action")) {
        strcpy(action, v[i]);
      }

      if (!strcmp(p[i], "name")) {
        sscanf(v[i], "%[^\"] ", &chname[0]);
      }

      if (!strcmp(p[i], "addr")) {
        sscanf(v[i], "%[^\"] ", &addr[0]);
      }

      if (!strcmp(p[i], "id")) {
        sscanf(v[i], "%d", &c);
      }

      if (!strcmp(p[i], "port")) {
        sscanf(v[i], "%d", &port);
      }

      if (!strcmp(p[i], "stype")) {
        sscanf(v[i], "%d", &stype);
      }

      if (!strcmp(p[i], "value")) {
        sscanf(v[i], "%f", &floatval);
        //printf("Floatval = %f ",floatval);
      }

    }

    // All done parsing. Process actions....

    // Process element name / value updates
    if (!strcmp(action, "myelement")) {
      // Get channel.
      setElement(c, floatval, 0);
      strcpy(shm->elements[c].name, chname);
      write_elements();
    }

    // Update sensor type in physical I/O table and file
    if (!strcmp(action, "stype")) {
      shm->elements[c].stype = stype;
      write_elements();
    }

    // Update sensor gain in physical I/O table and file
    if (!strcmp(action, "gain")) {
      shm->elements[c].gain = floatval;
      write_elements();
    }

    // Update sensor offset in physical I/O table and file
    if (!strcmp(action, "offset")) {
      if (!shm->config->centigrade
          && shm->stypes[shm->elements[c].stype].conversion == 't') {
        shm->elements[c].offset = (float) (floatval / 1.8);
      } else {
        shm->elements[c].offset = floatval;
      }

      write_elements();
    }

    // Process element add for physical i/o (from physical I/O page)
    if (!strcmp(action, "newphysio")) {
      linkElement(c);
      strcpy(shm->elements[c].name, hardwareName(c));
      //setElement(j, 0, 1);
      write_elements();
      shm->config->reload = (unsigned long int) -1;
    }

    printTabs("physical");
    printSubTabs(subview);

    /*****************************************************************************/

    // Show physical I/O
    /*****************************************************************************/

    if (subview == AIVIEW) {
      char *headers[] = { "ID", "Name", "Type", "Gain", "Offset", "Value",
          "Element" };
      printSectionHeader(7, headers);
      for (i = 0; i < shm->counts->element_count; i++) {
        if (shm->elements[i].pvn == 'p' && shm->elements[i].ptype == 's') {
          if (i % 2) {
            strcpy(&bgcolor[0], "lightblue");
          } else {
            strcpy(&bgcolor[0], "white");
          }
          printf("<tr>");
          printf("<td class=smallpad bgcolor=%s>%d</td>", bgcolor, i);
          printf("<td class=smallpad bgcolor=%s>%s</td>", bgcolor,
              hardwareName(i));
          // Sensor type selection box - update on change
          printf("<form action=vestaPhysicalEdit method=get>\n");
          printf("<td class=smallpad bgcolor=%s>\n", bgcolor);
          printf("<input type=hidden name=view value=physical>\n");
          printf("<input type=hidden name=action value=stype>\n");
          printf("<input type=hidden name=subview value=0>\n");
          printf("<input type=hidden name=id value=%d>\n", i);
          printf("<select name=stype onchange=\"submit()\">\n");
          for (x = 0; x < shm->counts->stype_count; x++) {
            printf("    <option value=%d %s>%s</option>\n", x,
                (x == shm->elements[i].stype ? "selected" : ""),
                shm->stypes[x].sname);
          }
          printf("   </select> \n  </td></form>\n");
          // Gain input box
          printf("<form action=vestaPhysicalEdit method=get>\n");
          printf("<td class=smallpad bgcolor=%s>\n", bgcolor);
          printf("<input type=hidden name=id value=%d>\n", i);
          printf(
              "<input name=value value=%2.3f size=6 onchange=\"submit()\">\n",
              shm->elements[i].gain);
          printf("<input type=hidden name=view value=physical>\n");
          printf("<input type=hidden name=action value=gain>\n");
          printf("</td></form>");

          // Offset input box
          printf("<form action=vestaPhysicalEdit method=get>\n");
          printf("<td class=smallpad bgcolor=%s>\n", bgcolor);
          printf("<input type=hidden name=id value=%d>\n", i);
          if (!shm->config->centigrade
              && shm->stypes[shm->elements[i].stype].conversion == 't') {
            printf(
                "<input name=value value=%2.3f size=6 onchange=\"submit()\">\n",
                shm->elements[i].offset * 1.8);
          } else {
            printf(
                "<input name=value value=%2.3f size=6 onchange=\"submit()\">\n",
                shm->elements[i].offset);
          }
          printf("<input type=hidden name=view value=physical>\n");
          printf("<input type=hidden name=action value=offset>\n");
          printf("</td></form>");

          // get analog sensor value

          reading = shm->aivalue[shm->elements[i].hwchannel];
          if (!shm->config->centigrade
              && shm->stypes[shm->elements[i].stype].conversion == 't') {
            reading = (float) (reading * 1.8 + 32);
          }
          printf("<td class=smallpad bgcolor=%s>%1.1f</td>", bgcolor, reading);
          if (shm->elements[i].next != 0) {
            printf("<td class=smallpad bgcolor=%s>%s</td>", bgcolor,
                shm->elements[i].name);
          } else {
            printf("<td class=smallpad bgcolor=%s>", bgcolor);
            printf(
                "<a href=vestaPhysicalEdit?id=%d&action=newphysio&editmode=p&view=physical>Create Element</a>\n",
                i);
            printf("</td>");
          }
          printf("</tr>\n");
        }
      }
    }

    if (subview == AOVIEW) {
      char *headers[] = { "ID", "Name", "Type", "Gain", "Offset", "Value",
          "Element" };
      printSectionHeader(7, headers);
      for (i = 0; i < shm->counts->element_count; i++) {
        if (shm->elements[i].pvn == 'p' && shm->elements[i].ptype == 'c') {
          if (i % 2) {
            strcpy(&bgcolor[0], "lightblue");
          } else {
            strcpy(&bgcolor[0], "white");
          }
          printf("<tr>");
          printf("<td class=smallpad bgcolor=%s>%d</td>", bgcolor, i);
          printf("<td class=smallpad bgcolor=%s>%s</td>", bgcolor,
              hardwareName(i));
          // Sensor type selection box - update on change
          printf("<form action=vestaPhysicalEdit method=get>\n");
          printf("<td class=smallpad bgcolor=%s>\n", bgcolor);
          printf("<input type=hidden name=view value=physical>\n");
          printf("<input type=hidden name=action value=stype>\n");
          printf("<input type=hidden name=subview value=1>\n");
          printf("<input type=hidden name=id value=%d>\n", i);
          printf("<select name=stype onchange=\"submit()\">\n");
          for (x = 0; x < shm->counts->stype_count; x++) {
            printf("    <option value=%d %s>%s</option>\n", x,
                (x == shm->elements[i].stype ? "selected" : ""),
                shm->stypes[x].sname);
          }
          printf("   </select> \n  </td></form>\n");
          // Gain input box
          printf("<form action=vestaPhysicalEdit method=get>\n");
          printf("<td class=smallpad bgcolor=%s>\n", bgcolor);
          printf("<input type=hidden name=id value=%d>\n", i);
          printf(
              "<input name=value value=%2.3f size=6 onchange=\"submit()\">\n",
              shm->elements[i].gain);
          printf("<input type=hidden name=view value=physical>\n");
          printf("<input type=hidden name=action value=gain>\n");
          printf("</td></form>");

          // Offset input box
          printf("<form action=vestaPhysicalEdit method=get>\n");
          printf("<td class=smallpad bgcolor=%s>\n", bgcolor);
          printf("<input type=hidden name=id value=%d>\n", i);
          if (!shm->config->centigrade
              && shm->stypes[shm->elements[i].stype].conversion == 't') {
            printf(
                "<input name=value value=%2.3f size=6 onchange=\"submit()\">\n",
                shm->elements[i].offset * 1.8);
          } else {
            printf(
                "<input name=value value=%2.3f size=6 onchange=\"submit()\">\n",
                shm->elements[i].offset);
          }
          printf("<input type=hidden name=view value=physical>\n");
          printf("<input type=hidden name=action value=offset>\n");
          printf("</td></form>");

          // get analog output value
          reading = elementValue(i);
          printf("<td class=smallpad bgcolor=%s>%1.1f</td>", bgcolor, reading);

          if (shm->elements[i].next != 0) {
            printf("<td class=smallpad bgcolor=%s>%s</td>", bgcolor,
                shm->elements[i].name);
          } else {
            printf("<td class=smallpad bgcolor=%s>", bgcolor);
            printf(
                "<a href=vestaPhysicalEdit?id=%d&action=newphysio&editmode=p&view=physical>Create Element</a>\n",
                i);
            printf("</td>");
          }
          printf("</tr>\n");
        }
      }
    }

    if (subview == DIVIEW) {
      char *headers[] = { "ID", "Name", "Type", "Gain", "Offset", "Value",
          "Element" };
      printSectionHeader(7, headers);
      for (i = 0; i < shm->counts->element_count; i++) {
        if (shm->elements[i].pvn == 'p' && shm->elements[i].ptype == 'i') {
          if (i % 2) {
            strcpy(&bgcolor[0], "lightblue");
          } else {
            strcpy(&bgcolor[0], "white");
          }
          printf("<tr>");
          printf("<td class=smallpad bgcolor=%s>%d</td>", bgcolor, i);
          printf("<td class=smallpad bgcolor=%s>%s</td>", bgcolor,
              hardwareName(i));
          // Sensor type
          printf("<td class=smallpad bgcolor=%s> DI-7260 (Discrete In)</td>\n",
              bgcolor);
          // Gain & Offset not applicable
          printf("<td class=smallpad bgcolor=%s>&nbsp;</td>\n", bgcolor);
          printf("<td class=smallpad bgcolor=%s>&nbsp;</td>\n", bgcolor);

          // get  value
          dio[0] = shm->divalue[shm->elements[i].hwchannel];
          rmask = (byte_t) (1 << shm->elements[i].bitnum);
          printf("<td class=smallpad bgcolor=%s>%d</td>", bgcolor,
              (dio[0] & rmask) >> shm->elements[i].bitnum);

          if (shm->elements[i].next != 0) {
            printf("<td class=smallpad bgcolor=%s>%s</td>", bgcolor,
                shm->elements[i].name);
          } else {
            printf("<td class=smallpad bgcolor=%s>", bgcolor);
            printf(
                "<a href=vestaPhysicalEdit?id=%d&action=newphysio&editmode=p&view=physical&subview=2>Create Element</a>\n",
                i);
            printf("</td>");
          }
          printf("</tr>\n");
        }
      }
    }

    if (subview == DOVIEW) {
      char *headers[] = { "ID", "Name", "Type", "Gain", "Offset", "Value",
          "Element" };
      printSectionHeader(7, headers);
      for (i = 0; i < shm->counts->element_count; i++) {
        if (shm->elements[i].pvn == 'p' && shm->elements[i].ptype == 'o') {
          if (i % 2) {
            strcpy(&bgcolor[0], "lightblue");
          } else {
            strcpy(&bgcolor[0], "white");
          }
          printf("<tr>");
          printf("<td class=smallpad bgcolor=%s>%d</td>", bgcolor, i);
          printf("<td class=smallpad bgcolor=%s>%s</td>", bgcolor,
              hardwareName(i));
          // Sensor type
          printf("<td class=smallpad bgcolor=%s> DI-7260 (Discrete In)</td>\n",
              bgcolor);
          // Gain & Offset not applicable
          printf("<td class=smallpad bgcolor=%s>&nbsp;</td>\n", bgcolor);
          printf("<td class=smallpad bgcolor=%s>&nbsp;</td>\n", bgcolor);

          // get  value
          dio[0] = shm->divalue[shm->elements[i].hwchannel];
          rmask = (byte_t) (1 << shm->elements[i].bitnum);
          printf("<td class=smallpad bgcolor=%s>%d</td>", bgcolor,
              (dio[0] & rmask) >> shm->elements[i].bitnum);

          if (shm->elements[i].next != 0) {
            printf("<td class=smallpad bgcolor=%s>%s</td>", bgcolor,
                shm->elements[i].name);
          } else {
            printf("<td class=smallpad bgcolor=%s>", bgcolor);
            printf(
                "<a href=vestaPhysicalEdit?id=%d&action=newphysio&editmode=p&view=physical&subview=3>Create Element</a>\n",
                i);
            printf("</td>");
          }
          printf("</tr>\n");
        }
      }
    }

    if (subview == NETVIEW) {
      char *headers[] = { "ID", "Address", "Port", "Remote Element", "Value",
          "Element" };
      printSectionHeader(7, headers);
      tr = 1;
      for (i = 2; i < shm->counts->element_count; i++) {
        if (shm->elements[i].pvn == 'v' && shm->elements[i].ptype == 'n') {
          if (tr++ % 2) {
            strcpy(&bgcolor[0], "lightblue");
          } else {
            strcpy(&bgcolor[0], "white");
          }
          printf("<tr>");
          printf("<td class=smallpad bgcolor=%s>%d</td>", bgcolor, i);

          // Address input box
          printf("<form action=vestaPhysicalEdit method=get>\n");
          printf("<td class=smallpad bgcolor=%s>\n", bgcolor);
          printf("<input type=hidden name=id value=%d>\n", i);
          printf("<input name=addr size=10 value=%s onchange=\"submit()\">\n",
              shm->elements[i].vdev.netio.ip);
          printf("<input type=hidden name=view value=physical>\n");
          printf("<input type=hidden name=subview value=5>\n");
          printf("<input type=hidden name=action value=naddr>\n");
          printf("</td></form>");

          // Port input box
          printf("<form action=vestaPhysicalEdit method=get>\n");
          printf("<td class=smallpad bgcolor=%s>\n", bgcolor);
          printf("<input type=hidden name=id value=%d>\n", i);
          printf("<input name=port size=5 value=%d onchange=\"submit()\">\n",
              shm->elements[i].vdev.netio.port);
          printf("<input type=hidden name=view value=physical>\n");
          printf("<input type=hidden name=subview value=5>\n");
          printf("<input type=hidden name=action value=nport>\n");
          printf("</td></form>");

          // Remote Element input box
          printf("<form action=vestaPhysicalEdit method=get>\n");
          printf("<td class=smallpad bgcolor=%s>\n", bgcolor);
          printf("<input type=hidden name=id value=%d>\n", i);
          printf("<input name=name size=5 value=%d onchange=\"submit()\">\n",
              shm->elements[i].vdev.netio.remoteElementId);
          printf("<input type=hidden name=view value=physical>\n");
          printf("<input type=hidden name=subview value=5>\n");
          printf("<input type=hidden name=action value=neid>\n");
          printf("</td></form>");

          // get  value
          printf("<td class=smallpad bgcolor=%s>%1.1f</td>", bgcolor,
              elementValue(i));

          printf("<td class=smallpad bgcolor=%s>%s</td>", bgcolor,
              shm->elements[i].name);
          printf("</tr>\n");
        }
      }
    }

    printf("</td></tr></table>\n");
    // Close container table
    printf("</tr>\n</table >\n");

    printf("</body>\n</html>\n");

  }
}
  void printSectionHeader(int cols, char *headers[]) {
    int i;

    printf("<tr>\n");
    for (i = 0; i < cols; i++) {
      printf("<th class=padded bgcolor=white>%s</th>\n", headers[i]);
    }
    printf("</tr>\n");
  }

