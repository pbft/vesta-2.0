
/*****************************************************************************/
//    This file is part of the NoFossil Control System Software (NFCSS)
//    Copyright 2007, 2008, 2009 Bill Kuhns
//
//    NFCSS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    any later version.

//    NFCSS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with NFCSS.  If not, see <http://www.gnu.org/licenses/>.
/*****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
//#include <sys/ipc.h>
//#include <sys/shm.h>
#include "vesta.h"
#include <fcntl.h>
//#include <sys/mman.h>
#include <sys/dir.h>
#include <sys/param.h>


/*****************************************************************************/

main(){
  float reading;
  int c,i,j,r,n, x, vc,tc, hdr;
  int physical,element,rule,home,logs,sysview;
  int styp;
  int pnl,con,ch;
  char chtype[3];
  char pathname[80];
  struct direct **files;
  int file_select();
  int fcount;
  char *ptr;

  char bgcolor[10]; 
  char entry[200];
  char chname[21];
  char *myquery, *p[20], *v[20];
  int intval;
  float floatval;
  int shmid, dio0, dio1, dio2, mybit;

  int fd;
  byte_t dio[20], rmask, newbit;

  // form variables
  char rtype, action;
  int edit;
  char gle,io,etype, op;
  short int id, e1, e2, e3;
  short int v1, ib, tf;
  short int n1, n2, n3, n4, n5, n6;
  short int e_ctl, e_sp, e_pv, e_load, e_co;
  float e1val, e2val, e3val, kc, ti, lmin, lmax, dbval, diffval, v1val;
  float coMin, coMax, purgeLimit, purgeCycle, purgeInterval;
  short int diff;
  short int db;
  short int target;
  char ctext[80];

  FILE *fp;
  char linebuff[256];

  rule_struct temp_rule;

  struct tm *loctime;

  // Get shared memory links
  get_shm(0);


  /*****************************************************************************/

  // Get form data, process any edits

  /*****************************************************************************/
  myquery = getenv("QUERY_STRING");

  /* break apart parameter string on '&' and '=' characters */

  j = 0;

  // Weirdness: sometimes myquery is NULL, and you don't want to do strlen on it.
  // Other times, it's a zero lenght string. Either way, don't parse it. 

  if (myquery != NULL){
    if (strlen(myquery) > 0){
      while(myquery=(split(myquery,&p[j],&v[j]))){
        j++;
      }
      j++;
      cleanup(j,&v[0]);
    }
  }

  element=0;
  rule=0;
  physical=0;
  home=0;
  logs=0;
  sysview=0;
  floatval = 0;
  e1 = e2 = e3 = db = diff = 0;
  v1val = -314159;
  n1 = 0;
  action = ' ';
  edit = 0;

  // Set element values to shared memory values for all variables
  //set_elements(shm, &elements, element_count);

  // Start of body tag

  if (j > 0){
    for(i=0;i<j;i++){

      if (!strcmp(p[i], "target")){
        sscanf(v[i],"%hd",&target);
      }

      if (!strcmp(p[i], "gle")){
        sscanf(v[i],"%c",&gle);
      }

      if (!strcmp(p[i], "ib")){
        sscanf(v[i],"%hd",&ib);
      }

      if (!strcmp(p[i], "db")){
        sscanf(v[i],"%hd",&db);
      }

      if (!strcmp(p[i], "dbval")){
        sscanf(v[i],"%f",&dbval);
      }

      if (!strcmp(p[i], "tf")){
        sscanf(v[i],"%hd",&tf);
      }

      if (!strcmp(p[i], "io")){
        sscanf(v[i],"%c",&io);
      }

      if (!strcmp(p[i], "etype")){
        sscanf(v[i],"%c",&etype);
      }

      if (!strcmp(p[i], "rtype")){
        sscanf(v[i],"%c",&rtype);
      }

      if (!strcmp(p[i], "n1")){
        sscanf(v[i],"%hd",&n1);
      }

      if (!strcmp(p[i], "n2")){
        sscanf(v[i],"%hd",&n2);
      }

      if (!strcmp(p[i], "n3")){
        sscanf(v[i],"%hd",&n3);
      }

      if (!strcmp(p[i], "n4")){
        sscanf(v[i],"%hd",&n4);
      }

      if (!strcmp(p[i], "n5")){
        sscanf(v[i],"%hd",&n5);
      }

      if (!strcmp(p[i], "n6")){
        sscanf(v[i],"%hd",&n6);
      }

      if (!strcmp(p[i], "op")){
        sscanf(v[i],"%c",&op);
      }

      if (!strcmp(p[i], "e1")){
        sscanf(v[i],"%hd",&e1);
      }

      if (!strcmp(p[i], "e1val")){
        sscanf(v[i],"%f",&e1val);
      }

      if (!strcmp(p[i], "e2")){
        sscanf(v[i],"%hd",&e2);
      }

      if (!strcmp(p[i], "e2val")){
        sscanf(v[i],"%f",&e2val);
      }

      if (!strcmp(p[i], "v1val")){
        sscanf(v[i],"%f",&v1val);
      }

      if (!strcmp(p[i], "diffval")){
        sscanf(v[i],"%f",&diffval);
      }

      if (!strcmp(p[i], "e3")){
        sscanf(v[i],"%hd",&e3);
      }

      if (!strcmp(p[i], "e3val")){
        sscanf(v[i],"%f",&e3val);
      }

      if (!strcmp(p[i], "v1")){
        sscanf(v[i],"%hd",&v1);
      }

      if (!strcmp(p[i], "diff")){
        sscanf(v[i],"%hd",&diff);
      }

      if (!strcmp(p[i], "action")){
        sscanf(v[i],"%c",&action);
      }

      if (!strcmp(p[i], "id")){
        sscanf(v[i],"%hd",&id);
      }

      if (!strcmp(p[i], "e_co")){
        sscanf(v[i],"%hd",&e_co);
      }

      if (!strcmp(p[i], "e_pv")){
        sscanf(v[i],"%hd",&e_pv);
      }

      if (!strcmp(p[i], "e_sp")){
        sscanf(v[i],"%hd",&e_sp);
      }

      if (!strcmp(p[i], "e_ctl")){
        sscanf(v[i],"%hd",&e_ctl);
      }

      if (!strcmp(p[i], "e_load")){
        sscanf(v[i],"%hd",&e_load);
      }

      if (!strcmp(p[i], "kc")){
        sscanf(v[i],"%f",&kc);
      }

      if (!strcmp(p[i], "ti")){
        sscanf(v[i],"%f",&ti);
      }

      if (!strcmp(p[i], "lmin")){
        sscanf(v[i],"%f",&lmin);
      }

      if (!strcmp(p[i], "lmax")){
        sscanf(v[i],"%f",&lmax);
      }

      if (!strcmp(p[i], "coMin")){
        sscanf(v[i],"%f",&coMin);
      }

      if (!strcmp(p[i], "coMax")){
        sscanf(v[i],"%f",&coMax);
      }

      if (!strcmp(p[i], "purgeLimit")){
        sscanf(v[i],"%f",&purgeLimit);
      }

      if (!strcmp(p[i], "purgeCycle")){
        sscanf(v[i],"%f",&purgeCycle);
      }

      if (!strcmp(p[i], "purgeInterval")){
        sscanf(v[i],"%f",&purgeInterval);
      }

      if (!strcmp(p[i], "ctext")){
        sscanf(v[i],"%[^\"] ",&ctext);
      }

      if (!strcmp(p[i], "edit")){
        edit = 1;
      }

      if (!strcmp(p[i], "drule")){
        rtype = 'd';
      }

      if (!strcmp(p[i], "arule")){
        rtype = 'a';
      }

      if (!strcmp(p[i], "trule")){
        rtype = 't';
      }
    }
  }

  // We haven't printed an HTML header yet
  hdr = 0;

  // d is down
  if (action == 'd'){
    // If this isn's the last rule
    if (id < (shm->counts->rule_count)-1){
      memcpy(&temp_rule,&shm->rules[id+1],sizeof(rule_struct));
      memcpy(&shm->rules[id+1],&shm->rules[id],sizeof(rule_struct));
      memcpy(&shm->rules[id],&temp_rule,sizeof(rule_struct));
      write_rules(shm);
      printf ("Content-type: text/html\n\n");
      printf("<body onload='window.opener.location = window.opener.location;self.close();'>");
      hdr = 1;
    }
  }

  // u is up
  if (action == 'u'){
    // If this isn's the last rule
    if (id > 0){
      memcpy(&temp_rule,&shm->rules[id-1],sizeof(rule_struct));
      memcpy(&shm->rules[id-1],&shm->rules[id],sizeof(rule_struct));
      memcpy(&shm->rules[id],&temp_rule,sizeof(rule_struct));
      write_rules(shm);
      printf ("Content-type: text/html\n\n");
      printf("<html><body onload='window.opener.location = window.opener.location;self.close();'></html>\n");
      hdr = 1;
    }
  }

  // x is delete
  if (action == 'x' && (rtype != 'p')){
    for (r=id;r<(shm->counts->rule_count-1);r++){
      memcpy(&shm->rules[r],&shm->rules[r+1],sizeof(rule_struct));
    }
    shm->counts->rule_count--;
    write_rules(shm);
    printf ("Content-type: text/html\n\n");
    printf("<body onload='window.opener.location = window.opener.location;self.close();'>");
    //printf ("Content-type: text/plain\n");
    //printf ("Status: 204 No Content\n\n");
    hdr = 1;
  }

  // either edit or new....
  if (edit && (rtype != 'p')){
    // Three possibilities:
    // 1) The rule is a new rule with id = arule_count
    // 2) The rule is an update to an existing rule
    // 3) The rule is a new rule to be inserted between existing rules

    // Option 1
    if (id == shm->counts->rule_count){
      shm->counts->rule_count++;
    }else{
      // Option 3. Move rules down to make room
      if (id < shm->counts->rule_count && action == 'n'){
        for (r = shm->counts->rule_count; r > id; r--){
          memcpy(&shm->rules[r],&shm->rules[r-1],sizeof(rule_struct));
        }
        shm->counts->rule_count++;
      }
    }
    //printf ("Content-type: text/html\n\n");

    // In any event, write the appropriate data to shared memory.
    switch (rtype){
      case 'm':
        //printf("Rule addr %u<br>\n",&shm->rules[id]);
        shm->rules[id].mrule.rtype='m';
        shm->rules[id].mrule.target=target;
        if(e1 == 0){
          shm->rules[id].mrule.e1.eflag = (int) 0;
          //printf("fv1 = %f %f\n",e1val,shm->rules[id].mrule.e1.rval.value);
          shm->rules[id].mrule.e1.rval.value = (float) e1val;
          //printf("fv1 = %f %f\n",e1val,shm->rules[id].mrule.e1.rval.value);
        }else{
          shm->rules[id].mrule.e1.eflag = (int) 1;
          //printf("fe1 = %d %d\n",e1,shm->rules[id].mrule.e1.rval.eid);
          shm->rules[id].mrule.e1.rval.eid= (short int) e1;
          //printf("fe1 = %d %d\n",e1,shm->rules[id].mrule.e1.rval.eid);
        }
        shm->rules[id].mrule.op=op;
        if(e2 == 0){
          shm->rules[id].mrule.e2.eflag = (int) 0;
          shm->rules[id].mrule.e2.rval.value = (float) e2val;
          //printf("fv2 = %f %f\n",e2val,shm->rules[id].mrule.e2.rval.value);
        }else{
          shm->rules[id].mrule.e2.eflag = (int) 1;
          //printf("fe2 = %d (%d) element %d\n",e2,shm->rules[id].mrule.e2.rval.eid,shm->rules[id].mrule.e2.eflag);
          shm->rules[id].mrule.e2.rval.eid=(short int) e2;
          //printf("fe2 = %d (%d) element %d\n",e2,shm->rules[id].mrule.e2.rval.eid,shm->rules[id].mrule.e2.eflag);
       }
        break;
      case 'd':
        shm->rules[id].drule.rtype='d';
        shm->rules[id].drule.target=target;
        shm->rules[id].drule.e1=e1;
        shm->rules[id].drule.gle=gle;
        //shm->rules[id].drule.e2.rval.eid=e2;
        if(e2 == 0){
          shm->rules[id].drule.e2.eflag = 0;
          shm->rules[id].drule.e2.rval.value = e2val;
        }else{
          shm->rules[id].drule.e2.eflag = 1;
          shm->rules[id].drule.e2.rval.eid=e2;
        }
        //shm->rules[id].drule.diff.rval.eid=diff;
        if(diff == 0){
          shm->rules[id].drule.diff.eflag = 0;
          shm->rules[id].drule.diff.rval.value = diffval;
        }else{
          shm->rules[id].drule.diff.eflag = 1;
          shm->rules[id].drule.diff.rval.eid=diff;
        }
        //shm->rules[id].drule.deadband.rval.eid=db;
        if(db == 0){
          shm->rules[id].drule.deadband.eflag = 0;
          shm->rules[id].drule.deadband.rval.value = dbval;
        }else{
          shm->rules[id].drule.deadband.eflag = 1;
          shm->rules[id].drule.deadband.rval.eid=db;
       }
        /*
        printf("Target = %d (%d)<br>\n",shm->rules[id].drule.target,target);
        printf("e1 = %d (%d)<br>\n",shm->rules[id].drule.e1,e1);
        printf("e2flag = %d<br>\n",shm->rules[id].drule.e2.eflag);
        printf("e2 = %d (%d)<br>\n",shm->rules[id].drule.e2.rval.eid,e2);
        printf("e2val = %f (%f)<br>\n",shm->rules[id].drule.e2.rval.value,e2val);
        printf("diffflag = %d<br>\n",shm->rules[id].drule.diff.eflag);
        printf("diff = %d (%d)<br>\n",shm->rules[id].drule.diff.rval.eid,diff);
        printf("diffval = %f (%f)<br>\n",shm->rules[id].drule.diff.rval.value,diffval);
        printf("dbflag = %d<br>\n",shm->rules[id].drule.deadband.eflag);
        printf("db = %d (%d)<br>\n",shm->rules[id].drule.deadband.rval.eid,db);
        printf("dbval = %f (%f)<br>\n",shm->rules[id].drule.deadband.rval.value,dbval);
        */

        break;
      case 'a':
        shm->rules[id].arule.rtype='a';
        shm->rules[id].arule.target=target;
        if(v1val != -314159){
          shm->rules[id].arule.v1.eflag = 0;
          shm->rules[id].arule.v1.rval.value = v1val;
        }else{
          shm->rules[id].arule.v1.eflag = 1;
          shm->rules[id].arule.v1.rval.eid=v1;
        }
        shm->rules[id].arule.e1=e1;
        shm->rules[id].arule.n1=n1;
        shm->rules[id].arule.e2=e2;
        shm->rules[id].arule.n2=n2;
        shm->rules[id].arule.e3=e3;
        shm->rules[id].arule.n3=n3;
        break;
      case 't':
        shm->rules[id].trule.rtype='t';
        shm->rules[id].trule.target=target;
        shm->rules[id].trule.e1=e1;
        shm->rules[id].trule.ib=ib;
        shm->rules[id].trule.tf=tf;
        //if e2 was not set them we have float
        if(e2 == 0){
          shm->rules[id].trule.e2.eflag = 0;
          shm->rules[id].trule.e2.rval.value = e2val;
        }else{
          shm->rules[id].trule.e2.eflag = 1;
          shm->rules[id].trule.e2.rval.eid=e2;
        }
        break;
      case 'c':
        shm->rules[id].crule.rtype='c';
        strcpy(&shm->rules[id].crule.ctext,ctext);
        break;
      case 'w':
        shm->rules[id].wrule.rtype='w';
        strcpy(&shm->rules[id].wrule.ctext,ctext);
        shm->rules[id].wrule.delay = (short int) kc;
        shm->rules[id].wrule.delay2 = (short int) lmin;
        shm->rules[id].wrule.interval = (short int) ti;
        shm->rules[id].wrule.state = (short int) e_co;
        shm->rules[id].wrule.wdata = (short int) e_pv;
        shm->rules[id].wrule.tnt = (byte_t) n1;
        shm->rules[id].wrule.sns = (byte_t) n2;
        break;
    }
    write_rules();
    shm->config->reload = -1;
    // We called ourselves. Special body tag contents
    printf ("Content-type: text/html\n\n");
    printf("<body onload='window.opener.location = window.opener.location;self.close();'>");
    hdr = 1;
  }

  if (rtype == 'p'){

    // x is delete
    if (action == 'x'){
      for (r=id;r<(shm->counts->pid_count-1);r++){
        memcpy(&shm->pids[r],&shm->pids[r+1],sizeof(pid_struct));
      }
      shm->counts->pid_count--;
      write_pids(shm->pids,shm->counts->pid_count);
      shm->config->reload = -1;
      printf ("Content-type: text/html\n\n");
      printf("<body onload='window.opener.location = window.opener.location;self.close();'>");
      hdr = 1;
    }
    // either edit or new....
    if (edit){

      // Option 1
      if (id == shm->counts->pid_count){
        shm->counts->pid_count++;
      }

      shm->pids[id].e_ctl = e_ctl;
      shm->pids[id].e_pv = e_pv;
      shm->pids[id].e_co = e_co;
      shm->pids[id].e_load = e_load;
      shm->pids[id].e_sp = e_sp;
      shm->pids[id].kc=kc;
      shm->pids[id].ti=ti;
      shm->pids[id].lmin=lmin;
      shm->pids[id].lmax=lmax;
      shm->pids[id].coMin=coMin;
      shm->pids[id].coMax=coMax;
      shm->pids[id].purgeLimit=purgeLimit;
      shm->pids[id].purgeCycle=purgeCycle;
      shm->pids[id].purgeInterval=purgeInterval;

      write_pids(shm->pids,shm->counts->pid_count);
      shm->config->reload = -1;
      printf ("Content-type: text/html\n\n");
      printf("<body onload='window.opener.location = window.opener.location;self.close();'>");
      hdr = 1;
    }
  } // Done with actions for pids

  if (hdr == 0){
    // We called ourselves. Special body tag contents
    printf ("Content-type: text/html\n\n");
    printf ("<html><head><link rel=stylesheet type=text/css href=/vesta.css></head>\n");

    //printf("<body onload='window.opener.location = window.opener.location;self.close();'>");
    printf("<body>");
  }else{
    exit(0);
  }

  // Make sure there's some view selected. Default is 'home'

  if ((rule + element + physical + logs + sysview) == 0){
    home = 1;
  } 

  // Start HTML
  printf ("<h3>Vesta System Web Interface</h3>\n");


  printf("<tr>\n<td>\n<table>\n");

  // Could be showing existing or creating new. If new, show all choices
  r = 1;
  if (action == 'n'){
    r = 0;
    //if (shm->rules[id].mrule.rtype == 'p'){
    if (rtype == 'p'){
      prule_line(id,r);
    }else{
      printf("<hr><table><tr colspan 4><b>Mathematical Rule</b></tr>\n");
      mrule_line(id,r);
      printf("</table>\n");

      printf("<hr><table><tr colspan 4><b>Differential Rule</b></tr>\n");
      drule_line(id,r);
      printf("</table>\n");

      printf("<hr><table><tr colspan 4><b>Logical Rule</b></tr>\n");
      arule_line(id,r);
      printf("</table>\n");

      printf("<hr><table><tr colspan 4><b>Timer Rule</b></tr>\n");
      trule_line(id,r);
      printf("</table>\n");

      printf("<hr><table><tr colspan 4><b>Warning</b></tr>\n");
      wrule_line(id,r);
      printf("</table>\n");

      printf("<hr><table><tr colspan 4><b>Comment</b></tr>\n");
      crule_line(id,r);
      printf("</table>\n");
    }
  }else{
    switch (rtype){
      case 'm': mrule_line(id,r);
      break;
      case 'd': drule_line(id,r);
      break;
      case 'a': arule_line(id,r);
      break;
      case 't': trule_line(id,r);
      break;
      case 'c': crule_line(id,r);
      break;
      case 'w': wrule_line(id,r);
      break;
      case 'p': prule_line(id,r);
      break;
    }
  }

  printf("</table>\n");

  printf("</body>\n</html>\n");

}

void print_rule_selection(int existing, int element_index, char etype, char ctype, int rule_field, char *name){ 

  if (etype == 'v' || etype == 'c'){
    printf("<option class=grey ");
  }else{
    switch (ctype) {
      case 'o':
        printf("<option class=blue ");
        break;
      case 'c':
        printf("<option class=yellow ");
        break;
      case 'i':
        printf("<option class=green ");
        break;
      case 's':
        printf("<option ");
        break;
    }
  }
  //print_rule_selection(existing, j, shm->elements[j].pvn, shm->elements[j].ptype, shm->rules[i].drule.e1, shm->elements[j].name);

  if (existing) {
    printf("value=%d %s>%s</option>\n",element_index,(element_index==rule_field ? "selected" : ""),name);
  }else{
    printf("value=%d>%s</option>\n",element_index,name);
  }
}

void crule_line(int i, int existing) {
  int j;

  printf("<form action=vestaRuleEdit method=get>\n");

  printf("<tr><td class=smallpad>// <input type=text name=ctext size=72  maxlength=78 value=\"%s\"></td>\n",shm->rules[i].crule.ctext);

  // Update button
  printf("<td><input type=hidden name=view value=rule>\n");
  printf("<input type=hidden name=id value=%d>\n",i);
  printf("<input type=hidden name=rtype value=c>\n");
  if(existing == 1){
    printf("<input type=hidden name=action value=e>\n");
    printf("<input type=submit name=edit value=Update></td>\n");
  }else{
    printf("<input type=hidden name=action value=n>\n");
    printf("<input type=submit name=edit value=Create></td>\n");
  }
  printf("</form>\n");

}

void mrule_line(int i, int existing) {
  int j;

  printf("<form action=vestaRuleEdit method=get>\n");

  printf("<tr><td class=smallpad>Set <select name=target>\n");
  printf("<option value=0 selected>--</option>\n");

  for (j=shm->elements[1].next;j != -1;j = shm->elements[j].next){
    // valid targets would be outputs and virtual outputs
    if (shm->elements[j].io == 'o'){
      print_rule_selection(existing, j, shm->elements[j].pvn, shm->elements[j].ptype, shm->rules[i].mrule.target, shm->elements[j].name);
    }
  }
  printf("</select></td>\n");

  printf("<td class=smallpad align=right>to <select name=e1>\n");
  if (!existing || !shm->rules[i].mrule.e1.eflag){
    printf("<option value=0 selected>--</option>\n");
  }else{
    printf("<option value=0>--</option>\n");
  }
  for (j=shm->elements[1].next;j != -1;j = shm->elements[j].next){
    // Sensors and constants valid here
    if (shm->elements[j].ptype == 's' || shm->elements[j].pvn == 'v'){
      print_rule_selection(existing, j, shm->elements[j].pvn, shm->elements[j].ptype, shm->rules[i].mrule.e1.rval.eid, shm->elements[j].name);
    }
  }
  printf("</select>\n");
  printf("<br>\n");
  // If existing float, show value else blank input box
  if(existing && !shm->rules[i].mrule.e1.eflag){
    printf("<input name = e1val type=text size=10 value=%f>",shm->rules[i].mrule.e1.rval.value);
  }else{
    printf("<input name = e1val type=text size=10>");
  }

  printf("</td>\n");


  printf("<td class=smallpad><select name=op>\n");
  printf("<option value=\"+\" %s>plus </option>\n",(shm->rules[i].mrule.op == '+' ? "selected" : ""));
  printf("<option value=\"-\" %s>minus </option>\n",(shm->rules[i].mrule.op == '-' ? "selected" : ""));
  printf("<option value=\"*\" %s>times </option>\n",(shm->rules[i].mrule.op == '*' ? "selected" : ""));
  printf("<option value=\"/\" %s>divided by </option>\n",(shm->rules[i].mrule.op == '/' ? "selected" : ""));
  printf("</select></td>\n");

  printf("<td class=smallpad align=right><select name=e2>\n");
  if (!existing || !shm->rules[i].mrule.e2.eflag){
    printf("<option value=0 selected>--</option>\n");
  }else{
    printf("<option value=0>--</option>\n");
  }
  for (j=shm->elements[1].next;j != -1;j = shm->elements[j].next){
    // Sensors and constants valid here
    if (shm->elements[j].ptype == 's' || shm->elements[j].pvn == 'v'){
      print_rule_selection(existing, j, shm->elements[j].pvn, shm->elements[j].ptype, shm->rules[i].mrule.e2.rval.eid, shm->elements[j].name);
    }
  }
  printf("</select>\n");
  printf("<br>\n");
  // If existing float, show value else blank input box
  if(existing && !shm->rules[i].mrule.e2.eflag){
    printf("<input name = e2val type=text size=10 value=%f>",shm->rules[i].mrule.e2.rval.value);
  }else{
    printf("<input name = e2val type=text size=10>");
  }

  printf("</td>\n");


  // Update button
  printf("<td><input type=hidden name=view value=rule>\n");
  printf("<input type=hidden name=id value=%d>\n",i);
  printf("<input type=hidden name=rtype value=m>\n");
  if(existing == 1){
    printf("<input type=hidden name=action value=e>\n");
    printf("<input type=submit name=edit value=Update></td>\n");
  }else{
    printf("<input type=hidden name=action value=n>\n");
    printf("<input type=submit name=edit value=Create></td>\n");
  }
  printf("</form>\n");

}

// Print a line for a logical (assertion) rule. Could be existing or a form to add a new line

void arule_line(int i, int existing){

  int j;

  printf("<form action=vestaRuleEdit method=get>\n");

  printf("<tr><td class=smallpad>Set <select name=target>\n");
  if (!existing){
    printf("<option value=0 selected>--</option>\n");
  }else{
    printf("<option value=0>--</option>\n");
  }
  for (j=0;j != -1;j = shm->elements[j].next){

    // valid targets would be outputs and variables
    if (shm->elements[j].io == 'o'|| shm->elements[j].pvn == 'v'){
      print_rule_selection(existing, j, shm->elements[j].pvn, shm->elements[j].ptype, shm->rules[i].arule.target, shm->elements[j].name);
    }
  }
  printf("</select></td>\n");

  // v1

  printf("<td class=smallpad align=right>to <select name=v1>\n");
  printf("<option value=0 %s>--</option>\n",(shm->rules[i].arule.e1==0 ? "selected" : ""));
  for (j=0;j != -1;j = shm->elements[j].next){
    // valid choices would be anything
    print_rule_selection(existing, j, shm->elements[j].pvn, shm->elements[j].ptype, shm->rules[i].arule.v1.rval.eid, shm->elements[j].name);
  }
  printf("</select>\n");
  printf("<br>\n");
  // If existing float, show value else blank input box
  if(existing && !shm->rules[i].arule.v1.eflag){
    printf("<input name = v1val type=text size=10 value=%f>",shm->rules[i].arule.v1.rval.value);
  }else{
    printf("<input name = v1val type=text size=10>");
  }

  printf("</td>\n");

  // e1 / n1

  printf("<td class=smallpad>if <select name=e1>\n");
  printf("<option value=0 %s>--</option>\n",(shm->rules[i].arule.e1==0 ? "selected" : ""));
  for (j=0;j != -1;j = shm->elements[j].next){
    // valid choices would be anything
    print_rule_selection(existing, j, shm->elements[j].pvn, shm->elements[j].ptype, shm->rules[i].arule.e1, shm->elements[j].name);
  }
  printf("</select></td>\n");

  printf("<td class=smallpad><select name=n1>\n");
  printf("<option value=0 %s>is true</option>\n",(shm->rules[i].arule.n1 == 0 ? "selected" : ""));
  printf("<option value=1 %s>is not true</option>\n",(shm->rules[i].arule.n1 == 1 ? "selected" : ""));
  printf("</select></td>\n");

  // e2 / n2

  printf("<td class=smallpad>and <select name=e2>\n");
  printf("<option value=0 %s>--</option>\n",(shm->rules[i].arule.e2==0 ? "selected" : ""));
  for (j=shm->elements[1].next;j != -1;j = shm->elements[j].next){
    print_rule_selection(existing, j, shm->elements[j].pvn, shm->elements[j].ptype, shm->rules[i].arule.e2, shm->elements[j].name);
  }
  printf("</select></td>\n");

  printf("<td class=smallpad><select name=n2>\n");
  printf("<option value=0 %s>is true</option>\n",(shm->rules[i].arule.n2 == 0 ? "selected" : ""));
  printf("<option value=1 %s>is not true</option>\n",(shm->rules[i].arule.n2 == 1 ? "selected" : ""));
  printf("</select></td>\n");

  // e3 / n3

  printf("<td class=smallpad>and <select name=e3>\n");
  printf("<option value=0 %s>--</option>\n",(shm->rules[i].arule.e3==0 ? "selected" : ""));
  for (j=shm->elements[1].next;j != -1;j = shm->elements[j].next){
    print_rule_selection(existing, j, shm->elements[j].pvn, shm->elements[j].ptype, shm->rules[i].arule.e3, shm->elements[j].name);
  }
  printf("</select></td>\n");

  printf("<td class=smallpad><select name=n3>\n");
  printf("<option value=0 %s>is true</option>\n",(shm->rules[i].arule.n3 == 0 ? "selected" : ""));
  printf("<option value=1 %s>is not true</option>\n",(shm->rules[i].arule.n3 == 1 ? "selected" : ""));
  printf("</select></td>\n");

  // Update button
  printf("<td><input type=hidden name=view value=rule>\n");
  printf("<input type=hidden name=id value=%d>\n",i);
  printf("<input type=hidden name=rtype value=a>\n");
  if(existing == 1){
    printf("<input type=hidden name=action value=e>\n");
    printf("<input type=submit name=edit value=Update></td>\n");
  }else{
    printf("<input type=hidden name=action value=n>\n");
    printf("<input type=submit name=edit value=Create></td>\n");
  }

  printf("</tr></form>\n");

}

void drule_line(int i, int existing) {
  int j;

  printf("<form action=vestaRuleEdit method=get>\n");

  printf("<tr><td class=smallpad>Set <select name=target>\n");
  printf("<option value=0 selected>--</option>\n");
  for (j=shm->elements[1].next;j != -1;j = shm->elements[j].next){
    // valid targets would be outputs and virtual outputs
    if (shm->elements[j].io == 'o'){
      print_rule_selection(existing, j, shm->elements[j].pvn, shm->elements[j].ptype, shm->rules[i].drule.target, shm->elements[j].name);
    }
  }
  printf("</select></td>\n");
  //printf(" i=%d e1=%d ",i,shm->rules[i].drule.e1);
  printf("<td class=smallpad>if <select name=e1>\n");
  if (!existing) printf("<option value=0 selected>--</option>\n");
  for (j=shm->elements[1].next;j != -1;j = shm->elements[j].next){
    // only sensors valid here
    if (shm->elements[j].ptype == 's' || shm->elements[j].io == 'o'){
      print_rule_selection(existing, j, shm->elements[j].pvn, shm->elements[j].ptype, shm->rules[i].drule.e1, shm->elements[j].name);
    }
  }
  printf("</select></td>\n");

  printf("<td class=smallpad align=right>is at least <select name=diff>\n");
  if (!existing || !shm->rules[i].drule.diff.eflag){
    printf("<option value=0 selected>--</option>\n");
  }else{
    printf("<option value=0>--</option>\n");
  }
  for (j=shm->elements[1].next;j != -1;j = shm->elements[j].next){
    // only constants valid here
    if (shm->elements[j].pvn == 'v'){
      print_rule_selection(existing, j, shm->elements[j].pvn, shm->elements[j].ptype, shm->rules[i].drule.diff.rval.eid, shm->elements[j].name);
    }
  }
  printf("</select>\n");
  printf("<br>\n");
  // If existing float, show value else blank input box
  if(existing && !shm->rules[i].drule.diff.eflag){
    printf("<input name = diffval type=text size=10 value=%f>",shm->rules[i].drule.diff.rval.value);
  }else{
    printf("<input name = diffval type=text size=10>");
  }

  printf("</td>\n");

  printf("<td class=smallpad><select name=gle>\n");
  printf("<option value=g %s>greater than</option>\n",(shm->rules[i].drule.gle == 'g' ? "selected" : ""));
  printf("<option value=l %s>less than</option>\n",(shm->rules[i].drule.gle == 'l' ? "selected" : ""));
  printf("<option value=e %s>equal to</option>\n",(shm->rules[i].drule.gle == 'e' ? "selected" : ""));
  printf("</select></td>\n");

  printf("<td class=smallpad align=right><select name=e2>\n");
  if (!existing || !shm->rules[i].drule.e2.eflag){
    printf("<option value=0 selected>--</option>\n");
  }else{
    printf("<option value=0>--</option>\n");
  }
  for (j=shm->elements[1].next;j != -1;j = shm->elements[j].next){
    // Sensors and constants valid here
    if (shm->elements[j].ptype == 's' || shm->elements[j].pvn == 'v'){
      print_rule_selection(existing, j, shm->elements[j].pvn, shm->elements[j].ptype, shm->rules[i].drule.e2.rval.eid, shm->elements[j].name);
    }
  }
  printf("</select>\n");
  printf("<br>\n");
  // If existing float, show value else blank input box
  if(existing && !shm->rules[i].drule.e2.eflag){
    printf("<input name = e2val type=text size=10 value=%f>",shm->rules[i].drule.e2.rval.value);
  }else{
    printf("<input name = e2val type=text size=10>");
  }

  printf("</td>\n");

  printf("<td class=smallpad align=right>with a deadband of <select name=db>\n");
  if (!existing || !shm->rules[i].drule.deadband.eflag){
    printf("<option value=0 selected>--</option>\n");
  }else{
    printf("<option value=0>--</option>\n");
  }
  for (j=shm->elements[1].next;j != -1;j = shm->elements[j].next){
    // only constants valid here
    if (shm->elements[j].pvn == 'v'){
      print_rule_selection(existing, j, shm->elements[j].pvn, shm->elements[j].ptype, shm->rules[i].drule.deadband.rval.eid, shm->elements[j].name);
    }
  }
  printf("</select>\n");
  printf("<br>\n");
  // If existing float, show value else blank input box
  if(existing && !shm->rules[i].drule.deadband.eflag){
    printf("<input name = dbval type=text size=10 value=%f>",shm->rules[i].drule.deadband.rval.value);
  }else{
    printf("<input name = dbval type=text size=10>");
  }

  printf("</td>\n");

  // Update button
  printf("<td><input type=hidden name=view value=rule>\n");
  printf("<input type=hidden name=id value=%d>\n",i);
  printf("<input type=hidden name=rtype value=d>\n");
  if(existing == 1){
    printf("<input type=hidden name=action value=e>\n");
    printf("<input type=submit name=edit value=Update></td>\n");
  }else{
    printf("<input type=hidden name=action value=n>\n");
    printf("<input type=submit name=edit value=Create></td>\n");
  }
  printf("</form>\n");

}


// Trule line

void trule_line(int i, int existing) {
  int j;

  printf("<form action=vestaRuleEdit method=get>\n");

  printf("<tr><td class=smallpad>Set <select name=target>\n");

  printf("<option value=0 selected>--</option>\n");

  for (j=shm->elements[1].next;j != -1;j = shm->elements[j].next){
    // valid targets are variables only
    if (shm->elements[j].pvn == 'v'){
      print_rule_selection(existing, j, shm->elements[j].pvn, shm->elements[j].ptype, shm->rules[i].trule.target, shm->elements[j].name);
    }
  }
  printf("</select></td>\n");

  // e2
  // could be either element of float value, could be existing or new
  printf("<td class=smallpad align=right>for <select name=e2>\n");
  // If new of float, show unselected option
  if (!existing || !shm->rules[i].trule.e2.eflag){
    printf("<option value=0 selected>--</option>\n");
  }else{
    printf("<option value=0>--</option>\n");
  }
  for (j=shm->elements[1].next;j != -1;j = shm->elements[j].next){
    // valid values would be sensor inputs and variables
    if (shm->elements[j].pvn == 'v' || (shm->elements[j].pvn == 'p' && shm->elements[j].ptype == 's')) {
      print_rule_selection(existing, j, shm->elements[j].pvn, shm->elements[j].ptype, shm->rules[i].trule.e2.rval.eid, shm->elements[j].name);
    }
  }
  printf("</select>\n");
  printf("<br>\n");
  // If existing float, show value else blank input box
  if(existing && !shm->rules[i].trule.e2.eflag){
    printf("<input name = e2val type=text size=10 value=%f>",shm->rules[i].trule.e2.rval.value);
  }else{
    printf("<input name = e2val type=text size=10>");
  }

  printf("</td>\n");

  // e1

  printf("<td class=smallpad> seconds when <select name=e1>\n");
  if (!existing) printf("<option value=0 selected>--</option>\n");
  for (j=shm->elements[1].next;j != -1;j = shm->elements[j].next){
    // valid targets would be outputs and virtual outputs
    if (shm->elements[j].io == 'o' || (shm->elements[j].pvn == 'p' && shm->elements[j].ptype == 'i')){
      print_rule_selection(existing, j, shm->elements[j].pvn, shm->elements[j].ptype, shm->rules[i].trule.e1, shm->elements[j].name);
    }
  }
  printf("</select></td>\n");

  // ib (Is / Becomes)
  printf("<td class=smallpad><select name=ib>\n");
  printf("<option value=0 %s>is</option>\n",(shm->rules[i].trule.ib == 0 ? "selected" : ""));
  printf("<option value=1 %s>becomes</option>\n",(shm->rules[i].trule.ib == 1 ? "selected" : ""));
  printf("</select></td>\n");

  // tf (true / talse)
  printf("<td class=smallpad><select name=tf>\n");
  printf("<option value=1 %s>true</option>\n",(shm->rules[i].trule.tf == 1 ? "selected" : ""));
  printf("<option value=0 %s>false</option>\n",(shm->rules[i].trule.tf == 0 ? "selected" : ""));
  printf("</select></td>\n");


  // Update button
  printf("<td><input type=hidden name=view value=rule>\n");
  printf("<input type=hidden name=id value=%d>\n",i);
  printf("<input type=hidden name=rtype value=t>\n");
  if(existing == 1){
    printf("<input type=hidden name=action value=e>\n");
    printf("<input type=submit name=edit value=Update></td>\n");
  }else{
    printf("<input type=hidden name=action value=n>\n");
    printf("<input type=submit name=edit value=Create></td>\n");
  }
  printf("</form>\n");

}



// PID rule line

void prule_line(int i, int existing) {
  int j;
  float floatval;

  printf("<form action=vestaRuleEdit method=get>\n");

  // co
  printf("<tr><td class=smallpad>Control <select name=e_co>\n");

  printf("<option value=0 selected>--</option>\n");

  for (j=0;j != -1;j = shm->elements[j].next){
    // valid targets are analog and digital outputs only
    if (shm->elements[j].pvn == 'p' && shm->elements[j].io == 'o'){
      print_rule_selection(existing, j, shm->elements[j].pvn, shm->elements[j].ptype, shm->pids[i].e_co, shm->elements[j].name);
    }
  }
  printf("</select></td>\n");

  // pv

  printf("<td class=smallpad>to make <select name=e_pv>\n");
  if (!existing) printf("<option value=0 selected>--</option>\n");
  for (j=0;j != -1;j = shm->elements[j].next){
    // valid values would be sensor inputs or non-state variables
    if ((shm->elements[j].pvn == 'p' && shm->elements[j].ptype == 's') || (shm->elements[j].pvn == 'v' && shm->elements[j].name[0] != '~')) {
      print_rule_selection(existing, j, shm->elements[j].pvn, shm->elements[j].ptype, shm->pids[i].e_pv, shm->elements[j].name);
    }
  }
  printf("</select></td>\n");

  // setpoint

  printf("<td class=smallpad> close to <select name=e_sp>\n");
  if (!existing) printf("<option value=0 selected>--</option>\n");
  for (j=0;j != -1;j = shm->elements[j].next){
    // valid targets would be variables
    if (shm->elements[j].pvn == 'v'){
      print_rule_selection(existing, j, shm->elements[j].pvn, shm->elements[j].ptype, shm->pids[i].e_sp, shm->elements[j].name);
    }
  }
  printf("</select></td>\n");

  // load

  printf("<td class=smallpad>based on <select name=e_load>\n");
  if (!existing) printf("<option value=0 selected>--</option>\n");
  for (j=0;j != -1;j = shm->elements[j].next){
    // valid values would be sensor inputs
    if (shm->elements[j].pvn == 'p' && shm->elements[j].ptype == 's') {
      print_rule_selection(existing, j, shm->elements[j].pvn, shm->elements[j].ptype, shm->pids[i].e_load, shm->elements[j].name);
    }
  }
  printf("</select></td>\n");

  // control

  printf("<td class=smallpad>when <select name=e_ctl>\n");
  if (!existing) printf("<option value=0 selected>--</option>\n");
  for (j=0;j != -1;j = shm->elements[j].next){
    // valid values would be variables and discrete i/o
    if (shm->elements[j].pvn == 'v' || ((shm->elements[j].pvn == 'p') && (shm->elements[j].ptype == 'i' || shm->elements[j].ptype == 'o'))) {
      print_rule_selection(existing, j, shm->elements[j].pvn, shm->elements[j].ptype, shm->pids[i].e_ctl, shm->elements[j].name);
    }
  }
  printf("</select> is true</td>\n");

  printf("</tr><tr>\n");
  /*
  float kc;           // Proportional gain
  float ti;           // Integral damping
  float lmin;         // Load value corresponding to minimum output
  float lmax;         // Load value corresponding to maximum output
  float coMin;        // Minimum allowable output during controlled operation
  float coMax;        // Maximum allowable output during controlled operation
  float purgeLimit;    // Output value below which purge is triggered
  float purgeInterval; // Time between purge cycles
  float purgeCycle;    // Length of purge
  */
  // kc
  floatval = existing ? shm->pids[i].kc : 0;
  printf("<td class=smallpad>Gain =  <input size=6 name=kc value=%2.1f>\n",floatval);

  // ti
  floatval = existing ? shm->pids[i].ti : 0;
  printf("<td class=smallpad>Integral Damping =  <input size=6 name=ti value=%2.1f>\n",floatval);

  // lmin
  floatval = existing ? shm->pids[i].lmin : 0;
  printf("<td class=smallpad>lmin =  <input size=6 name=lmin value=%2.1f>\n",floatval);

  // lmax
  floatval = existing ? shm->pids[i].lmax : 0;
  printf("<td class=smallpad>lmax =  <input size=6 name=lmax value=%2.1f>\n",floatval);

  printf("</tr><tr>\n");

  // coMin
  floatval = existing ? shm->pids[i].coMin : 0;
  printf("<td class=smallpad>Minimum Output =  <input size=6 name=coMin value=%2.1f>\n",floatval);

  // coMax
  floatval = existing ? shm->pids[i].coMax : 0;
  printf("<td class=smallpad>Maximum Output =  <input size=6 name=coMax value=%2.1f>\n",floatval);

  // purgeLimit
  floatval = existing ? shm->pids[i].purgeLimit : 0;
  printf("<td class=smallpad>Purge Limit =  <input size=6 name=purgeLimit value=%2.1f>\n",floatval);

  // purgeInterval
  floatval = existing ? shm->pids[i].purgeInterval : 0;
  printf("<td class=smallpad>Purge Interval =  <input size=6 name=purgeInterval value=%2.1f>\n",floatval);

  // purgeCycle
  floatval = existing ? shm->pids[i].purgeCycle : 0;
  printf("<td class=smallpad>Purge Cycle =  <input size=6 name=purgeCycle value=%2.1f>\n",floatval);

  // Update button
  printf("<td><input type=hidden name=view value=rule>\n");
  printf("<input type=hidden name=id value=%d>\n",i);
  printf("<input type=hidden name=rtype value=p>\n");
  if(existing == 1){
    printf("<input type=hidden name=action value=e>\n");
    printf("<input type=submit name=edit value=Update></td>\n");
  }else{
    printf("<input type=hidden name=action value=n>\n");
    printf("<input type=submit name=edit value=Create></td>\n");
  }
  printf("</form>\n");

}

// Warning rule line

void wrule_line(int i, int existing) {
  int j;
  float floatval;

  printf("<form action=vestaRuleEdit method=get>\n");

  printf("<tr><td class=smallpad>Enter message;recipient;recipient2<br><input type=text name=ctext size=64  maxlength=64 value=\"%s\"></td></tr>\n",shm->rules[i].wrule.ctext);

  // We'll use 'co' for our controlling state
  // use kc for delay 1
  floatval = existing ? shm->rules[i].wrule.delay : 0;
  printf("<td class=smallpad>Send message above after <input size=6 name=kc value=%2.0f> seconds \n",floatval);
  printf("when <select name=e_co>\n");
  printf("<option value=0 selected>--</option>\n");

  for (j=shm->elements[1].next;j != -1;j = shm->elements[j].next){
    // valid values would be variables and discrete i/o
    if (shm->elements[j].pvn == 'v' || ((shm->elements[j].pvn == 'p') && (shm->elements[j].ptype == 'i' || shm->elements[j].ptype == 'o'))) {
      print_rule_selection(existing, j, shm->elements[j].pvn, shm->elements[j].ptype, shm->rules[i].wrule.state, shm->elements[j].name);
    }
  }
  printf("</select>\n");

  // n1 for is / is not
  printf("<select name=n1>\n");
  printf("<option value=1 %s>is true</option>\n",(shm->rules[i].wrule.tnt == 1 ? "selected" : ""));
  printf("<option value=0 %s>is not true</option>\n",(shm->rules[i].wrule.tnt == 0 ? "selected" : ""));
  printf("</select></tr>\n");

  // We'll use 'pv' for our data element
  printf("<tr><td class=smallpad>Include the value of <select name=e_pv>\n");

  printf("<option value=0 selected>--</option>\n");

  for (j=shm->elements[1].next;j != -1;j = shm->elements[j].next){
    // valid values would be anything
    //if (shm->elements[j].pvn == 'v' || ((shm->elements[j].pvn == 'p') && (shm->elements[j].ptype == 'i' || shm->elements[j].ptype == 'o'))) {
      print_rule_selection(existing, j, shm->elements[j].pvn, shm->elements[j].ptype, shm->rules[i].wrule.wdata, shm->elements[j].name);
    //}
  }
  printf("</select></td></tr>\n");

  // use ti for interval
  floatval = existing ? shm->rules[i].wrule.interval : 0;
  printf("<tr><td class=smallpad>Send additional messages every <input size=6 name=ti value=%2.0f> seconds</td></tr>\n",floatval);

  // use lmin for delay 2
  floatval = existing ? shm->rules[i].wrule.delay2 : 0;
  printf("<tr><td class=smallpad>Escalate to second recipient after <input size=6 name=lmin value=%2.0f> seconds</td></tr>\n",floatval);

  // n2 for send / don't send
  printf("<td class=smallpad><select name=n2>\n");
  printf("<option value=0 %s>Don't Send</option>\n",(shm->rules[i].wrule.sns == 0 ? "selected" : ""));
  printf("<option value=1 %s>Send</option>\n",(shm->rules[i].wrule.sns == 1 ? "selected" : ""));
  printf("</select> message when warning is no longer active.</td>\n");

  // Update button
  printf("<td><input type=hidden name=view value=rule>\n");
  printf("<input type=hidden name=id value=%d>\n",i);
  printf("<input type=hidden name=rtype value=w>\n");
  if(existing == 1){
    printf("<input type=hidden name=action value=e>\n");
    printf("<input type=submit name=edit value=Update></td>\n");
  }else{
    printf("<input type=hidden name=action value=n>\n");
    printf("<input type=submit name=edit value=Create></td>\n");
  }
  printf("</form>\n");

}


