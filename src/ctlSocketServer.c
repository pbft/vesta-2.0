/*****************************************************************************/
//    This file is part of the NoFossil Control System Software (NFCSS)
//    Copyright 2007, 2008, 2009 Bill Kuhns
//
//    NFCSS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    any later version.
//    NFCSS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//    You should have received a copy of the GNU General Public License
//    along with NFCSS.  If not, see <http://www.gnu.org/licenses/>.
/*****************************************************************************/

// This software is based on an excellent example of a minimalist web server by Nigel Griffiths:
// http://www.ibm.com/developerworks/systems/library/es-nweb/index.html
// Monitor port 7280 and process request to read and write to shared memory
// 
// Cross site scripting does not allow this to work on multiple controllers. Possible solution
// in FireFox:
// you probably want to grant
// netscape.security.PrivilegeManager.enablePrivilege("UniversalBrowserRead") in your script.

#include <stdio.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <errno.h>
#include "vesta.h"
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#include <sys/socket.h>
#include <netinet/in.h>

#include <arpa/inet.h>

#define BUFSIZE 12000
#define BACKLOG 10
#define ERROR      42
#define LOG        44
#define PROCESSNAME "ctlSocketServer"

void logger(int type, char *s1) {

  switch (type) {
    case ERROR:
      printlog(PROCESSNAME, s1, 1);
      break;
    case LOG:
      printlog(PROCESSNAME, s1, 0);
      break;
  }
  if (type == ERROR)
    exit(3);
}

/* this is a child web server process, so we can exit on errors */
void web(int fd, int hit) {
  int j, file_fd, buflen;
  long i, ret, len;
  char * cptr;

  static char send_data[BUFSIZE + 1];
  static char recv_data[BUFSIZE + 1];
  static char sbuff[40];

  char *rptr;
  int pi, elementid, response, result;
  float fval;
  static char div[40], cmd[40], value[40], cmd2[40], value2[40];
  byte_t dio[20], rmask, newbit;
  float reading;
  char *myip;
  FILE *fp;
  char filename[40];

  int nbytes = BUFSIZE;
  response = 0;
  sprintf(cmd, "");
  sprintf(value, "");
  sprintf(cmd2, "");
  sprintf(value2, "");
  sprintf(recv_data, "");
  sprintf(send_data, "");


  ret = read(fd, recv_data, BUFSIZE); // read Web request in one go
  if (ret == 0 || ret == -1) { // read failure stop now
    sprintf(sbuff, "failed to read browser request: %d", ret);
    logger(ERROR, sbuff);
  }

  /*
  len = 0;
  while ( (ret = read(fd, &recv_data[len], BUFSIZE)) > 0)
  {
    len += ret;
    recv_data[len] = 0;
  }
  */

  if (ret > 0 && ret < BUFSIZE) { // return code is valid chars
    recv_data[ret] = 0; // terminate the buffer
  } else {
    recv_data[0] = 0;
    sprintf(sbuff, "Failed to read browser request: %d", ret);
    logger(ERROR, sbuff);
  }
  sprintf(sbuff, "Read browser request: %d", ret);
  //logger(LOG, sbuff);
  //logger(LOG, recv_data);
  // ************** Parse request *******************************
  //
  // Could be random browser request (favicon.ico, for instance) - "GET /favicon.ico HTTP/1.1"
  // Could be request for web page "GET / HTTP/1.1"
  // Could be command - GET /?get=11&ip=xx.xx.xx.xx HTTP/1.1

  // We don't care about "GET /? or POST /?"
  if(recv_data[0] == 'G'){
    rptr = &recv_data[6];
  }else{
    rptr = &recv_data[7];
  }
   // request string is 'GET /?div=xxx&cmd=value;' - parse it for div, cmd and value
  // div=nw001&get=2;div=nw002&get=3;div=nw003&get=4
  sprintf(send_data,
      "HTTP/1.0 200 OK\nContent-type: text/html\nAccess-Control-Allow-Origin: *\n\n");
  while (sscanf(rptr, "div=%[^'&']&%[^'=']=%[^'&']&", div, cmd, value) == 3) {
    // ************** Command is 'file' ************************************
    if (!strcmp(cmd, "file")) {
      logger(LOG, sbuff);
      //logger(LOG, "processing file: ");
      //logger(LOG,value);
      // div = filename: ctlpanel.txt, for instance
      // value = path of html doc: /private/index.html, for instance

      rptr = strstr(rptr, "file;");
      rptr += 5;
      cptr = strstr(rptr, "HTTP");
      if (cptr){
        cptr += -1;
        //cptr[0] = '\0';
      }

      if (strstr(value, "/private")) {
        sprintf(filename, "/var/www/private/");
      }else {
        if (strstr(value, "/public")) {
          sprintf(filename, "/var/www/public/");
        } else {
          logger(ERROR, "Invalid file path");
          logger(ERROR, value);
        }
      }
      strcat(filename, div);
      sprintf(sbuff, "Wrote file %s", filename);
      logger(LOG, sbuff);
      fp = fopen(filename, "w");
      fprintf(fp, url_decode(rptr));
      fclose(fp);
    }
    // ************** Command is 'set' *************************************
    if (!strcmp(cmd, "set")) {
      sscanf(rptr, "div=%[^'&']&%[^'=']=%[^'&']&%[^'=']=%s", div, cmd, value,
          cmd2, value2);
      // Next parameter is value
      sscanf(value, "%d", &elementid);
      sscanf(value2, "%f", &fval);
      // Check for in range element id
      if (elementid >= shm->counts->element_count || elementid < 0) {
        logger(ERROR, "Invalid Element ID");
      }
      // Physical I/O is more difficult - each type needs special handling.
      // State variables and discrete outputs are treated differently
      if (shm->elements[elementid].pvn == 'p'
          || shm->elements[elementid].name[0] == '~') {
        //pi = shm->elements[elementid].physio;
        // state variable / discrete output. set bit to value2 unless value2 = 2. Then toggle instead.
        if (shm->elements[elementid].ptype == 'o'
            || shm->elements[elementid].name[0] == '~') {
          if (!strcmp(value2, "0")) {
            fval = 0;
          }
          if (!strcmp(value2, "1")) {
            fval = 1;
          }
          if (!strcmp(value2, "2")) {
            fval = elementValue(elementid) ? 0 : 1;
          }
        }
      }
      setElement(elementid, fval, 1);
      sprintf(sbuff, "%s;%1.1f\n", div, fval);
      strcat(send_data, sbuff);
      response = 1;
    }

    // ************** Command is 'get' *************************************
    // Is this one a 'get' command? If so, get element id
    if (!strcmp(cmd, "get")) {
      sscanf(value, "%d", &elementid);
      fval = elementValue(elementid);

      //sprintf(send_data, "%s;%1.1f", div, fval);
      sprintf(sbuff, "%s;%1.1f\n", div, fval);
      strcat(send_data, sbuff);
      response = 1;
    }
    // move pointer
    if (strstr(rptr, ";div") != NULL ) {
      rptr = strstr(rptr, ";div") + 1;
    } else {
      rptr = strstr(rptr, "\n");
    }
  }
  if (response) {
    (void) write(fd, send_data, strlen(send_data));

  }

  close(fd);
  exit(1);
}

int main(int argc, char **argv) {
  int i, port, pid, listenfd, socketfd, hit;
  socklen_t length;
  static struct sockaddr_in cli_addr; /* static = initialised to zeros */
  static struct sockaddr_in serv_addr; /* static = initialised to zeros */

  /* Become deamon + unstopable and no zombies children (= no wait()) */
  if (fork() != 0)
    return 0; /* parent returns OK to shell */
  (void) signal(SIGCLD, SIG_IGN ); /* ignore child death */
  (void) signal(SIGHUP, SIG_IGN ); /* ignore terminal hangups */
  for (i = 0; i < 32; i++)
    (void) close(i); /* close open files */
  (void) setpgrp(); /* break away from process group */

  get_shm(0);
  logger(LOG, "socket server starting");
  /* setup the network socket */
  if ((listenfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    logger(ERROR, "system call: socket");
  port = 7280;
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_addr.s_addr = htonl(INADDR_ANY );
  serv_addr.sin_port = htons(port);
  if (bind(listenfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
    logger(ERROR, "system call: bind");
  if (listen(listenfd, 64) < 0)
    logger(ERROR, "system call: listen");
  for (hit = 1;; hit++) {
    length = sizeof(cli_addr);
    if ((socketfd = accept(listenfd, (struct sockaddr *) &cli_addr, &length))
        < 0)
      logger(ERROR, "system call: accept");
    if ((pid = fork()) < 0) {
      logger(ERROR, "system call: fork");
    } else {
      if (pid == 0) { /* child */
        (void) close(listenfd);
        web(socketfd, hit); /* never returns */
      } else { /* parent */
        (void) close(socketfd);
      }
    }
  }
}
