#include <termios.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/signal.h>
#include <sys/types.h>

#define BAUDRATE B19200
#define MODEMDEVICE "/dev/ttyAM0"
#define _POSIX_SOURCE 1 /* POSIX compliant source */
#define FALSE 0
#define TRUE 1

volatile int STOP = FALSE;

void signal_handler_IO(int status); /* definition of signal handler */
int wait_flag = TRUE; /* TRUE while no signal received */

main() {
  int modem, c, res, n, i, imptr;
  int maxmsg = 25;
  struct termios oldtio, newtio;
  struct sigaction saio; /* definition of signal action */
  sigset_t set = { 0 };
  unsigned char ibuf[255];
  unsigned char imsg[255];
  unsigned char obuf[255];

  /* open the device to be non-blocking (read will return immediatly) */
  modem = open(MODEMDEVICE, O_RDWR | O_NOCTTY | O_NONBLOCK);
  if (modem < 0) {
    perror(MODEMDEVICE);
    exit(-1);
  }

  /* install the signal handler before making the device asynchronous */
  saio.sa_handler = signal_handler_IO;
  saio.sa_mask = set;
  saio.sa_flags = 0;
  saio.sa_restorer = NULL;
  sigaction(SIGIO, &saio, NULL );

  /* allow the process to receive SIGIO */
  fcntl(modem, F_SETOWN, getpid());
  /* Make the file descriptor asynchronous (the manual page says only
   O_APPEND and O_NONBLOCK, will work with F_SETFL...) */
  //fcntl(modem, F_SETFL, FASYNC);
  fcntl(modem, F_SETFL, O_ASYNC|O_NONBLOCK|fcntl(modem, F_GETFL));

  tcgetattr(modem, &oldtio); /* save current port settings */
  /* set new port settings for canonical input processing */
  //newtio.c_cflag = BAUDRATE | CRTSCTS | CS8 | CLOCAL | CREAD;
  newtio.c_cflag = BAUDRATE | CS8 | CLOCAL | CREAD;
  newtio.c_iflag = IGNPAR | ICRNL;
  newtio.c_oflag = 0;
  newtio.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);
  newtio.c_cc[VMIN] = 1;
  newtio.c_cc[VTIME] = 0;
  tcflush(modem, TCIFLUSH);
  tcsetattr(modem, TCSANOW, &newtio);

  // Light off
  /*
  obuf[0] = 0x02;
  obuf[1] = 0x62;
  obuf[2] = 0x2a;
  obuf[3] = 0x26;
  obuf[4] = 0x9e;
  obuf[5] = 0x00;
  obuf[6] = 0x13;
  obuf[7] = 0xff;
  obuf[8] = 0x00;

  */

  // Light status
  obuf[0] = 0x02;
  obuf[1] = 0x62;
  obuf[2] = 0x2a;
  obuf[3] = 0x26;
  obuf[4] = 0x9e;
  obuf[5] = 0x00;
  obuf[6] = 0x19;
  obuf[7] = 0xff;
  obuf[8] = 0x00;


  /* loop while waiting for input. normally we would do something
   useful here */
  n = write(modem, obuf, 8);
  //printf("%d.\n",n);
  imptr = 0;
  while (STOP == FALSE) {

    usleep(200000);
    /* after receiving SIGIO, wait_flag = FALSE, input is available
     and can be read */
    if (wait_flag == FALSE) {
      // imptr    ibuf[0]
      // 0        2         Start of received message
      // 0        <> 2      Error
      // >0       *         building message
      // >MAX     *         Error
      res = read(modem, ibuf, 255);
      /*
      // if we're not in the middle of getting a message, imptr will be 0 and ibuf[0] will be 2
      if(imptr == 0 && ibuf[0] == 0x02){
        strcpy(&imsg[0],ibuf,res);
        imptr += res;
        imsg[imptr] = 0x00;
      }
      if(imptr == 0 && ibuf[0] != 0x02){
        printf("Error: not start of message. %d bytes: %s",res,ibuf);
      }
      */
      printf("%d: ",res);
      for(i=0;i<res;i++){
        printf("%X ",(int)ibuf[i]);
      }
      printf("\n");
      wait_flag = TRUE; /* wait for new input */
    }
  }
  /* restore old port settings */
  tcsetattr(modem, TCSANOW, &oldtio);
}

/***************************************************************************
 * signal handler. sets wait_flag to FALSE, to indicate above loop that     *
 * characters have been received.                                           *
 ***************************************************************************/

void signal_handler_IO(int status) {
  wait_flag = FALSE;
}
