
/*****************************************************************************/
//    This file is part of the NoFossil Control System Software (NFCSS)
//    Copyright 2007, 2008, 2009 Bill Kuhns
//
//    NFCSS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    any later version.

//    NFCSS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with NFCSS.  If not, see <http://www.gnu.org/licenses/>.
/*****************************************************************************/

// *********** BUG LIST ******************************************************

#define PROCESSNAME  FUNCTION_NAME(insteon)
// Monitor shared memory and make control decisions

#include <stdio.h>
#include <fcntl.h>
#include <sys/types.h>
#include "vesta.h"
#include <termios.h>
#include <unistd.h>
#include <sys/signal.h>

#define BAUDRATE B19200
//#define MODEMDEVICE "/dev/ttyAM1"
#define MODEMDEVICE "/dev/ttyS1"
#define _POSIX_SOURCE 1 /* POSIX compliant source */
#define FALSE 0
#define TRUE 1

/*
typedef struct  {
  char address[3];
  short int idevtype;
  long int lastResponse;
  long int nextPoll;
  short int devcount;   // sub-device within single Insteon
  short int battery;
  short int signal;
} insteon_struct;
*/

volatile int STOP = FALSE;

void signal_handler_IO(int status); /* definition of signal handler */

instype_struct itypes[20];
float devstates[200];
int itypecount;
int modem, c, n;
unsigned char obuf[255];

main(){
  long long start_usec;
  FILE *fp;
  int i, x, cycle;
  int sleeptime;
  struct timeval now;

  int msgLength = 25;
  int msgFlag;
  struct termios oldtio, newtio;
  struct sigaction saio; /* definition of signal action */
  sigset_t set = { 0 };
  //unsigned char ibuf[255];
  //unsigned char msg[255];
  char entry[200];

  //sprintf(entry, "setserial /dev/ttyAM1 divisor 48");
  sprintf(entry, "setserial /dev/ttyS1 divisor 48");
  //printf("Executing %s<br>\n",entry);
  system(entry);


  /* open the device to be non-blocking (read will return immediately) */
  modem = open(MODEMDEVICE, O_RDWR | O_NOCTTY | O_NONBLOCK);
  if (modem < 0) {
    perror(MODEMDEVICE);
    exit(-1);
  }

  /* install the signal handler before making the device asynchronous */
  saio.sa_handler = signal_handler_IO;
  saio.sa_mask = set;
  saio.sa_flags = 0;
  saio.sa_restorer = NULL;
  sigaction(SIGIO, &saio, NULL );

  /* allow the process to receive SIGIO */
  fcntl(modem, F_SETOWN, getpid());
  /* Make the file descriptor asynchronous (the manual page says only
   O_APPEND and O_NONBLOCK, will work with F_SETFL...) */
  //fcntl(modem, F_SETFL, FASYNC);
  fcntl(modem, F_SETFL, O_ASYNC|O_NONBLOCK|fcntl(modem, F_GETFL));

  tcgetattr(modem, &oldtio); /* save current port settings */
  /* set new port settings for canonical input processing */
  //newtio.c_cflag = BAUDRATE | CRTSCTS | CS8 | CLOCAL | CREAD;
  newtio.c_cflag = BAUDRATE | CS8 | CLOCAL | CREAD;
  newtio.c_iflag = IGNPAR | ICRNL;
  newtio.c_oflag = 0;
  newtio.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);
  newtio.c_cc[VMIN] = 1;
  newtio.c_cc[VTIME] = 0;
  tcflush(modem, TCIFLUSH);
  tcsetattr(modem, TCSANOW, &newtio);

  // Get shared memory links
  get_shm(INSTEON_ID);

  //===========================================================================================================
  //
  // Initialize and begin the execution loop
  //
  //===========================================================================================================

  start_usec = getNow();
  shm->config->reload = shm->config->reload | ( 1 << INSTEON_ID);
  //imptr = 0;
  cycle = 0;

  // Read Insteon types
  itypecount = readInsteonTypeFile(&itypes[0]);

  // Do forever
  while (1){
    // If someone else changed disk files, reload
    if (reloadRequired(INSTEON_ID)){
      //sprintf(logbuffer, "Reloading files");
      read_vdevs();
      // Initialize devices
      x = 0;
      for(i=2;i<shm->counts->element_count;i++){
        if(shm->elements[i].pvn == 'v' && shm->elements[i].ptype == 'i'){
          shm->elements[i].vdev.insteon.lastResponse = (long int) time(NULL);
          // Set next poll time for really soon
          shm->elements[i].vdev.insteon.nextPoll = (long int) (2 * x++ + 1) + shm->elements[i].vdev.insteon.lastResponse;
          devstates[i] = elementValue(i);
          // Device-specific code: Set r/w for each shared memory variable, claim writeable elements
          // Claim our elements IF we can read from them (and write to shm)
          // 1,"2447D Dimmer",1.000000,0.000000,1,"n",0,3
          // 2,"2843 Door Switch",1.000000,0.000000,1,"n",0,1
          // 3,"2842 Motion Det.",1.000000,0.000000,1,"n",0,1
          // 4,"2443 Micro on/off",1.000000,0.000000,1,"n",0,3
          // 5,"2441 Thermostat",1.000000,0.000000,4,"n",0,3
          // 6,"74551 Garage Door",1,0,2,"n",0,3

          switch(shm->elements[i].vdev.insteon.idevtype){
						case 1:
							claimElement(i,INSTEON_ID);
						break;
						case 2:
							claimElement(i,INSTEON_ID);
						break;
						case 3:
							claimElement(i,INSTEON_ID);
						break;
						case 4:
							claimElement(i,INSTEON_ID);
						break;
						case 5:
							claimElement(i,INSTEON_ID);
						break;
						case 6:
							// Garage door: element 0 is sensor, 1 is command
							if(shm->elements[i].vdev.insteon.devindex == 0){
								claimElement(i,INSTEON_ID);
							}
							break;
						case 7:
							claimElement(i,INSTEON_ID);
						break;
						break;
					}
        }
      }
      printlog(PROCESSNAME, logbuffer, 0);
      // Clear our bit in the reload flag
      clearReloadFlag(INSTEON_ID);
    }

    for(i=2;i<shm->counts->element_count;i++){
      if(shm->elements[i].pvn == 'v' && shm->elements[i].ptype == 'i'){
      	//printf("Device %d rw %d value %d\n",
      	//		(int)shm->elements[i].vdev.insteon.idevtype,
      	//		(int)itypes[shm->elements[i].vdev.insteon.idevtype].rw,
      	//		(int)elementValue(i));
        // Check to see if Vesta changed value for devices we can write to
        if (elementValue(i) != devstates[i] && (itypes[shm->elements[i].vdev.insteon.idevtype].rw & 2)){
          devstates[i] = elementValue(i);
          //printf("Sending msg to device %d\n",i);
          writeDevice(i);
          // sleep 20 msec to allow message to be sent (should actually take 5msec)
          usleep(20000);
          // Increment poll interval to allow device to settle
          shm->elements[i].vdev.insteon.nextPoll += 10;

        }
        // Check if this device is due for poll
        if (shm->elements[i].vdev.insteon.nextPoll <= (long int) time(NULL)){
          if(itypes[shm->elements[i].vdev.insteon.idevtype].poll){
            shm->elements[i].vdev.insteon.nextPoll = (long int) itypes[shm->elements[i].vdev.insteon.idevtype].poll + (long int) time(NULL);
          }else{
            shm->elements[i].vdev.insteon.nextPoll = 255 + (long int) time(NULL);
          }
          pollDevice(i);
        }
      }
    }
    // Sleep for desired interval
    // timedSleep(PROCESSNAME, shm->config->rule_period, &start_usec);
    timedSleep(PROCESSNAME, 5, &start_usec);
  }
/* restore old port settings */
tcsetattr(modem, TCSANOW, &oldtio);
}

// Query device for status. Should result in message in a while.
pollDevice(int i){
  printf("Polling %d\n",i);
  obuf[0] = 0x02;
  obuf[1] = 0x62;
  obuf[2] = shm->elements[i].vdev.insteon.address[0];
  obuf[3] = shm->elements[i].vdev.insteon.address[1];
  obuf[4] = shm->elements[i].vdev.insteon.address[2];
  obuf[5] = 0x00;
  obuf[6] = 0x19;
  obuf[7] = 0x00;
  obuf[8] = 0x00;
  switch ((int) shm->elements[i].vdev.insteon.idevtype){
    case 1:                                     // 2447D Light Dimmer
      n = write(modem, obuf, 8);
    break;
    case 5:                                     // 2441 Thermostat
    	switch (shm->elements[i].vdev.insteon.devindex) {
    	case 0:
        obuf[6] = 0x6A;
        obuf[7] = 0x00;
        n = write(modem, obuf, 8);
      break;
    	case 1:
        obuf[6] = 0x6A;
        obuf[7] = 0x60;
        n = write(modem, obuf, 8);
      break;
    	}
    break;
    case 6:                                     // 2447D Light Dimmer
      n = write(modem, obuf, 8);
    break;
  }
  // We should get response in less than 2 seconds
  shm->elements[i].vdev.insteon.responseDue = (2 + (long int) time(NULL));
}

int checksum(char *msg){
	int i, csum=0;
	for(i=6;i<21;i++){
		csum += (~msg[i]+1);
	}
	return(csum);
}

/************************************************************************************************
 *
 * Update a device with data from shared memory
 *
 ************************************************************************************************/

writeDevice(int i){
  // Message frame
	memset(obuf,0,200);
  obuf[0] = 0x02;
  obuf[1] = 0x62;
  obuf[2] = shm->elements[i].vdev.insteon.address[0];
  obuf[3] = shm->elements[i].vdev.insteon.address[1];
  obuf[4] = shm->elements[i].vdev.insteon.address[2];
  switch ((int) shm->elements[i].vdev.insteon.idevtype){
    case 1:                                     // 2447D Light Dimmer
      // turn light off
      if(elementValue(i) == 0){
        obuf[6] = 0x13;
      }else{
        // light on to specified value
        obuf[6] = 0x11;
        obuf[7] = (char) elementValue(i);
      }
      n = write(modem, obuf, 8);
    break;
    case 5:																			// thermostat
    	switch (shm->elements[i].vdev.insteon.devindex) {
				case 2:										// Heat Setpoint
					obuf[5] = 0x10;					// Extended Message
					obuf[6] = 0x6D;
					obuf[7] = (unsigned char) (elementValue(i) * 2);
					obuf[21] = checksum(obuf);
				break;
				case 3:										// Cool Setpoint
					obuf[5] = 0x10;					// Extended Message
					obuf[6] = 0x6C;
					obuf[7] = (unsigned char) (elementValue(i) * 2);
					obuf[21] = checksum(obuf);
				break;
    	}
    	//02 62 01 78 5A 1F 2F 00 00 02 1F FF 08 E2 EF 14 82 86 00 00 EF CD
      if(elementValue(i) == -1){								// Set for status reporting 1
        obuf[5] = 0x1F;					// Extended Message
        obuf[6] = 0x2F;
        obuf[7] = 0x00;
        obuf[9] = 0x02;
        obuf[10] = 0x1F;
        obuf[11] = 0xFF;
        obuf[12] = 0x08;
        obuf[13] = 0xE2;
        obuf[14] = 0xEF;
        obuf[15] = 0x2B;
        obuf[16] = 0x96;
        obuf[17] = 0xB3;
        obuf[20] = 0xEF;
        obuf[21] = checksum(obuf);
      }
      if(elementValue(i) == -2){								// Set for status reporting 2
        obuf[5] = 0x1F;					// Extended Message
        obuf[6] = 0x2E;
        obuf[7] = 0x00;
        obuf[9] = 0x08;
        obuf[21] = checksum(obuf);
      }
      n = write(modem, obuf, 22);
    break;
    case 6:                                     // Garage Door
    	if(shm->elements[i].vdev.insteon.devindex == 1){
				obuf[5] = 0x0F;
				obuf[7] = 0xFF;
				// turn relay off
				if(elementValue(i) == 0){
					obuf[6] = 0x13;
				}else{
					// turn relay on
					obuf[6] = 0x11;
				}
			  n = write(modem, obuf, 8);
    	}
    break;
    case 8:                                     // 2444 Open/Close (relay)
				obuf[5] = 0x0F;
				obuf[7] = 0xFF;
				// turn relay off
				if(elementValue(i) == 0){
					obuf[6] = 0x13;
				}else{
					// turn relay on
					obuf[6] = 0x11;
				}
			  n = write(modem, obuf, 8);
		break;
    case 9:                                     // 2458 MorningLinc (lock)
				obuf[5] = 0x0F;
				obuf[7] = 0xFF;
				// turn relay off
				if(elementValue(i) == 0){
					obuf[6] = 0x13;
				}else{
					// turn relay on
					obuf[6] = 0x11;
				}
			  n = write(modem, obuf, 8);
    break;
  }

}


printMessage(char *msg, int msglength){
  int i, msgFlag=8;
  if(msg[1] == 0x62){
  	msgFlag = 5;
  }
  for(i=0;i<msglength;i++){
    printf("%2X ",(int)msg[i]);
  }
  for(;i < 12;i++){
  	printf("   ");
  }
  printf("bcast: %d grp: %d ack: %d ext: %d",
      (msg[msgFlag] & 0x80) ? 1 : 0,
      (msg[msgFlag] & 0x40) ? 1 : 0,
      (msg[msgFlag] & 0x20) ? 1 : 0,
      (msg[msgFlag] & 0x10) ? 1 : 0);
  printf("\n");
}

/************************************************************************************************
 *
 * We have a message that came in and seems to be complete. Process it as appropriate.
 *
 ************************************************************************************************/

processMessage(char *msg, int msglength){
  int dimmer, i, cmd1;
  char addr[3];

  // Find out who sent it - address is in message bytes 2,3,4
  i = insteonElementFromAddr(&msg[2],1);
  // If it's from a known device
  printMessage(msg,msglength);
  if(i != -1){
    // Update last response time
    shm->elements[i].vdev.insteon.lastResponse = (long int) time(NULL);
    // Perform any type-specific actions
    switch ((int) shm->elements[i].vdev.insteon.idevtype){
      case 1:                                     // 2447D Light Dimmer
      	if(!process2447D_Dimmer(msg,i)){
          printf("2447 Dimmer unhandled msg: ");
          printMessage(msg,msglength);
      	}
      break;
      case 2:                                     // Door Switch
        process2843DoorSwitch(msg,i);
      break;
      case 3:                                     // motion detector
      	process2843MotionDetector(msg, i);
      break;
      case 5:                                     // Thermostat
      	process2441Thermostat(msg, i);
      break;
      case 6:                                     // Garage Door
      	process74551GarageDoor(msg, i);
      break;
      case 7:                                     // Keypad Dimmer
        printf("2486D Dimmer: ");
        printMessage(msg,msglength);
      	process2486Dimmer(msg, i);
      break;
      case 8:                                     // 2444
      	process2444Micro(msg, i);
      break;
      case 9:                                     // 2458 - Same as 2444
      	process2444Micro(msg, i);
      break;
      default:
        printf("No Handler for %d:\n",(int) shm->elements[i].vdev.insteon.idevtype);
        printMessage(msg,msglength);
        break;
    }
  }else{
    printf("No address match:\n");
    printMessage(msg,msglength);
  }
}

/***************************************************************************
* Individual device message handlers.																	     *
* Should *not* directly send messages to devices - must execute and return *
* very quickly.                                                            *
***************************************************************************/

/* Traffic from Vesta-initiated 'set' command (set to level 'A'):
2 62 2A 26 9E 0 11 A 6 						// ack response from PLM
2 50 2A 26 9E 2B 96 B3 20 11 A		// echo from dimmer (2x)
2 62 2A 26 9E 0 19 0 6						// ack from dimmer (5x)
2 50 2A 26 9E 2B 96 B3 20 2 18		// Mystery (doc says cmd1=2 is 'delete from all-link group) (2x)
2 50 2A 26 9E 2B 96 B3 20 2 A 		// Mystery as well, but cmd2 is commanded level (2x)
*/

/* Traffic from dimmer-initiated level change (set to level '3D'):
2 50 2A 26 9E 0 0 1 CB 17 1				// From dimmer - 17 is 'manual level change', 1 is 'up' (2x)
2 50 2A 26 9E 0 0 1 CB 18 0 			// From dimmer - 18 is 'stopped changing'
(Vesta polls dimmer on seeing cmd1=18)
2 62 2A 26 9E 0 19 0 6 						// ack from dimmer
2 50 2A 26 9E 2B 96 B3 20 2 3D	  // Mystery, but cmd2 is new level (2x)
*/

/* Traffic from dimmer-initiated 'off':
2 50 2A 26 9E 0 0 1 CB 13 0				// From dimmer - 13 is 'set to off' (broadcast?)
2 62 2A 26 9E 0 13 0 6 						// ack from dimmer
(Vesta sets shared memory variable)
2 50 2A 26 9E 2B 96 B3 43 13 1 		// From dimmer - 13 is 'set to off'
2 50 2A 26 9E 13 1 1 CB 6 0
*/

/* Traffic from dimmer-initiated 'on':
2 50 2a.26.9e	 0 0 1 CB 11 0 			// From dimmer - 11 is 'set to on' (broadcast?)
2 62 2A 26 9E 0 19 0 6
2 50 2A 26 9E 2B 96 B3 67 19 0 		// From dimmer - 19 is 'light status request'
2 50 2A 26 9E 2B 96 B3 41 11 1 		// From dimmer - 11 is 'set to on'
2 62 2A 26 9E 0 19 0 6
2 50 2A 26 9E 11 1 1 CB 6 0
2 50 2A 26 9E 2B 96 B3 20 2 42		// Mystery, but cmd2 is new level (2x)
*/

// Strategy: If cmd1=2, set shared memory to cm2 value. Will cause brief error on response to
// Vesta-initiated change.
// On cmd1=18, dimmer has initiated change. Poll to find level.
// On cmd1=11, dimmer initiated 'on'. Poll to get level.
// On cmd1=13, dimmer initiated 'off'. Set shared memory variable to 0

int process2447D_Dimmer(char *msg, int i){
	int cmd2, cmd1, x=0;
	cmd1 = (int) msg[9];
	cmd2 = (int) msg[10];
	if(cmd1 == 0x02 || cmd1 == 0x00 ){
		if ( (int) devstates[i] != cmd2){
			setElement(i,(float)cmd2,0);
			devstates[i] = (float) cmd2;
		}
		x = 1;
	}
	if(cmd1 == 0x11){
		// Force poll next cycle
		shm->elements[i].vdev.insteon.nextPoll = 0;
		x = 1;
	}
	if(cmd1 == 0x13){
		setElement(i,0,0);
		x = 1;
	}
	if(cmd1 == 0x18){
		shm->elements[i].vdev.insteon.nextPoll = 0;
		x = 1;
	}
	return(x);
}

/* Door switch opening
2 50 28 43 D4 0 0 1 CB 11 1
2 50 28 43 D4 2B 96 B3 41 11 1
2 50 28 43 D4 11 1 1 CB 6 0
*/

/* Door switch closing
2 50 28 43 D4 0 0 1 CB 13 1
2 50 28 43 D4 2B 96 B3 41 13 1
2 50 28 43 D4 13 1 1 CB 6 0
*/
process2843DoorSwitch(char *msg, int i){
	int cmd1;
	cmd1 = (int) msg[9];

	if(cmd1 == 0x13){
		setElement(i,1,0);
	}
	if(cmd1 == 0x11){
		setElement(i,0,0);
	}
}

/* Motion Detector triggered
2 50 2D 6D 6D 0 0 1 C7 11 1
2 50 2D 6D 6D 2B 96 B3 41 11 1
2 50 2D 6D 6D 11 2 1 C7 6 0
*/

/* Motion Detector quiet
2 50 2D 6D 6D 0 0 1 C7 13 1
2 50 2D 6D 6D 2B 96 B3 41 13 1
2 50 2D 6D 6D 13 2 1 C7 6 0
*/
process2843MotionDetector(char *msg, int i){
	int cmd1;
	cmd1 = (int) msg[9];

	if(cmd1 == 0x11){
		setElement(i,1,0);
	}
	if(cmd1 == 0x13){
		setElement(i,0,0);
	}
}

// Thermostat. At present this would be a response to a status request. Unfortunately,
// the response doesn't echo the command2 byte that identifies which data was requested.
// Kludge: temp (contrary to docs) is 2x actual temp, which should put it above 100.
// Therefore any value above 100 is temp, below is humidity.
// Temp is index 0, humidity is index 1
// 2 50 2F 7A 9B 2B 96 B3 20 6A 8E
// 2 50 2F 7A 9B 2B 96 B3 20 6A 35

process2441Thermostat(char *msg, int i){
	int cmd1, cmd2, mtype, flags, dev;

	// If cmd1 is 6A, is response to poll. Need to figure out who asked.
	// Search elements for responseDue > now.
	dev = i;
	mtype = (int) msg[1];
	cmd1 = (int) msg[9];
	cmd2 = (int) msg[10];
	//printf("Dev: %d Index: %d Now: %u Due %u\n",dev,shm->elements[dev].vdev.insteon.devindex,time(NULL),shm->elements[dev].vdev.insteon.responseDue);
	if(cmd1 == 0x6A){
		while(shm->elements[dev].vdev.insteon.responseDue < time(NULL) && dev != -1){
			dev = insteonElementFromAddr(&msg[2],dev);
			//printf("Dev: %d Index: %d Now: %u Due %u\n",dev,shm->elements[dev].vdev.insteon.devindex,time(NULL),shm->elements[dev].vdev.insteon.responseDue);
		}
		if(dev == -1){
			printf("Failed to find correct device\n");
			return;
		}

		shm->elements[dev].vdev.insteon.responseDue = time(NULL) -1;
		if(shm->elements[dev].vdev.insteon.devindex == 0){
			// Temp
			setElement(dev,(float)cmd2/2,0);
			//printf("Set Temp\n");
		}
		if(shm->elements[dev].vdev.insteon.devindex == 1){
			// Humidity
			setElement(dev,(float)cmd2,0);
			//printf("Set Humidity\n");
		}
	}
}


/* Garage Door Opening
2 50 2B F9 B 0 0 1 CB 13 0
2 50 2B F9 B 2B 96 B3 41 13 1
2 50 2B F9 B 13 1 1 CB 6 0
*/

/* Garage Door closing
2 50 2B F9 B 0 0 1 CB 11 0
2 50 2B F9 B 2B 96 B3 41 11 1
2 50 2B F9 B 11 1 1 CB 6 0
*/

/* Setting garage door relay
 2 62 2B F9  B  F 11 FF  6          bcast: 0 grp: 0 ack: 0 ext: 0
 2 50 2B F9  B 2B 96 B3 2B 11 FF    bcast: 0 grp: 0 ack: 1 ext: 0
*/

/* Result from poll - position should be closed
 2 62 2B F9  B  0 19  0  6          bcast: 0 grp: 0 ack: 0 ext: 0
 2 50 2B F9  B 2B 96 B3 20  1  0    bcast: 0 grp: 0 ack: 1 ext: 0
 2 62 2B F9  B  0 19  0  6          bcast: 0 grp: 0 ack: 0 ext: 0
 2 50 2B F9  B 2B 96 B3 20  1  0    bcast: 0 grp: 0 ack: 1 ext: 0
*/

// Garage door is dual device. Index 0 is position sensor.
process74551GarageDoor(char *msg, int i){
	int cmd1, flags;

	// Only update index 0 (door position)
	if(shm->elements[i].vdev.insteon.devindex != 0){
		printf("Finding correct device. Originally %d ",i);
		i = insteonElementFromAddr(&msg[2],i);
		printf("now %d\n",i);
		if(i == -1){
			printf("Failed to find correct device\n");
			return;
		}
	}
	flags = (int) msg[8];
	cmd1 = (int) msg[9];
	if(flags == 0x41){
		if(cmd1 == 0x11){
			setElement(i,1,0);
			// Poll in 10 seconds
			//shm->elements[i].vdev.insteon.nextPoll = (int) time(NULL) + 10;
			shm->elements[i].vdev.insteon.nextPoll = 0;
		}
		if(cmd1 == 0x13){
			setElement(i,0,0);
		}
	}
}
/*
On press/activate, button B:
2486D Dimmer:  2 50 22 95 49  0  0  2 C3 11  0    bcast: 1 grp: 1 ack: 0 ext: 0
2486D Dimmer:  2 50 22 95 49 2B 96 B3 43 11  2    bcast: 0 grp: 1 ack: 0 ext: 0 repeated 10x
2486D Dimmer:  2 50 22 95 49 11  1  2 C3  6  1    bcast: 1 grp: 1 ack: 0 ext: 0
On press/deactivate, button B:
2486D Dimmer:  2 50 22 95 49  0  0  2 C3 13  0    bcast: 1 grp: 1 ack: 0 ext: 0
2486D Dimmer:  2 50 22 95 49 2B 96 B3 43 13  2    bcast: 0 grp: 1 ack: 0 ext: 0 repeated 13x
2486D Dimmer:  2 50 22 95 49 13  1  2 C3  6  1    bcast: 1 grp: 1 ack: 0 ext: 0

*/

process2486Dimmer(char *msg, int i){
	int cmd2, cmd1;
	cmd1 = (int) msg[9];
	cmd2 = (int) msg[10];
	if(cmd1 == 0x02 || cmd1 == 0x00 ){
		if ( (int) devstates[i] != cmd2){
			setElement(i,(float)cmd2,0);
			devstates[i] = (float) cmd2;
		}
	}
	if(cmd1 == 0x11){
		// Force poll next cycle
		shm->elements[i].vdev.insteon.nextPoll = 0;
	}
	if(cmd1 == 0x13){
		setElement(i,0,0);
	}
	if(cmd1 == 0x18){
		shm->elements[i].vdev.insteon.nextPoll = 0;
	}
}

/*

'Up' button press
2 50 2C 96 39  0  0  1 CB 11  0    bcast: 1 grp: 1 ack: 0 ext: 0
2 50 2C 96 39 33 5F AB 41 11  1    bcast: 0 grp: 1 ack: 0 ext: 0
2 50 2C 96 39 11  1  1 C7  6  0    bcast: 1 grp: 1 ack: 0 ext: 0

'Down' button press
 2 50 2C 96 39  0  0  1 CB 13  0    bcast: 1 grp: 1 ack: 0 ext: 0
 2 50 2C 96 39 33 5F AB 41 13  1    bcast: 0 grp: 1 ack: 0 ext: 0
 2 50 2C 96 39 13  1  1 CB  6  0    bcast: 1 grp: 1 ack: 0 ext: 0

*/

process2444Micro(char *msg, int i){
	int cmd2, cmd1, flags;
	cmd1 = (int) msg[9];
	cmd2 = (int) msg[10];

	flags = (int) msg[8];
	cmd1 = (int) msg[9];
	//if(flags == 0x41){
		if(cmd1 == 0x11){
			setElement(i,1,0);
			// Poll in 10 seconds
			//shm->elements[i].vdev.insteon.nextPoll = (int) time(NULL) + 10;
			shm->elements[i].vdev.insteon.nextPoll = 0;
		}
		if(cmd1 == 0x13){
			setElement(i,0,0);
		}
	//}
}


/***************************************************************************
* signal handler. sets wait_flag to FALSE, to indicate above loop that     *
* characters have been received.                                           *
***************************************************************************/

void signal_handler_IO(int status) {

  static int i, imptr = 0, res;
  static int msgLength = 25;
  static unsigned char ibuf[255];
  static unsigned char msg[255];

  // We got some data to read. Add it after last byte in buffer.
  imptr += read(modem, &ibuf[imptr], 255);
  ibuf[imptr] = '\0';
  // Now we have bytes in buffer. First *should* be 0x02.
  // Search through buffer for 0x02
  i=0;
  while(ibuf[i] != 0x02 && i<imptr){
  	i++;
  }
  // If we didn't have 0x02 at the start, shift buffer (may zero it out)
  if(i>0){
  	//printf("Junk at start of buffer: ");
  	//printMessage(ibuf,imptr);
    memcpy(&obuf[0],&ibuf[i],imptr-i);
    memcpy(&ibuf[0],&obuf[0],imptr-i);
    imptr -= i;
    ibuf[imptr] = '\0';
    //printf("Cleaned buffer: ");
  	//printMessage(ibuf,imptr);
  }
  // If we have a message left, try and process it

	if(imptr > 0){
		// If NAK, skip
		if(ibuf[2] == 0x15){
			msgLength = 3;
		}else{
			// Set expected message length
			// Add extended message check here
			if(ibuf[1] == 0x62){
				if(ibuf[5] & 0x10){
					msgLength = 23;
				}else{
					msgLength = 9;
				}
			}
			if(ibuf[1] == 0x50){
				msgLength = 11;
			}
			if(ibuf[1] == 0x51){
				msgLength = 25;
			}
		}
	}

  while (imptr >= msgLength){
    // copy message to msg, slide balance of input buffer down.
    memcpy(msg,ibuf,msgLength);
    //printMessage(msg,msgLength);
    if(msgLength > 3 && msg[1] != 0x62){
      processMessage(msg,msgLength);
    }
    if(imptr > msgLength){
      memcpy(&obuf[0],&ibuf[msgLength],imptr - msgLength);
      memcpy(&ibuf[0],&obuf[0],imptr - msgLength);
      imptr -= msgLength;
      if(ibuf[0] == 0x02){
        if(ibuf[2] == 0x15){
          msgLength = 3;
        }else{
          // Set expected message length
    			if(ibuf[1] == 0x62){
    				if(ibuf[5] & 0x10){
    					msgLength = 23;
    				}else{
    					msgLength = 9;
    				}
    			}
    			if(ibuf[1] == 0x50){
    				msgLength = 11;
    			}
    			if(ibuf[1] == 0x51){
    				msgLength = 25;
    			}
        }
      }else{
      	if(res > 0){
					printf("Error: not start of message 2. %d bytes - ",res);
					for(i=0;i<res;i++){
						printf("%X ",(int)ibuf[i]);
					}
					printf("\n");
				}
      }
    }else{
      imptr = 0;
    }

  }


}

/* door open going 'open'
Message: 2 50 28 43 D4  0  0  1 C7 11 1
Message: 2 50 28 43 D4  0  0  1 CB 11 1
Message: 2 50 28 43 D4 2B 96 B3 41 11 1
Message: 2 50 28 43 D4 11  1  1 CB  6 0
Message: 2 50 28 43 D4 11  1  1 CB  6 0

door open going 'closed'
Message: 2 50 28 43 D4 0 0 1 C7 13 1
Message: 2 50 28 43 D4 0 0 1 CB 13 1
Message: 2 50 28 43 D4 2B 96 B3 41 13 1
Message: 2 50 28 43 D4 13 1 1 C7 6 0
Message: 2 50 28 43 D4 13 1 1 C7 6 0


Motion sensor
2 50 2D 6D 6D 0 0 1 CB 11 1
2 50 2D 6D 6D 0 0 1 CB 11 1
2 50 2D 6D 6D 2B 96 B3 41 11 1
2 50 2D 6D 6D 11 2 1 CB 6 0
2 50 2D 6D 6D 11 2 1 CB 6 0
*/

