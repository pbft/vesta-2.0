
/*****************************************************************************/
//    This file is part of the NoFossil Control System Software (NFCSS)
//    Copyright 2007, 2008, 2009 Bill Kuhns
//
//    NFCSS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    any later version.

//    NFCSS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with NFCSS.  If not, see <http://www.gnu.org/licenses/>.
/*****************************************************************************/

// Implement fan control algorithm
// Four basic modes:
//
// 0: Off. We are not controlling fan. Fan is off.
// 1: startup/burnout - attempt to attain maximum temperature slope -
//    hunt either side of current setting, exit when at target or at dropout temp
// 
// 2: maintain - try to hold temp at target. Exit to burnout when unable to reach target for extended period
//
// 3: purge/maintain - run fan for short periodic bursts. Enter when above target by more than a bit, exit when at target

// Stage 3 is only by external comand. Not implemented yet
int stage1(float, float);
int stage2(float, float);

#include <stdio.h>
#include <time.h>
#include <sys/time.h>
#include <fcntl.h>
#include <sys/types.h>
#include "vesta.h"

#define PROCESSNAME  FUNCTION_NAME(fanctl)

#ifndef max
  #define max( a, b ) ( ((a) > (b)) ? (a) : (b) )
#endif

#ifndef min
  #define min( a, b ) ( ((a) < (b)) ? (a) : (b) )
#endif

#define USEC_PER_SEC 1000000
#define purgehigh 1
#define purgelow 60

float oldtemp;
int stage;

main(){

  int lastcycle[MAX_ANALOG_OUT];
  float errsum[MAX_ANALOG_OUT];
  int i;
  int pi;
  float speed;
  float nspeed;
  float ctemp;

  FILE *fp;
  char linebuff[256];

  int ctlelement;
  int tempelement;
  int fanelement;
  int targetelement;
  int startelement;
  long long start_usec;
  long long now_usec;
  char linebuff2[256];

  // Get shared memory links
  get_shm(USR2_ID);

  start_usec = getNow();

  fp = fopen("/usr/local/vesta/data/fan-ctl.csv","r");
  //fgets(linebuff,256,fp);
  fscanf(fp,"%d,%d,%d,%d,%d",
      &ctlelement,&tempelement,&fanelement,&targetelement,&startelement);
  fclose(fp);

  // Assume that we might be on to start
  stage = 1;
  oldtemp = 0;

  speed = elementValue(fanelement);
  stage1(ctemp,0);

  while(1){
    // If we're not supposed to be running, shut off fan
    ctemp = elementValue(tempelement);
    if(elementValue(ctlelement) == 0.0){
      stage = 0;
      nspeed = 0;
      printf("fan: ctl element is zero\n");
    }else{
      // figure out what stage we're in if stage is 0
      if(stage == 0){
        // If 'start' is set, take control
        if(elementValue(startelement) > 0){
          stage = 1;
        }
        // If above 300F, we're at least stage 1
        if(ctemp > 300){
          stage = 1;
        }
        // If above 900F, we're probably stage 2
        if(ctemp > 900){
          stage = 2;
        }
        // If above 1200F, we're stage 2
        if(ctemp > 1200){
          stage = 2;
        }
      }

      printf("fan: Loop speed %f\n",speed);
      // We have a stage now. Process accordingly
      switch (stage){
        case 0:
          //speed = 0;
        break;

        case 1: nspeed = stage1(ctemp,nspeed);
        break;
        case 2: nspeed = stage2(ctemp,nspeed);
        break;
        //case 3: speed = stage3(elements[tempelement].val,speed);
        //break;
      }
    }
    if (stage > 0){
      nspeed = max(30,min(100,nspeed));
    }
    setElement( fanelement, nspeed, 0);
    printf("fan: Stage %d speed set to %f\n",stage,nspeed);

    // Sleep for desired interval
    timedSleep(PROCESSNAME, 50, &start_usec);
  }
}

int stage1(float xtemp, float xfan){
  struct timeval now;
  static long long cold_usec;
  long long now_usec;
  static float lastfan = 0;
  static float dir = 5;
  static float lastdir = 5;
  static int coldtimer;
  static float lastgain = 0;
  float gain;
  float myspeed;

  gettimeofday(&now,NULL);
  now_usec = now.tv_sec * 1000000 + now.tv_usec;

  printf("fan: Stage1 temp %f oldtemp %f lastgain %f input speed %f\n",xtemp, oldtemp, lastgain, xfan);


  // If we just started, set fan to 50 and exit
  if(xfan == 0){
    myspeed = 50;
    lastfan = 50;
    oldtemp = xtemp;
    lastgain = 0;
    lastdir = 5;
    dir = 5;
    cold_usec = now_usec;
    stage = 1;
    return(myspeed);
  }

  // We didn't just start. Many posibilities....
  // If we reached target, return stage 2
  if(xtemp >= 1000){
    lastfan = xfan;
    lastgain = xtemp-oldtemp;
    oldtemp = xtemp;
    cold_usec = now_usec;
    lastdir = 5;
    dir = 5;
    stage = 2;
    return(xfan);
  }

  // If we're cold and have been cold a long time, give up
  if(xtemp < 300){
    coldtimer = (int)(now_usec - cold_usec)/1000000;
    if(coldtimer > 600){
      lastfan = 0;
      oldtemp = xtemp;
      lastdir = -5;
      dir = -5;
      stage = 0;
      return(0);
    }
  }

  // We aren't at target and we haven't given up. See if gain is better or worse than previous
  gain = xtemp - oldtemp;
  oldtemp = xtemp;
  cold_usec = now_usec;
  if(gain < lastgain){
    // If gain is worse, see if we switched directions
    if (dir != lastdir){
      // We switched directions - that was a mistake.
      dir = lastdir;
    }else{
      // We continued in the same direction
    }
    lastdir = lastdir * -1;
    printf("Gain %f is worse than lastgain %f\n",gain,lastgain);
  }
  lastgain = gain;
  oldtemp = xtemp;
  myspeed = xfan+lastdir;
  stage = 1;
  return(myspeed);
}


int stage2(float temp, float fan){
  struct timeval now;
  static long long cold_usec;
  long long now_usec;
  static int coldtimer;
  float speed;

  gettimeofday(&now,NULL);
  now_usec = now.tv_sec * 1000000 + now.tv_usec;

  // Above target - slow down
  if(temp > 1100){
    cold_usec=now_usec;
    speed = fan-5.0;
  }else{
    // Below target - check for timeout
    if(coldtimer > 600){
      stage = 1;
      return(fan);
    }
    // increase fan speed
    speed = fan*5.0;
  }
  stage = 2;
  return(speed);
}



