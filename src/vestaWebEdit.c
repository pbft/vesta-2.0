/*****************************************************************************/
//    This file is part of the NoFossil Control System Software (NFCSS)
//    Copyright 2007, 2008, 2009 Bill Kuhns
//
//    NFCSS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    any later version.
//    NFCSS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//    You should have received a copy of the GNU General Public License
//    along with NFCSS.  If not, see <http://www.gnu.org/licenses/>.
/*****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include "vesta.h"
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/dir.h>
#include <sys/param.h>

extern int alphasort();

/*****************************************************************************/

main() {
  float reading;
  int c, i, j, n, x, vc;
  int physical, element, rule, home, logs, sysview;
  char view[15];
  int subview;
  int e_edit, r_edit, p_edit;
  int stype;
  int pnl, con, ch;
  char chtype[3];
  char pathname[80];
  struct direct **files;
  int file_select();
  int fcount;
  char *ptr;
  float floatval;
  struct timeval now;
  time_t timestamp;
  struct tm * timeinfo;
  char buffer[30];

  char bgcolor[10];
  char entry[200];
  char chname[40];

  byte_t dio[20], rmask;

  // form variables
  int n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12;

  FILE *fp;
  char *myquery, *p[20], *v[20];
  char linebuff[256];
  char action[20];
  char editmode;

  element_struct temp_element;

  get_shm(0);

  printf("Content-type: text/html\n\n<html>");
  printf("<head><link rel=stylesheet type=text/css href=/vesta.css></head>\n");
  printf("<body bgcolor=darkblue>\n");

  /*****************************************************************************/

  // Get form data, process any edits
  /*****************************************************************************/
  myquery = getenv("QUERY_STRING");

  /* break apart parameter string on '&' and '=' characters */

  j = 0;

  // Weirdness: sometimes myquery is NULL, and you don't want to do strlen on it.
  // Other times, it's a zero length string. Either way, don't parse it.

  if (myquery != NULL ) {
    if (strlen(myquery) > 0) {
      while ((myquery = (split(myquery, &p[j], &v[j])))) {
        j++;
      }
      j++;
      cleanup(j, &v[0]);
    }
  }

  // Set element values to shared memory values for all variables
  //set_elements(shm, elements, shm->element_count);

  e_edit = 0;
  r_edit = 0;
  p_edit = 0;
  element = 0;
  rule = 0;
  physical = 0;
  home = 0;
  logs = 0;
  sysview = 0;
  subview = 0;
  floatval = 0;
  n1 = 0;
  strcpy(view,"home");

  if (j > 0) {
    for (i = 0; i < j; i++) {
      //printf("p: %s v: %s i: %d<br>\n", p[i], v[i], i);

      // Set view flag
      if (!strcmp(p[i], "view")) {
        strcpy(view,v[i]);
        if (!strcmp(v[i], "home")) {
          home = 1;
        }
        if (!strcmp(v[i], "physical")) {
          physical = 1;
        }
        if (!strcmp(v[i], "element")) {
          element = 1;
        }
        if (!strcmp(v[i], "rule")) {
          rule = 1;
        }
        if (!strcmp(v[i], "logs")) {
          logs = 1;
        }
        if (!strcmp(v[i], "system")) {
          sysview = 1;
        }
      }

      if (!strcmp(p[i], "subview")) {
        sscanf(v[i], "%d", &subview);
      }

      // Set action
      if (!strcmp(p[i], "action")) {
        strcpy(action, v[i]);
      }

      // Set edit mode
      if (!strcmp(p[i], "editmode")) {
        editmode = v[i][0];
      }

      if (!strcmp(p[i], "n1")) {
        sscanf(v[i], "%d", &n1);
      }

      if (!strcmp(p[i], "n2")) {
        sscanf(v[i], "%d", &n2);
      }

      if (!strcmp(p[i], "n3")) {
        sscanf(v[i], "%d", &n3);
      }

      if (!strcmp(p[i], "n4")) {
        sscanf(v[i], "%d", &n4);
      }

      if (!strcmp(p[i], "n5")) {
        sscanf(v[i], "%d", &n5);
      }

      if (!strcmp(p[i], "n6")) {
        sscanf(v[i], "%d", &n6);
      }

      if (!strcmp(p[i], "n7")) {
        sscanf(v[i], "%d", &n7);
      }

      if (!strcmp(p[i], "n8")) {
        sscanf(v[i], "%d", &n8);
      }

      if (!strcmp(p[i], "n9")) {
        sscanf(v[i], "%d", &n9);
      }

      if (!strcmp(p[i], "n10")) {
        sscanf(v[i], "%d", &n10);
      }

      if (!strcmp(p[i], "n11")) {
        sscanf(v[i], "%d", &n11);
      }

      if (!strcmp(p[i], "n12")) {
        sscanf(v[i], "%d", &n12);
      }

      if (!strcmp(p[i], "name")) {
        sscanf(v[i], "%[^\"] ", &chname[0]);
      }

      if (!strcmp(p[i], "id")) {
        sscanf(v[i], "%d", &c);
      }

      if (!strcmp(p[i], "stype")) {
        sscanf(v[i], "%d", &stype);
      }

      if (!strcmp(p[i], "value")) {
        sscanf(v[i], "%f", &floatval);
        //printf("Floatval = %f ",floatval);
      }
    }

    // All done parsing. Process actions....

    // Process element name / value updates
    if (!strcmp(action, "myelement")) {
      // Get channel.
      setElement(c, floatval, 0);
      strcpy(shm->elements[c].name, chname);
      write_elements();
    }

    // get_update
    if (!strcmp(action, "Fetch")) {
      sprintf(entry,
          "wget -q -O /var/vesta/sd/update.tgz \"http://www.vecs.org/software/2.0/update/update.tgz\"");
      system(entry);
      sprintf(entry,
          "wget -q -O /usr/local/vesta/update/update \"http://www.vecs.org/software/2.0/update/update\"");
      system(entry);
    }

    // Update IP Address
    if (!strcmp(action, "update_ip")) {
      sprintf(entry, "/etc/init.d/getip");
      //printf("Executing %s<br>\n",entry);
      system(entry);
    }

    // backup
    if (!strcmp(action, "Backup")) {
      sprintf(entry,
          "/bin/tar -czf /var/vesta/backups/%s.tgz /usr/local/vesta/data/ /var/www/private /var/www/public",
          chname);
      //printf("Executing %s<br>\n",entry);
      system(entry);
    }

    // restore
    if (!strcmp(action, "Restore")) {
      restoreBackup(chname);
    }

    // Update config file
    if (!strcmp(action, "Update")) {
      shm->config->centigrade = n1;

      shm->config->io_period = n2;
      if (shm->config->io_period < 2) {
        shm->config->io_period = 2;
      }
      if (shm->config->io_period > 100) {
        shm->config->io_period = 100;
      }

      shm->config->rule_period = n3;
      if (shm->config->rule_period < 2) {
        shm->config->rule_period = 2;
      }
      if (shm->config->rule_period > 600) {
        shm->config->rule_period = 600;
      }

      shm->config->log_period = n4;
      if (shm->config->log_period < 10) {
        shm->config->log_period = 10;
      }
      if (shm->config->log_period > 60000) {
        shm->config->log_period = 60000;
      }

      shm->config->sqllog_period = n5;
      if (shm->config->sqllog_period < 10) {
        shm->config->sqllog_period = 10;
      }
      if (shm->config->sqllog_period > 60000) {
        shm->config->sqllog_period = 60000;
      }

      shm->config->pid_period = n6;
      if (shm->config->pid_period < 10) {
        shm->config->pid_period = 10;
      }
      if (shm->config->pid_period > 60000) {
        shm->config->pid_period = 60000;
      }

      shm->config->ha7_period = n7;
      if (shm->config->ha7_period < 1000) {
        shm->config->ha7_period = 1000;
      }
      if (shm->config->ha7_period > 60000) {
        shm->config->ha7_period = 60000;
      }

      shm->config->netio_period = n8;
      if (shm->config->netio_period < 10) {
        shm->config->netio_period = 10;
      }
      if (shm->config->netio_period > 60000) {
        shm->config->netio_period = 60000;
      }

      shm->config->usr1_period = n9;
      if (shm->config->usr1_period < 2) {
        shm->config->usr1_period = 2;
      }
      if (shm->config->usr1_period > 60000) {
        shm->config->usr1_period = 60000;
      }

      shm->config->usr2_period = n10;
      if (shm->config->usr2_period < 2) {
        shm->config->usr2_period = 2;
      }
      if (shm->config->usr2_period > 60000) {
        shm->config->usr2_period = 60000;
      }

      shm->config->usr3_period = n11;
      if (shm->config->usr3_period < 2) {
        shm->config->usr3_period = 2;
      }
      if (shm->config->usr3_period > 60000) {
        shm->config->usr3_period = 60000;
      }

      shm->config->usr4_period = n12;
      if (shm->config->usr4_period < 2) {
        shm->config->usr4_period = 2;
      }
      if (shm->config->usr4_period > 60000) {
        shm->config->usr4_period = 60000;
      }

      write_config();
      shm->config->reload = (unsigned long int) -1;
    }

    // Update sensor type in physical I/O table and file
    if (!strcmp(action, "stype")) {
      shm->elements[c].stype = stype;
      write_elements();
    }

    // Update sensor gain in physical I/O table and file
    if (!strcmp(action, "gain")) {
      shm->elements[c].gain = floatval;
      write_elements();
    }

    // Update sensor offset in physical I/O table and file
    if (!strcmp(action, "offset")) {
      if (!shm->config->centigrade
          && shm->stypes[shm->elements[c].stype].conversion == 't') {
        shm->elements[c].offset = (float) (floatval / 1.8);
      } else {
        shm->elements[c].offset = floatval;
      }

      write_elements();
    }

    // Process element 'delete' requests
    if (!strcmp(action, "delbtn")) {
    	unlinkVariable(c);
    }

    // Process element 'move up' requests
    // A ('n')
    // B n.next
    // C n.next.next
    // D
    // Element c is moving up. After move:
    // a -> c
    // c -> b
    // b -> d
    if (!strcmp(action, "MoveUp")) {
      for (n = 0; n != -1; n = shm->elements[n].next) {
        if (shm->elements[shm->elements[n].next].next == c) {
          // b points to d
          shm->elements[shm->elements[n].next].next = shm->elements[c].next;
          // c points to b
          shm->elements[c].next = shm->elements[n].next;
          // a points to c
          shm->elements[n].next = c;
          break;
        }
      }
      write_elements();
    }

    // Process element 'move down' requests
    // B ('n')
    // C
    // D c.next
    // E c.next.next
    // Element c is moving down. After move:
    // b -> d
    // d -> c
    // c -> e
    // Process element 'move down' requests
    if (!strcmp(action, "MoveDn")) {
      for (n = 0; n != -1; n = shm->elements[n].next) {
        if (shm->elements[n].next == c) {
          j = shm->elements[shm->elements[c].next].next; // e
          // d -> c
          shm->elements[shm->elements[c].next].next = c;
          // b points to d
          shm->elements[n].next = shm->elements[c].next;
          // c points to e
          shm->elements[c].next = j;

          break;
        }
      }
      write_elements();
    }

    // Process element add. Only variables can be added here.
    if (!strcmp(action, "Create")) {
      // Find an unused variable
    	c = linkUnusedVariable();
      strcpy(shm->elements[c].name, chname);
      setElement(c, floatval, 0);

      write_elements();
      shm->config->reload = (unsigned long int) -1;
    }
  }

  printTabs(view);

  // Contained table for elements
  printf("        <tr>\n          <td valign=top>\n          <table width=100%>\n");

  /*****************************************************************************/

  // Show elements
  /*****************************************************************************/

  if (home || element || rule) {

    //printf("element_count = shm->element_count\n");

    // **************** Temporary fix? **********************************
    //

    //config->shminit = 0;

    // Do physical I/O first: read values
    printf(
        "  <tr>\n    <td colspan=3 class=boldblue><b>Sensor Inputs</b></td>\n  </tr>\n");

    // Analog Inputs
    for (i = 1; i != -1; i = shm->elements[i].next) {
      if (shm->elements[i].next != 0) {
        // Different logic for each element type...
        if (shm->elements[i].pvn == 'p' || shm->elements[i].pvn == 'n') {
          if (shm->elements[i].ptype == 's') {
            sprintf(linebuff, "%1.1f", elementValue(i));
            print_element(i, shm->elements[i].owner, linebuff,
                shm->elements[i].name, "white", editmode == 'e', 0);
          }
        }
      }
    }

    // Analog Outputs
    printf("  <tr>\n    <td colspan=3 class=xsmall><hr></td>\n  </tr>\n");
    printf(
        "  <tr>\n    <td colspan=3 class=boldblue><b>Analog Outputs</b></td>\n  </tr>\n");
    for (i = 1; i != -1; i = shm->elements[i].next) {
      if (shm->elements[i].next != 0) {
        // Different logic for each element type...
        if (shm->elements[i].pvn == 'p') {
          if (shm->elements[i].ptype == 'c') {
            sprintf(linebuff, "%1.1f", elementValue(i));
            print_element(i, shm->elements[i].owner, linebuff,
                shm->elements[i].name, "yellow", editmode == 'e', 0);
          }
        }
      }
    }
    //printf("  <tr>\n    <td colspan=3 class=xsmall><hr></td>\n  </tr>\n");
    // If we're not editing, make two side-by-side tables
    //if (!e_edit){
    printf(
        "</table>\n</td>\n<td width=25>&nbsp</td>\n<td valign=top align=left>\n<table>\n");
    // }

    // Discrete Inputs
    printf(
        "  <tr>\n    <td colspan=3 class=boldblue><b>Discrete Inputs</b></td>\n  </tr>\n");
    for (i = 2; i != -1; i = shm->elements[i].next) {
      if (shm->elements[i].next != 0) {
        // Different logic for each element type...
        if (shm->elements[i].pvn == 'p' || shm->elements[i].pvn == 'n') {
          if (shm->elements[i].ptype == 'i') {
            if (elementValue(i) == 0) {
              print_element(i, shm->elements[i].owner, "False",
                  shm->elements[i].name, "lightgreen", editmode == 'e', 0);
            } else {
              print_element(i, shm->elements[i].owner, "True",
                  shm->elements[i].name, "green", editmode == 'e', 0);
            }
          }
        }
      }
    }

    // Discrete outputs
    printf("  <tr>\n    <td colspan=3 class=xsmall><hr></td>\n  </tr>\n");
    printf(
        "  <tr>\n    <td colspan=3 class=boldblue><b>Discrete Outputs</b></td>\n  </tr>\n");
    for (i = 2; i != -1; i = shm->elements[i].next) {
      if (shm->elements[i].next != 0) {
        // Different logic for each element type...
        if (shm->elements[i].pvn == 'p') {
          if (shm->elements[i].ptype == 'o') {
            if (elementValue(i) == 0) {
              print_element(i, shm->elements[i].owner, "False",
                  shm->elements[i].name, "lightblue", editmode == 'e', 0);
            } else {
              print_element(i, shm->elements[i].owner, "True",
                  shm->elements[i].name, "blue", editmode == 'e', 0);
            }
          }
        }
      }
    }
    //printf(" <tr>\n    <td colspan=3 class=xsmall><hr></td>\n  </tr>\n");

    printf(
        "</table>\n</td>\n<td width=25>&nbsp</td>\n<td valign=top align=left>\n<table>\n");

    // Variables
    printf(
        "  <tr>\n    <td colspan=5 class=boldblue><b>State Variables</b></td>\n  </tr>\n");
    for (i = 2; i != -1; i = shm->elements[i].next) {
      if (shm->elements[i].pvn == 'v' && shm->elements[i].name[0] == '~'
          && shm->elements[i].next != 0) {
        if (elementValue(i) == 0) {
          print_element(i, shm->elements[i].owner, "False",
              shm->elements[i].name, "lightblue", editmode == 'e', 0);
        } else {
          print_element(i, shm->elements[i].owner, "True",
              shm->elements[i].name, "blue", editmode == 'e', 0);
        }
      }
    }

    printf(
        "</table>\n</td>\n<td width=25>&nbsp</td>\n<td valign=top align=left>\n<table>\n");

    // Variables
    printf(
        "  <tr>\n    <td colspan=5 class=boldblue><b>Variables</b></td>\n  </tr>\n");
    for (i = 2; i != -1; i = shm->elements[i].next) {
      if (shm->elements[i].pvn == 'v' && shm->elements[i].name[0] != '~'
          && shm->elements[i].next != 0) {
        sprintf(linebuff, "%1.1f", elementValue(i));
        print_element(i, shm->elements[i].owner, linebuff,
            shm->elements[i].name, "#D3D3D3", editmode == 'e', editmode == 'e');
      }
    }

    // Horizontal rule
    //printf("<tr><td colspan=3><hr></td></tr>\n");

    /*****************************************************************************/

    // Form to add new data elements
    /*****************************************************************************/
    //printf ("<table bgcolor=white width=100%>\n");
    if (element) {
      printf("  <tr>\n");
      printf("    <td colspan=5><hr></td>\n");
      printf("  </tr>\n");
      printf("  <tr>\n");
      printf("    <td colspan=5>\n");
      printf("      <table>\n");
      printf("        <form action=vestaWebEdit method=get>\n");
      printf("        <tr>\n");
      printf("          <td class=smallpad>Name</td>\n");
      printf("          <td class=smallpad>Value</td>");
      printf("          <td class=smallpad>&nbsp;</td>");
      printf("        </tr>\n");
      printf("        <tr>");
      printf("          <td><input type=text name=name></td>\n");
      printf("          <td><input type=text name=value size=5>\n");
      printf("              <input type=hidden name=editmode value=e>\n");
      printf(
          "              <input type=hidden name=view value=element></td>\n");
      printf(
          "          <td><input type=submit name=action value=Create></td>\n");
      printf("        </tr>");
      printf("        </form>\n");
      printf("      </table>\n");
      printf("    </td>\n");
      printf("  </tr>\n");
    }
    printf("  <tr>\n");
    //printf("    <td colspan=3><hr></td>\n");
    printf("  </tr>\n");

    // Close contained element table and data / row of white container table
    printf("</table>\n</td>\n</tr>\n");

    //printf("</table>\n");

  }
  if (rule) {
    /*****************************************************************************/

    // Show rules
    /*****************************************************************************/
    // Horizontal rule
    printf("<tr><td colspan=8><hr></td></tr>\n");

    printf("<tr><td colspan=8><b>Rules</b></td></tr>\n");
    printf("<tr><td colspan=8><table>\n");
    //printf("Rulecount = %d, %c\n",shm->rule_count,shm->rules[0].mrule.rtype);
    for (i = 0; i < shm->counts->rule_count; i++) {
      //printf("<td>Rule %d %c target %d</td>\n",i,shm->rules[i].mrule.rtype,shm->rules[i].mrule.target);
      //exit(1);
      switch (shm->rules[i].mrule.rtype) {
        case 'm':
          print_mrule(i, 1);
          break;
        case 'd':
          print_drule(i, 1);
          break;
        case 'a':
          print_arule(i, 1);
          break;
        case 't':
          print_trule(i, 1);
          break;
        case 'c':
          print_crule(i, 1);
          break;
        case 'w':
          print_wrule(i, 1);
          break;
      }
    }
    printf("<tr colspan=8>\n");
    printf(
        "<td class=smallpad><a target=rulewindow href=/cgi-bin/private/vestaRuleEdit?id=%d&action=n>New</a></td>\n",
        shm->counts->rule_count);
    printf("</tr>\n");
    printf("</table></td></tr>\n");

    // Horizontal rule
    printf("<tr><td colspan=8><hr></td></tr>\n");

    /*****************************************************************************/

    // Show rules: PID Rules
    /*****************************************************************************/

    printf("<tr><td colspan=8><b>PID Rules</b></td></tr>\n");
    printf("<tr><td colspan=8><table>\n");
    for (i = 0; i < shm->counts->pid_count; i++) {
      if (shm->pids[i].e_ctl != 0) {
        print_prule(i, 1);
      }
    }
    printf("<tr colspan=8>\n");
    printf(
        "<td class=smallpad><a target=rulewindow href=/cgi-bin/private/vestaRuleEdit?rtype=p&id=%d&action=n>New</a></td>\n",
        i);
    printf("</tr>\n");
    printf("</table>\n</td>\n</tr>\n");
  } // end if (rule)

  // ******************* Logs Tab **********************************

  if (logs == 1) {
    strcpy(&pathname[0], "/var/vesta/sd/logs/");
    //printf("<a href=logdump>log</a><br>\n");
    fcount = scandir(pathname, &files, file_select, alphasort);
    j = 0;
    printf("<table border=0 cellpadding=2 width=100%>\n");
    printf("<tr><td>\n");
    for (i = 1; i < fcount + 1; ++i) {
      if(strlen(files[i - 1]->d_name) == 8){
        printf(
          "<a href=/cgi-bin/private/vestaLogDump?file=/var/vesta/sd/logs/%s>%s</a><br>\n",
          files[i - 1]->d_name, files[i - 1]->d_name);
      }else{
        printf(
          "<a href=/logs/%s>%s</a><br>\n",
          files[i - 1]->d_name, files[i - 1]->d_name);
      }
      if(j++ == 50){
        j=0;
        printf("</td><td>\n");
      }
    }
    printf("</td></tr></table>\n");
  }

  // ******************* System Tab **********************************

  if (sysview == 1) {

    // Wrapper table: 1 row, three cells.
    printf("<table border=1 cellpadding=2 width=100%><tr>\n");

    // First cell: Network & Version, Settings
    printf("<td valign=top>\n");

    printf("<table>\n");

    printf(
        "<tr>\n    <td colspan=2 class=boldblue><b>Network And Version</b></td></tr>\n");

    printf("<tr><td class=smallpad>Serial Number: </td>");
    fp = fopen("/etc/serial", "r");
    fgets(linebuff, 256, fp);
    printf("<td class=smallpad>%s</td></tr>\n", linebuff);
    fclose(fp);

    printf("<tr><td class=smallpad>Internal IP Address: </td>");
    fp = fopen("/var/www/internalip", "r");
    fgets(linebuff, 256, fp);
    printf(
        "<td class=smallpad><a href=vestaWebEdit?view=system&action=update_ip>%s</a></td></tr>\n",
        linebuff);
    fclose(fp);

    printf("<tr><td class=smallpad>External IP Address: </td>");
    fp = fopen("/var/www/externalip", "r");
    fgets(linebuff, 256, fp);
    printf(
        "<td class=smallpad><a href=vestaWebEdit?view=system&action=update_ip>%s</a></td></tr>\n",
        linebuff);
    fclose(fp);

    printf("<tr><td class=smallpad>Installed Software Version: </td>");
    fp = fopen("/var/www/version", "r");
    fgets(linebuff, 256, fp);
    printf("<td class=smallpad>%s</td></tr>\n", linebuff);
    fclose(fp);

    printf("<tr><td class=smallpad>Current Released Version: </td>");
    fp = fopen("/var/www/cversion", "r");
    fgets(linebuff, 256, fp);
    printf("<td class=smallpad>%s</td></tr>\n", linebuff);
    fclose(fp);

    printf(
        "<tr><td class=smallpad>Current Version:</td>");
    printf("<form action=vestaWebEdit method=get>\n");
    printf(
        "<td class=smallpad><input type=hidden name=view value=system><input type=submit name=action value=Fetch></td></tr>\n");
    printf("</form>\n");
    printf("</table>\n");
    printf("<hr>\n");

    // Config form
    printf("<table>\n");
    printf(
        "<tr>\n    <td colspan=2 class=boldblue><b>Settings</b></td></tr>\n");
    printf("<form action=vestaWebEdit method=get>\n");
    int netio_period;
    int usr1_period;
    int usr2_period;
    int usr3_period;
    int usr4_period;

    printf(
        "<tr><td class=smallpad>Centigrade: </td><td><input name=n1 value=1 type=checkbox %s></td></tr>\n",
        (shm->config->centigrade != 0) ? "checked" : "");
    printf(
        "<tr><td class=smallpad>I/O Period (tenths): </td><td><input name=n2 value=%d size=4></td></tr>\n",
        shm->config->io_period);
    printf(
        "<tr><td class=smallpad>Rule Period (tenths): </td><td><input name=n3 value=%d size=4></td></tr>\n",
        shm->config->rule_period);
    printf(
        "<tr><td class=smallpad>Log Period (tenths): </td><td><input name=n4 value=%d size=4></td></tr>\n",
        shm->config->log_period);
    printf(
        "<tr><td class=smallpad>SQL Log Period (tenths): </td><td><input name=n5 value=%d size=4></td></tr>\n",
        shm->config->sqllog_period);
    printf(
        "<tr><td class=smallpad>PID Period (tenths): </td><td><input name=n6 value=%d size=4></td></tr>\n",
        shm->config->pid_period);
    printf(
        "<tr><td class=smallpad>1-wire Period (tenths): </td><td><input name=n7 value=%d size=4></td></tr>\n",
        shm->config->ha7_period);
    printf(
        "<tr><td class=smallpad>Network I/O (tenths): </td><td><input name=n8 value=%d size=4></td></tr>\n",
        shm->config->netio_period);
    printf(
        "<tr><td class=smallpad>User 1 Period (tenths): </td><td><input name=n9 value=%d size=4></td></tr>\n",
        shm->config->usr1_period);
    printf(
        "<tr><td class=smallpad>User 2 Period (tenths): </td><td><input name=n10 value=%d size=4></td></tr>\n",
        shm->config->usr2_period);
    printf(
        "<tr><td class=smallpad>User 3 Period (tenths): </td><td><input name=n11 value=%d size=4></td></tr>\n",
        shm->config->usr3_period);
    printf(
        "<tr><td class=smallpad>User 4 Period (tenths): </td><td><input name=n12 value=%d size=4></td></tr>\n",
        shm->config->usr4_period);

    printf(
        "<tr><td colspan=2 align=left><input type=submit name=action value=Update></td></tr>\n");
    printf("</table>\n");
    printf("<input type=hidden name=view value=system>\n");
    printf("</form>\n");

    // End of first cell
    printf("</td>");

    // Next Cell: Backup & Restore
    printf("<td valign=top>");

    // Backup form

    printf("<form action=vestaWebEdit method=get>\n");
    printf("<table>\n");
    printf(
        "<tr>\n    <td colspan=2 class=boldblue><b>Backup</b></td>\n  </tr>\n");
    printf(
        "<tr><td class=smallpad>Name: </td><td><input name=name size=10></td>\n");
    printf(
        "<td colspan=2 align=left><input type=submit name=action value=Backup></td></tr>\n");
    printf("</table>\n");
    printf("<input type=hidden name=view value=system>\n");
    printf("</form>\n");

    printf("<hr>\n");

    // Show backup files
    strcpy(&pathname[0], "/var/vesta/backups");
    //printf("<a href=logdump>log</a><br>\n");
    fcount = scandir(pathname, &files, file_select, alphasort);
    //printf("Files=%d\n",fcount);
    printf("<table>\n");
    printf(
        "<tr>\n    <td colspan=2 class=boldblue><b>Restore</b></td>\n  </tr>\n");

    for (i = 1; i < fcount + 1; ++i) {
      ptr = rindex(files[i - 1]->d_name, '.');
      if ((ptr != NULL )&& (!strcmp(ptr,".tgz"))){
      printf( "<form action=vestaWebEdit method=get>\n");
      printf("<tr><td class=smallpad>%s<input type=hidden name=name value=%s></td>\n",files[i-1]->d_name,files[i-1]->d_name);
      printf( "<td class=smallpad><input type=hidden name=view value=system><input type=submit name=action value=Restore></td></tr>\n");
      printf("</form>\n");
    }
  }
    printf("</table>\n");

    // End of second cell
    printf("</td>");

    // Next Cell: Files
    printf("<td valign=top>");

    // See individual csv files
    printf("<table>\n");
    printf(
        "<tr>\n    <td colspan=2 class=boldblue><b>Files</b></td>\n  </tr>\n");

    // Links to csv files
    strcpy(&pathname[0], "/usr/local/vesta/etc/");
    fcount = scandir(pathname, &files, file_select, alphasort);
    for (i = 1; i < fcount + 1; ++i) {
      ptr = rindex(files[i - 1]->d_name, '.');
      if ((ptr != NULL )&& (!strcmp(ptr,".csv"))){
        printf("<tr><td class=smallpad>\n");
        printf("<a href=/etc/%s>%s</a><br>\n",files[i-1]->d_name,files[i-1]->d_name);
        printf("</td></tr>\n");
      }
    }
    strcpy(&pathname[0], "/usr/local/vesta/data/");
    fcount = scandir(pathname, &files, file_select, alphasort);
    for (i = 1; i < fcount + 1; ++i) {
      ptr = rindex(files[i - 1]->d_name, '.');
      if ((ptr != NULL )&& (!strcmp(ptr,".csv"))){
        printf("<tr><td class=smallpad>\n");
        printf("<a href=/data/%s>%s</a><br>\n",files[i-1]->d_name,files[i-1]->d_name);
        printf("</td></tr>\n");
      }
    }
    printf("</table>\n");
  }
  printf("</table  >\n");

  // End of last cell. Close row and wrapper table

  printf("</td></tr></table>\n");
  // Close container table
  printf("</td>\n</tr>\n</table >\n");

  printf("</body>\n</html>\n");

}

int file_select(struct direct *entry) {

  if ((strcmp(entry->d_name, ".") == 0) || (strcmp(entry->d_name, "..") == 0))
    return (0);
  else
    return (-1);
}

