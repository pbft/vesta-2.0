/*****************************************************************************/
//    This file is part of the Vesta Control System Software Suite
//    Copyright Bill Kuhns and Vermont Energy Control Systems LLC.
//
//    This is free software: you can redistribute it and/or modify
//    it under the terms of the Lesser GNU General Public License (LGPL)
//    as published by the Free Software Foundation, either version 3 of the
//    License, or the Free Software Foundation, either version 3 of the License,
//    or any later version.
//
//    This software is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    Lesser GNU General Public License for more details.
//
//    You should have received a copy of the Lesser GNU General Public License
//    along with this software.  If not, see <http://www.gnu.org/licenses/>.
/*****************************************************************************/

/*****************************************************************************/
// homepage.c
// Display leftmost tab of web interface, including other tabs with clickable
// links.
// Shows auto-refreshed display of element values and rules, with links to
// public and private auto-generated gui pages.
/*****************************************************************************/

#include <stdio.h>
#include <sys/types.h>
#include <sys/shm.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/dir.h>
#include <sys/param.h>
#include "vesta.h"

main(){
  float reading;
  int i, vc;
  char linebuff[256];

  get_shm(0);

  // Page header and body tag with refresh interval
  printf ("Content-type: text/html\n\n<html>");
  printf ("<head><link rel=stylesheet type=text/css href=/vesta.css>\n");
  printf ("</head>\n");
  printf ("<body bgcolor=darkblue>\n");

  // Print page header and tabs, highlight 'home' tab
  printTabs("home");

  // Contained table for elements
  printf("<tr><td valign=top><table>\n");

  /*****************************************************************************/

  // Show elements

  /*****************************************************************************/

  // Do physical I/O first: read values
  printf("<tr><td colspan=3 class=boldblue><b>Sensor Inputs</b></td></tr>\n");

  for (i=1;i != -1;i = shm->elements[i].next) {
    if (shm->elements[i].next != 0) {
      // Different logic for each element type...
      if (shm->elements[i].pvn == 'p' || shm->elements[i].pvn == 'n') {
        if (shm->elements[i].ptype == 's') {
          sprintf(linebuff, "%1.1f", elementValue(i));
          print_element(i, shm->elements[i].owner, linebuff,
              shm->elements[i].name, "#D3D3D3",0,0);
        }
      }
    }
  }


  // Analog Outputs
  printf("<tr><td colspan=3 class=xsmall><hr></td></tr>\n");
  printf("<tr><td colspan=3 class=boldblue><b>Analog Outputs</b></td></tr>\n");
  for (i=1;i != -1;i = shm->elements[i].next) {
    if (shm->elements[i].next != 0){
      // Different logic for each element type...
      if (shm->elements[i].pvn == 'p') {
        if (shm->elements[i].ptype == 'c') {
          sprintf(linebuff,"%1.1f",elementValue(i));
          print_element(i,shm->elements[i].owner,linebuff,shm->elements[i].name,"yellow",0,0);
        }
      }
    }
  }

  // make two side-by-side tables

  printf("</table></td><td width=25>&nbsp</td><td valign=top align=left><table>\n");

  // Discrete Inputs
  printf("<tr><td colspan=3 class=boldblue><b>Discrete Inputs</b></td></tr>\n");
  for (i=1;i != -1;i = shm->elements[i].next) {
    if (shm->elements[i].next != 0){
      // Different logic for each element type...
      if (shm->elements[i].pvn == 'p') {
        if (shm->elements[i].ptype == 'i') {
          if(elementValue(i) == 0){
            print_element(i,shm->elements[i].owner,"False",shm->elements[i].name,"lightgreen",0,0);
          }else{
            print_element(i,shm->elements[i].owner,"True",shm->elements[i].name,"green",0,0);
          }
        }
      }
    }
  }

  // Discrete outputs
  printf("<tr><td colspan=3 class=xsmall><hr></td></tr>\n");
  printf("<tr><td colspan=3 class=boldblue><b>Discrete Outputs</b></td></tr>\n");
  for (i=1;i != -1;i = shm->elements[i].next) {
    if (shm->elements[i].next != 0){
      // Different logic for each element type...
      if (shm->elements[i].pvn == 'p') {
        if (shm->elements[i].ptype == 'o') {
          if(elementValue(i) == 0){
            print_element(i,shm->elements[i].owner,"False",shm->elements[i].name,"lightblue",0,0);
          }else{
            print_element(i,shm->elements[i].owner,"True",shm->elements[i].name,"blue",0,0);
          }
        }
      }
    }
  }

  // make two side-by-side tables
  printf("</table></td><td width=25>&nbsp</td><td valign=top align=left><table>\n");

  // Variables
  printf("  <tr>\n    <td colspan=5 class=boldblue><b>State Variables</b></td>\n  </tr>\n");
  for (i=1;i != -1;i = shm->elements[i].next) {
    if (shm->elements[i].pvn == 'v' && shm->elements[i].name[0] == '~' && shm->elements[i].next != 0) {
      if(elementValue(i) == 0){
        print_element(i,shm->elements[i].owner,"False",shm->elements[i].name,"lightblue",0,0);
      }else{
        print_element(i,shm->elements[i].owner,"True",shm->elements[i].name,"blue",0,0);
      }
    }
  }

  printf("</table>\n</td>\n<td width=25>&nbsp</td>\n<td valign=top align=left>\n<table>\n");

  // Variables
  printf("  <tr>\n    <td colspan=5 class=boldblue><b>Variables</b></td>\n  </tr>\n");
  for (i=1;i != -1;i = shm->elements[i].next) {
    if (shm->elements[i].pvn == 'v' && shm->elements[i].name[0] != '~' && shm->elements[i].next != 0) {
      sprintf(linebuff,"%1.1f",elementValue(i));
      print_element(i,shm->elements[i].owner,linebuff,shm->elements[i].name,"#D3D3D3",0,0);
    }
  }

  // Close contained element table and data / row of white container table
  printf("</table></td></tr>\n");

  // System Status
  printf("<tr><td colspan=7><hr></td></tr>\n");
  printf("<tr><td class=boldblue valign=top>System Status:</td><td colspan=6 align=left valign=top>");
  printf("<pre>Errors and warnings:\n");
  FILE *ls = popen("tail -4 /var/vesta/sd/logs/errlog.csv", "r");
  char buf[256];
  while (fgets(buf, sizeof(buf), ls) != 0) {
      printf("%s",buf);
  }
  pclose(ls);
  printf("</pre>");
  printf("<pre>Information:\n");
  ls = popen("tail -4 /var/vesta/sd/logs/vesta.csv", "r");
  while (fgets(buf, sizeof(buf), ls) != 0) {
      printf("%s",buf);
  }
  pclose(ls);
  printf("</pre>");

  printf("</td></tr>\n");

  printf("<tr><td colspan=7><hr></td></tr>\n");

  /*****************************************************************************/

  // Show rules

  /*****************************************************************************/

  printf("<tr><td colspan=6><b>Rules</b></td></tr>\n");
  printf("<tr><td colspan=6><table>\n");
  for (i=0;i<shm->counts->rule_count;i++){
    switch (shm->rules[i].mrule.rtype){
      case 'm': print_mrule(i,0);
      break;
      case 'd': print_drule(i,0);
      break;
      case 'a': print_arule(i,0);
      break;
      case 't': print_trule(i,0);
      break;
      case 'c': print_crule(i,0);
      break;
    }
  }
  printf("</table></td></tr>\n");

  // Horizontal rule
  printf("<tr><td colspan=7><hr></td></tr>\n");


  /*****************************************************************************/

  // Show rules: PID Rules

  /*****************************************************************************/

  printf("<tr><td colspan=7><b>PID Rules</b></td></tr>\n");
  printf("<tr><td colspan=7><table>\n");
  for (i=0;i<shm->counts->pid_count;i++){
    if (shm->pids[i].e_ctl != 0){
      print_prule(i,0);
    }
  }
  printf("</table>\n</td>\n</tr>\n");

  // Horizontal rule
  printf("<tr><td colspan=7><hr></td></tr>\n");

  /*****************************************************************************/

  // Control Panel Links

  /*****************************************************************************/

  printf("<tr><td colspan=7><b>Control Panels</b></td></tr>\n");
  printf("<tr><td colspan=3><a href=/public/index.html>Public Control Panel</a></td>\n");
  printf("<td colspan=3><a href=/private/index.html>Private Control Panel</a></td></tr>\n");

  // Close container table
  printf("</td></tr></table>\n");

  printf("</body></html>\n");
  return(0);
}



