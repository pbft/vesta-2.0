/*****************************************************************************/
//    This file is part of the NoFossil Control System Software (NFCSS)
//    Copyright 2007, 2008, 2009 Bill Kuhns
//
//    NFCSS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    any later version.
//    NFCSS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//    You should have received a copy of the GNU General Public License
//    along with NFCSS.  If not, see <http://www.gnu.org/licenses/>.
/*****************************************************************************/

// Implement modified PID algorithm for closed-loop control
// BUGS:
//  - Need to read lower limit and purge variables from config file
#include <stdio.h>
#include <fcntl.h>
#include <math.h>
#include <sys/mman.h>
#include <sys/types.h>
#include "vesta.h"

#define PROCESSNAME  FUNCTION_NAME(pidctl)

main() {

  unsigned long long start_usec;
  unsigned long long now_usec;
  unsigned long long purge_timer[MAX_ANALOG_OUT];
  unsigned long long purge_cycle[MAX_ANALOG_OUT];

  int lastcycle[MAX_ANALOG_OUT];
  float errsum[MAX_ANALOG_OUT];
  int i;

  // Get shared memory links
  get_shm(PID_ID);

  // PID variables. Pid variables are of three sources:
  // 1) Shared memory variables. In this case, the pid_struct contains the element
  //    number of the shared memory variable.
  // 2) Configuration variables. These are read directly from the config file
  // 3) Calculated variables. These may be local if they don't need to be persistent, 
  //    otherwise they're in the pid struct.

  // Pid Struct:
  /*
   short int e_ctl;          // Element that enables this PID channel
   short int e_co;           // Element: Control Output
   short int e_sp;           // Element: Setpoint
   short int e_pv;           // Element: Process variable - what we want to drive to setpoint
   short int e_load;         // Element: Load predictor
   float kc;           // Proportional gain
   float ti;           // Integral damping
   float lmin;         // Load value corresponding to minimum output
   float lmax;         // Load value corresponding to maximum output
   float coMin;        // Minimum allowable output during controlled operation
   float coMax;        // Maximum allowable output during controlled operation
   float purgeLimit;    // Output value below which purge is triggered
   float purgeInterval; // Time between purge cycles
   float purgeCycle;    // Length of purge
   */

  // Variable to hold temporary 'working' values
  float sp;           // Setpoint
  float pv;           // Process variable (temp)
  float co;           // Control Output
  float ctl;          // PID enable element
  float load;         // Load predictor
  float bias;         // Estimated steady state output
  float err;          // Current error
  float coMax;
  float coMin;
  float purgeLimit;
  float purgeInterval;
  float purgeCycle;

  start_usec = getNow();

  // Purge cycle is 180 seconds unless reset
  for (i = 0; i < shm->counts->pid_count; i++) {
    purge_timer[i] = start_usec
        + shm->pids[i].purgeInterval * USEC_PER_SEC;
    purge_cycle[i] = start_usec;
  }

  //******************************************************************************************
  //
  // PID control - modified algorithm. Calculate control output (co) based on control bias (cb)
  // error, controller proportional gain (kc), and controller integral damping (ti). For now, derivative
  // gain is ignored.
  //
  //******************************************************************************************

  for (i = 0; i < shm->counts->pid_count; i++) {
    errsum[i] = 0;
    // Force write of 0 if we are inactive on startup.
    lastcycle[i] = 1;
  }

  // Do forever
  while (1) {
    if (reloadRequired(PID_ID)) {
      // Claim our elements
      if (shm->pids[i].e_ctl != 0) {
        //printf("PID %d ctl = %d\n",i,shm->pids[i].e_ctl);
        for (i = 0; i < shm->counts->pid_count; i++) {
          claimElement(shm->pids[i].e_co, PID_ID);
          //printf("Claiming %d\n",shm->pids[i].e_co);
        }
      }
      // Clear our bit in the reload flag
      clearReloadFlag(PID_ID);
    }

    now_usec = getNow();
    //printf("1: now = %llu ",now_usec);
    for (i = 0; i < shm->counts->pid_count; i++) {
      ctl = elementValue(shm->pids[i].e_ctl);


      // Get shm values

      pv = elementValue(shm->pids[i].e_pv);
      sp = elementValue(shm->pids[i].e_sp);
      load = elementValue(shm->pids[i].e_load);
      coMax = shm->pids[i].coMax;
      coMin = shm->pids[i].coMin;
      purgeLimit = shm->pids[i].purgeLimit;
      purgeInterval = shm->pids[i].purgeInterval;
      purgeCycle = shm->pids[i].purgeCycle;

      //printf("PID: %d ctl element %d value %f\n",i,shm->pids[i].e_ctl,ctl);
      // Basic loop:
      // - Calculate bias
      // - Calcualte error
      // - Calculate errsum
      // - Calculate output
      // - Handle windup if needed
      // - Write output

      // Bias calculation
      // Lmin and lmax are temperature differentials (setpoint - outdoor)
      // co = 100 at lmax
      // co = 0 at lmin
      // assume linear relationship between load variable and co over range lmax to lmin

      bias = 100
          * ((load - (shm->pids[i].lmin))
              / (shm->pids[i].lmax - shm->pids[i].lmin));

      // Calculate error

      err = sp - pv;

      // Calculate errsum

      errsum[i] += err;

      // Calculate output

      co = bias + shm->pids[i].kc * err
          + (shm->pids[i].kc / shm->pids[i].ti) * errsum[i];

      //printf("i=%d, pv=%f, sp=%f, load=%f, Bias=%f, err=%f, errsum=%f, co=%f\n",i,pv,sp,load,bias,err,errsum[i],co);
      //i=0, pv=174.919556, sp=180.000000, load=67.777344, Bias=48.412388, err=5.080444, errsum=68.064163, co=9.762022

      // Handle windup. Current strategy is to just decrement the error that was just added, leaving the sum
      // unchanged. Clearing errsum makes output change suddenly when limit is reached. Just holding errsum
      // constant seems OK - if we overshoot, error will change sign and co will be in bounds next time.
      //printf("CO = %f min = %f max = %f i=%d\n",co,coMin,coMax,i);
      if (co > 100) {
        co = 100;
        errsum[i] -= err;
      }

      if (co < 0) {
        co = 0;
        errsum[i] -= err;
      }

      // Set lower output limit.
      if (co < coMin) {
        co = coMin;
      }

      // Set upper output limit.
      if (co > coMax) {
        co = coMax;
      }

      // If invalid, set to zero. This can happen at startup due to division by zero before values are initialized
      if(isnan(co)){
        co = 0;
      }

      // Purge logic. If purgeCycle is zero, no purge
      if (purgeCycle > 0.0) {

        // If output is above purgeLimit, extend timer
        if (co > purgeLimit) {
          purge_timer[i] = now_usec + (unsigned long long)(purgeInterval * USEC_PER_SEC);
        }

        // Three possibilities:
        // 1: We're in a purge cycle
        // 2: We need to start a purge cycle
        // 3: Business as usual
        // Do we need to start a purge cycle?
        if (purge_timer[i] < now_usec) {
          purge_timer[i] = now_usec + (unsigned long long)(purgeInterval * USEC_PER_SEC);
          purge_cycle[i] = now_usec + (unsigned long long)(purgeCycle * USEC_PER_SEC);
          //printf("s-");
        }
        //printf("2: now = %llu \n",now_usec);

        // If we're in a purge cycle, output is 100
        if (purge_cycle[i] > now_usec) {
          co = coMax;
          //printf("p: %f %llu %llu: %llu\n",purgeCycle,purge_cycle[i],now_usec,purge_cycle[i]-now_usec);
        }
      }
      // Write output. If we are inactive now and were active last cycle, write 0 to output.
      if (ctl == 0) {
        shm->pids[i].active = 0;
        if (lastcycle[i]) {
          co = 0;
          setElement(shm->pids[i].e_co, co, 0);
          lastcycle[i] = 0;
          //printf ("PID exiting: Set ao channel %d to %f\n",shm->pids[i].e_co,co);
        }
        // We're not active - zero errsum
        errsum[i] = 0;
      } else {
        //printf("PID: %d ctl element %d value %f\n",i,shm->pids[i].e_ctl,ctl);
        shm->pids[i].active = 1;
        setElement(shm->pids[i].e_co, co, 0);
        //printf ("Set ao channel %d to %f\n",shm->pids[i].e_co,co);
        lastcycle[i] = 1;
      }
      //1386175461,851,136
      //1386175412,601,159

      //printf ("Set ao channel %d to %f\n",shm->pids[i].e_co,co);

    }

    // Sleep for desired interval
    timedSleep(PROCESSNAME, shm->config->pid_period, &start_usec);
  }
}
