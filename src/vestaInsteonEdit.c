/*****************************************************************************/
//    This file is part of the Vesta Control System Software Suite
//    Copyright Bill Kuhns and Vermont Energy Control Systems LLC.
//
//    This is free software: you can redistribute it and/or modify
//    it under the terms of the Lesser GNU General Public License (LGPL)
//    as published by the Free Software Foundation, either version 3 of the License,
//    or any later version.
//
//    This software is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    Lesser GNU General Public License for more details.
//
//    You should have received a copy of the Lesser GNU General Public License
//    along with this software.  If not, see <http://www.gnu.org/licenses/>.
/*****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include "vesta.h"
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/dir.h>
#include <sys/param.h>

/*****************************************************************************/

main() {
  float reading;
  int c, i, j, n, x, vc, tr, port;
  int physical, element, rule, home, logs, sysview;
  int subview;
  int e_edit, r_edit, p_edit;
  int stype;
  int pnl, con, ch;
  char chtype[3];
  char pathname[80];
  struct direct **files;
  int fcount;
  char *ptr;
  float floatval;
  struct timeval now;
  time_t timestamp;
  struct tm * timeinfo;
  char buffer[30];

  char entry[200];
  char chname[40];
  char addr[20];
  char bgcolor[10];

  byte_t dio[20], rmask;

  FILE *fp;
  char *myquery, *p[20], *v[20];
  char linebuff[256];
  char action[20];
  char editmode;

  //insteon_struct idevs[20];
  instype_struct itypes[20];
  char addrbuff[10];
  char bytebuff[3];
  //int idevcount = 0;
  int itypecount = 0;

  element_struct temp_element;

  get_shm(0);

  printf("Content-type: text/html\n\n<html>");
  printf("<head><link rel=stylesheet type=text/css href=/vesta.css></head>\n");
  printf("<body bgcolor=darkblue>\n");

  /*****************************************************************************/

  // Get form data, process any edits
  /*****************************************************************************/
  myquery = getenv("QUERY_STRING");

  /* break apart parameter string on '&' and '=' characters */

  j = 0;

  // Weirdness: sometimes myquery is NULL, and you don't want to do strlen on it.
  // Other times, it's a zero length string. Either way, don't parse it.

  if (myquery != NULL ) {
    if (strlen(myquery) > 0) {
      while ((myquery = (split(myquery, &p[j], &v[j])))) {
        j++;
      }
      j++;
      cleanup(j, &v[0]);
    }
  }

  // Set element values to shared memory values for all variables
  //set_elements(shm, elements, shm->element_count);

  e_edit = 0;
  r_edit = 0;
  p_edit = 0;
  element = 0;
  rule = 0;
  physical = 0;
  home = 0;
  logs = 0;
  sysview = 0;
  subview = 0;
  floatval = 0;

  if (j > 0) {
    for (i = 0; i < j; i++) {
      //printf("p: %s v: %s i: %d<br>\n", p[i], v[i], i);

      if (!strcmp(p[i], "subview")) {
        sscanf(v[i], "%d", &subview);
      }
      // Set action
      if (!strcmp(p[i], "action")) {
        strcpy(action, v[i]);
      }

      if (!strcmp(p[i], "name")) {
        sscanf(v[i], "%[^\"]", &chname[0]);
      }

      if (!strcmp(p[i], "addr")) {
        sscanf(v[i], "%s", &addr[0]);
      }

      if (!strcmp(p[i], "id")) {
        sscanf(v[i], "%d", &c);
      }

      if (!strcmp(p[i], "port")) {
        sscanf(v[i], "%d", &port);
      }

      if (!strcmp(p[i], "stype")) {
        sscanf(v[i], "%d", &stype);
      }

      if (!strcmp(p[i], "value")) {
        sscanf(v[i], "%f", &floatval);
        //printf("Floatval = %f ",floatval);
      }

    }

    // All done parsing. Process actions....
    itypecount = readInsteonTypeFile(&itypes[0]);

    // Update insteon type in file
    if (!strcmp(action, "itype")) {
      shm->elements[c].vdev.insteon.idevtype = stype;
      write_vdevs();
      shm->config->reload =
          shm->config->reload | (unsigned long int) INSTEON_ID;
    }

    // Update insteon address in file
    // Will have to see if the number of associated shm elements changes....
    if (!strcmp(action, "iaddr")) {
      memcpy(bytebuff, &addr[0], 2);
      sscanf(bytebuff, "%x", &x);
      shm->elements[c].vdev.insteon.address[0] = (char) x;

      memcpy(bytebuff, &addr[3], 2);
      sscanf(bytebuff, "%x", &x);
      shm->elements[c].vdev.insteon.address[1] = (char) x;

      memcpy(bytebuff, &addr[6], 2);
      sscanf(bytebuff, "%x", &x);
      shm->elements[c].vdev.insteon.address[2] = (char) x;
      write_vdevs();
      shm->config->reload =
          shm->config->reload | (unsigned long int) INSTEON_ID;

    }
    // Update sensor gain in physical I/O table and file
    if (!strcmp(action, "gain")) {
      shm->elements[c].gain = floatval;
      write_elements();
    }

    // Update sensor offset in physical I/O table and file
    if (!strcmp(action, "offset")) {
      if (!shm->config->centigrade
          && shm->stypes[shm->elements[c].stype].conversion == 't') {
        shm->elements[c].offset = (float) (floatval / 1.8);
      } else {
        shm->elements[c].offset = floatval;
      }

      write_elements();
    }
    // Process element delete for Insteon
    if (!strcmp(action, "delbtn")) {
    	stype = shm->elements[c].vdev.insteon.idevtype;
    	memcpy(buffer,shm->elements[c].vdev.insteon.address,3);
    	buffer[3] = '\0';
    	j = 1;
    	for(i=0;i<itypes[stype].devcount;i++){
    		j = insteonElementFromAddr(buffer,j);
    		if(j>-1){
    			//printf("Unlinking %d",j);
    			unlinkVariable(j);
    		}else{
    			printf("Can't find variable to unlink when deleting Insteon device %s\n",buffer);
    		}
    	}
      write_elements();
      write_vdevs();
      shm->config->reload = (unsigned long int) -1;
    }
    // Process element add for new Insteon
    if (!strcmp(action, "newinsteon")) {
      // Find unused variable(s) as needed for this type
    	for(i=0;i<itypes[stype].devcount;i++){
				c = linkUnusedVariable();

				sprintf(shm->elements[c].name, "%s:%d", itypes[stype].sname,i);

				shm->elements[c].ptype = 'i';
				shm->elements[c].vdev.vtype = 'i';
				shm->elements[c].vdev.insteon.idevtype = stype;
				shm->elements[c].vdev.insteon.devindex = i;

				memcpy(bytebuff, &addr[0], 2);
				sscanf(bytebuff, "%x", &x);
				shm->elements[c].vdev.insteon.address[0] = (char) x;

				memcpy(bytebuff, &addr[3], 2);
				sscanf(bytebuff, "%x", &x);
				shm->elements[c].vdev.insteon.address[1] = (char) x;

				memcpy(bytebuff, &addr[6], 2);
				sscanf(bytebuff, "%x", &x);
				shm->elements[c].vdev.insteon.address[2] = (char) x;
    	}
      write_elements();
      write_vdevs();
      shm->config->reload = (unsigned long int) -1;

    }
  }
  printTabs("physical");

  printSubTabs(INSVIEW);

  /*****************************************************************************/

  // Show physical I/O
  /*****************************************************************************/

  char *headers[] = { "ID", "Address", "Type", "Gain", "Offset", "Value",
      "Element" };
  printSectionHeader(7, headers);

  // Look for Insteon devices in shared memory. Show first data element for each.

  tr = 1;
  for (i = 2; i < shm->counts->element_count; i++) {
    if (shm->elements[i].pvn == 'v'
    		&& shm->elements[i].ptype == 'i'
    		&& shm->elements[i].next != 0
    		&& shm->elements[i].vdev.insteon.devindex == 0) {

      if (tr++ % 2) {
        strcpy(&bgcolor[0], "lightblue");
      } else {
        strcpy(&bgcolor[0], "white");
      }
      printf("<tr>");

      printf("<td class=smallpad bgcolor=%s>%d</td>\n", bgcolor, i);

      // Address input box
      printf("<form action=vestaInsteonEdit method=get>\n");
      printf("<td class=smallpad bgcolor=%s>\n", bgcolor);
      printf("<input type=hidden name=id value=%d>\n", i);
      printf(
          "<input name=addr size=10 value=%hX.%hX.%hX onchange=\"submit()\">\n",
          shm->elements[i].vdev.insteon.address[0],
          shm->elements[i].vdev.insteon.address[1],
          shm->elements[i].vdev.insteon.address[2]);
      printf("<input type=hidden name=view value=physical>\n");
      printf("<input type=hidden name=subview value=5>\n");
      printf("<input type=hidden name=action value=iaddr>\n");
      printf("</td></form>");

      // Sensor type selection box - update on change
      printf("<form action=vestaInsteonEdit method=get>\n");
      printf("<td class=smallpad bgcolor=%s>\n", bgcolor);
      printf("<input type=hidden name=subview value=5>\n");
      printf("<input type=hidden name=action value=itype>\n");
      printf("<input type=hidden name=id value=%d>\n", i);
      printf("<select name=stype onchange=\"submit()\">\n");

      for (x = 1; x < itypecount; x++) {
        printf("    <option value=%d %s>%s</option>\n", x,
            (x == shm->elements[i].vdev.insteon.idevtype ? "selected" : ""),
            itypes[x].sname);
      }
      printf("   </select> \n  </td></form>\n");

      // Gain input box
      printf("<form action=vestaInsteonEdit method=get>\n");
      printf("<td class=smallpad bgcolor=%s>\n", bgcolor);
      printf("<input type=hidden name=id value=%d>\n", i);
      printf("<input name=value value=%2.3f size=6 onchange=\"submit()\">\n",
          shm->elements[i].gain);
      printf("<input type=hidden name=view value=physical>\n");
      printf("<input type=hidden name=action value=gain>\n");
      printf("</td></form>");

      // Offset input box
      printf("<form action=vestaInsteonEdit method=get>\n");
      printf("<td class=smallpad bgcolor=%s>\n", bgcolor);
      //printf("<input type=hidden name=id value=%d>\n", idevs[i].elementId);

      if (!shm->config->centigrade
          && itypes[shm->elements[i].vdev.insteon.idevtype].conversion == 't') {
        printf("<input name=value value=%2.3f size=6 onchange=\"submit()\">\n",
            shm->elements[i].offset * 1.8);
      } else {
        printf("<input name=value value=%2.3f size=6 onchange=\"submit()\">\n",
            shm->elements[i].offset);
      }

      printf("<input type=hidden name=view value=physical>\n");
      printf("<input type=hidden name=action value=offset>\n");
      printf("</td></form>");

      // get  value
      reading = elementValue(i);
      printf("<td class=smallpad bgcolor=%s>%1.1f</td>\n", bgcolor, reading);
      printf("<td class=smallpad bgcolor=%s>%s</td>", bgcolor,
          shm->elements[i].name);
    	printf("<td class=nopad bgcolor=%s>\n",bgcolor);
    	printf("<a href=/cgi-bin/private/vestaInsteonEdit?id=%d&action=delbtn>",i);
    	printf("<img hspace=2 border=0 src=/images/delete-btn.png></img></a></td>\n");

      printf("</tr>\n");

    }
  }

  // Form to add new device
  if (tr % 2) {
    strcpy(&bgcolor[0], "lightblue");
  } else {
    strcpy(&bgcolor[0], "white");
  }
  printf("<tr><td colspan=7>");
  printf("<form action=vestaInsteonEdit method=get>\n");
  printf("<table background=/images/metal_bg2.jpg width=100%>\n");
  printf("<tr>");

  // Address input box
  printf("<td class=smallpad bgcolor=%s>Address: \n", bgcolor);
  printf("<input name=addr size=10 value=00.00.00>\n");
  printf("<input type=hidden name=view value=physical>\n");
  printf("<input type=hidden name=action value=newinsteon>\n");
  printf("</td>");

  // Sensor type selection box - update on change
  printf("<td class=smallpad bgcolor=%s>Model: \n", bgcolor);
  printf("<select name=stype>\n");

  for (x = 1; x < itypecount; x++) {
    printf("    <option value=%d >%s</option>\n", x, itypes[x].sname);
  }
  printf("   </select> \n  </td>\n");

  printf(
      "<td class=smallpad bgcolor=%s><input type=submit name=ibtn value=Add></td>",
      bgcolor);

  printf("</tr></table></form></tr>\n");

  printf("</td></tr></table>\n");
  // Close container table
  printf("</tr>\n</table >\n");

  printf("</body>\n</html>\n");

}

void printSectionHeader(int cols, char *headers[]) {
  int i;

  printf("<tr>\n");
  for (i = 0; i < cols; i++) {
    printf("<th class=padded bgcolor=white>%s</th>\n", headers[i]);
  }
  printf("</tr>\n");
}

