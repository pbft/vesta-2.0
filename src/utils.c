/*****************************************************************************/
//    This file is part of the NoFossil Control System Software (NFCSS)
//    Copyright 2007, 2008, 2009 Bill Kuhns
//
//    NFCSS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    any later version.

//    NFCSS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with NFCSS.  If not, see <http://www.gnu.org/licenses/>.
/*****************************************************************************/

// Utility functions used for web site cgi scripts
// split() parses form parameters, makeheader() creates HTML headers,
// 

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "vesta.h"
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <errno.h>

void read_arule(char *, arule_struct *);
void read_drule(char *, drule_struct *);
void read_mrule(char *, mrule_struct *);
void read_trule(char *, trule_struct *);
void read_crule(char *, crule_struct *);
void read_config();
void read_diskfiles();
void read_rules();
int read_stypes();
int read_pids();
void initializeElements();
char * hardwareName(int i);

// Shared memory globals
shm_index_struct localShmIndex;
int spacer[100];
shm_index_struct *shm = &localShmIndex;
count_struct *shmaddr = NULL;

FILE *logfile;
FILE *errfile;
char logbuffer[121];

void read_diskfiles(){
  read_config();
  // Set empty elements and physio structures
  //puts("initelelemnts");
  initializeElements();
  //write_elements();
  //write_physio();
  //puts("readrules");
  read_rules();
  //puts("readelelemnts");
  read_elements();
  //puts("read elements");
  read_vdevs();
  shm->counts->stype_count = (short int)read_stypes();
  //printf("Read stype");
  shm->counts->pid_count = (short int)read_pids();
  //printf("Read pid");
}

void read_rules(){
  FILE *rfp;
  char rtype;
  char buff[100];

  rfp = fopen("/usr/local/vesta/data/rules.csv","r");
  if (rfp == NULL){
    rfp = fopen("/usr/local/vesta/data/rules.csv","w");
  }else{
    shm->counts->rule_count = 0;
    while(fgets(buff,100,rfp) != NULL){
      sscanf(buff,"\"%c\"",&rtype);
      switch (rtype){
        case 'm': read_mrule(buff,&shm->rules[shm->counts->rule_count].mrule);
        break;
        case 'd': read_drule(buff,&shm->rules[shm->counts->rule_count].drule);
        break;
        case 'a': read_arule(buff,&shm->rules[shm->counts->rule_count].arule);
        break;
        case 't': read_trule(buff,&shm->rules[shm->counts->rule_count].trule);
        break;
        case 'c': read_crule(buff,&shm->rules[shm->counts->rule_count].crule);
        break;
        case 'w': read_wrule(buff,&shm->rules[shm->counts->rule_count].wrule);
        break;
      }
      shm->counts->rule_count++;
    }
  }
  shm->config->reload = -1;
}

void read_old_drule(char *buffer, drule_struct *drule){
  sscanf(buffer,"\"%c\",%d,%d,\"%c\",%d,%d,%d",
      &drule->rtype,&drule->target,&drule->e1,&drule->gle,&drule->e2.rval.eid,&drule->diff.rval.eid,&drule->deadband.rval.eid);
  drule->e2.eflag = 1;
  drule->diff.eflag = 1;
  drule->deadband.eflag = 1;
}

void read_drule(char *buffer, drule_struct *drule){
  sscanf(buffer,"\"%c\",%d,%d,\"%c\",%d,%f,%d,%f,%d,%f",
      &drule->rtype,&drule->target,&drule->e1,&drule->gle,
      &drule->e2.eflag,&drule->e2.rval.value,
      &drule->diff.eflag,&drule->diff.rval.value,
      &drule->deadband.eflag,&drule->deadband.rval.value);
  if(drule->e2.eflag){
    drule->e2.rval.eid = (int)drule->e2.rval.value;
  }
  if(drule->diff.eflag){
    drule->diff.rval.eid = (int)drule->diff.rval.value;
  }
  if(drule->deadband.eflag){
    drule->deadband.rval.eid = (int)drule->deadband.rval.value;
  }
  //printf("Buffer %s addr %u type %c target %d e1 %d e2flg %d e2val %f e2eid %d difflg %d diffval %f\n",
  //    buffer,drule,drule->rtype,drule->target,drule->e1,drule->e2.eflag,drule->e2.rval.value,drule->e2.rval.eid,drule->diff.eflag,drule->diff.rval.value);
}

void read_old_arule(char *buffer, arule_struct *arule){
  sscanf(buffer,"\"%c\",%d,%d,%d,%d,%d,%d,%d,%d",&arule->rtype,&arule->target,&arule->v1.rval.eid,&arule->e1,&arule->n1,&arule->e2,&arule->n2,&arule->e3,&arule->n3);
  arule->v1.eflag = 1;
}

void read_arule(char *buffer, arule_struct *arule){
  arule_struct rule;
  sscanf(buffer,"\"%c\",%d,%d,%f,%d,%d,%d,%d,%d,%d",
      &rule.rtype,&rule.target,
      &rule.v1.eflag,&rule.v1.rval.value,
      &rule.e1,&rule.n1,&rule.e2,&rule.n2,&rule.e3,&rule.n3);
  if(rule.v1.eflag){
    rule.v1.rval.eid = (int) rule.v1.rval.value;
  }
  memcpy(arule,&rule,sizeof(arule_struct));
  /*
  printf("Buffer %s addr %u type %c target %d v1 %d v1val %f e1 %d n1 %d e2 %d n2 %d e3 %d n3 %d \n",
      buffer,arule,arule->rtype,(int) arule->target,arule->v1.eflag,arule->v1.rval.value,arule->e1,arule->n1,arule->e2,arule->n2,arule->e3,arule->n3);
  printf("addr %u type %c target %d v1 %d v1val %f e1 %d n1 %d e2 %d n2 %d e3 %d n3 %d \n",
      &rule,rule.rtype,rule.target,rule.v1.eflag,rule.v1.rval.value,rule.e1,rule.n1,rule.e2,rule.n2,rule.e3,rule.n3);
  printf("v1 %u v1.eflag %u v1.rval %u v1.rval.eid %u v1.rval.value %u\n",
      &arule->v1,&arule->v1.eflag,&arule->v1.rval,&arule->v1.rval.eid,&arule->v1.rval.value);
  */
}

void read_trule(char *buffer, trule_struct *trule){
  sscanf(buffer,"\"%c\",%d,%d,%d,%d,%d,%f",&trule->rtype,&trule->target,&trule->e1,&trule->ib,&trule->tf,&trule->e2.eflag,&trule->e2.rval.value);
  if(trule->e2.eflag){
    trule->e2.rval.eid = (int)trule->e2.rval.value;
  }
}

void read_old_trule(char *buffer, trule_struct *trule){
  char c;
  sscanf(buffer,"\"%c\",%d,%d,%d,%d,%d",&trule->rtype,&trule->target,&trule->e1,&trule->ib,&trule->tf,&trule->e2.rval.eid);
  trule->e2.eflag = 1;
}

void read_old_mrule(char *buffer, mrule_struct *mrule){
  char c;
  sscanf(buffer,"\"%c\",%d,%d,\"%c\",%d",&mrule->rtype,&mrule->target,&mrule->e1.rval.eid,&mrule->op,&mrule->e2.rval.eid);
  mrule->e1.eflag = 1;
  mrule->e2.eflag = 1;
}

void read_mrule(char *buffer, mrule_struct *mrule){
  char c;
  sscanf(buffer,"\"%c\",%d,%d,%f,\"%c\",%d,%f",
      &mrule->rtype,&mrule->target,
      &mrule->e1.eflag,&mrule->e1.rval.value,
      &mrule->op,
      &mrule->e2.eflag,&mrule->e2.rval.value);
  if(mrule->e1.eflag){
    mrule->e1.rval.eid = (int)mrule->e1.rval.value;
  }
  if(mrule->e2.eflag){
    mrule->e2.rval.eid = (int)mrule->e2.rval.value;
  }
}

void read_crule(char *buffer, crule_struct *crule){
  sscanf(buffer,"\"%c\",\"%[^\"]\"",&crule->rtype,&crule->ctext);
}

void read_wrule(char *buffer, wrule_struct *wrule){
  sscanf(buffer,"\"%c\",%hd,%hd,%hd,%hd,%hd,%hd,%hd,\"%[^\"]\"",
      &wrule->rtype,&wrule->state,
      &wrule->wdata,&wrule->delay,
      &wrule->interval,&wrule->delay2,
      &wrule->tnt,&wrule->sns,
      &wrule->ctext);
}

void write_rules(){
  FILE *fp;
  int i;

  fp = fopen("/usr/local/vesta/data/rules.csv","w");
  for (i=0;i<shm->counts->rule_count;i++){
    switch (shm->rules[i].mrule.rtype){
      case 'm':
        fprintf(fp,"\"m\",%d,%d,%f,\"%c\",%d,%f\n",
            shm->rules[i].mrule.target,
            shm->rules[i].mrule.e1.eflag,
            shm->rules[i].mrule.e1.eflag ? (float)shm->rules[i].mrule.e1.rval.eid : shm->rules[i].mrule.e1.rval.value,
            shm->rules[i].mrule.op,
            shm->rules[i].mrule.e2.eflag,
            shm->rules[i].mrule.e2.eflag ? (float)shm->rules[i].mrule.e2.rval.eid : shm->rules[i].mrule.e2.rval.value);
        break;
      case 'd':
        fprintf(fp,"\"d\",%d,%d,\"%c\",%d,%f,%d,%f,%d,%f\n",
            shm->rules[i].drule.target,
            shm->rules[i].drule.e1,
            shm->rules[i].drule.gle,
            shm->rules[i].drule.e2.eflag,
            shm->rules[i].drule.e2.eflag ? (float)shm->rules[i].drule.e2.rval.eid : shm->rules[i].drule.e2.rval.value,
            shm->rules[i].drule.diff.eflag,
            shm->rules[i].drule.diff.eflag ? (float)shm->rules[i].drule.diff.rval.eid : shm->rules[i].drule.diff.rval.value,
            shm->rules[i].drule.deadband.eflag,
            shm->rules[i].drule.deadband.eflag ? (float)shm->rules[i].drule.deadband.rval.eid : shm->rules[i].drule.deadband.rval.value);
        break;
      case 'a': 
        fprintf(fp,"\"a\",%d,%d,%f,%d,%d,%d,%d,%d,%d\n",
            shm->rules[i].arule.target,
            shm->rules[i].arule.v1.eflag,
            shm->rules[i].arule.v1.eflag ? (float)shm->rules[i].arule.v1.rval.eid : shm->rules[i].arule.v1.rval.value,
            shm->rules[i].arule.e1,
            shm->rules[i].arule.n1,
            shm->rules[i].arule.e2,
            shm->rules[i].arule.n2,
            shm->rules[i].arule.e3,
            shm->rules[i].arule.n3);
        break;
      case 't': 
        fprintf(fp,"\"t\",%d,%d,%d,%d,%d,%f\n",
            shm->rules[i].trule.target,
            shm->rules[i].trule.e1,
            shm->rules[i].trule.ib,
            shm->rules[i].trule.tf,
            shm->rules[i].trule.e2.eflag,
            shm->rules[i].trule.e2.eflag ? (float)shm->rules[i].trule.e2.rval.eid : shm->rules[i].trule.e2.rval.value);
        break;
      case 'w':
        fprintf(fp,"\"w\",%d,%d,%d,%d,%d,%d,%d,\"%s\"\n",
            shm->rules[i].wrule.state,
            shm->rules[i].wrule.wdata,
            shm->rules[i].wrule.delay,
            shm->rules[i].wrule.interval,
            shm->rules[i].wrule.delay2,
            shm->rules[i].wrule.tnt,
            shm->rules[i].wrule.sns,
            shm->rules[i].wrule.ctext);
        break;
      case 'c':
        fprintf(fp,"\"c\",\"%s\"\n",
            shm->rules[i].crule.ctext);
        break;
    }
  }
  fclose(fp);
}

int read_stypes(){

  FILE *fp;
  char linebuff[256];
  int i;

  fp = fopen("/etc/vesta/stype.csv","r");
  i=0;    
  fgets(linebuff,256,fp);

  while (fscanf(fp,"%hd,\"%[^\"]\",%f,%f,%hd,\"%c\"",&shm->stypes[i].stype,&shm->stypes[i].sname,&shm->stypes[i].gain,&shm->stypes[i].offset,&shm->stypes[i].lut,&shm->stypes[i].conversion)>0){
    i++;
  }
  fclose(fp);
  return(i);
}

void initializeElements(){

  int i,j,k,p,e,n,ai,ao,di,disco,nio;
  // iterate through hwconfig and build physio and elements
  i=0;
  p=0;
  n=0;
  e=2;
  ai=ao=di=disco=nio=0;
  // set first two elements
  shm->elements[0].next = 1;
  shm->elements[0].owner = 0;
  shm->elements[0].pvn = 'c';
  shm->elements[0].io = 'i';
  shm->elements[0].val = &shm->evals[0];
  shm->evals[0] = (float)1.0;
  sprintf(shm->elements[0].name,"TRUE");
  shm->elements[1].next = -1;
  shm->elements[1].owner = 0;
  shm->elements[1].pvn = 'c';
  shm->elements[1].io = 'i';
  shm->elements[1].val = &shm->evals[1];
  shm->evals[1] = 0;
  sprintf(shm->elements[1].name,"FALSE");
  // For each line in hwconfig
  while(shm->hwconfig[i].index == i){
    // Point hwconfig to physio
    shm->hwconfig[i].eid = (short int) e;
    for(j=0;j<shm->hwconfig[i].channelcount;j++){
      for(k=0;k<shm->hwconfig[i].ioperchannel;k++){
        switch (shm->hwconfig[i].ctype){
          case analog_in:
            //printf("Analog in %d %s\n",e,shm->elements[e].name);
            shm->elements[e].ptype = 's';
            shm->elements[e].stype = 7;
            shm->elements[e].hwchannel = ai;
            shm->elements[e].gain = 1;
            shm->elements[e].offset = 0;
            shm->elements[e].status = 0;
            shm->elements[e].pvn = 'p';
            shm->elements[e].io = 'i';
            sprintf(shm->elements[e].name,"Analog In %02d",ai+1);
            shm->elements[e].next = 0;
            shm->elements[e].owner = 0;
            shm->elements[e].val = &shm->evals[e];
            ai++;
            p++;
            e++;
          break;
          case analog_out:
            //printf("Analog out %d %s\n",e,shm->elements[e].name);
            shm->elements[e].ptype = 'c';
            shm->elements[e].stype = 6;
            shm->elements[e].hwchannel = ao;
            shm->elements[e].gain = 1;
            shm->elements[e].offset = 0;
            shm->elements[e].status = 0;
            shm->elements[e].pvn = 'p';
            shm->elements[e].io = 'o';
            sprintf(shm->elements[e].name,"Analog Out %02d",ao+1);
            shm->elements[e].next = 0;
            shm->elements[e].owner = 0;
            shm->elements[e].val = &shm->evals[e];
            ao++;
            p++;
            e++;
          break;
          case discrete_in:
            //printf("Discrete in %d %s\n",e,shm->elements[e].name);
            shm->elements[e].ptype = 'i';
            shm->elements[e].stype = 3;
            shm->elements[e].hwchannel = (int)di/8;
            shm->elements[e].bitnum = k;
            shm->elements[e].gain = 1;
            shm->elements[e].offset = 0;
            shm->elements[e].status = 0;
            shm->elements[e].pvn = 'p';
            shm->elements[e].io = 'i';
            sprintf(shm->elements[e].name,"Discrete In %02d",di+1);
            shm->elements[e].next = 0;
            shm->elements[e].owner = 0;
            shm->elements[e].val = &shm->evals[e];
            di++;
            p++;
            e++;
          break;
          case discrete_out:
            //printf("Discrete out %d %s\n",e,shm->elements[e].name);
            shm->elements[e].ptype = 'o';
            shm->elements[e].stype = 4;
            shm->elements[e].hwchannel = (int)disco/8;
            shm->elements[e].bitnum = k;
            shm->elements[e].gain = 1;
            shm->elements[e].offset = 0;
            shm->elements[e].status = 0;
            shm->elements[e].pvn = 'p';
            shm->elements[e].io = 'o';
            sprintf(shm->elements[e].name,"Discrete Out %02d",disco+1);
            shm->elements[e].next = 0;
            shm->elements[e].owner = 0;
            shm->elements[e].val = &shm->evals[e];
            disco++;
            p++;
            e++;
          break;
          case variable:
            //printf("Variable %d %s\n",e,shm->elements[e].name);
            shm->elements[e].pvn = 'v';
            shm->elements[e].io = 'o';
            sprintf(shm->elements[e].name,"Unassigned %02d",e);
            shm->elements[e].next = 0;
            shm->elements[e].owner = 0;
            shm->elements[e].val = &shm->evals[e];
            e++;
         break;

        }
      }

    }
    i++;
  }
  //printf("Last element %d netio %d ai %d ao %d di %d do %d p %d\n",e,n,ai,ao,di,disco,p);
  //write_elements();
}

int read_elements(){

  FILE *fp;
  char linebuff[256];
  int i,c;
  int a,p;
  float fval;
  char etype;
  char io;
  char name[40];
  int *old, *new;

  fp = fopen("/usr/local/vesta/data/elements_v2.csv","r");
  if(fp == NULL){
    write_elements();
    sprintf(logbuffer, "created new elements.csv");
    printlog("utils", logbuffer, 1);
    fp = fopen("/usr/local/vesta/data/elements_v2.csv","r");
  }
  i=0;

  fgets(linebuff,256,fp);
  while (fgets(linebuff,256,fp)){
    sscanf(linebuff,"%d,%hd,%hd,\"%c\",\"%c\",%f,\"%[^\"]\",%hd,\"%c\",%hd,%hd,%f,%f,\"%c\"",
        &c,
        &shm->elements[i].next,
        &shm->elements[i].owner,
        &shm->elements[i].pvn,
        &shm->elements[i].io,
        &fval,
        &shm->elements[i].name,
        &shm->elements[i].stype,
        &shm->elements[i].ptype,
        &shm->elements[i].hwchannel,
        &shm->elements[i].bitnum,
        &shm->elements[i].gain,
        &shm->elements[i].offset
        );
    // If it's a virtual output, assign it a channel and bit
    if (shm->elements[i].pvn == 'v' || shm->elements[i].pvn == 'c'){
      setElement(i, fval, 0);
    }
    shm->elements[i].val = &shm->evals[i];
    shm->elements[i].owner = 0;
    shm->elements[i].vdev.vtype = 'x';
    i++;
  }
  fclose(fp);
  return(i);
}

void write_elements(){
  FILE *fp, *fp2;
  int i,x,y;
  char ptype;

  fp = fopen("/usr/local/vesta/data/elements_v2.csv","w");
  fprintf(fp,"\"Index\",\"Next\",\"owner\",\"pvn\",\"io\",\"value\",\"name\",\"stype\",\"ptype\",\"hwchannel\",\"bitnum\",\"gain\",\"offset\"\n");
  //printf("Writing %d lines\n",shm->element_count);
  for (i=0;i<shm->counts->element_count;i++){
      //fprintf(fp,"%d,%d,%d,\"%c\",\"%c\",%f,\"%s\"\n",shm->elements[i].next,shm->elements[i].owner,shm->elements[i].physio,shm->elements[i].pvn,shm->elements[i].io,*shm->elements[i].val,shm->elements[i].name);
    if(shm->elements[i].ptype == NULL){
      ptype = 'x';
    }else{
      ptype = shm->elements[i].ptype;
    }
    fprintf(fp,"%d,%hd,%hd,\"%c\",\"%c\",%f,\"%s\",%hd,\"%c\",%hd,%hd,%f,%f\n",
          i,
          shm->elements[i].next,
          shm->elements[i].owner,
          shm->elements[i].pvn,
          shm->elements[i].io,
          shm->evals[i],
          shm->elements[i].name,
          shm->elements[i].stype,
          ptype,
          shm->elements[i].hwchannel,
          shm->elements[i].bitnum,
          shm->elements[i].gain,
          shm->elements[i].offset
          );

  }
  fclose(fp);
  // Create user graphical panel
  fp = fopen("/var/www/public/ctlpanel.txt","w");
  fp2 = fopen("/var/www/private/ctlpanel.txt","w");

  // Sensors
  x = 140;
  y = 20;
  fprintf(fp,"\"label\",%d,%d,\"\",3,16,\"w\",\"Sensor Inputs:\"\n",x+46,y);
  fprintf(fp2,"\"label\",%d,%d,\"\",3,16,\"w\",\"Sensor Inputs:\"\n",x+46,y);
  y += 10;
  for (i=1;i != -1;i = shm->elements[i].next){
    if (shm->elements[i].pvn == 'p') {
      if (shm->elements[i].ptype == 's' || shm->elements[i].ptype == 'n'){
        fprintf(fp,"\"text\",%d,%d,\"\",%d,\"white\",\"solid\",\"black\",\"\",\"\",\"\",\"\",\"%s\"\n",x,y,i,shm->elements[i].name);
        fprintf(fp2,"\"text\",%d,%d,\"\",%d,\"white\",\"solid\",\"black\",\"\",\"\",\"\",\"\",\"%s\"\n",x,y,i,shm->elements[i].name);
        y += 20;
      }
    }
  }

  // Analog Outputs
  y += 20;
  fprintf(fp,"\"label\",%d,%d,\"\",3,16,\"w\",\"Analog Outputs:\"\n",x+46,y);
  fprintf(fp2,"\"label\",%d,%d,\"\",3,16,\"w\",\"Analog Outputs:\"\n",x+46,y);
  y += 10;
  for (i=1;i != -1;i = shm->elements[i].next){
    if (shm->elements[i].pvn == 'p') {
      if (shm->elements[i].ptype == 'c'){
        fprintf(fp,"\"text\",%d,%d,\"\",%d,\"yellow\",\"solid\",\"black\",\"\",\"\",\"\",\"\",\"%s\"\n",x,y,i,shm->elements[i].name);
        fprintf(fp2,"\"set\",%d,%d,\"\",%d,\"yellow\",\"solid\",\"black\",\"\",\"\",\"\",\"\",\"%s\"\n",x,y,i,shm->elements[i].name);
        y += 20;
      }
    }
  }

  // Discrete Inputs
  x += 210;
  y = 20;
  fprintf(fp,"\"label\",%d,%d,\"\",3,16,\"w\",\"Discrete Inputs:\"\n",x+12,y);
  fprintf(fp2,"\"label\",%d,%d,\"\",3,16,\"w\",\"Discrete Inputs:\"\n",x+12,y);
  y += 10;
  for (i=1;i != -1;i = shm->elements[i].next){
    if (shm->elements[i].pvn == 'p') {
      if (shm->elements[i].ptype == 'i'){
        fprintf(fp,"\"light\",%d,%d,\"\",%d,10,10,\"lightgreen\",\"green\",\"%s\"\n",x,y,i,shm->elements[i].name);
        fprintf(fp2,"\"light\",%d,%d,\"\",%d,10,10,\"lightgreen\",\"green\",\"%s\"\n",x,y,i,shm->elements[i].name);
        y += 20;
      }
    }
  }

  // Discrete Outputs
  x += 180;
  y = 20;
  fprintf(fp,"\"label\",%d,%d,\"\",3,16,\"w\",\"Discrete Outputs:\"\n",x+12,y);
  fprintf(fp2,"\"label\",%d,%d,\"\",3,16,\"w\",\"Discrete Outputs:\"\n",x+12,y);
  y += 10;
  for (i=1;i != -1;i = shm->elements[i].next){
    if (shm->elements[i].pvn == 'p') {
      if (shm->elements[i].ptype == 'o'){
        fprintf(fp,"\"light\",%d,%d,\"\",%d,10,10,\"lightblue\",\"blue\",\"%s\"\n",x,y,i,shm->elements[i].name);
        fprintf(fp2,"\"toggle\",%d,%d,\"\",%d,2,\"\",\"\",\"%s\"\n",x,y,i,shm->elements[i].name);
        y += 20;
      }
    }
  }

  // State Variables
  x += 180;
  y = 20;
  fprintf(fp,"\"label\",%d,%d,\"\",3,16,\"w\",\"State Variables:\"\n",x+12,y);
  fprintf(fp2,"\"label\",%d,%d,\"\",3,16,\"w\",\"State Variables:\"\n",x+12,y);
  y += 10;
  for (i=1;i != -1;i = shm->elements[i].next){
    if (shm->elements[i].pvn == 'v' && shm->elements[i].name[0] == '~') {
      fprintf(fp,"\"light\",%d,%d,\"\",%d,10,10,\"lightgrey\",\"grey\",\"%s\"\n",x,y,i,shm->elements[i].name);
      fprintf(fp2,"\"toggle\",%d,%d,\"\",%d,2,\"\",\"\",\"%s\"\n",x,y,i,shm->elements[i].name);
      y += 20;
    }
  }

  // Variables
  x += 180;
  y = 20;
  fprintf(fp,"\"label\",%d,%d,\"\",3,16,\"w\",\"Variables:\"\n",x+46,y);
  fprintf(fp2,"\"label\",%d,%d,\"\",3,16,\"w\",\"Variables:\"\n",x+46,y);
  y += 10;
  for (i=1;i != -1;i = shm->elements[i].next){
    if (shm->elements[i].pvn == 'v' && shm->elements[i].name[0] != '~') {
      fprintf(fp,"\"text\",%d,%d,\"\",%d,\"lightgray\",\"solid\",\"black\",\"\",\"\",\"\",\"\",\"%s\"\n",x,y,i,shm->elements[i].name);
      fprintf(fp2,"\"set\",%d,%d,\"\",%d,\"lightgray\",\"solid\",\"black\",\"\",\"\",\"\",\"\",\"%s\"\n",x,y,i,shm->elements[i].name);
      y += 20;
    }
  }

  fclose(fp);
  fclose(fp2);

}

void read_config(){

  FILE *fp;
  char linebuff[256];


  fp = fopen("/usr/local/vesta/data/config.csv","r");

  fgets(linebuff,256,fp);
  fscanf(fp,"%hd,%hd,%hd,%hd,%hd,%hd,%hd,%hd,%hd,%hd,%hd,%hd",
      &shm->config->centigrade,
      &shm->config->io_period,
      &shm->config->rule_period,
      &shm->config->log_period,
      &shm->config->sqllog_period,
      &shm->config->pid_period,
      &shm->config->ha7_period,
      &shm->config->netio_period,
      &shm->config->usr1_period,
      &shm->config->usr2_period,
      &shm->config->usr3_period,
      &shm->config->usr4_period
      );

  // Older config files have no values for some variables. Preload with safe numbers:
  if (shm->config->log_period == 0) shm->config->log_period = 600;
  if (shm->config->sqllog_period == 0) shm->config->sqllog_period = 600;
  if (shm->config->pid_period == 0) shm->config->pid_period = 20;
  if (shm->config->ha7_period == 0) shm->config->ha7_period = 600;
  if (shm->config->netio_period == 0) shm->config->netio_period = 600;
  if (shm->config->usr1_period == 0) shm->config->usr1_period = 600;
  if (shm->config->usr2_period == 0) shm->config->usr2_period = 600;
  if (shm->config->usr3_period == 0) shm->config->usr3_period = 600;
  if (shm->config->usr4_period == 0) shm->config->usr4_period = 600;

  fclose(fp);
}

void write_config(){
  FILE *fp;

  //printf ("J=%d\n",j);

  fp = fopen("/usr/local/vesta/data/config.csv","w");
  fprintf(fp,"\"Centigrade\",\"IO period\",\"Rules period\",\"Log period\",\"SQL Log period\",\"PID period\",\"HA7 period\"\n");

  fprintf(fp,"%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d\n",
      shm->config->centigrade,
      shm->config->io_period,
      shm->config->rule_period,
      shm->config->log_period,
      shm->config->sqllog_period,
      shm->config->pid_period,
      shm->config->ha7_period,
      shm->config->netio_period,
      shm->config->usr1_period,
      shm->config->usr2_period,
      shm->config->usr3_period,
      shm->config->usr4_period
      );

  fclose(fp);
}

int read_pids(){
  /*
  short int e_ctl;          // Element that enables this PID channel
  short int e_co;           // Element: Control Output
  short int e_sp;           // Element: Setpoint
  short int e_pv;           // Element: Process variable - what we want to drive to setpoint
  short int e_load;         // Element: Load predictor
  float kc;           // Proportional gain
  float ti;           // Integral damping
  float lmin;         // Load value corresponding to minimum output
  float lmax;         // Load value corresponding to maximum output
  float coMin;        // Minimum allowable output during controlled operation
  float coMax;        // Maximum allowable output during controlled operation
  float purgeLimit;    // Output value below which purge is triggered
  float purgeInterval; // Time between purge cycles
  float purgeCycle;    // Length of purge
  */
  FILE *fp;
  char linebuff[256];
  int i;
  for(i=0;i<shm->counts->ao_count;i++){
    shm->pids[i].e_ctl = 0;
  }

  fp = fopen("/usr/local/vesta/data/piddata.csv","r");
  i=0;
  fgets(linebuff,256,fp);
  // In older installations, last five parameters may not be in data file. Set purgeCycle to zero to disable purge.
  // Also set coMin and coMax to safe values
  shm->pids[i].purgeCycle = 0;
  shm->pids[i].coMin = 0;
  shm->pids[i].coMax = 100;
  while (fscanf(fp,"%hd,%hd,%hd,%hd,%hd,%f,%f,%f,%f,%f,%f,%f,%f,%f",
      &shm->pids[i].e_ctl,&shm->pids[i].e_co, &shm->pids[i].e_sp, &shm->pids[i].e_pv, &shm->pids[i].e_load,
      &shm->pids[i].kc, &shm->pids[i].ti, &shm->pids[i].lmin, &shm->pids[i].lmax,
      &shm->pids[i].coMin,&shm->pids[i].coMax,&shm->pids[i].purgeLimit,&shm->pids[i].purgeInterval,&shm->pids[i].purgeCycle)>0){
    //printf("PID: %d control is element %d\n",i,shm->pids[i].e_ctl);
    i++;
  }
  fclose(fp);
  return(i);
}

void write_pids(){
  FILE *fp;
  int i;

  //printf ("J=%d\n",j);

  fp = fopen("/usr/local/vesta/data/piddata.csv","w");
  fprintf(fp,"\"ctl\",\"co\",\"sp\",\"pv\",\"load\",\"kc\",\"ti\",\"lmin\",\"lmax\",\"coMin\",\"coMax\",\"purgeLimit\",\"purgeInterval\",\"purgeCycle\"\n");

  for (i=0;i<shm->counts->pid_count;i++){
    fprintf(fp,"%d,%d,%d,%d,%d,%f,%f,%f,%f,%f,%f,%f,%f,%f\n",
        shm->pids[i].e_ctl,shm->pids[i].e_co, shm->pids[i].e_sp, shm->pids[i].e_pv, shm->pids[i].e_load,
        shm->pids[i].kc, shm->pids[i].ti, shm->pids[i].lmin, shm->pids[i].lmax,
        shm->pids[i].coMin,shm->pids[i].coMax,shm->pids[i].purgeLimit,shm->pids[i].purgeInterval,shm->pids[i].purgeCycle);
  }
  fclose(fp);
}

void read_vdevs(){
  FILE *vfp;
  char vtype;
  int i;
  char linebuff[256];

  vfp = fopen("/usr/local/vesta/data/vdevs.csv","r");
  if (vfp == NULL){
    vfp = fopen("/usr/local/vesta/data/vdevs.csv","w");
  }else{
    while(fgets(linebuff,250,vfp) != NULL){
      sscanf(linebuff,"\"%c\"",&vtype);
      switch (vtype){
        case 'i': readInsteon(linebuff);
        break;
        case 'n': readNetIo(linebuff);
        break;
      }
    }
  }
  shm->config->reload = -1;
  fclose(vfp);
}

void readNetIo(char *buffer){
  char vtype;
  int x;
  short int eid, port, reid;
  char addrbuff[20];
  netio_struct *ndev;

  sscanf(buffer,"\"%c\",%hd,\"%[^\"]\",%hd,%hd",&vtype, &eid, addrbuff, &port, &reid);
  shm->elements[eid].vdev.vtype = vtype;
    // part of associated element record
  ndev = &shm->elements[eid].vdev.netio;
  memcpy(ndev->ip,addrbuff,16);
  ndev->remoteElementId = reid;
  shm->elements[eid].ptype = 'n';
  ndev->port = port;
}

void readInsteon(char *buffer){
  char vtype;
  int x;
  short int eid, idevtype, devindex;
  char bytebuff[3];
  char addrbuff[10];
  insteon_struct *idev;

  sscanf(buffer,"\"%c\",%hd,%hd,%hd,\"%[^\"]\"",&vtype, &eid, &idevtype, &devindex, addrbuff);
  if(eid == 0){
    // master device has own shm struct
    idev = shm->insteonMaster;
  }else{
    shm->elements[eid].vdev.vtype = 'i';
    // part of associated element record
    idev = &shm->elements[eid].vdev.insteon;
  }
  memcpy(bytebuff, &addrbuff[0], 2);
  sscanf(bytebuff, "%x", &x);
  idev->address[0] = (char) x;

  memcpy(bytebuff, &addrbuff[3], 2);
  sscanf(bytebuff, "%x", &x);
  idev->address[1] = (char) x;

  memcpy(bytebuff, &addrbuff[6], 2);
  sscanf(bytebuff, "%x", &x);
  idev->address[2] = (char) x;

  shm->elements[eid].ptype = 'i';
  idev->idevtype = idevtype;
  idev->devindex = devindex;
  //printf("Read insteon %d: %s\n",devindex,buffer);
}

void write_vdevs(){

  FILE *fp;
  int i;

  fp = fopen("/usr/local/vesta/data/vdevs.csv", "w");
  // Write record for Insteon Master device
  fprintf(fp,"\"i\",0,0,\"%hx.%hx.%hx\"\n",
      shm->insteonMaster->address[0],
      shm->insteonMaster->address[1],
      shm->insteonMaster->address[2]);

  for (i=2;i<shm->counts->element_count;i++){
    if(shm->elements[i].pvn == 'v'){
      switch (shm->elements[i].ptype){
        case 'i':
        	// Record Struct: vtype("i"),ElementID,Devtype,SubIndex,address
          fprintf(fp,"\"i\",%d,%d,%d,\"%hx.%hx.%hx\"\n",
              i,
              shm->elements[i].vdev.insteon.idevtype,
              shm->elements[i].vdev.insteon.devindex,
              shm->elements[i].vdev.insteon.address[0],
              shm->elements[i].vdev.insteon.address[1],
              shm->elements[i].vdev.insteon.address[2]);
          break;
        case 'n':
          fprintf(fp,"\"n\",%d,\"%s\",%d,%d\n",
              i,
              shm->elements[i].vdev.netio.ip,
              shm->elements[i].vdev.netio.port,
              shm->elements[i].vdev.netio.remoteElementId);
          break;
      }
    }

  }
  fclose(fp);
}

// Read insteon type file, return type count
int readInsteonTypeFile(instype_struct itypes[]){
  FILE *fp;
  int i = 0;
  char linebuff[256];

  fp = fopen("/etc/vesta/instype.csv", "r");
  fgets(linebuff, 256, fp);
  while (fscanf(fp, "%hd,\"%[^\"]\",%f,%f,%hd,\"%c\",%hd,%hd", &itypes[i].stype,
      &itypes[i].sname, &itypes[i].gain, &itypes[i].offset, &itypes[i].devcount,
      &itypes[i].conversion, &itypes[i].poll, &itypes[i].rw) > 0) {
    i++;
  }
  fclose(fp);
  return(i);
}

// Find device in idevs based on address (three consecutive bytes). Return device index or -1.
// Problem: Some devices need more than one shared memory variable - garage door command and
// garage door sensor, for instance. Both will have the same Insteon address.
// Solution: start at next item after caller-supplied element. If there's more than 1
// then caller starts with previous element.
insteonElementFromAddr(char *addr, int start){
	int i, j;
	for (i = shm->elements[start].next; i != -1; i = shm->elements[i].next) {
		if (shm->elements[i].next != 0) {
			if(!memcmp(addr,shm->elements[i].vdev.insteon.address,3)){
				return(i);
			}
		}
	}
	return(-1);
}

void get_shm(int pid){
  int shmid;
  size_t shmsize;
  int i,hwcount,aicount,aocount,dicount,docount,varcount,ecount;
  short int a;
  int b,c,x;
  FILE *fp;
  key_t key = 9700;
  char ctype[40];

  //printf("Getting shared memory\n");
  // return id
  openLogFile();
  shmid = shmget(key, 1, 0);
  if (shmid == -1){

    char *linebuff = malloc(120);
    hw_struct *hw = (hw_struct *) malloc(50 * sizeof(hw_struct));

    fp = fopen("/etc/vesta/hwconfig.csv","r");
    if(fp == NULL){
      sprintf(logbuffer, "could not open /dev/mem");
      printlog("get_shm", logbuffer, 1);
      exit(1);
    }
    i=aicount=aocount=dicount=docount=0;
    varcount=2;

    fgets(linebuff,256,fp);

    hwcount=0;
    while (fscanf(fp, "%hd,\"%[^\"]\",%hd,%hd,%hd,\"%[^\"]\",%hd,%hd",
        &a, &hw[hwcount].name, &hw[hwcount].handler, &hw[hwcount].type, &hw[hwcount].ordinal,
        ctype, &hw[hwcount].channelcount, &hw[hwcount].ioperchannel) > 0) {

      if (!strcmp(ctype, "analog_in")) {
        aicount += hw[hwcount].channelcount * hw[hwcount].ioperchannel;
        hw[hwcount].ctype = analog_in;
      }
      if (!strcmp(ctype, "analog_out")) {
        aocount += hw[hwcount].channelcount * hw[hwcount].ioperchannel;
        hw[hwcount].ctype = analog_out;
      }
      if (!strcmp(ctype, "discrete_in")) {
        dicount += hw[hwcount].channelcount * hw[hwcount].ioperchannel;
        hw[hwcount].ctype = discrete_in;
      }
      if (!strcmp(ctype, "discrete_out")) {
        docount += hw[hwcount].channelcount * hw[hwcount].ioperchannel;
        hw[hwcount].ctype = discrete_out;
      }
      if (!strcmp(ctype, "variable")) {
        varcount += hw[hwcount].channelcount * hw[hwcount].ioperchannel;
        hw[hwcount].ctype = variable;
      }
      hwcount++;
    }
    fclose(fp);

    ecount = aicount+aocount+dicount+docount+varcount;

    // Figure out size of shared memory
    shmsize =
        sizeof(count_struct)
        + sizeof(config_struct)
        + ecount * sizeof(float)
        + ecount * sizeof(element_struct)
        + aicount * sizeof(float)
        + aicount * sizeof(byte_t)
        + dicount * sizeof(byte_t)
        + docount * sizeof(byte_t)
        + 20 * sizeof(hw_struct)
        + 40 * sizeof(stype_struct)
        + MAX_RULES * sizeof(rule_struct)
        + aocount * sizeof(pid_struct)
        + sizeof(int)
        + sizeof(insteon_struct)
        + 4 * 80 * sizeof(char);

    //printf("Creating shm at %d bytes\n",shmsize);
    free(linebuff);
    //printf("Creating shm at %d bytes\n",shmsize);
    // Create shared memory
    shmid = shmget(key, shmsize, (IPC_CREAT | 0666));
    if (shmid == -1){
      sprintf(logbuffer, "shmget failed");
      printlog("get_shm", logbuffer, 1);
      exit(1);
    }

    // Link to our shared memory
    //shm = shmat(shmid, SHBASE, 0);
    shmaddr = shmat(shmid, NULL, 0);
    if (shmaddr == -1) {
      sprintf(logbuffer, "shmat failed");
      printlog("get_shm", logbuffer, 1);
      exit(1);
    }
    //printf("Shared Memory at: %u %d bytes\n",shmaddr, shmsize);

    sprintf(logbuffer, "Shared Memory at: %u %d bytes\n",shmaddr, shmsize);
    printlog("get_shm", logbuffer, 0);

    shm->counts = shmaddr;
    // Set counters
    shm->counts->element_count = (short int) ecount;
    shm->counts->ai_count = (short int) aicount;
    shm->counts->ao_count = (short int) aocount;
    shm->counts->di_count = (short int) dicount;
    shm->counts->do_count = (short int) docount;
    shm->counts->var_count = (short int) varcount;
    shm->counts->hw_count = (short int) hwcount;
    setShmPointers();

    //printf("netios 1: %x \n",shm->netios);
    shm->config->shmid = shmid;
    shm->config->shmsize = shmsize;
    // Initialize hwstruct
    //printf("hwcount = %d\n",hwcount);
    for(i=0;i<hwcount;i++){
      shm->hwconfig[i].index = i;
      strcpy(shm->hwconfig[i].name,hw[i].name);
      shm->hwconfig[i].handler = hw[i].handler;
      shm->hwconfig[i].type = hw[i].type;
      shm->hwconfig[i].ordinal = hw[i].ordinal;
      shm->hwconfig[i].ctype = hw[i].ctype;
      shm->hwconfig[i].channelcount = hw[i].channelcount;
      shm->hwconfig[i].ioperchannel = hw[i].ioperchannel;
      //printf("Set hw %d: type %d %s %hd\n",i,shm->hwconfig[i].type,shm->hwconfig[i].name,shm->hwconfig[i].handler);
    }
    //printf("netios 2: %x elements %x\n",shm->netios,shm->evals);

    free(hw);
    for(i=0;i<MAX_ELEMENTS;i++){
      shm->elements[i].val = &shm->evals[i];
      //printf("eval %d = %d\n",i,shm->elements[i].val);
    }
    read_diskfiles(shm);


    //printf("hw 0: %s %d\n",shm->hwconfig[0].name,shm->hwconfig[0].handler);
    shm->config->shminit = 1;
    shm->config->reload = -1;
    //printf("shm key %d, size = %d at %ld\n",key, shmsize,shm);

  }else{

    //printf("Shared memory found 1: %x\n",shmid);
    // Link to our shared memory
    //shm = shmat(shmid, SHBASE, 0);
    shmaddr = shmat(shmid, NULL, 0);
    if (shmaddr == -1) {
      perror("shmat");
      exit(1);
    }
    //printf("Shared Memory at: %u %d bytes\n",shmaddr, shmsize);
    setShmPointers();
    //printf("SHM = %u -> %u\n",shm,*shm);
    //printf("shm->config: %u\n",shm->config);

    //printf("Shared memory found 2: %x\n",shm);
  }
    // Force initial file read for client
  if(pid){
    //printf("Setting reload from %x to %x\n",shm->config->reload,shm->config->reload | (unsigned long int)pid);
    shm->config->reload = (unsigned long int)shm->config->reload | (unsigned long int)pid;
    //printf("Setting reload from %x to %d\n",shm->config->reload,pid);
  }
  //printf("logrec size = %d\n",sizeof(logrecstruct));

}

void setShmPointers(){
  // Set global shared memory links. shm is a pointer to shm_index_struct in local address space.
  // The pointers need to be set to wherever shared memory ended up being mapped for this process.
/*
  typedef struct {
    count_struct *counts;
    config_struct *config;
    float *evals;
    hw_struct *hwconfig;
    rule_struct *rules;
    element_struct *elements;
    stype_struct *stypes;
    float *aivalue;
    byte_t *aistatus;
    byte_t *divalue;
    byte_t *dovalue;
    pid_struct *pids;
    netio_struct *netios;
    ha7struct *ha7;
    int *msgindex;
    char *status;
  } shm_index_struct;
*/


  //printf("shm %u shmaddr %u %d\n",shm,sizeof(count_struct));

  shm->counts = shmaddr;

  shm->config = shm->counts + 1;
  //printf("config %u %d\n",shm->config,sizeof(config_struct));

  shm->evals = shm->config + 1;
  //printf("evals %u %d %d %d\n",shm->evals,sizeof(float),shm->counts->element_count,sizeof(float)*shm->counts->element_count);

  shm->elements = shm->evals + shm->counts->element_count;
  //printf("elements %u %d %d %d\n",shm->elements,sizeof(element_struct),shm->counts->element_count,sizeof(element_struct)*shm->counts->element_count);

  shm->rules = shm->elements + shm->counts->element_count;
  //printf("rules %u %d %d %d\n",shm->rules,sizeof(rule_struct),MAX_RULES,sizeof(rule_struct)*MAX_RULES);

  shm->aivalue = shm->rules + MAX_RULES;
  //printf("aivalue %u %d %d %d\n",shm->aivalue,sizeof(float),shm->counts->ai_count,sizeof(float)*shm->counts->ai_count);

  shm->aistatus = shm->aivalue + shm->counts->ai_count;
  //printf("aistatus %x %d %d %d\n",shm->aistatus,sizeof(byte_t),shm->counts->ai_count,sizeof(byte_t)*shm->counts->ai_count);

  shm->divalue = shm->aistatus + shm->counts->ai_count;
  //printf("divalue %x %d %d %d\n",shm->divalue,sizeof(byte_t),shm->counts->di_count,sizeof(byte_t)*shm->counts->di_count);

  shm->dovalue = shm->divalue + shm->counts->di_count;
  //printf("dovalue %ud %d %d %d\n",shm->dovalue,sizeof(byte_t),shm->counts->do_count,sizeof(byte_t)*shm->counts->do_count);

  shm->hwconfig = shm->dovalue + shm->counts->do_count;
  //printf("netios %x %d %d %d\n",shm->netios,sizeof(netio_struct),shm->counts->netio_count,sizeof(netio_struct)*shm->counts->netio_count);

  shm->stypes = shm->hwconfig + 20;
  //printf("stype %u %d %d %d\n",shm->stypes,sizeof(stype_struct),20,sizeof(stype_struct)*20);

  shm->pids = shm->stypes + 40;
  //printf("pids %u %d %d %d\n",shm->pids,sizeof(pid_struct),shm->counts->ao_count,sizeof(pid_struct)*shm->counts->ao_count);

  shm->msgindex = shm->pids + shm->counts->ao_count;
  //printf("msgindex %u %d\n",shm->msgindex,sizeof(int));

  shm->status = shm->msgindex + 1;
  //printf("status %u %d\n",shm->msgindex,320 * sizeof(char));

  shm->insteonMaster = shm->status + 1;

}

void linkElement(int e){
  int j, prev;
  // we're given new (unlinked) element e. Link it as last of its physio type.
  // If none of its type, after last of previous type.
  // Failing that, add to end of chain.

  prev = 1; // 'FALSE'

  // find last sensor
  for(j=1;j != -1;j = shm->elements[j].next){
    if(shm->elements[j].ptype == 's'){
      prev = j;
    }
  }

  // If new element is a sensor, tag it on
  if(shm->elements[e].ptype == 's'){
    shm->elements[e].next = shm->elements[prev].next;
    shm->elements[prev].next = e;
    return;
  }

  // find last analog out
  for(j=1;j != -1;j = shm->elements[j].next){
    if(shm->elements[j].ptype == 'c'){
      prev = j;
    }
  }

  // If new element is analog out, tag it on
  if(shm->elements[e].ptype == 'c'){
    shm->elements[e].next = shm->elements[prev].next;
    shm->elements[prev].next = e;
    return;
  }

  // find last discrete in
  for(j=1;j != -1;j = shm->elements[j].next){
    if(shm->elements[j].ptype == 'i'){
      prev = j;
    }
  }

  // If new element is discrete in, tag it on
  if(shm->elements[e].ptype == 'i'){
    shm->elements[e].next = shm->elements[prev].next;
    shm->elements[prev].next = e;
    return;
  }

  // find last discrete out
  for(j=1;j != -1;j = shm->elements[j].next){
    if(shm->elements[j].ptype == 'o'){
      prev = j;
    }
  }

  // If new element is discrete out, tag it on
  if(shm->elements[e].ptype == 'o'){
    shm->elements[e].next = shm->elements[prev].next;
    shm->elements[prev].next = e;
    return;
  }

  // find last state variable
  for(j=1;j != -1;j = shm->elements[j].next){
    if(shm->elements[j].pvn == 'v' && shm->elements[j].name[0] == '~'){
      prev = j;
    }
  }

  // If new element is state variable, tag it on
  if(shm->elements[e].pvn == 'v' && shm->elements[e].name[0] == '~'){
    shm->elements[e].next = shm->elements[prev].next;
    shm->elements[prev].next = e;
    return;
  }

  // find last normal variable
  for(j=1;j != -1;j = shm->elements[j].next){
    if(shm->elements[j].pvn == 'v' && shm->elements[j].name[0] != '~'){
      prev = j;
    }
  }

  // If new element is normal variable, tag it on
  if(shm->elements[e].pvn == 'v' && shm->elements[e].name[0] != '~'){
    shm->elements[e].next = shm->elements[prev].next;
    shm->elements[prev].next = e;
    return;
  }
}

int linkUnusedVariable(){
	int c = 0;
	while ((shm->elements[c].pvn != 'v' || shm->elements[c].next != 0)
			&& c < shm->counts->element_count) {
		c++;
	}
	if (c >= shm->counts->element_count && shm->elements[c].next != 0) {
		printf("Error: Can't find unused variable");
		exit(1);
	}
	// c is our new element
	linkElement(c);
	return(c);
}

void unlinkVariable(int c){
	// find the element that points to us
	int n = 0;
	while (shm->elements[n].next != c && n < shm->counts->element_count) {
		n++;
	}
	if (n >= shm->counts->element_count && shm->elements[n].next != c) {
		printf("Error: Element pointer not found for element %hd", c);
		exit(1);
	}
	// n points to us. Make n point to our 'next'
	shm->elements[n].next = shm->elements[c].next;
	shm->elements[c].next = 0;
	if(shm->elements[c].pvn == 'v'){
		shm->elements[c].ptype = 'x';
		shm->elements[c].vdev.vtype = 'x';
	}
	shm->elements[c].owner = 0;
	strcpy(shm->elements[c].name, hardwareName(c));
	write_elements();
	write_vdevs();
	shm->config->reload = (unsigned long int) -1;
}

char * hardwareName(i){
  static char buffer[20];
  if(shm->elements[i].pvn == 'p'){
    switch (shm->elements[i].ptype){
      case 's':
        sprintf(buffer,"Analog Input %2d",shm->elements[i].hwchannel+1);
        break;
      case 'c':
        sprintf(buffer,"Analog Output %2d",shm->elements[i].hwchannel+1);
        break;
      case 'i':
        sprintf(buffer,"Discrete Input %2d",shm->elements[i].hwchannel*8+shm->elements[i].bitnum+1);
        break;
      case 'o':
        sprintf(buffer,"Discrete Output %2d",shm->elements[i].hwchannel*8+shm->elements[i].bitnum+1);
        break;
    }
  }
  if(shm->elements[i].pvn == 'v'){
    sprintf(buffer,"Element %3d",i);
  }
  if(shm->elements[i].pvn == 'n'){
    sprintf(buffer,"Network I/O %2d",shm->elements[i].hwchannel+1);
  }
  return(&buffer);
}

void setElementFromInput(int i){

  // Some input process has set a hardware input value in shared memory.
  // Set the value of the corresponding element if any.
  // Hardware inputs are in SI units, elements are in user-selected units

  float reading;
  byte_t dio[MAX_DISCRETE_IN], rmask, newbit;


  switch (shm->elements[i].ptype) {
    case 's':
      reading = shm->aivalue[shm->elements[i].hwchannel];
      if(!shm->config->centigrade  && shm->stypes[shm->elements[i].stype].conversion == 't'){
        reading = reading * 1.8 + 32;
      }
      //*shm->elements[i].val = reading;
      shm->evals[i] = reading;
      //printf("Element %d (ch %d) set to %f\n",i,shm->elements[i].hwchannel,reading);
      break;
    case 'i':
      dio[0] = shm->divalue[shm->elements[i].hwchannel];
      rmask = 1 << shm->elements[i].bitnum;
      //*shm->elements[i].val = (float) ((dio[0] & rmask) >> shm->elements[i].bitnum);
      shm->evals[i] = (float) ((dio[0] & rmask) >> shm->elements[i].bitnum);

      break;

    default:
      break;
  }
}

void setElement(int i, float value, int file){

  int j;

  // Set element value. Complicated because element may have hardware i/o.
  // If it's a hardware input, we may override the hwio task value
  // If it's an output we have to set the hardware as well as the element
  // If it's a variable, we check the 'file' flag to see if we should write to disk

  float reading;
  byte_t dio[MAX_DISCRETE_IN], rmask, newbit;

  // Is it physical I/O?
  //printf("PVN = %c, Type = %c\n",shm->elements[i].ptype,shm->elements[i].pvn);
  if (shm->elements[i].pvn == 'p') {
    // Analog input. We'll set the element but NOT the hardware (aivalue) structure
    switch (shm->elements[i].ptype) {
      case 's':
        //*shm->elements[i].val = value;
        shm->evals[i] = value;
        break;
        // Discrete input. We'll set the element but NOT the hardware (divalue) structure
        // This means that if we set one bit in a byte and hwio then sets another bit, ours is lost
        // Should not be an issue since hwio should read whole bytes anyway.
      case 'i':
        //*shm->elements[i].val = value ? 1 : 0;
        shm->evals[i] = value ? 1 : 0;
        break;
        // Analog out
      case 'c':
        //*shm->elements[i].val = value;
        shm->evals[i] = value;
        break;
        // Discrete out - we set the dovalue as well
      case 'o':
        //*shm->elements[i].val = value;
        shm->evals[i] = value;
        dio[0] = shm->dovalue[shm->elements[i].hwchannel];
        rmask = 1 << shm->elements[i].bitnum;
        newbit = 0;
        if (value != 0){
          newbit = rmask;
        }
        dio[0] = dio[0] & (~rmask);
        dio[0] = dio[0] | newbit;
        shm->dovalue[shm->elements[i].hwchannel] = dio[0];
        break;
    }
  }
  // Variables - no special handling now. May add special types later
  // We do check to see if we should write elements to disk
  if (shm->elements[i].pvn != 'p') {
    //printf("Shmaddr %u - Setting elemtn %d at %u %u to %f\n",shmaddr,i,shm->elements,&shm->elements[i].val,value);
    //printf("Setting element %d to %f\n",i,value);
    //*shm->elements[i].val = value;
    shm->evals[i] = value;
    if (file){
      //puts("1");
      write_elements();
      //puts("2");
    }
    //puts("3");
  }

}

float elementValue(int i){
  //return *shm->elements[i].val;
  return shm->evals[i];
}

void clearReloadFlag(int pid){
  shm->config->reload = shm->config->reload & ~ (1 << pid);
  //printf("PID %2d mask %s\n",pid,byte_to_binary(shm->config->reload));
}

int reloadRequired(int pid){
  return shm->config->reload & (1 << pid);
}

int claimElement(int i, int pid){
  char msgbuff[80];
  int retval;

  retval = 0;
  if(shm->elements[i].owner != pid && shm->elements[i].owner != 0){
    sprintf(logbuffer, "Element %d claimed by %d and %d",i,shm->elements[i].owner,pid);
    printlog("utils", logbuffer, 1);
    retval = shm->elements[i].owner;
  }
  shm->elements[i].owner = pid;
  return(retval);
}

unsigned long long getNow(){
  struct timeval now;
  if(gettimeofday(&now,NULL)){
    printlog("utils:getNow returned error %s", logbuffer,strerror(errno), 1);
  }
  return (unsigned long long) now.tv_sec * USEC_PER_SEC + now.tv_usec;
}

void timedSleep(char processname[], unsigned short int period, unsigned long long *start_usec){
  unsigned long long now_usec;
  char msgbuff[256];
  int sleeptime;

  now_usec = getNow();

  // Did system clock change throw us into the future? If so, start time will be more than one period away.
  if(*start_usec > (unsigned long long)(now_usec + (period * USEC_PER_SEC / 10))){
    sprintf(logbuffer, "1: Start time in future: %llu start, %llu now, %d period",
        (unsigned long long)(*start_usec/1000000),(unsigned long long)(now_usec/1000000),(int)period);
    printlog(processname, logbuffer, 1);
    *start_usec = now_usec + (period * USEC_PER_SEC / 10);
    sleeptime = (period * USEC_PER_SEC / 10);
  }else{
    *start_usec += period * USEC_PER_SEC / 10;
    sleeptime = (int)(*start_usec - now_usec);
  }

  if(sleeptime <= 0){
		sprintf(logbuffer, "Exceeded time slot by %0.0f milliseconds",(float)-sleeptime*1000/USEC_PER_SEC-(float)period*10);
		//sprintf(logbuffer, "over time slice %f sec",(float)(sleeptime/-1000000));
		printlog(processname, logbuffer, 0);
    *start_usec = now_usec + 10000;
    sleeptime = 100000;
  }

  // This shouldn't execute
  if(sleeptime > (period * USEC_PER_SEC / 10)){
    sprintf(logbuffer, "3: Start time in future: %llu start, %llu now",(unsigned long long)(*start_usec/1000000),(unsigned long long)(now_usec/1000000));
    //sprintf(logbuffer, "Sleep time > period: %f sec",(float)(sleeptime/1000000));
    printlog(processname, logbuffer, 1);
    sleeptime = (period * USEC_PER_SEC / 10);
  }
  usleep(sleeptime);
}

void post_message(char *msg){
  struct timeval now;
  time_t timestamp;
  struct tm * timeinfo;
  char buffer[79];
  gettimeofday (&now,NULL);
  timestamp = time(NULL);
  time (timestamp);
  timeinfo = localtime (&timestamp);
  strftime (buffer,10, "%H:%M",timeinfo);
  // sprintf(&status[*msgindex],"%s %s",buffer,msg);
  // *msgindex = (*msgindex+1) % 4;
}

void unescape(char *source)
{
  int i,cchar;

  i=0;
  while(source[i]){
    // escaped characters: %xx
    if(source[i] == '%'){
      sscanf(&source[i+1],"%2x",&cchar);
      source[i] = (char)cchar;
      strcpy(&source[i+1],&source[i+3]);
    }
    i++;
  }
}

/* split an input string into parameter/value pairs. Pick
   a parameter and value off the front, return pointer to the
   balance of the string, 0 if we're done.

   p and v are indeterminate if input string has no '=' char.
 */

char *split(char *source, char **p, char **v)
{

  char *amp, *eq;

  /* if there's an = sign, make it an end-of-line */
  if(eq=strchr(source,'=')){
    eq[0] = '\0';
    *p = source;
    *v = eq+1;
    eq++;
    // Look for ampersand
    if(amp=strchr(eq,'&')){
      // Make it a string terminator
      amp[0] = '\0';
      return(amp+1);
    }else{
      // Maybe we're parsing a cookie. Separator is '; '
      if(amp=strchr(eq,';')){
        amp[0] = '\0';
        return(amp+2);
      }else{
        return(0);
      }
    }
  }
  return(0);
}

// Translate + and %xx characters in array of cgi values

void cleanup(int pcount, char **v){
  int i, j, cchar;

  for(j=0;j<pcount;j++){
    i = 0;
    while(v[j][i]){
      if(v[j][i] == '+'){
        v[j][i] = ' ';
      }
      // escaped characters: %xx
      if(v[j][i] == '%'){
        sscanf(&v[j][i+1],"%2x",&cchar);
        v[j][i] = (char)cchar;
        strcpy(&v[j][i+1],&v[j][i+3]);
      }
      i++;
    }
  }

}

char from_hex(char ch) {
  return isdigit(ch) ? ch - '0' : tolower(ch) - 'a' + 10;
}

char to_hex(char code) {
  static char hex[] = "0123456789abcdef";
  return hex[code & 15];
}

char *url_encode(char *str) {
  char *pstr = str, *buf = malloc(strlen(str) * 3 + 1), *pbuf = buf;
  while (*pstr) {
    if (isalnum(*pstr) || *pstr == '-' || *pstr == '_' || *pstr == '.' || *pstr == '~')
      *pbuf++ = *pstr;
    else if (*pstr == ' ')
      *pbuf++ = '+';
    else
      *pbuf++ = '%', *pbuf++ = to_hex(*pstr >> 4), *pbuf++ = to_hex(*pstr & 15);
    pstr++;
  }
  *pbuf = '\0';
  return buf;
}

char *url_decode(char *str) {

  char *pstr = str, *buf = malloc(strlen(str) + 1), *pbuf = buf;
  while (*pstr) {
    if (*pstr == '%') {
      if (pstr[1] == '%') {
        *pbuf++ = *pstr;
      }else{
        if (pstr[1] && pstr[2]) {
          *pbuf++ = from_hex(pstr[1]) << 4 | from_hex(pstr[2]);
          pstr += 2;
        }
      }
    } else if (*pstr == '+') {
      *pbuf++ = ' ';
    } else {
      *pbuf++ = *pstr;
    }
    pstr++;
  }
  *pbuf = '\0';

  return buf;
}

const char *byte_to_binary(int x)
{
    static char b[65];
    b[0] = '\0';

    int z;
    for (z = 64; z > 0; z >>= 1)
    {
        strcat(b, ((x & z) == z) ? "1" : "0");
    }

    return b;
}

void openLogFile(){

	logfile = fopen("/var/vesta/sd/logs/vesta.csv","a");
	if (!logfile){
		printf("Error opening logfile\n");
	}else{
		chown("/var/vesta/sd/logs/vesta.csv",580,580);
	}
	errfile = fopen("/var/vesta/sd/logs/errlog.csv","a");
	if (!errfile){
		printf("Error opening logfile\n");
	}else{
		chown("/var/vesta/sd/logs/errlog.csv",580,580);
	}
}

void printlog(char *pname, char *msg, int err){
	  struct timeval now;
	  time_t timestamp;
	  struct tm * timeinfo;
	  char tbuffer[21];
	  gettimeofday (&now,NULL);
	  timestamp = time(NULL);
	  //time (timestamp);
	  timeinfo = localtime (&timestamp);
	  strftime(tbuffer,20,"%m/%d/%Y %H:%M:%S",timeinfo);
	  if(err){
		  fprintf(errfile,"%s\t%s\t%s\n",tbuffer,pname,msg);
		  fflush(errfile);
	  }else{
		  fprintf(logfile,"%s\t%s\t%s\n",tbuffer,pname,msg);
		  fflush(logfile);
	  }
}
