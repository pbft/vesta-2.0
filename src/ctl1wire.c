
/*****************************************************************************/
//    This file is part of the NoFossil Control System Software (NFCSS)
//    Copyright 2007, 2008, 2009 Bill Kuhns
//
//    NFCSS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    any later version.

//    NFCSS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with NFCSS.  If not, see <http://www.gnu.org/licenses/>.
/*****************************************************************************/

// Read analog inputs from HA7net 1-wire controller and write to shared memory
// This is a periodic background task

#include <stdio.h>
#include <fcntl.h>
#include <sys/types.h>
#include "vesta.h"

#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#include <netinet/tcp.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

#define PROCESSNAME  FUNCTION_NAME(1WIRE)

/*
#if defined (__STDC__) && __STDC__
char *xmalloc (size_t);
char *xstrdup (char *string);
#else
char *xmalloc ();
#endif
*/

int socket_connect(char *host, in_port_t port){
  struct hostent *hp;
  struct sockaddr_in addr;
  int on = 1, sock;     
  
  if((hp = gethostbyname(host)) == NULL){
    herror("gethostbyname");
    exit(1);
  }

  bcopy(hp->h_addr, &addr.sin_addr, hp->h_length);
  addr.sin_port = htons(port);
  addr.sin_family = AF_INET;

  //sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
  sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
  setsockopt(sock, IPPROTO_TCP, TCP_NODELAY, (const char *)&on, sizeof(int));

  if(sock == -1){
    perror("setsockopt");
    exit(1);
  }

  if(connect(sock, (struct sockaddr *)&addr, sizeof(struct sockaddr_in)) == -1){
    perror("connect");
    exit(1);
  }

  return sock;
}
 
#define BUFFER_SIZE 2048

int main(int argc, char *argv[]){
  FILE *fp;
  int fd;
  int i;
  char buffer[BUFFER_SIZE];
  char *trow;
  float temp;
  char fname[80];
  char query[256];
  char query2[256];
  /*
  ha7struct ha7;
  long long start_usec;



  // Get shared memory links
  get_shm(ONEWIRE_ID);

  fd = open ("/dev/mem" , O_RDWR|O_SYNC);
  if (fd < 0){
      perror ("open");
      exit(1);
  }

  sprintf(&fname[0],"/usr/local/vecs/data/ha7.csv");

  fp = fopen(fname,"r");

  fscanf(fp,"%s",&ha7.ip[0]);

  shm->counts->ha7_count= 0;
  while (fscanf(fp,"%LX",&ha7.devids[shm->counts->ha7_count])>0){
    //printf("Sensor %LX\n",sensorids[shm->ha7_count]);
    shm->counts->ha7_count++;
  }
  fclose(fp);

  sprintf(logbuffer, "1-wire tables read: HA7net at %s with %d sensors\n",ha7.ip,shm->counts->ha7_count);
  printlog(PROCESSNAME, logbuffer, 0);

  start_usec = getNow();

  // Do forever
  while (1){

    if (shm->config->reload & ONEWIRE_ID){
      sprintf(logbuffer, "Reloading files");
      printlog(PROCESSNAME, logbuffer, 0);

     
      // Clear our bit in the reload flag
      shm->config->reload = shm->config->reload & ~ONEWIRE_ID;
    }

    fd = socket_connect(ha7.ip, 80);


    // Build a query for the HA7net that will read all the temp sensors.

    // Start of query...
    sprintf(query,"GET /1Wire/ReadTemperature.html?Address_Array=");
  
    // list of sensors to read
    for(i=0;i<shm->counts->ha7_count;i++){
      sprintf(query2,"%s%LX",i>0 ? ",":"",ha7.devids[i]);
      strcat(query,query2);
      //printf("%s\n",query);
    }
  
    // end of query
    sprintf(query2," HTTP/1.0\n\nAccept: text/html\n\n");
    strcat(query,query2);
    //printf("%s\n",query);
  
    // http query of HA7net controller
    write(fd, query, strlen(query));
    bzero(buffer, BUFFER_SIZE);
  
    i=0;
    while((read(fd, buffer, BUFFER_SIZE - 1) != 0) && i<shm->counts->ha7_count) {
      // We're getting HTML page with all the temp data. Look for sensor ID. Repeat until we have all the sensors
      trow=buffer;
      //printf("---%d\n",i);
      sprintf(fname,"%LX",ha7.devids[i]);
      trow=strstr(trow,fname);
      while(trow != NULL){
        //printf("...%d\n",i);
        // The temp is after the next instance of VALUE= and is surrounded by double quotes.
        if(trow=strstr(trow,"VALUE=")){
          // We have a temp value. i is the channel number within the pool of iwire channels
          // Set -40 in case there's an error and this channel doesn't respond
          temp=-40;
          // Read the value
          sscanf(trow,"VALUE=\"%f\"",&temp);
          //printf("1wire: temp %f \n",temp);
 
          // ************** Need to add calibration code here *********************
          // Calibrate. This is a kludge. It depends on physio (physical.csv) having
          // the first 16 entries corresponding to 16 AD9700 analog inputs and the next 16
          // corresponding to 1-wire devices.

          // calibrate based on sensor type (should be gain of 1 and offset of 0 for 1 -wire, but might not be in the future
          temp = temp * shm->stypes[shm->elements[16+i].stype].gain +  shm->stypes[shm->elements[16+i].stype].offset;

          // Individual sensor calibration
          temp = temp * shm->elements[16+i].gain +  shm->elements[16+i].offset;

          //printf("1wire: gain %f offset %f temp %f \n",physio[16+i].gain, physio[16+i].offset, temp);
          shm->aivalue[16+i] = temp;
        }
        i++;
        //printf("inc: %d\n",i);
        if(i<shm->counts->ha7_count){
          sprintf(fname,"%LX",ha7.devids[i]);
          trow=strstr(trow,fname);
        }else{
          trow = NULL;
        }
        //printf("%s\n",trow);
      }
      //printf(">>>%d\n",i);
      bzero(buffer, BUFFER_SIZE);

    }
    shutdown(fd, SHUT_RDWR); 
    close(fd); 

    // Sleep for desired interval
    timedSleep(PROCESSNAME, shm->config->ha7_period, &start_usec);

  }
    */
  return 0;
}
