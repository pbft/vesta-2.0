
/*****************************************************************************/
//    This file is part of the NoFossil Control System Software (NFCSS)
//    Copyright 2007, 2008, 2009 Bill Kuhns
//
//    NFCSS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    any later version.

//    NFCSS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with NFCSS.  If not, see <http://www.gnu.org/licenses/>.
/*****************************************************************************/

// *********** BUG LIST ******************************************************

#define PROCESSNAME  FUNCTION_NAME(rule engine)
// Monitor shared memory and make control decisions

#include <stdio.h>
#include <fcntl.h>
#include <sys/types.h>
#include "vesta.h"

void process_arule(int);
void process_drule(int);
void process_trule(int);
void process_mrule(int);
float getRvalue(ruleParm_struct *);

char msgbuff[256];
int sleeptime;

typedef struct {
  unsigned long long start_usec;
  unsigned long long lastmsg;
} wrule_local_struct;

wrule_local_struct wrules[MAX_RULES];

// ****** FIX: NEEDS TO BE CALLOC
float myelements[2][250];

main(){
  long long start_usec;
  float reading;
  int hwch,bitnum,vc;
  short int i,targetid;
  byte_t dio[MAX_DISCRETE_IN], rmask, newbit;

  // Get shared memory links
  get_shm(CTL_ID);

  // This program uses rules and data elements from shared memory.
  // Rules are data structures specific to this program.
  // This program makes two copies of all data element values at the beginning of each pass.
  // The first copy is kept as a reference to see if rules have made a change (for deadband, for instance)
  // The second copy is updated as the program process the rule set.
  // It's written back to shared memory at the end of the pass.

  //===========================================================================================================
  //
  // Initialize and begin the execution loop
  //
  //===========================================================================================================

  start_usec = getNow();
  for (i=0;i<MAX_RULES;i++){
    wrules[i].lastmsg = 0;
    wrules[i].start_usec = 0;
  }

  // Do forever
  while (1){
    // If someone else changed disk files, reload
    if (reloadRequired(CTL_ID)){
      sprintf(logbuffer, "Reloading files");
      printlog(PROCESSNAME, logbuffer, 0);

      // Claim ownership of shm elements that we set
      for (i=0;i<shm->counts->rule_count;i++){
        if(shm->rules[i].mrule.target != 0){
          switch (shm->rules[i].mrule.rtype){
            case 'm': claimElement(shm->rules[i].mrule.target,CTL_ID);
            break;
            case 'd': claimElement(shm->rules[i].drule.target,CTL_ID);
            break;
            case 'a': claimElement(shm->rules[i].arule.target,CTL_ID);
            break;
            case 't': claimElement(shm->rules[i].trule.target,CTL_ID);
            break;
          }
        }
      }
      // set historic element values
      for (i=0;i<shm->counts->element_count;i++){
        //printf("%d %ux\n",i,(int)*shm->elements[i].val);
        myelements[0][i] = elementValue(i);
      }

      // Clear our bit in the reload flag
      clearReloadFlag(CTL_ID);
    }
    // Copy element values
    for (i=0;i<shm->counts->element_count;i++){
      myelements[1][i] = elementValue(i);
      //printf("Element %d %0.2f\n",i,myelements[1][i]);
    }
    // Set all output elements except variables to zero for elements belonging to rules
    for (i=0;i<shm->counts->rule_count;i++){
      if(shm->rules[i].mrule.rtype != 'c' && shm->rules[i].mrule.rtype != 'w'){
        targetid = shm->rules[i].mrule.target;
        // Only for those elements that we control...
       //printf("%d Element %d %s %c %c ",shm->elements[targetid].owner,targetid,shm->elements[targetid].name,shm->elements[targetid].io,shm->elements[targetid].etype);
       if (shm->elements[targetid].owner == CTL_ID){
         // Outputs, not variables, and not analog outputs
          if (shm->elements[targetid].io == 'o' && shm->elements[targetid].pvn != 'v' && shm->elements[targetid].ptype != 'c') {

            myelements[1][targetid] = 0;
            //printf("Set to 0");
          }
          // If it's a state variable, zero it also. Starts with '~'
          if (shm->elements[targetid].io == 'o' && shm->elements[targetid].name[0] == '~') {

            myelements[1][targetid] = 0;
            //printf("Set to 0");
          }

        }
       //printf("\n");
      }
    }

    // Process rules next

    for (i=0;i<shm->counts->rule_count;i++){
      switch (shm->rules[i].mrule.rtype){
        case 'm': process_mrule(i);
        break;
        case 'd': process_drule(i);
        break;
        case 'a': process_arule(i);
        break;
        case 't': process_trule(i);
        break;
        case 'w': process_wrule(i);
        break;
      }
    }

    // ************************************************************************************************ 
    // write results to shared memory
    // sleep until next interval
    // ************************************************************************************************ 

    // Read all of our digital IO from shared memory - other tasks may have set some values.

    // Outputs only :-)
    //for (i=0;i<MAX_DISCRETE_IN;i++){
    //  dio[i] = shm->dovalue[i];
    //}
    for (i=0;i<shm->counts->rule_count;i++){
      switch (shm->rules[i].mrule.rtype){
        case 'm': setElement( shm->rules[i].mrule.target, myelements[1][shm->rules[i].mrule.target], 0);
        break;
        case 'd': setElement( shm->rules[i].mrule.target, myelements[1][shm->rules[i].mrule.target], 0);
        break;
        case 'a': setElement( shm->rules[i].mrule.target, myelements[1][shm->rules[i].mrule.target], 0);
        break;
        case 't': setElement( shm->rules[i].mrule.target, myelements[1][shm->rules[i].mrule.target], 0);
        break;
      }

    }

    for (i=0;i<shm->counts->element_count;i++){
      // If we own it, write it
      if (shm->elements[i].owner == CTL_ID){
        setElement( i, myelements[1][i], 0);
        //printf("Setting %d to %f\n",i,myelements[1][i]);
      }
    }

    // Copy historic element values
    for (i=0;i<shm->counts->element_count;i++){
      myelements[0][i] = myelements[1][i];
    }

    // Sleep for desired interval
    timedSleep(PROCESSNAME, shm->config->rule_period, &start_usec);
  }

}

void sendMessage(int i, int dest, int warn){
  char entry[200];
  char msg[80];
  char serial[10];
  char *body;
  char *dest1;
  char *dest2;

  //printf("ctext = %s\n",shm->rules[i].wrule.ctext);
  strcpy(msg,shm->rules[i].wrule.ctext);

  // Replace #data with %0.1f
  if(dest1 = strstr(msg,"#data")){
    strncpy(dest1,"%0.1f",5);
    sprintf(entry,msg,elementValue(shm->rules[i].wrule.wdata));
    strcpy(msg,entry);
  }
  body = url_encode(strtok(msg,";"));
  dest1 = strtok(NULL,";");
  dest2 = strtok(NULL,";");

  if(dest == 1){
    sprintf(entry,"wget -q \"http://www.vecs.org/warning.php?recipient=%s&warn=%d&message=%s&serial=\"`cat /etc/serial` > /dev/null",dest1,warn,body);
    printf("%s\n",entry);
    system(entry);
  }
  if(dest == 2){
    sprintf(entry,"wget -q \"http://www.vecs.org/warning.php?recipient=%s&warn=%d&message=%s&serial=\"`cat /etc/serial` > /dev/null",dest2,warn,body);
    system(entry);
  }
}

void process_wrule(int i){
	unsigned long long now;
	// Rule is triggered?
  if((shm->rules[i].wrule.tnt && elementValue(shm->rules[i].wrule.state)) || (shm->rules[i].wrule.tnt == 0 && elementValue(shm->rules[i].wrule.state) == 0)){
  	now = getNow();
 	// Is it new?
    if(wrules[i].start_usec == 0){
      wrules[i].start_usec = now;
      // Force initial message after delay
      wrules[i].lastmsg = now - (shm->rules[i].wrule.interval - shm->rules[i].wrule.delay) * USEC_PER_SEC;
    }
		// Has message1 interval expired since last message?
		if((wrules[i].lastmsg + shm->rules[i].wrule.interval* USEC_PER_SEC) <= now){
			sendMessage(i,1,1);
			wrules[i].lastmsg = now;
		}
		// Send escalated message?
		if(wrules[i].start_usec + (shm->rules[i].wrule.delay2 * USEC_PER_SEC) <= now){
			sendMessage(i,2,1);
			wrules[i].start_usec = now;
		}
    shm->rules[i].wrule.active=1;
  }else{
    // Rule is not triggered
  	// First pass after trigger?
  	if(wrules[i].start_usec != 0){
  		// Send message on warning end?
  		if(shm->rules[i].wrule.sns){
  			sendMessage(i,1,0);
  		}
  	}
    wrules[i].start_usec = 0;
    //printf("wrule not triggered: tnt = %d, state = %d\n",shm->rules[i].wrule.tnt,(int)elementValue(shm->rules[i].wrule.state));
    shm->rules[i].wrule.active=0;
  }

}

void process_mrule(int i){

  switch (shm->rules[i].mrule.op) {
    //case '+': myelements[1][shm->rules[i].mrule.target] = myelements[1][shm->rules[i].mrule.e1] + myelements[1][shm->rules[i].mrule.e2];
    case '+': myelements[1][shm->rules[i].mrule.target] = getRvalue(&shm->rules[i].mrule.e1) + getRvalue(&shm->rules[i].mrule.e2);
    break;
    //case '-': myelements[1][shm->rules[i].mrule.target] = myelements[1][shm->rules[i].mrule.e1] - myelements[1][shm->rules[i].mrule.e2];
    case '-': myelements[1][shm->rules[i].mrule.target] = getRvalue(&shm->rules[i].mrule.e1) - getRvalue(&shm->rules[i].mrule.e2);
    break;
    //case '*': myelements[1][shm->rules[i].mrule.target] = myelements[1][shm->rules[i].mrule.e1] * myelements[1][shm->rules[i].mrule.e2];
    case '*': myelements[1][shm->rules[i].mrule.target] = getRvalue(&shm->rules[i].mrule.e1) * getRvalue(&shm->rules[i].mrule.e2);
    break;
    //case '/': myelements[1][shm->rules[i].mrule.target] = myelements[1][shm->rules[i].mrule.e1] / myelements[1][shm->rules[i].mrule.e2];
    case '/': myelements[1][shm->rules[i].mrule.target] = getRvalue(&shm->rules[i].mrule.e1) / getRvalue(&shm->rules[i].mrule.e2);
    break;
    default: printf("Error in ctl: mrules op = %c\n",shm->rules[i].mrule.op);
    break;
  }
  /*
  printf("Rule type %c\n",shm->rules[i].mrule.rtype);
  printf("Rule target %hd\n",shm->rules[i].mrule.target);
  printf("Rule active %hd\n",shm->rules[i].mrule.active);
  printf("Rule e1 %hd %hd %f\n",shm->rules[i].mrule.e1.eflag,shm->rules[i].mrule.e1.rval.eid,shm->rules[i].mrule.e1.rval.value);
  printf("Rule op %c\n",shm->rules[i].mrule.op);
  printf("Rule e2 %hd %hd %f\n",shm->rules[i].mrule.e2.eflag,shm->rules[i].mrule.e2.rval.eid,shm->rules[i].mrule.e2.rval.value);
  printf("element 2 value %f\n",myelements[1][2]);
  printf("element 3 value %f\n",myelements[1][3]);
  */
  shm->rules[i].mrule.active=1;
}

void process_drule(int i){
  int j;
  float deadband;
  rule_struct *myrule;
  myrule = &shm->rules[i];
  j = 0;
  deadband = getRvalue(&shm->rules[i].drule.deadband);

  // Greater than
  if (shm->rules[i].drule.gle == 'g'){
    // if already triggered, use lower threshold
    if(myelements[0][shm->rules[i].drule.target] != 0){
      if (myelements[1][shm->rules[i].drule.e1] > (getRvalue(&shm->rules[i].drule.e2) + getRvalue(&shm->rules[i].drule.diff) - deadband)){
        j = 1;
      }
    }else{
      if (myelements[1][shm->rules[i].drule.e1] > (getRvalue(&shm->rules[i].drule.e2) + getRvalue(&shm->rules[i].drule.diff))){
        j = 1;
      }
    }
  }
  // Less than
  if (shm->rules[i].drule.gle == 'l'){
    // if not already triggered, use higher threshold
    if(myelements[0][shm->rules[i].drule.target] != 0){
      if (myelements[1][shm->rules[i].drule.e1] < (getRvalue(&shm->rules[i].drule.e2) - getRvalue(&shm->rules[i].drule.diff))){
        j = 1;
      }
    }else{
      if (myelements[1][shm->rules[i].drule.e1] < (getRvalue(&shm->rules[i].drule.e2) - getRvalue(&shm->rules[i].drule.diff) - deadband)){
        j = 1;
      }
    }
  }
  myelements[1][shm->rules[i].drule.target] = j;
  shm->rules[i].drule.active=j;
}

void process_arule(int i){
  // If any of the referenced elements are discrete outputs, they will have been set to 0 at the beginning of this cycle.
  // They *may* get set to 1 by a rule subsequent to this one, leading to confusing results.
  // We use lastval (value previous to this cycle). That means if a previous rule in *this* cycle set the value to 1, we won't know until
  // next cycle.

  int oldval, newval;
  rule_struct *myrule;
  myrule = &shm->rules[i];

  // Process only actual rules - target will not be 0
  if (shm->rules[i].arule.target > 0){
    // Set output to current value (an earlier rule may have set it)
    //oldval = (int)shm->elements[shm->rules[i].arule.target].val;

    newval = ((int)myelements[1][shm->rules[i].arule.e1] == 0 ? 0 : 1)  ^ shm->rules[i].arule.n1;
    if (shm->rules[i].arule.e2 > 0){
      newval = newval && (((int)myelements[1][shm->rules[i].arule.e2] == 0 ? 0 : 1) ^ shm->rules[i].arule.n2);
      if (shm->rules[i].arule.e3 > 0){
        newval = newval && (((int)myelements[1][shm->rules[i].arule.e3] == 0 ? 0 : 1) ^ shm->rules[i].arule.n3);
      }
    }
    // If newval is nonzero, get value from v1
    //printf("Rule %d newval=%d\n",i,newval);
    if (newval != 0){
      myelements[1][shm->rules[i].arule.target] = getRvalue(&shm->rules[i].arule.v1);
      shm->rules[i].arule.active=1;
    }else{
      shm->rules[i].arule.active=0;
    }
    //printf("Rule %d %d %f %d %f\n",i,shm->rules[i].arule.e2,(int)myelements[1][shm->rules[i].arule.e2],(int)myelements[1][shm->rules[i].arule.e3],(int)myelements[1][shm->rules[i].arule.e3]);
  }
}

void process_trule(int i){
  rule_struct *myrule;
  int j;

  myrule = &shm->rules[i];

  j = 0;  // Check whether we triggered
  if (shm->rules[i].trule.target > 0){

    //printf ("Timer %d set to %d\n",i, (int)shm->elements[shm->rules[i].trule.target].val);
    // Do we trigger? Case 1: 'is' and 'true' and e1 is true
    if (shm->rules[i].trule.ib == 0 && myelements[1][shm->rules[i].trule.e1] !=0 && shm->rules[i].trule.tf == 1){
      //myelements[1][shm->rules[i].trule.target] = myelements[1][shm->rules[i].trule.e2];
      myelements[1][shm->rules[i].trule.target] = getRvalue(&shm->rules[i].trule.e2);
      j = 1;
    }

    // Do we trigger? Case 2: 'is' and 'false' and e1 is false
    if (shm->rules[i].trule.ib == 0 && myelements[1][shm->rules[i].trule.e1] ==0 && shm->rules[i].trule.tf == 0){
      //myelements[1][shm->rules[i].trule.target] = myelements[1][shm->rules[i].trule.e2];
      myelements[1][shm->rules[i].trule.target] = getRvalue(&shm->rules[i].trule.e2);
      j = 2;
    }

    // Do we trigger? Case 3: 'becoming' and 'true' and e1 is true and e1 was false
    if (shm->rules[i].trule.ib == 1 && myelements[1][shm->rules[i].trule.e1] != 0 && shm->rules[i].trule.tf == 1 && myelements[0][shm->rules[i].trule.e1] == 0){
      //myelements[1][shm->rules[i].trule.target] = myelements[1][shm->rules[i].trule.e2];
      myelements[1][shm->rules[i].trule.target] = getRvalue(&shm->rules[i].trule.e2);
     j = 3;
    }

    // Do we trigger? Case 4: 'becoming' and 'false' and e1 is false and e1 was true
    if (shm->rules[i].trule.ib == 1 && myelements[1][shm->rules[i].trule.e1] == 0 && shm->rules[i].trule.tf == 0 && myelements[0][shm->rules[i].trule.e1] != 0){
      //myelements[1][shm->rules[i].trule.target] = myelements[1][shm->rules[i].trule.e2];
      myelements[1][shm->rules[i].trule.target] = getRvalue(&shm->rules[i].trule.e2);
      j = 4;
    }

    //printf ("Timer: ib = %d, tf = %d, val = %d, last = %d, trig = %d\n",shm->rules[i].trule.ib,shm->rules[i].trule.tf,(int)shm->elements[shm->rules[i].trule.e1].val,(int)shm->elements[shm->rules[i].trule.e1].lastval,j);


    // If we didn't trigger AND we haven't already been decremented, then decrement timer
    if ((j == 0) && (myelements[1][shm->rules[i].trule.target] > 0) && (myelements[1][shm->rules[i].trule.target] == myelements[0][shm->rules[i].trule.target])){
      //printf("Timer: %d rule %d element %d time%f\n",i,j,shm->rules[i].trule.target,shm->elements[shm->rules[i].trule.target].val);
      myelements[1][shm->rules[i].trule.target] -= ((float)shm->config->rule_period/10.0);
      //printf("Timer: %d decrement %f\n",i,((float)shm->config->rule_period/10));
      if (myelements[1][shm->rules[i].trule.target] < .2){
        myelements[1][shm->rules[i].trule.target] = 0;
      }
    }else{
      //printf("No decrement: rule = %d, trigger %d target %d = %f\n",i,j,shm->rules[i].trule.target,shm->elements[shm->rules[i].trule.target].val);
    }
  }
  shm->rules[i].trule.active=j;
}

float getRvalue(ruleParm_struct *e){
  if(e->eflag){
    return(myelements[1][e->rval.eid]);
  }else{
    return(e->rval.value);
  }
}
