
/*****************************************************************************/
//    This file is part of the NoFossil Control System Software (NFCSS)
//    Copyright 2007, 2008, 2009 Bill Kuhns
//
//    NFCSS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    any later version.

//    NFCSS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with NFCSS.  If not, see <http://www.gnu.org/licenses/>.
/*****************************************************************************/

// Read analog inputs from TS-9700 A/D card and output timestamped binary file

#include <stdio.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/types.h>
#include "../include/mysql/mysql.h"
#include "vesta.h"

#define PROCESSNAME  FUNCTION_NAME(sqllogger)

main(){

  struct timeval now;
  long long start_usec;
  long start_sec;
  long long now_usec;
  int sleeptime;

  time_t curtime;
  struct tm *loctime;
  MYSQL myserver;
  MYSQL_ROW myrow;
  MYSQL_RES *result;

  int i,j,acount,dcount;
  char outbuff[5000];
  char buff2[5000];
  char myservername[40];
  char myusername[40];
  char mypassword[40];
  int myport;
  char mydbname[40];
  char mytablename[40];
  FILE *fp;
  char linebuff[256];
  char fldname[5];
  byte_t rmask, newbit;

  typedef struct{
    char type;
    int index;
    char fldname[5];
    unsigned int bitnum;
  } e_struct;
  e_struct *etypes;
  unsigned int dvalues[10];

  // Get shared memory links

  get_shm(SQLLOGGER_ID);

  gettimeofday(&now,NULL);
  start_sec = now.tv_sec;
  start_usec = now.tv_sec * 1000000 + now.tv_usec;

  curtime = time(NULL);
  loctime=localtime(&curtime);

  fp = fopen("/usr/local/vesta/data/sql-logger.csv","r");

  //fgets(linebuff,256,fp);
  fscanf(fp,"\"%[^\"]\",\"%[^\"]\",\"%[^\"]\",%d,\"%[^\"]\",\"%[^\"]\"",
      &myservername,&myusername,&mypassword,&myport,&mydbname,&mytablename);
  fclose(fp);
  //printf("%s %s %s %d %s %s\n",myservername,myusername,mypassword,myport,mydbname,mytablename);

  mysql_init(&myserver);
  if (!(mysql_real_connect(&myserver,myservername,myusername,NULL,mydbname,myport,NULL,0))){
    printf("Error Connecting to %s user %s db %s table %s\n",myservername,myusername,mydbname,mytablename);
    abort();
  }

  etypes = (e_struct *) calloc(shm->counts->element_count,sizeof(char));

  for(i=0;i<shm->counts->element_count;i++){
      if(shm->elements[i].pvn == 'v'){
        // Variable - could be normal or state
        if(shm->elements[i].name[0] == '~'){
          etypes[i].type = 'd';
          etypes[i].index = (unsigned int) dcount/32;
          etypes[i].bitnum = dcount % 32;
          dcount++;
        }else{
          acount++;
          etypes[i].type  = 'a';
       }
      }else{
        // Not a variable. 'o' and 'i' are discretes
        if(shm->elements[i].ptype == 'o' || shm->elements[i].ptype == 'i'){
          etypes[i].type = 'd';
          etypes[i].index = (unsigned int) dcount/32;
          etypes[i].bitnum = dcount % 32;
          dcount++;
        }else{
          acount++;
          etypes[i].type = 'a';
        }
      }
      if(etypes[i].type  == 'a'){
        sprintf(etypes[i].fldname,"a%03d",i);
      }
      if(i % 32 == 0){
        sprintf(fldname,"d%03d",(unsigned int) i/32);
      }
      if(etypes[i].type  == 'd'){
        strcpy(etypes[i].fldname,fldname);
      }
  }

  sprintf(linebuff,"select count(extra) from information_schema.columns where table_name=\"%s\"",mytablename);
  printf("%s\n",linebuff);
  mysql_query(&myserver,linebuff);
  result = mysql_store_result(&myserver);
  myrow = mysql_fetch_row(result);
  printf("%s\n",myrow[0]);
  if(!strcmp(myrow[0], "0")){
    // Build tables


    sprintf(linebuff,"create table `%s` (`uid` int(11) NOT NULL AUTO_INCREMENT, ",mytablename);
    strcat(linebuff,"`ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP, ");
    strcat(linebuff,"`ts1` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00', ");
    strcat(linebuff,"`loctime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00', ");
    strcat(linebuff,"`layout` int(11) NOT NULL, ");
    strcat(linebuff,"PRIMARY KEY (`uid`), KEY `loctime` (`loctime`))");
    printf("%s\n",linebuff);
    mysql_query(&myserver,linebuff);


    printf("Acount = %d, dcount = %d\n",acount,dcount);
    for(i=0;i<shm->counts->element_count;i++){
      if(etypes[i].type  == 'a' && shm->elements[i].next != 0){
        sprintf(linebuff,"ALTER TABLE %s ADD a%03d FLOAT NULL",mytablename,i);
        printf("%s\n",linebuff);
        mysql_query(&myserver,linebuff);
        sprintf(linebuff,"insert into layoutitem values (NULL,%d, %s, %d,'%c','%s',%d,%d)",1,etypes[i].fldname,i,'a',shm->elements[i].name,0,0);
        //printf("%s\n",linebuff);
        mysql_query(&myserver,linebuff);
      }
    }
    for(i=0;i<shm->counts->element_count;i++){
      if(i % 32 == 0){
        sprintf(linebuff,"ALTER TABLE %s ADD d%03d INT NULL",mytablename,(unsigned int) i/32);
        printf("%s\n",linebuff);
        mysql_query(&myserver,linebuff);
      }
      if(etypes[i].type  == 'd' && shm->elements[i].next != 0){
        sprintf(linebuff,"insert into layoutitem values (NULL,%d,%d,'%c','%s',%d,%d)",1,i,'d',shm->elements[i].name,etypes[i].index,etypes[i].bitnum);
        printf("%s\n",linebuff);
        mysql_query(&myserver,linebuff);
      }
    }
  }
  // Force initial config file load
  //shm->reload = shm->reload | SQLLOGGER_ID;
  while (1){

    gettimeofday(&now,NULL);
    curtime = time(NULL);
    loctime=localtime(&curtime);
    sprintf(&outbuff[0],"insert into %s (ts,loctime,layout",mytablename );
    strftime(&buff2[0],40," values (NULL,\"%F %T\",1",loctime);

    for(i=0;i<shm->counts->element_count;i++){
      if(etypes[i].type  == 'a' && shm->elements[i].next != 0){
        sprintf(linebuff,",a%03d",i);
        strcat(outbuff,linebuff);
        sprintf(linebuff,",%f",elementValue(i));
        strcat(buff2,linebuff);
      }
      if(etypes[i].type  == 'd' && shm->elements[i].next != 0){
        j = (int) i / 32;
        rmask = 1 << etypes[i].bitnum;
        newbit = 0;
        if (elementValue(i) != 0){
          newbit = rmask;
        }
        dvalues[j] = dvalues[j] & (~rmask);
        dvalues[j] = dvalues[j] | newbit;
      }
    }
    for(i=0;i<=(shm->counts->element_count/32);i++){
      sprintf(linebuff,",d%03d",i);
      strcat(outbuff,linebuff);
      sprintf(linebuff,",%d",dvalues[i]);
      strcat(buff2,linebuff);
    }
    strcat(outbuff,")");
    strcat(buff2,")");
    strcat(outbuff,buff2);
    //printf("%s\n",outbuff);
    mysql_query(&myserver,outbuff);
    // Sleep for desired interval
    timedSleep(PROCESSNAME, shm->config->sqllog_period, &start_usec);
  }

}


