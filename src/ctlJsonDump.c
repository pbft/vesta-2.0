
/*****************************************************************************/
//    This file is part of the NoFossil Control System Software (NFCSS)
//    Copyright 2007, 2008, 2009 Bill Kuhns
//
//    NFCSS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    any later version.

//    NFCSS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with NFCSS.  If not, see <http://www.gnu.org/licenses/>.
/*****************************************************************************/

// Read binary logfile(s) and output JSON response

/* Response will have two objects: Elements and data
 * Elements will contain the following for each element:
 *  ID
 *  Name
 *  pvn (physical / virtual / network)
 *  ptype - s/c/i/o/x - (Sensor / Control / Input /  Output / Other)
 *  conversion (t)emp, (p)ressure, (n)one
 *
 * Data will be an array of numeric values for each data element
 *
 * The request will specify the following:
 *  Start time (unix timestamp)
 *  End time (unix timestamp)
 *  Desired interval (in seconds) between data points - 0 means all data points
 *  mf - element data from (m)emory or (f)ile
 *
 *
 */


// A logfile is all the data from a single day
// Data in the logfile is record headers and binary images from shared memory.
// At present there are two types of data: Shared Memory image and raw data
// Each type of data record is prefaced by a header record which contains a timestamp and
// the size and type of the following data: as follows:

// Header: data type 's', size 50880 (or whatever), time xxx-xx-xx 00:00
// Data: Shared memory image
// Header: data type 'd', size 768 (or whatever), time xxx-xx-xx xx:xx
// Data: element values
// Header: data type 'd', size 768 (or whatever), time xxx-xx-xx xx:xx
// Data: element values
// .....

// At present, the first and only the first record contains the shared memory image.
// That means that if elements are added or changed during the day, their names will not appear in the logfile.
// The data will be there under the default element name.
// This could be addressed by writing a new header when changes are made.

#include <fcntl.h>

#include <stdio.h>
#include <sys/types.h>
#include "vesta.h"


// Print element data in JSON format
void print_header(shm_index_struct *lshm) {
	int i, j;
	j = 0;
	printf("Content-type: text/html\nAccess-Control-Allow-Origin: *\n\n[\n");
	for (i = 2; i < lshm->counts->element_count; i++) {
		if (lshm->elements[i].next != 0) {
			if (j++ > 0) {
				printf(",\n");
			}
			printf(
					"{ \"id\":\"%d\", \"name\":\"%s\", \"pvc\":\"%c\", \"io\":\"%c\", \"conv\":\"%c\", \"data\":[] }",
					i, lshm->elements[i].name, lshm->elements[i].pvn,
					lshm->elements[i].ptype,
					lshm->stypes[lshm->elements[i].stype].conversion);
		}
	}
	printf("\n]\n");
}

main(){
  int i,j,k,c,interval,head,nohead,tz,e,all;
  char units;
  FILE *fp;
  byte_t elemental[500];
  char filename[60];
  char cdate[5], sdate[5];
  char *myquery, *p[8], *v[8];
  float fval;

  struct timeval now;
  time_t start_sec;
  time_t end_sec;
  long now_usec;

  time_t curtime;
  time_t disptime;
  time_t start_seconds;
  time_t end_seconds;
  time_t secs, local_secs, gmt_secs;
  struct tm *logtime;
  struct tm *stime;
  struct tm *etime;
  struct tm *ctime;
  int gmtoffset;

  shm_index_struct llocalShmIndex;
  int spacer[100];
  shm_index_struct *lshm = &llocalShmIndex;
  count_struct *lshmaddr = NULL;

  logheaderstruct logheader;
  float *evals;

  long long start_usec;

  struct tm *loctime;

  shm_index_struct *shmindex;
  logrecstruct logrec;

  // Get shared memory links
  get_shm(READ_ONLY_ID);


  myquery = (char*)getenv("QUERY_STRING");

  /* break apart parameter string on '&' and '=' characters */

  j = 0;

  // Weirdness: sometimes myquery is NULL, and you don't want to do strlen on it.
  // Other times, it's a zero length string. Either way, don't parse it.

  if (myquery != NULL){
    if (strlen(myquery) > 0){
      while(myquery=(char*)split(myquery,&p[j],&v[j])){
        j++;
      }
      j++;
      cleanup(j,&v[0]);
    }
  }

  // This program requires one parameter: the filename of the logfile.
  // There are several others that can be optionally specified. Default behaviors are defined here.

  // start and end are epoch times. They can be used to limit which datalog records are output.
  // Default is to output all records in the logfile.
  start_seconds = 0;
  end_seconds = time(NULL);

  // 'nohead' is true if we want data with no header. False by default - send header line
  // 'head' is true if we want only the header. False by default - send data too
  nohead = 0;
  head = 0;
  // By default, timezone is GMT. User can specify offset in hours.
  tz = 0;
  // By default, units are read from log data configuration. Can be overridden with query string data
  units = 'X';
  // By default, only print elements (not unassigned I/O)
  all = 0;
  head = 0;
  interval = 0;

  if (j > 0){
    for(i=0;i<j;i++){
      //printf("p: %s v: %s<br>\n",p[i],v[i]);

      if (!strcmp(p[i], "header")){
        sscanf(v[i],"%d",&head);
      }
      if (!strcmp(p[i], "start")){
        sscanf(v[i],"%ld",&start_sec);
        start_seconds = (time_t)start_sec;
      }
      if (!strcmp(p[i], "end")){
        sscanf(v[i],"%ld",&end_sec);
        end_seconds = (time_t)end_sec;
      }
      if (!strcmp(p[i], "interval")){
        sscanf(v[i],"%d",&interval);
      }
      if (!strcmp(p[i], "tz")){
        sscanf(v[i],"%d",&tz);
      }
      if (!strcmp(p[i], "units")){
        sscanf(v[i],"%c",&units);
      }
      if (!strcmp(p[i], "nohead")){
        nohead = 1;
      }
      if (!strcmp(p[i], "all")){
        all = 1;
      }
    }
  }

  // Determine filename from starttime
  stime = localtime(&start_seconds);
  strftime(&sdate,5,"%m%d",stime);

  strftime(&filename,60,"/var/vesta/sd/logs/%m%d.log",stime);
  fp = fopen(filename,"r");
  if (fp == NULL){
  	printf("ctlJsonDump: Can't open file %f\n",filename);
  }
  // read header. Future version should just read size value and error check.
  fread(&logheader,sizeof(logheader),1,fp);
  lshmaddr = malloc(logheader.nextrecsize);
  fread(lshmaddr,logheader.nextrecsize,1,fp);


  if(!feof(fp)){
    lshm->counts = lshmaddr;
    lshm->config = lshm->counts + 1;
    lshm->evals = lshm->config + 1;
    lshm->elements = lshm->evals + lshm->counts->element_count;
    lshm->rules = lshm->elements + lshm->counts->element_count;
    lshm->aivalue = lshm->rules + MAX_RULES;
    lshm->aistatus = lshm->aivalue + lshm->counts->ai_count;
    lshm->divalue = lshm->aistatus + lshm->counts->ai_count;
    lshm->dovalue = lshm->divalue + lshm->counts->di_count;
    lshm->hwconfig = lshm->dovalue + lshm->counts->do_count;
    lshm->stypes = lshm->hwconfig + 20;


    // Just print header. If it's today, print from current shared memory, else from logfile
    if(head == 1){
      time(&curtime);
      ctime = localtime(&curtime);
      strftime(&cdate,5,"%m%d",ctime);
    	if(!strcmp(cdate,sdate)){
    		print_header(shm);
    	}else{
    	  //sprintf(logbuffer, "!= Cdate %s, sdate %s",cdate,sdate);
    	  //printlog("ctlJsonDump", logbuffer, 1);
    		print_header(lshm);
    	}
    		exit(0);
    }


// If units weren't specified, get from log data config record
    if(units == 'X'){
//      units = logheader.shm.config[0].centigrade ? 'C' : 'F';
    }
  }else{
    printf("No file %s\n",filename);
  }
  fflush(stdout);

  printf("Content-type: text/html\nAccess-Control-Allow-Origin: *\n\n[ \n");

  evals = malloc(lshm->counts->element_count * sizeof(float));
  j=0;
  secs = 0;
  while(!feof(fp) && ((secs + interval) < end_seconds)){
    fread(&logheader,sizeof(logheader),1,fp);
    if(!feof(fp)){
      fread(evals,logheader.nextrecsize,1,fp);
      disptime = logheader.curtime;
      logtime=(struct tm *)localtime(&disptime);
      if(logheader.curtime > (start_seconds) && logheader.curtime < (end_seconds) && logheader.curtime >= (secs + interval) ){
        secs = logheader.curtime;
        if (j++>0){
          printf(",\n");
        }
        printf("{\"Time\":\"%u\", ",mktime(logtime));
        k = 0;
        for(i=2;i<lshm->counts->element_count;i++){
          if (lshm->elements[i].next != 0) {
            if (k++ > 0){
              printf(",");
            }
            printf("\"%d\":%0.2f ",i,evals[i]);
          }
        }
        printf ("}\n");
      }
    }else{
      // Just hit end of file. Are we done?
      if((secs + interval + (lshm->config->log_period*10)) < end_seconds){
        //printf("secs: %d interval: %d end_seconds: %d",secs, interval, (int)end_seconds);
        // Open next file. Add a bit less than 24 hours to last log time to guarantee next day
        start_sec = secs + 23*3600;
        stime = localtime(&start_sec);
        fclose(fp);
        strftime(&filename,40,"/var/vesta/sd/logs/%m%d.log",stime);
        //printf("Opening file %s\n",filename);
        fp = fopen(filename,"r");
        // read header. Future version should just read size value and error check.
        fread(&logheader,sizeof(logheader),1,fp);
        free(lshmaddr);
        lshmaddr = malloc(logheader.nextrecsize);
        fread(lshmaddr,logheader.nextrecsize,1,fp);
        //fread(&logheader,sizeof(logheader),1,fp);
      }
    }
  }
  printf("\n]\n");

  fclose(fp);
  free(lshmaddr);
  free(evals);
}


