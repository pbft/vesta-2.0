
/*****************************************************************************/
//    This file is part of the NoFossil Control System Software (NFCSS)
//    Copyright 2007, 2008, 2009 Bill Kuhns
//
//    NFCSS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    any later version.

//    NFCSS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with NFCSS.  If not, see <http://www.gnu.org/licenses/>.
/*****************************************************************************/

// Read analog inputs from TS-9700 A/D card and output timestamped binary file

#include <stdio.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/types.h>
#include "vesta.h"

#define PROCESSNAME  FUNCTION_NAME(ctlLogger)

main(){
  float reading;
  float volts;
  int i,j; 
  FILE *fp;
  char fbuffer[1048576];
  //char sbuffer[60000];
  char outbuff[400];
  char filename[40];
  char filename2[40];
    
  long long start_usec;

  time_t curtime;
  struct tm *loctime;

  logheaderstruct logheader;
  shm_index_struct *shmindex;
  logrecstruct logrec;

  // Get shared memory links
  get_shm(LOGGER_ID);
  
  curtime = time(NULL);
  loctime=localtime(&curtime);
  start_usec = getNow();
  strftime(&filename,40,"/var/vesta/sd/logs/%m%d.log",loctime);
  strftime(&filename2,40,"/var/vesta/sd/logs/%m%d.log",loctime);

  // Check if file exists. If not, create it and write header record.
  if (fp = fopen(filename,"r")){
    fclose(fp);
    fp = fopen(filename,"a");
    setbuf ( fp , fbuffer );
    sprintf(logbuffer, "logger filename: %s",filename);
    printlog(PROCESSNAME, logbuffer, 0);
  }else{
    fp = fopen(filename,"a");
    setbuf ( fp , fbuffer );
    chown(filename,580,580);
    chmod(filename,0x1B4);
    logheader.nextrectype = 's';
    logheader.nextrecsize = shm->config->shmsize;
    memcpy(&logheader.curtime,&curtime,sizeof(time_t));
    shmindex = shm;
    fwrite(&logheader,sizeof(logheader),1,fp);
    //printf("Wrote %d header bytes\n",sizeof(logheader));
    //memcpy(sbuffer,shm->counts,shm->config->shmsize);
    //fwrite(sbuffer,logheader.nextrecsize,1,fp);
    //memcpy(sbuffer,shm->counts,shm->config->shmsize);
    fwrite(shm->counts,logheader.nextrecsize,1,fp);
    //printf("shm 1: %X, buffer 1: %X\n",shm->counts->var_count,sbuffer[0]);
    //printf("Wrote %d data bytes\n",logheader.nextrecsize);
    //printf("Log: Next rec size  / type = %d / %c, count = %d\n",logheader.nextrecsize,logheader.nextrectype,shm->counts->element_count);
    //printf("Counts = %d %d\n",shm->counts->rule_count,shm->counts->element_count);
    fflush(fp);
    //printf ("New logger filename: %s size: %d elements: %d logrec: %d\n",
    //    filename,logheader->logheadersize,shmindex->counts->element_count,(sizeof(logrecstruct)+(shm->counts->element_count * sizeof(float))));
    //free(logheader);
  }
  // Set up logrec
  while (1){
    // If we're told to reload, flush buffer
    if (reloadRequired(LOGGER_ID)){
      fflush(fp);
      clearReloadFlag(LOGGER_ID);
    }
    curtime = time(NULL);
    loctime=localtime(&curtime);
    strftime(&filename2,40,"/var/vesta/sd/logs/%m%d.log",loctime);
    // Check if it's a new day. If so, close old logfile and open new one.
    if(strcmp(filename,filename2)){
      fclose(fp);
      fp = fopen(filename2,"w");
      setbuf ( fp , fbuffer );
      chown(filename2,580,580);
      chmod(filename2,0x1B4);
      strcpy(filename,filename2);
      // New file - write entire shm in header record
      logheader.nextrectype = 's';
      logheader.nextrecsize = shm->config->shmsize;
      memcpy(&logheader.curtime,&curtime,sizeof(time_t));
      shmindex = shm;
      //memcpy(shmindex,shm,shm->config->shmsize);
      fwrite(&logheader,sizeof(logheader),1,fp);
      //fwrite(shmindex,shm->config->shmsize,1,fp);
      fwrite(shm->counts,logheader.nextrecsize,1,fp);
      //printf("Log: Next rec size  / type = %d / %c, count = %d\n",logheader.nextrecsize,logheader.nextrectype,shm->counts->element_count);
      //printf("Counts = %ul %ul %x %x\n",&shm->counts->rule_count,&shm->counts->element_count,shm->counts->rule_count,shm->counts->element_count);
      fflush(fp);
      sprintf(logbuffer, "New logger filename: %s",filename);
      printlog(PROCESSNAME, logbuffer, 0);
    }
    logheader.nextrectype = 'd';
    logheader.nextrecsize = shm->counts->element_count * sizeof(float);
    memcpy(&logheader.curtime,&curtime,sizeof(time_t));
    shmindex = shm;
    //memcpy(shmindex,shm,shm->config->shmsize);
    fwrite(&logheader,sizeof(logheader),1,fp);
    //fwrite(shmindex,shm->config->shmsize,1,fp);
    fwrite(shm->evals,(shm->counts->element_count * sizeof(float)),1,fp);
    //printf("Log: Next rec size  / type = %d / %c, count = %d\n",logheader.nextrecsize,logheader.nextrectype,shm->counts->element_count);
    //printf("Logged %f\n",shm->evals[2]);
    fflush(fp);

    // Sleep for desired interval
    timedSleep(PROCESSNAME, shm->config->log_period, &start_usec);
  }



}
