
/*****************************************************************************/
//    This file is part of the Vermont Energy Control Systems Software Suite
//    Copyright Bill Kuhns
//
//    This is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    any later version.

//    This software is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with this software.  If not, see <http://www.gnu.org/licenses/>.
/*****************************************************************************/

// Template for sample controller task
// As coded, uses the USR1 slot for task ID and task interval
// Requires taskSample controller setup as follows:
// element 2 is temp sensor 1
// element 3 is temp sensor 2
// element 4 is variable 'diff'
// element 5 is variable 'max'
// will set diff and max based on temp 1 and temp 2

// BUGS:
//  - None yet ;-)

#include <stdio.h>
#include <fcntl.h>            // For file I/O
#include "vesta.h"
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <sys/poll.h>

// Choose task name here - can be any reasonably short name. Used for status and error logging.
#define PROCESSNAME  FUNCTION_NAME(netio)
#define BUFFERSIZE 50

void error(char *msg)
{
  perror(msg);
  exit(0);
}

main(){

  // Variable for calculating sleep interval
  unsigned long long start_usec;
  int n, i, result, netcount;
  typedef struct {
    int valid;
    int eid;
    struct sockaddr_in serv_addr;
    int SocketFD;
  } netstruct;

  netstruct netcon[10];
  struct pollfd ufds[10];

  char buffer[BUFFERSIZE+1];
  char buffer2[BUFFERSIZE+1];

  // Variables to store shared memory values. Not necessary but can improve readability.
  float fval;

  // ************ One - time initialization ************

  // Get shared memory link. This will create shared memory if necessary. Must pass valid ID:
  // NETIO_ID, USR2_ID, USR3_ID, or USR4_ID
  // Get start time for sleep calculations
  get_shm(NETIO_ID);
  start_usec = getNow();
  n = 0;
  for(i=2;i<shm->counts->element_count;i++){
    if(shm->elements[i].pvn == 'v' && shm->elements[i].ptype == 'n'){
      netcon[n].eid = i;
      netcon[n].SocketFD = socket(AF_INET, SOCK_STREAM, 0);
      if (netcon[n].SocketFD < 0)
        error("ERROR opening socket");
      memset(&netcon[n].serv_addr, 0, sizeof netcon[0].serv_addr);
      netcon[n].serv_addr.sin_family = AF_INET;
      netcon[n].serv_addr.sin_port = htons(shm->elements[i].vdev.netio.port);
      inet_pton(AF_INET,shm->elements[i].vdev.netio.ip,&netcon[n].serv_addr.sin_addr);
      if (connect(netcon[n].SocketFD,(struct sockaddr *)&netcon[n].serv_addr,sizeof(netcon[0].serv_addr)) >= 0){
        netcon[n].valid = -1;
        ufds[n].fd = netcon[n].SocketFD;
        ufds[n].events = POLLIN | POLLOUT;
      }else{
        netcon[n].valid = 0;
        ufds[n].fd = -1;
      }
      n++;
    }
  }
  netcount = n;
  // Do forever
  while (1){
    puts("Tick...");
    // ************ As-needed reinitialization ************

    // Each cycle, check to see if we should respond to config changes in shared memory.
    // If we read a disk file, we should do that here too. Must use valid ID here as well
    if (reloadRequired(NETIO_ID)){
      // Claim our elements (the ones we'll write to)
      // claimElement(diff_id,NETIO_ID);
      // Clear our bit in the reload flag
      //shm->counts->netio_count = 1;

      //shm->netios[0].port = 7280;
      //sprintf(shm->netios[0].ip,"69.54.28.180");

    /*
      shm->netios[0].port = (short int) 7280;
      sprintf(shm->netios[0].ip,"192.168.1.42");
      shm->netios[0].localElementId = (short int) 98;
      shm->netios[0].remoteElementId = (short int) 2;
*/
      /*
      n = 0;
      for(i=2;i<shm->counts->element_count;i++){
        if(shm->elements[i].pvn == 'v' && shm->elements[i].ptype == 'n'){
          netcon[n].SocketFD = socket(AF_INET, SOCK_STREAM, 0);
          if (netcon[n].SocketFD < 0)
            error("ERROR opening socket");
          memset(&netcon[n].serv_addr, 0, sizeof netcon[0].serv_addr);
          netcon[n].serv_addr.sin_family = AF_INET;
          netcon[n].serv_addr.sin_port = htons(shm->elements[i].vdev.netio.port);
          inet_pton(AF_INET,shm->elements[i].vdev.netio.ip,&netcon[n].serv_addr.sin_addr);
          if (connect(netcon[n].SocketFD,(struct sockaddr *)&netcon[n].serv_addr,sizeof(netcon[0].serv_addr)) >= 0){
            netcon[n].valid = -1;
          }else{
            netcon[n].valid = 0;
          }
          n++;
        }
      }
      */
      clearReloadFlag(NETIO_ID);
    }

    for(n=0;n<netcount;n++){
        // Ignore connect failure - don't die if server isn't there
        if (netcon[n].valid){
          bzero(buffer,BUFFERSIZE);
          sprintf(buffer,"GET /?div=xxxxx&get=%d\n",shm->elements[netcon[n].eid].vdev.netio.remoteElementId);
          printf("Sending %s to %d\n",buffer,n);
          result = write(netcon[n].SocketFD,buffer,strlen(buffer));
          puts("a");
          if (result < 0)
            error("ERROR writing to socket");
          bzero(buffer2,BUFFERSIZE);
          puts("b");
          result = read(netcon[n].SocketFD,buffer2,BUFFERSIZE-1);
          puts("c");
          if (result < 0){
            error("ERROR reading from socket");
          }
          sscanf(buffer2,"%*[^';'];%f",&fval);
          printf("setting element %d to %f\n",netcon[n].eid,fval);
          setElement(netcon[n].eid,fval,0);
        }else{
          puts("Couldn't connect");
        }
    }
    // ************ Sleep ************

    // Sleep until next cycle. Select appropriate period for your ID.
    timedSleep(PROCESSNAME, shm->config->netio_period, &start_usec);

  }
}
