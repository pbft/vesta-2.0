
/*****************************************************************************/
//    This file is part of the Vermont Energy Control Systems Software Suite
//    Copyright Bill Kuhns
//
//    This is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    any later version.

//    This software is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with this software.  If not, see <http://www.gnu.org/licenses/>.
/*****************************************************************************/

// Template for sample controller task
// As coded, uses the USR1 slot for task ID and task interval
// Requires taskSample controller setup as follows:
// element 2 is temp sensor 1
// element 3 is temp sensor 2
// element 4 is variable 'diff'
// element 5 is variable 'max'
// will set diff and max based on temp 1 and temp 2

// BUGS:
//  - None yet ;-)

#include <stdio.h>
#include <fcntl.h>            // For file I/O
#include "vesta.h"

// Choose task name here - can be any reasonably short name. Used for status and error logging.
#define PROCESSNAME  FUNCTION_NAME(sample)

main(){

  // Variable for calculating sleep interval
  unsigned long long start_usec;

  // We need element ID values for elements that we'll use.
  // Normally will get list from disk file. In this case we'll hard code.

  int temp1_id = 3;
  int temp2_id = 4;
  int diff_id = 107;
  int max_id = 108;

  // Variables to store shared memory values. Not necessary but can improve readability.
  float temp1, temp2, diff, max;

  // ************ One - time initialization ************

  // Get shared memory link. This will create shared memory if necessary. Must pass valid ID:
  // USR1_ID, USR2_ID, USR3_ID, or USR4_ID
  // Get start time for sleep calculations
  get_shm(USR1_ID);
  start_usec = getNow();

  // Do forever
  while (1){

    // ************ As-needed reinitialization ************

    // Each cycle, check to see if we should respond to config changes in shared memory.
    // If we read a disk file, we should do that here too. Must use valid ID here as well
    if (reloadRequired(USR1_ID)){
      // Claim our elements (the ones we'll write to)
      claimElement(diff_id,USR1_ID);
      claimElement(max_id,USR1_ID);
      // Clear our bit in the reload flag
      clearReloadFlag(USR1_ID);
    }

    // ************ Read Shared Memory Values ************

    // Read values from shared memory using elementValue() function
    temp1 = elementValue(temp1_id);
    temp2 = elementValue(temp2_id);

    // ************ Console Debug example ************
    //printf("temp 1 value = %f temp 2 value = %f\n",temp1,temp2);

    // ************ Logfile Debug example ************
    //sprintf(logbuffer, "temp 1 value = %f temp 2 value = %f\n",temp1,temp2);
    //printlog(PROCESSNAME, logbuffer, 0);

    // ************ Perform Calculations ************
    // Calculate diff
    diff = temp1 - temp2;

    // Determine value for max
    if(temp1 > temp2){
      max = temp1;
    }else{
      max = temp2;
    }

    // ************ Write results to shared memory ************

    // Update shared memory with new values using setElement.
    // setElement needs element ID, value, and disk write flag. We don't need to save these values to disk.
    setElement(diff_id, diff, 0);
    setElement(max_id, max, 0);

    // ************ Sleep ************

    // Sleep until next cycle. Select appropriate period for your ID.
    timedSleep(PROCESSNAME, shm->config->usr1_period, &start_usec);

  }
}
