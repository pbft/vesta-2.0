
/*****************************************************************************/
//    This file is part of the NoFossil Control System Software (NFCSS)
//    Copyright 2007, 2008, 2009 Bill Kuhns
//
//    NFCSS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    any later version.

//    NFCSS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with NFCSS.  If not, see <http://www.gnu.org/licenses/>.
/*****************************************************************************/


// This is a bit of a kludge and my need to be addressed at some point:
// Shared memory contains pointers, so it must appear in each process address space at the same address.
// This seems to be safe for the 7260, but it might break at some point.

// Shared memory map address in process address space
// #define SHBASE 0x40000000

// Constants

#define SERVER_ID 1
#define CTL_ID  2
#define WEBEDIT_ID 3
#define LOGGER_ID 4
#define SQLLOGGER_ID 5
#define ONEWIRE_ID 6
#define PID_ID 7
#define NETIO_ID 8
#define READ_ONLY_ID 9
#define INSTEON_ID 10
#define WIRELESS900_ID 11
#define USR1_ID 20
#define USR2_ID 21
#define USR3_ID 22
#define USR4_ID 23

#define USEC_PER_SEC 1000000

// Hack to provide names that can be used as string arguments
#define FUNCTION_NAME(name) #name

// Array sizes

#define MAX_ELEMENTS 200

#define MAX_SENSORS 16
#define MAX_ANALOG_OUT 8
#define MAX_VARIABLES 80

// Discrete I/O comes in blocks of 8 bits. These are the number of those blocks.
#define MAX_DISCRETE_IN 10
#define MAX_DISCRETE_OUT 10
#define MAX_1WIRE_SENSORS 16
//#define MAX_PHYSIO (MAX_SENSORS + MAX_DISCRETE_IN + MAX_DISCRETE_OUT + MAX_1WIRE_SENSORS + MAX_ANALOG_OUT)
#define MAX_PHYSIO 200
#define MAX_RULES 400

#define AIVIEW 0
#define AOVIEW 1
#define DIVIEW 2
#define DOVIEW 3
#define NETVIEW 4
#define INSVIEW 5
#define XBWVIEW 6

typedef unsigned char byte_t;
//#define byte_t unsigned char

// I/O hardware
// the type field is for use by hardware I/O tasks. It might also be an array pointer some day:
// 1 = TS-9700 Analog In
// 2 = TS-9700 Analog Out
// 3 = DIO-64 Discrete In
// 4 = DIO-64 Discrete Out
// 5 = DS18B20 1-wire temp sensors
// 6 = Analog in from network
// 7 = Analog out set from network
// 8 = Discrete in from network
// 9 = Discrete out set from network
// 10 = Variable from network

enum CHTYPE
{ analog_in, analog_out, discrete_in, discrete_out, net_ai,  net_ao,  net_di,  net_do,  net_v, variable } ;

typedef struct  {
  char name[40];          // 'TS9700 analog inputs', for instance
  short int ctype;
  short int handler;            // ID of i/o handler from ID list above
  short int index;              // Index number if more than one instance
  short int type;
  short int ordinal;
  short int channelcount;
  short int ioperchannel; // 8 for 8 bit discrete, for instance
  short int eid;    // Index of first element for this block
} hw_struct;

typedef struct  {
  unsigned short int centigrade;
  unsigned short int io_period;
  unsigned short int rule_period;
  unsigned short int log_period;
  unsigned short int sqllog_period;
  unsigned short int pid_period;
  unsigned short int ha7_period;
  unsigned short int netio_period;
  unsigned short int usr1_period;
  unsigned short int usr2_period;
  unsigned short int usr3_period;
  unsigned short int usr4_period;
  unsigned long int reload;
  byte_t  shminit;
  unsigned int shmid;
  int shmsize;
  int req;
  int clr;
} config_struct;

/*
typedef struct {
  short int channel;          // Index number (redundant: array index)
  char ctype;           // s = sensor input, a=analog output i = discrete input, o=discrete output
  short int stype;            // Signal type, pointer to stype.csv record with gain, offset etc.
  char hwname[20];
  short int hwchannel;        // Channel number within particular i/o type
  short int bitnum;           // For discrete i/o, bit number within channel value
  float gain;
  float offset;
  short int lut;             // lookup table to use for this channel
} physio_struct;
*/

typedef struct {
  short int stype;
  char sname[20];
  float gain;
  float offset;
  short int lut;
  char conversion;      // For SI / US units. t = temp, p = pressure, n = none
} stype_struct;

// net IO struct
typedef struct  {
  char ip[16];
  short int port;
  short int remoteElementId;
  long int lastResponse;
} netio_struct;

// insteon struct (insteon instances)
typedef struct  {
  char address[3];
  short int idevtype;
  long int lastResponse;
  long int responseDue;
  long int nextPoll;
  short int devindex;   // sub-device within single Insteon
  short int battery;
  short int signal;
  short int rw;					// 0: no communication, 1: We read from it to shm, 2: We write to it from shm, 3: both r & w
} insteon_struct;

// Union of all virtual (external) device structures
typedef union {
  short int vtype;
  netio_struct netio;
  insteon_struct insteon;
} vdev_struct;

typedef struct {
  short int next;       // ID of next element in sequence, 0 if not active
  short int owner;      // Process that writes to this element
  short int stype;      // if physical, Signal type: pointer to stype.csv record with gain, offset etc.
                        // If virtual, could be pointer to device type
  float *val;
  char name[40];
  char pvn;             // c = constant, p = physical, v = virtual. Only 'p' and 'c' are fixed (mapped to actual physical channel)
  char io;              // i = input, o = output, b = both
  char ptype;           // if physical, s = sensor input, a=analog output i = discrete input, o=discrete output
                        // if virtual, v=variable, i = insteon, n = network, w = wireless
  short int hwchannel;  // Channel number within particular i/o type
  short int bitnum;     // For discrete i/o, bit number within channel value
  float gain;           // For channel-specific calibration
  float offset;         // For channel-specific calibration
  short int status;     // nonzero means OK
  vdev_struct vdev;     // union of virtual device-specific data
} element_struct;

/*
// 1wire via ha7net
typedef struct  {
    char ip[20];
    char sig[2];
    short cmd;
    short port;
    short sslport;
    char sn[12];
    char dname[64];
    double devids[MAX_1WIRE_SENSORS];
    int devtypes[MAX_1WIRE_SENSORS];
} ha7struct;
*/

// insteon master struct (insteon types)
typedef struct  {
  short int stype;
  char sname[20];
  float gain;
  float offset;
  short int devcount;   // How many sub-devices?
  char conversion;      // For SI / US units. t = temp, p = pressure, n = none
  short int poll;       // Poll Interval - Device must be polled: can be changed by user and doesn't report change
  short int rw;         // 0: no communication, 1: We read from it to shm, 2: We write to it from shm, 3: both r & w
} instype_struct;

// Some rule parameters can be either an element ID or a float value. We'll use a union.
typedef union {
  int eid;
  float value;
} rval_struct;

// the element flag is true if the value is an element ID, false if it's a float
typedef struct {
  int  eflag;
  rval_struct rval;
} ruleParm_struct;

// Differential rules
typedef struct {
  char rtype;
  int active;
  int target;
  char gle;
  int e1;
  ruleParm_struct e2;
  ruleParm_struct diff;
  ruleParm_struct deadband;
} drule_struct;


// Timer rules

// Set target if e1 is/becomes true/false for e2 seconds
typedef struct {
  char rtype;
  int active;
  int target;
  int e1;
  int ib;
  int tf;
  ruleParm_struct e2;
} trule_struct;

typedef struct {
  char rtype;
  int active;
  int target;
  int e1;
  int n1;
  int e2;
  int n2;
  int e3;
  int n3;
  ruleParm_struct v1;
} arule_struct;

// Mathematical rules
typedef struct {
  char rtype;
  int active;
  int target;
  char op;
  ruleParm_struct e1;
  ruleParm_struct e2;
} mrule_struct;

// Comments
typedef struct {
  char rtype;
  char ctext[80];
} crule_struct;

// Warnings
typedef struct {
  char rtype;
  short int active;
  short int state;
  short int wdata;
  short int delay;
  short int interval;
  short int delay2;
  byte_t tnt;           // True / not true
  byte_t sns;           // Send / not send
  char ctext[65];
} wrule_struct;

// New scheme: shared memory and disk file will contain a list of intermingled rules.
// In shared memory we'll use a union to store rules.

typedef union {
  arule_struct arule;
  drule_struct drule;
  trule_struct trule;
  mrule_struct mrule;
  crule_struct crule;
  wrule_struct wrule;
} rule_struct;

typedef struct {
  byte_t active;
  short int e_ctl;          // Element that enables this PID channel
  short int e_co;           // Element: Control Output
  short int e_sp;           // Element: Setpoint
  short int e_pv;           // Element: Process variable - what we want to drive to setpoint
  short int e_load;         // Element: Load predictor
  float kc;           // Proportional gain
  float ti;           // Integral damping
  float lmin;         // Load value corresponding to minimum output
  float lmax;         // Load value corresponding to maximum output
  float coMin;        // Minimum allowable output during controlled operation
  float coMax;        // Maximum allowable output during controlled operation
  float purgeLimit;    // Output value below which purge is triggered
  float purgeInterval; // Time between purge cycles
  float purgeCycle;    // Length of purge
} pid_struct;

typedef struct {
  short int var_count;
  short int hw_count;
  short int rule_count;
  short int element_count;
  short int stype_count;
  short int ai_count;
  short int ao_count;
  short int di_count;
  short int do_count;
  short int pid_count;
  short int netio_count;
  short int ha7_count;
} count_struct;

// Shared memory structure
typedef struct {
  float *evals;
  config_struct *config;
  count_struct *counts;
  hw_struct *hwconfig;
  rule_struct *rules;
  element_struct *elements;
  stype_struct *stypes;
  float *aivalue;
  byte_t *aistatus;
  byte_t *divalue;
  byte_t *dovalue;
  pid_struct *pids;
  vdev_struct *vdevs;
  int *msgindex;
  char *status;
  insteon_struct *insteonMaster;
} shm_index_struct;


// Log record structure

// This is the same as the first portion of shmstruct - this is what we log every interval.

typedef struct {
  // Up to 16 analog inputs
  //byte_t  aicount;
  //float aivalue[MAX_SENSORS];

  // Up to 8 analog outputs
  //byte_t  aocount;
  //float aovalue[MAX_ANALOG_OUT];

  // Up to 80 discrete inputs (1 bit each)
  //byte_t  dicount;
  //byte_t  divalue[MAX_DISCRETE_IN];

  // Up to 80 discrete outputs
  //byte_t  docount;
  //byte_t  dovalue[MAX_DISCRETE_OUT];

  int element_count;
  float evals[MAX_ELEMENTS];

} logdatastruct;

typedef struct {
  int logrecsize;
  time_t curtime;
  //float evals[MAX_ELEMENTS];
  //logdatastruct logdata;
} logrecstruct;

typedef struct {
  int nextrecsize;
  char nextrectype;         //(s)hared memory, (d)ata
  time_t curtime;
  float version;
} logheaderstruct;

extern shm_index_struct *shm;
extern char logbuffer[];

void clearReloadFlag(int);
int reloadRequired(int);
int claimElement(int, int);

void read_diskfiles();
void print_element(int, int, char *, char *, char *, int, int);
void print_mrule(int, int);
void print_drule(int, int);
void print_arule(int, int);
void print_trule(int, int);
void print_crule(int, int);
void print_prule(int, int);
void print_wrule(int, int);
void get_shm(int);
char *split(char *, char **, char **);
void cleanup(int, char **);
void set_elements(int);
void write_elements();
int replace_mrule_element(int, int, int);
int replace_arule_element(int, int, int);
int replace_trule_element(int, int, int);
int replace_prule_element(int, int, int);
int replace_drule_element(int, int, int);
int replace_wrule_element(int, int, int);
void write_config();
void write_rules();
int read_elements();
void setElement(int, float, int);
float elementValue(int);
unsigned long long getNow();
void timedSleep(char[], unsigned short int, unsigned long long *);
